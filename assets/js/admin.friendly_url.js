$(function(){
	var charList = {'à':'a', 'á':'a', 'ạ':'a', 'ả':'a', 'ã':'a', 'â':'a', 'ầ':'a', 'ấ':'a', 'ậ':'a', 
        'ẩ':'a', 'ẫ':'a', 'ă':'a', 'ằ':'a', 'ắ':'a', 'ặ':'a', 'ẳ':'a', 'ẵ':'a', 'è':'e', 'é':'e', 'ẹ':'e',
        'ẻ':'e', 'ẽ':'e', 'ê':'e', 'ề':'e', 'ế':'e', 'ệ':'e', 'ể':'e', 'ễ':'e', 'ì':'i', 'í':'i', 'ị':'i', 
        'ỉ':'i', 'ĩ':'i', 'ò':'o', 'ó':'o', 'ọ':'o', 'ỏ':'o', 'õ':'o', 'ô':'o', 'ồ':'o', 'ố':'o', 'ộ':'o', 
        'ổ':'o', 'ỗ':'o', 'ơ':'o', 'ờ':'o', 'ớ':'o', 'ợ':'o', 'ở':'o', 'ỡ':'o', 'ù':'u', 'ú':'u', 'ụ':'u', 
        'ủ':'u', 'ũ':'u', 'ư':'u', 'ừ':'u', 'ứ':'u', 'ự':'u', 'ử':'u', 'ữ':'u', 'ỳ':'y', 'ý':'y', 'ỵ':'y', 
        'ỷ':'y', 'ỹ':'y', 'đ':'d'};

	$('.friendly-url').each(function(){
		var self = $(this);
		var ref = self.attr('ref');
		if( ref ){
			var fromObj = $(ref);
			fromObj.keyup(function(){
				var input = $(this).val().toLowerCase();
                var ansi_url = input.replace(/./g, function(c) {return c in charList? charList[c] : c});
				var frieldly_url = ansi_url.replace(/ +/g,'-').replace(/[^a-z0-9-_]/g,'').trim();
				self.val(frieldly_url);
			});
		}
	});
});