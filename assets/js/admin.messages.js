$(function(){
	var MSG_ACTIVE_TABS = [];
	var ACTIVE_USER_ID = 0;
	ACTIVE_USER_ID = $('.page-messages ul li.active a[data-toggle="tab"]').attr('href').replace('#user-', '');

	var get_display_message = function(msg){
		var reversed = ( 0 === parseInt(msg.f) );
  		return '<div class="message '+(reversed ? 'reversed' : '')+'"> \
            <a class="message-img" href="#"><img src="http://placehold.it/120" alt=""></a> \
            <div class="message-body"> \
                '+msg.m+' \
                <span class="attribution">'+msg.ts+'</span> \
            </div> \
        </div>';
	};

	var show_message_user = function(panel, user_id){
		var char_container = $(panel + ' .chat');
		char_container.html('<i class="icon-spinner7 spin"></i>');
		$.getJSON(BASE_URL+'/messages/get_message/'+user_id, function(data){
	  		// reversed
	  		if( data.length == 0 ) return;
	  		MSG_ACTIVE_TABS.push(user_id);
	  		var $template = '';
	  		for(var i in data){
	  			var msg = data[i];
	  			$template += get_display_message(msg);
	  		}
		  	char_container.html($template);
		  	char_container.animate({ scrollTop: char_container.prop("scrollHeight")}, 1000);
	  	}).error(function(){
			char_container.html('<div class="alert alert-danger fade in block-inner"> \
			  	<button type="button" class="close" data-dismiss="alert">×</button> \
			    <i class="icon-cancel-circle"></i>  Loading error!\
			</div>');
	  	}).always(function(){
	  		char_container.find('i.spin').remove();
	  	});
	}

	show_message_user('#user-'+ACTIVE_USER_ID, ACTIVE_USER_ID);

	$('.page-messages a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
		var panel = $(e.target).attr('href');
	  	var user_id = panel.replace('#user-', '');
	  	ACTIVE_USER_ID = user_id;

	  	$('textarea[name=enter-message]').focus();
	  	
	  	if( $.inArray( user_id, MSG_ACTIVE_TABS ) >= 0 ) return;

	  	show_message_user(panel, user_id);
	});

	$('textarea[name=enter-message]').keypress(function(e){
		if(e.which == 13) {
			var currentdate = new Date(); 
    		var datetime = currentdate.getFullYear() + "/"  
                + (currentdate.getMonth()+1) + "/"  
                + currentdate.getDay() + " "  
                + currentdate.getHours() + ":"  
                + currentdate.getMinutes() + ":" 
                + currentdate.getSeconds();

			var msg = {};
			msg.f = 0;
			msg.t = ACTIVE_USER_ID;
			msg.m = $(this).val();
			msg.ts = datetime;

	        $template = get_display_message(msg);

	        var char_container = $('#user-' + msg.t + ' .chat');
		  	char_container.append($template);
		  	char_container.animate({ scrollTop: char_container.prop("scrollHeight")}, 1000);

		  	$.post(BASE_URL+'/messages/push/', msg, function(resp){
		  		if(resp !== '1'){
		  			console.log('error:' + JSON.stringify(msg));
		  		}
		  	});

		  	$(this).val('');
	    }
	});

	setInterval(function(){
		var user_array = [];
		$.each($('.page-messages a[data-toggle="tab"]'), function(){
			var panel = $(this).attr('href');
	  		var user_id = panel.replace('#user-', '');
	  		try{
	  			parseInt(user_id);
	  			user_array.push(user_id);

	  		}catch(e){}
		});

		$.getJSON(BASE_URL+'/messages/get_message_part/', {'user_id': user_array.join(',')}, function(data){
	  		// reversed
	  		if( data.length == 0 ) return;
	  		for(var i in data){
	  			var msg = data[i];
	  			$template = get_display_message(msg);

			  	var char_container = $('#user-' + msg.f + ' .chat');
			  	char_container.append($template);
			  	char_container.animate({ scrollTop: char_container.prop("scrollHeight")}, 1000);
	  		}
	  	});

	}, 4000);
});