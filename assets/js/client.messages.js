!function(t){t.titleAlert=function(e,l){if(t.titleAlert._running&&t.titleAlert.stop(),t.titleAlert._settings=l=t.extend({},t.titleAlert.defaults,l),!l.requireBlur||!t.titleAlert.hasFocus){l.originalTitleInterval=l.originalTitleInterval||l.interval,t.titleAlert._running=!0,t.titleAlert._initialText=document.title,document.title=e;var i=!0,n=function(){t.titleAlert._running&&(i=!i,document.title=i?e:t.titleAlert._initialText,t.titleAlert._intervalToken=setTimeout(n,i?l.interval:l.originalTitleInterval))};t.titleAlert._intervalToken=setTimeout(n,l.interval),l.stopOnMouseMove&&t(document).mousemove(function(e){t(this).unbind(e),t.titleAlert.stop()}),l.duration>0&&(t.titleAlert._timeoutToken=setTimeout(function(){t.titleAlert.stop()},l.duration))}},t.titleAlert.defaults={interval:500,originalTitleInterval:null,duration:0,stopOnFocus:!0,requireBlur:!1,stopOnMouseMove:!1},t.titleAlert.stop=function(){t.titleAlert._running&&(clearTimeout(t.titleAlert._intervalToken),clearTimeout(t.titleAlert._timeoutToken),document.title=t.titleAlert._initialText,t.titleAlert._timeoutToken=null,t.titleAlert._intervalToken=null,t.titleAlert._initialText=null,t.titleAlert._running=!1,t.titleAlert._settings=null)},t.titleAlert.hasFocus=!0,t.titleAlert._running=!1,t.titleAlert._intervalToken=null,t.titleAlert._timeoutToken=null,t.titleAlert._initialText=null,t.titleAlert._settings=null,t.titleAlert._focus=function(){if(t.titleAlert.hasFocus=!0,t.titleAlert._running&&t.titleAlert._settings.stopOnFocus){var e=t.titleAlert._initialText;t.titleAlert.stop(),setTimeout(function(){t.titleAlert._running||(document.title=".",document.title=e)},1e3)}},t.titleAlert._blur=function(){t.titleAlert.hasFocus=!1},t(window).bind("focus",t.titleAlert._focus),t(window).bind("blur",t.titleAlert._blur)}(jQuery);
Date.prototype.today=function(){return ((this.getDate() < 10)?"0":"") + this.getDate() +"/"+(((this.getMonth()+1) < 10)?"0":"") + (this.getMonth()+1) +"/"+ this.getFullYear();};
Date.prototype.timeNow=function(){return ((this.getHours() < 10)?"0":"") + this.getHours() +":"+ ((this.getMinutes() < 10)?"0":"") + this.getMinutes() +":"+ ((this.getSeconds() < 10)?"0":"") + this.getSeconds();};

jQuery(document).ready(function($){

	var WB_CHAT = 'http://localhost:8080'; //'http://wbchat.webi.vn'
	var WB_CHAT_AJAX = '/wbchat/ajax.php'; //'http://wbchat.webi.vn/ajax.php'
	var WB_CONNECTED = false;
  	var TYPING_TIMER_LENGTH = 700; // ms
  	var __STORE_ID = 0;
  	var USER_ID = 0;
  	
  	var typing = false;
  	var lastTypingTime;

	function recoveryMessages(){
		$.getJSON(WB_CHAT_AJAX, function(data){
			var html_conent = [];
			for(var i = 0; i < data.length; i++) {
			    var m = data[i];
				html_conent.push('<li><label>'+m.n+'<i>' + m.t + '</i></label><p>' + m.m + '</p></li>');
			}
			var chat_message_box = $('.chat-box .chat-messages');
			if(chat_message_box.length > 0){
				chat_message_box.find('ul').append( html_conent );
				chat_message_box.scrollTop(chat_message_box[0].scrollHeight);
			}
		});
	}

	function reOpenChatWindow(){
		var chat_box = $('<div class="chat-box chat-minibox"><div class="chat-header"><i class="fa fa-circle chat-status online"></i> Tư vấn mua hàng<a href="#close" class="chat-toggle" title="Tư vấn mua hàng"><i class="fa fa-expand fa-fw">&nbsp;</i></a></div><div class="chat-messages"><ul></ul></div><div class="chat-typing"><span></span> is typing...</div><div class="chat-control"><textarea placeholder="Enter message here"></textarea></div></div>');
		if(Cookies.get('chat_box_status') == 'chat-minibox') chat_box.addClass('chat-minibox');
		$('body').append(chat_box);
		recoveryMessages();
	}

	function saveChatWindowStatus(){
		var chat_box = $('.chat-box');
		if(chat_box.hasClass('chat-minibox')) Cookies.set('chat_box_status', 'chat-minibox', { path: '/', expires: 60 });
		else Cookies.set('chat_box_status', null);
	}

	/**
	* Append message to chatbox
	* n: name
	* m: message
	* t: time
	* c: chatbox jquery object
	*/
	function appendMessage(n, m, t, c){
		var content = '<li><label>'+n+' <i>' + t + '</i></label><p>' + m + '</p></li>';
		var chat_message_box = c.find('.chat-messages');
		chat_message_box.find('ul').append( content );
		chat_message_box.scrollTop(chat_message_box[0].scrollHeight);
	}

	reOpenChatWindow();

	$('.chat-box .chat-header a').click(function(e){ e.preventDefault();});
	$('.chat-box .chat-header').click(function(){
		var chat_box = $(this).closest('.chat-box');
		chat_box.toggleClass('chat-minibox');
		var control = $(this).find('a i');
		if(chat_box.hasClass('chat-minibox')){
			control.removeClass('fa-minus');
			control.addClass('fa-expand');
		}else{
			control.removeClass('fa-expand');
			control.addClass('fa-minus');
		}
		saveChatWindowStatus();
	});

	$('.chat-box .chat-control textarea').keypress(function (e) {
  		if (e.which == 13) {
    		var message = $(this).val();

    		//send message
			var data = { name: 'Bạn', message: message, storeId: __STORE_ID };
			
			//append message to message box
			var date = new Date();
			var _time = date.today() + ' ' + date.timeNow();

			appendMessage(data.name, data.message, _time, $(this).closest('.chat-box'));

			//reset data input
			$(this).val('');

			//save message
			$.post('/message/rest/push_raw/', {f: USER_ID, t: __STORE_ID, m: data.message}, function(success){
				if( !success ){
					//failed
				}
			});

			e.preventDefault();
  		}
	});

	//connect to server
	$.get('/message/rest/connect', function(user_id){
		USER_ID = user_id;
		WB_CONNECTED = true;
		//init msg
		$.getJSON('/message/rest/init/'+USER_ID+'/'+__STORE_ID, function(data){
			if( data.length == 0 ) return;
	  		for(var i in data){
	  			var msg = data[i];
	  			var from = msg.f;
	  			if(msg.f === USER_ID){
	  				from = 'Bạn';
	  			}else{
	  				from = 'Tư vấn viên';
	  			}
	  			appendMessage(from, msg.m, msg.ts, $('.chat-box'));
	  		}
		});	
	});

/*
	socket.on('store offline', function(data){
		if(data.storeId == __STORE_ID){
			if($('.server-offline-message').length < 1){
				var content = '<li class="server-offline-message"><label>&nbsp;</label><p>Tư vấn viên hiện tại chưa thể trả lời bạn, vui lòng <a href="/lien-he.html" target="_blank">click vào đây</a> để gửi liên hệ.</p></li>';
				var chat_box = $('.chat-box');
				var chat_message_box = chat_box.find('.chat-messages');
				chat_message_box.find('ul').append( content );
				chat_message_box.scrollTop(chat_message_box[0].scrollHeight);
				var chat_status = chat_box.find('.chat-status');
				chat_status.removeClass('online');
			}
		}
	});

	socket.on('store online', function(data){
		if(data.storeId == __STORE_ID){
			var chat_box = $('.chat-box');
			var chat_status = chat_box.find('.chat-status');
			chat_status.addClass('online');
			chat_box.find('.server-offline-message').remove();
		}
	});
*/

	setInterval(function(){
		$.getJSON('/message/rest/index/'+USER_ID+'/'+__STORE_ID, function(data){
	  		// reversed
	  		if( data.length == 0 ) return;
	  		for(var i in data){
	  			var msg = data[i];

	  			appendMessage(msg.f, msg.m, msg.ts, $('.chat-box'));
	  		}
	  		$.titleAlert("Tin nhắn mới", {requireBlur:false,stopOnFocus:false,duration:4000,interval:1000});
	  	});

	}, 4000);

	var chat_message_box = $('.chat-box .chat-messages');
	chat_message_box.scrollTop(chat_message_box[0].scrollHeight);
});