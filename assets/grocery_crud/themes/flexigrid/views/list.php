<?php 
	$action_col_width = '40px';
	if( empty($column_width) ){
		$column_width = array();
		for($i=0; $i<count($columns); $i++) $column_width[] = (int)(80/count($columns));
	}else{
		$action_col_width = $column_width[count($column_width) - 1];
	}

	
	if(!empty($list)){
?><div class="bDiv" >
		<table cellspacing="0" cellpadding="0" border="0" id="flex1">
		<thead>
			<tr class='hDiv'>
				<?php $column_index = 0; foreach($columns as $column){
					$col_item_width = isset($column_width[$column_index]) ? $column_width[$column_index] : '0';
					$column_index++;
					if( $col_item_width === '0' ) $col_item_width = '';
				?>
				<th width='<?php echo $col_item_width?>'>
					<div class="text-left field-sorting <?php if(isset($order_by[0]) &&  $column->field_name == $order_by[0]){?><?php echo $order_by[1]?><?php }?>" 
						rel='<?php echo $column->field_name?>'>
						<?php echo $column->display_as?>
					</div>
				</th>
				<?php }?>
				<?php if(!$unset_delete || !$unset_edit || !$unset_read || !empty($actions)){?>
				<th align="left" abbr="tools" axis="col1" width='<?php echo $action_col_width?>'>
					<div class="text-right">
						<?php echo $this->l('list_actions'); ?>
					</div>
				</th>
				<?php }?>
			</tr>
		</thead>		
		<tbody>
<?php foreach($list as $num_row => $row){ ?>        
		<tr  <?php if($num_row % 2 == 1){?>class="erow"<?php }?>>
			<?php foreach($columns as $column){?>
			<td class='<?php if(isset($order_by[0]) &&  $column->field_name == $order_by[0]){?>sorted<?php }?>'>
				<div class='text-left'><?php echo $row->{$column->field_name} != '' ? $row->{$column->field_name} : '&nbsp;' ; ?></div>
			</td>
			<?php }?>
			<?php if(!$unset_delete || !$unset_edit || !$unset_read || !empty($actions)){?>
			<td align="right">
				<div class='tools pull-right'>				
					<?php if(!$unset_delete){?>
                    	<?php /*<a href='<?php echo $row->delete_url?>' title='<?php echo $this->l('list_delete')?> <?php echo $subject?>' class="delete-row" >
                    			<span class='delete-icon'></span>
                    	</a> */?>
                    	<a href="<?php echo $row->delete_url?>" class="btn btn-danger btn-icon btn-xs tip delete-row" title="<?php echo $this->l('list_delete')?> <?php echo $subject?>" data-original-title="<?php echo $this->l('list_delete')?> <?php echo $subject?>"><i class="icon-remove3"></i></a>
                    <?php }?>
                    <?php if(!$unset_edit){?>
						<?php /* <a href='<?php echo $row->edit_url?>' title='<?php echo $this->l('list_edit')?> <?php echo $subject?>' class="edit_button"><span class='edit-icon'></span></a> */?>
						<a href="<?php echo $row->edit_url?>" class="btn btn-info btn-icon btn-xs tip edit_button" title="<?php echo $this->l('list_edit')?> <?php echo $subject?>" data-original-title="<?php echo $this->l('list_edit')?> <?php echo $subject?>"><i class="icon-pencil"></i></a>
					<?php }?>
					<?php if(!$unset_read){?>
						<?php /*<a href='<?php echo $row->read_url?>' title='<?php echo $this->l('list_view')?> <?php echo $subject?>' class="edit_button"><span class='read-icon'></span></a> */?>
						<a href="<?php echo $row->read_url?>" class="btn btn-success btn-icon btn-xs tip edit_button" title="<?php echo $this->l('list_view')?> <?php echo $subject?>" data-original-title="<?php echo $this->l('list_view')?> <?php echo $subject?>"><i class="icon-search3"></i></a>
					<?php }?>
					<?php 
					if(!empty($row->action_urls)){
						foreach($row->action_urls as $action_unique_id => $action_url){ 
							$action = $actions[$action_unique_id];
					?>
							<a href="<?php echo $action_url; ?>" class="<?php echo $action->css_class; ?> crud-action" title="<?php echo $action->label?>"><?php 
								if(!empty($action->image_url))
								{
									?><img src="<?php echo $action->image_url; ?>" alt="<?php echo $action->label?>" /><?php 	
								}
							?></a>		
					<?php }
					}
					?>					
                    <div class='clear'></div>
				</div>
			</td>
			<?php }?>
		</tr>
<?php } ?>        
		</tbody>
		</table>
	</div>
<?php }else{?>
<table cellspacing="0" cellpadding="0" border="0" id="flex1" width="100%">
	<tr class='hDiv'>
		<td>
			<br/>
			&nbsp;&nbsp;&nbsp;&nbsp; <?php echo $this->l('list_no_items'); ?>
			<br/><br/>
		</td>
	</tr>
</table>
<?php }?>	
