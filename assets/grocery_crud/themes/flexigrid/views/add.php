<?php

	$this->set_css($this->default_theme_path.'/flexigrid/css/flexigrid.css');
	$this->set_js_lib($this->default_theme_path.'/flexigrid/js/jquery.form.js');
    $this->set_js_lib($this->default_javascript_path.'/jquery_plugins/jquery.form.min.js');
	$this->set_js_config($this->default_theme_path.'/flexigrid/js/flexigrid-add.js');

	$this->set_js_lib($this->default_javascript_path.'/jquery_plugins/jquery.noty.js');
	$this->set_js_lib($this->default_javascript_path.'/jquery_plugins/config/jquery.noty.config.js');
?>
<div class="flexigrid crud-form" style='width: 100%;' data-unique-hash="<?php echo $unique_hash; ?>">
	<div class="mDiv">
		<div class="ftitle">
			<div class='ftitle-left'>
				<i class="icon icon-plus-circle2"></i>&nbsp;&nbsp;<?php echo $this->l('form_add'); ?> <?php echo $subject?>
			</div>
			<div class='clear'></div>
		</div>
	</div>
<div id='main-table-box'>
	<?php echo form_open( $insert_url, 'method="post" id="crudForm"  enctype="multipart/form-data"'); ?>
		<div class='form-div'>
			<!-- Start rendering fields-->
			<?php if( $wrap_header !== NULL ){ ?>
					<div class="tabbable page-tabs">
		                <ul class="nav nav-tabs">
		                	<?php $tab_count = 0; foreach($wrap_header as $tab){ $tab_count++;?>
		                    <li <?php echo $tab_count === 1 ? 'class="active"' : ''?> ><a href="#tab-<?php echo $tab_count; ?>" data-toggle="tab"><?php echo $tab['icon'] ? '<i class="'.$tab['icon'].'"></i> ' : ''?><?php echo $tab['label']?></a></li>
		                    <?php }?>
		                </ul>
			<?php } ?>
			<?php if( $wrap_fields !== NULL ){ //start if
					//render wrap fields
					$tab_count = 0;
					echo '<div class="tab-content">';
					foreach ($wrap_fields as $tab) {
						$tab_count++;
						$counter = 0;
						echo '<div class="tab-pane fade '.($tab_count === 1 ? 'active in' : '').'" id="tab-'.$tab_count.'">';
						foreach ($tab as $field) {
							$even_odd = $counter % 2 == 0 ? 'odd' : 'even';
							$counter++;
						?>
						<div class='form-field-box <?php echo $even_odd?>' id="<?php echo $field->field_name; ?>_field_box">
							<div class='form-display-as-box' id="<?php echo $field->field_name; ?>_display_as_box">
								<?php echo $input_fields[$field->field_name]->display_as?><?php echo ($input_fields[$field->field_name]->required)? "<span class='required'>*</span> " : ""; ?>
							</div>
							<div class='form-input-box' id="<?php echo $field->field_name; ?>_input_box">
								<?php echo $input_fields[$field->field_name]->input?>
							</div>
							<div class='clear'></div>
						</div>
						<?php 
						} //end foreach
						echo '</div>';
					}
					echo '</div>';
					echo '</div>';
				}else{ //render normal fields
					$counter = 0;
					foreach($fields as $field) //start foreach
					{
						$even_odd = $counter % 2 == 0 ? 'odd' : 'even';
						$counter++;
				?>
				<div class='form-field-box <?php echo $even_odd?>' id="<?php echo $field->field_name; ?>_field_box">
					<div class='form-display-as-box' id="<?php echo $field->field_name; ?>_display_as_box">
						<?php echo $input_fields[$field->field_name]->display_as?><?php echo ($input_fields[$field->field_name]->required)? "<span class='required'>*</span> " : ""; ?>
					</div>
					<div class='form-input-box' id="<?php echo $field->field_name; ?>_input_box">
						<?php echo $input_fields[$field->field_name]->input?>
					</div>
					<div class='clear'></div>
				</div>
				<?php } //end foreach?>
			<?php } //end if?>
			<!-- /End rendering fields-->

			<!-- Start of hidden inputs -->
				<?php
					foreach($hidden_fields as $hidden_field){
						echo $hidden_field->input;
					}
				?>
			<!-- End of hidden inputs -->
			<?php if ($is_ajax) { ?><input type="hidden" name="is_ajax" value="true" /><?php }?>

			<div id='report-error' class='report-div error'></div>
			<div id='report-success' class='report-div success'></div>
		</div>
		<div class="pDiv">
			<div class='form-button-box'>
				<input id="form-button-save" type='submit' value='<?php echo $this->l('form_save'); ?>'  class="btn btn-success"/>
			</div>
<?php 	if(!$this->unset_back_to_list) { ?>
			<div class='form-button-box'>
				<input type='button' value='<?php echo $this->l('form_save_and_go_back'); ?>' id="save-and-go-back-button"  class="btn btn-info"/>
			</div>
			<div class='form-button-box'>
				<input type='button' value='<?php echo $this->l('form_cancel'); ?>' class="btn btn-danger" id="cancel-button" />
			</div>
<?php 	} ?>
			<div class='form-button-box'>
				<div class='small-loading' id='FormLoading'><?php echo $this->l('form_insert_loading'); ?></div>
			</div>
			<div class='clear'></div>
		</div>
	<?php echo form_close(); ?>
</div>
</div>
<script>
	var validation_url = '<?php echo $validation_url?>';
	var list_url = '<?php echo $list_url?>';

	var message_alert_add_form = "<?php echo $this->l('alert_add_form')?>";
	var message_insert_error = "<?php echo $this->l('insert_error')?>";
</script>