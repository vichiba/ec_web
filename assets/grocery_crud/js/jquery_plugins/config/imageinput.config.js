$(function(){
	var popupCenter = function(url, title, w, h) {
	    // Fixes dual-screen position                         Most browsers      Firefox
	    var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
	    var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

	    width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
	    height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

	    if(w.indexOf('%') > 0) w = width * parseInt(w.replace('%', ''))*0.01;
	    if(h.indexOf('%') > 0) h = height * parseInt(h.replace('%', ''))*0.01;

	    var left = ((width / 2) - (w / 2)) + dualScreenLeft;
	    var top = ((height / 2) - (h / 2)) + dualScreenTop;
	    var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

	    // Puts focus on the newWindow
	    if (window.focus) {
	        newWindow.focus();
	    }
	}

	$('.image-field .btn-upload').click(function(e){
		e.preventDefault();
		var self = $(this);

		window.elFinderPopup = {};
	    window.elFinderPopup.callBack = function(url) {
	        // Actions with url parameter here
		    if( url ){
		    	path = url.replace(/^.*\/\/[^\/]+/, '');
		    	var container = self.closest('.image-field');
		    	container.find('input').val(path);
		    	container.find('img').attr('src', url);
		    }
	        window.elFinderPopup = null;
	    };
	    popupCenter(BASE_URL+'/files/popup', 'Choose your file', '80%', '80%');
	});

	$('.image-field .btn-delete').click(function(e){
		e.preventDefault();
		var self = $(this);

		if(confirm('Are you sure that you want to remove this image?')){
			var container = self.closest('.image-field');
		    	container.find('input').val('');
		    	container.find('img').attr('src', 'http://placehold.it/200');
		}
	});
});