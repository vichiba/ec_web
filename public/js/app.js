$(document).ready(function(){
	$("a.ajax_get").on("click", function(e){
    	e.preventDefault();
		var self = $(this);

		if(LOGGED_IN === ''){
			alert(NOT_LOGGED_IN_MSG);
			return;
		}

		var timestamp = new Date().getTime();

		$.ajax({
		    type: "GET",
		    url: self.attr("href"),
		    data: {ts: timestamp},
		    success: function(data){
		    	if(data){
					//toogle data
					self.toggleClass("active");
					var like_cont = self.find('small');
					var num_likes = like_cont.text();
					if( !num_likes ) num_likes = 0;
					else{
						try{
							num_likes = parseInt(num_likes);
						}catch(e){}
					}
					if( self.hasClass('active') ){
						num_likes += 1;
						var icon = self.find('i.fa-heart-o')
						icon.removeClass('fa-heart-o');
						icon.addClass('fa-heart');
					}else{
						num_likes -= 1;
						num_likes = num_likes < 1 ? '' : num_likes;
						var icon = self.find('i.fa-heart');
						icon.removeClass('fa-heart');
						icon.addClass('fa-heart-o');
					}
					like_cont.text(num_likes);
		    	}
		    },
		    failure: function(errMsg) {
		    	console.log(errMsg);
		    	//self.removeClass("active");
		    }
		});
    });
	$('.readmore').each(function(index, el) {
		var self = $(this);
		var lessText = self.attr('less-text');
		var moreText = self.attr('more-text');
	    self.readmore({
		  speed: 100,
		  collapsedHeight: 140,
		  lessLink: '<a href="#">'+lessText+'</a>',
		  moreLink: '<a href="#">'+moreText+'</a>',
		});
	});
});