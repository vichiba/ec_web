<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Model extends CI_Model {
	public $table = '';
	public $primary_key = 'id';

	//0:Delete, 1: inactive, 2: OK
	const STATUS_ACTIVE 	= 2;
	const STATUS_INACTIVE 	= 1;
	const STATUS_DELETE 	= 0;

	public function __construct(){
		parent::__construct();

		//$this->load->driver('cache');
		$this->load->driver('cache', array('adapter' => 'memcached', 'backup' => 'file'));
		$this->load->database();
	}

	public function __destruct(){
    	if($this->db) $this->db->close();
    }

    public function insert($data){
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}
	
	public function update($id, $data){
		$this->db->where($this->primary_key, $id);
		$this->db->update($this->table, $data);
		return $this->db->affected_rows();
	}
	
	public function delete($id){
		$this->db->where($this->primary_key, $id);
		$this->db->delete($this->table);
	}
	
	public function exists($conditions){
		$this->db->where($conditions);
		$this->db->from($this->table);
		$query = $this->db->get();
		if( $query->num_rows() > 0 ){
			if( $query->num_rows() == 1 ){
				return $query->row()->{$this->primary_key};
			}
			return TRUE;
		}
		return FALSE;
	}

	public function find($id){
		$this->db->where($this->primary_key, $id);
		$this->db->from($this->table);
		return $this->db->get()->row();
	}

	public function find_all($conds = array(), $order = ''){
		if( !empty($conds) ){
			$this->db->where($conds);
		}
		if( !empty($order) ){
			$this->db->order_by($order);
		}
		$this->db->from($this->table);
		return $this->db->get()->result();
	}

	public function count_all($conds = array()){
		if( !empty($conds) ){
			$this->db->where($conds);
		}
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	protected function getCacheDriver(){
		return $this->cache->memcached;
	}

	protected function getCache($key){
		$this->getCacheDriver()->get($key);
	}

	protected function setCache($key, $value){
		// Save into the cache for 2 minutes
		$this->getCacheDriver()->save($key, $value, 120);
	}

	protected function clearCache($key){
		$this->getCacheDriver()->delete($key);
	}

	protected function clearAllCaches(){
		$this->getCacheDriver()->clean();
	}
	
	/**
    * Check access token in header is valid
    *
    * @author hungpd
    * @access protected
    * @return bool TRUE the Access Token is valid
    */
    protected function isValidAccessToken(){
        $authorization = $this->input->get_request_header('Authorization');
        $access_token = str_replace('Bearer ', '', $authorization);
        return trim($access_token) === $this->session->userdata('customer_access_token');
    }
}
/* End of file home.php */
/* Location: ./application/controllers/administrator/home.php */