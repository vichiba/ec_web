<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends MX_Controller {
	protected $remember_id = 'remember_id';
    protected $remember_token = 'remember_token';
    protected $remember_expired_time = '2562000'; // 1 month

    //0:Delete, 1: inactive, 2: OK
	const STATUS_ACTIVE 	= 2;
	const STATUS_INACTIVE 	= 1;
	const STATUS_DELETE 	= 0;

	public function __construct(){
		parent::__construct();
		
		$this->load->helper('cookie');

		$module_path = $this->uri->segment(2, 0);

		$remember_user_id = get_cookie($this->remember_id);
        $remember_user_token = get_cookie($this->remember_token);
        if( !empty($remember_user_id) && !empty($remember_user_token) ){
            $remember_user_obj = $this->users_model->find($remember_user_id);
            $this->session->set_userdata('remember_user_object', $remember_user_obj);
            if($remember_user_obj){
                $saved_token = $remember_user_obj->remember_token;
                if( $saved_token === $remember_user_token ){
                	$remember_token_expired = $remember_user_obj->remember_token_expired;
                	if($remember_token_expired && strtotime($remember_token_expired) - time() > 0){
                		$this->session->set_userdata('loggedin', TRUE);
	                    $this->session->set_userdata('user_id', $remember_user_obj->user_id);
	                    $this->session->set_userdata('user_roles', $remember_user_obj->role_filter);
	                    $this->session->set_userdata('user_fullname', $remember_user_obj->user_fullname);
	                    $this->session->set_userdata('user_photo', $remember_user_obj->user_photo);
	                    if( empty($module_path) || $module_path === 'auth'){
	                    	redirect(admin_url('dashboard'));
	                    }
                	}
                }
            }
        }
		
		if( $this->session->userdata('loggedin') !== TRUE && 
			$module_path !== 'auth' &&
			$module_path !== 'signin_with_different_account'
		){
			redirect(admin_url('auth'));
		}

		if($module_path !== 'auth'
			&& $module_path !== 'logout'
			&& $module_path !== 'permission_denied'
		){

			$pattern = $this->session->userdata('user_roles');
			
			$url = $this->uri->uri_string();

			if(!preg_match("/^(".preg_replace('/,/i', '|', preg_replace('/\//i', '\/', $pattern)).")/", $url)){
				show_403();
			}
		}
	}

	public function getDataOuput($data_render, $grocery_output = NULL){
		if( $grocery_output === NULL ) return $data_render;
		$js_arrays = $grocery_output->js_files;
		$css_arrays = $grocery_output->css_files;
		$output = $grocery_output->output;
		
		$recent_js_files = empty($data_render['js_files']) ? array() : $data_render['js_files'];
		$recent_css_files = empty($data_render['css_files']) ? array() : $data_render['css_files'];
		
		$data_render['js_files'] = array_merge($recent_js_files, $js_arrays);
		$data_render['css_files'] = array_merge($recent_css_files, $css_arrays);
		$data_render['output'] = $output;

		return $data_render;
	}
}
/* End of file home.php */
/* Location: ./application/controllers/administrator/home.php */