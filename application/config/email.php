<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$config = Array(
	'protocol'	=> 'smtp',
	'smtp_host' => get_config_value('SMTP_HOST'),
    'smtp_port' => get_config_value('SMTP_PORT'),
	'smtp_user' => get_config_value('SMTP_USER'),
	'smtp_pass' => get_config_value('SMTP_PASS'),
	'charset'	=> 'utf-8',
	'mailtype'  => 'html', 
	'newline'	=> "\r\n",
);

/* End of file email.php */
/* Location: ./application/config/email.php */