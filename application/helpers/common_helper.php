<?php

if( !function_exists('res') ){
	/**
	* Resource Path
	*/
	function res($relative_path){
		return site_url('assets/'.$relative_path);
	}
}

if( !function_exists('debug') ){
	function debug($variable, $die = FALSE){
		echo '<pre>';
		print_r($variable);
		echo '</pre>';
		if( $die ) die;
	}
}

if(!function_exists('get_current_time')){
	function get_current_time(){
		return date(get_time_format());
	}

	function get_time_format(){
		return 'Y-m-d H:i:s';
	}
}

if(!function_exists('time_elapsed_string')){
	function time_elapsed_string($datetime, $full = false) {
		if(!$datetime) return '';
	    $now = new DateTime;
	    $ago = new DateTime($datetime);
	    $diff = $now->diff($ago);

	    $diff->w = floor($diff->d / 7);
	    $diff->d -= $diff->w * 7;

	    $string = array(
	        'y' => 'year',
	        'm' => 'month',
	        'w' => 'week',
	        'd' => 'day',
	        'h' => 'hour',
	        'i' => 'minute',
	        's' => 'second',
	    );
	    foreach ($string as $k => &$v) {
	        if ($diff->$k) {
	            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
	        } else {
	            unset($string[$k]);
	        }
	    }

	    if (!$full) $string = array_slice($string, 0, 1);
	    return $string ? implode(', ', $string) . ' ago' : 'Just now';
	}

	function time_elapsed_string2($now, $ago, $full = false) {
		
	    $diff = $now->diff($ago);

	    $diff->w = floor($diff->d / 7);
	    $diff->d -= $diff->w * 7;

	    $string = array(
	        'y' => 'year',
	        'm' => 'month',
	        'w' => 'week',
	        'd' => 'day',
	        'h' => 'hour',
	        'i' => 'minute',
	        //'s' => 'second',
	    );
	    foreach ($string as $k => &$v) {
	        if ($diff->$k) {
	            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
	        } else {
	            unset($string[$k]);
	        }
	    }

	    if (!$full) $string = array_slice($string, 0, 1);
	    return $string ? implode(' and ', $string) : 'Just now';
	}

	function time_elapsed_string3($datetime, $full = false) {
		if(!$datetime) return '';
	    $now = new DateTime;
	    $ago = new DateTime($datetime);
	    $diff = $now->diff($ago);

	    $diff->w = floor($diff->d / 7);
	    $diff->d -= $diff->w * 7;

	    $string = array(
	        'y' => 'year',
	        'm' => 'month',
	        'w' => 'week',
	        'd' => 'day',
	        'h' => 'hrs',
	        'i' => 'min',
	        's' => 'sec',
	    );
	    foreach ($string as $k => &$v) {
	        if ($diff->$k) {
	            $v = $diff->$k . ' ' . $v;
	        } else {
	            unset($string[$k]);
	        }
	    }

	    if (!$full) $string = array_slice($string, 0, 1);
	    return $string ? implode(', ', $string) : '0 min';
	}
}

if( !function_exists('generate_token') ){
	/**
	 * generate_token
	 * Generates a random unhashed password / token / salt.
	 * Includes a safe guard to ensure vowels are removed to avoid offensive words when used for password generation.
	 * Additionally, 0, 1 removed to avoid confusion with o, i, l.
	 *
	 * @return string
	 */
	function generate_token($length = 8) 
	{
		$characters = '23456789BbCcDdFfGgHhJjKkMmNnPpQqRrSsTtVvWwXxYyZz';
		$count = mb_strlen($characters);

		for ($i = 0, $token = ''; $i < $length; $i++) 
		{
			$index = rand(0, $count - 1);
			$token .= mb_substr($characters, $index, 1);
		}
		return $token;
	}
}
if( !function_exists('__') ){
	/**
	 * Get language
	 * @param  string $line language key
	 * @return string      language value
	 */
	function __($line){
		$lang = lang($line);
		return $lang ? $lang : $line;
	}
}

if( !function_exists('get_config_value') ){
	/**
	 * Get config value
	 * @param  string $key config key
	 * @return string      config value
	 */
	function get_config_value($key, $need_detail = FALSE){
		$config = get_instance()->config_model->findByKey($key);
		if( $need_detail === TRUE ){
			return $config;
		}else{
			if( $config ){
				return $config->conf_value;
			}
		}
		return "";
	}
}
if( !function_exists('imgsrc') ){
	function imgsrc($src, $opts = ''){
		require_once(APPPATH.'/../static/phpThumb.config.php');
		$param = 'src='.$src;
		if($opts){
			$param .= '&'.$opts .'&';
		}
		return site_url(phpThumbURL($param, '/static'));
	}
}

function trackurl($content, $source_type, $source_id, $source_url, $dest_url){
	if( endsWith($source_url, '?') ) $source_url = substr($source_url, 0, -1);
	return site_url('track?source_content='.urlencode($content).'&source_type='.$source_type.'&source_id='.$source_id.'&source_url='.urlencode($source_url).'&dest_url='.urlencode($dest_url));
}

function startsWith($haystack, $needle) {
    // search backwards starting from haystack length characters from the end
    return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== false;
}

function endsWith($haystack, $needle) {
    // search forward starting from end minus needle length characters
    return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== false);
}

if( !function_exists('debug_mode') ){
	function debug_mode(){
		error_reporting(-1);
        ini_set('display_errors', 1);
	}
}
if( !function_exists('json_ntb') ){
    function json_ntb(&$item, $key){
    	$item = null === $item ? '' : $item;
    }
}
if( !function_exists('display_price') ){
	function display_price($item){
		$price = ((double)$item->sale_price ? $item->sale_price : $item->price);
		//$price = get_instance()->currency_model->change($item->currency, $price); //Da xu ly trong SQL
		$currency_list = get_instance()->config->item('currency');
		$symbol = isset($currency_list[$item->currency])?$currency_list[$item->currency]:'¥';
		$price_symbol = '<span style="font-weight: 400">'.$symbol.'</span>';
		return $price_symbol . '&nbsp;&nbsp;' . number_format($price);
	}
}