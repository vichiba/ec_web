<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require_once dirname(__FILE__) . '/../libraries/REST_Controller.php';

class Notify extends REST_Controller {

    const ACCESS_TOKEN_LENGTH = 30;

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        $this->load->model('image/image_model', '_model');
    }

    private function norm_response($post){
        unset($post->post_created_date);
        unset($post->post_modified_date);
        unset($post->post_user_id);

        return $post;
    }

    public function list_get(){
        if( $this->isValidAccessToken() && $this->isAccountActive() ){
            $ref_type = Image_model::TYPE_NOTIFY;
            $page = $this->get('page');
            $page_size = $this->get('page_size');

            if( !$page || !is_numeric($page) || (int)$page < 1 ) $page = "1";
            if( !$page_size || !is_numeric($page_size) || (int)$page_size < 1 ) $page_size = "20";

            $images = $this->_model->listActive( 0, $ref_type, $page, $page_size);

            if( $images ){
                $this->response($images, SELF::HTTP_OK);
            }

            $this->response_no_record();
        }
        $this->response_no_auth();
    }
}
