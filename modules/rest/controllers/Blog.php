<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require_once dirname(__FILE__) . '/../libraries/REST_Controller.php';

class Blog extends REST_Controller {

    const ACCESS_TOKEN_LENGTH = 30;

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        $this->load->model('blog/blog_model', '_model');
    }

    private function norm_response($post){
        unset($post->post_created_date);
        unset($post->post_modified_date);
        unset($post->post_user_id);
        // Remove all illegal characters from a url
        $url = filter_var($post->post_photo, FILTER_SANITIZE_URL);
        // Validate url
        if (!filter_var($url, FILTER_VALIDATE_URL) === false) {
            //echo("$url is a valid URL");
        } else {
            //echo("$url is not a valid URL");
            $post->post_photo = site_url($post->post_photo);
        }

        return $post;
    }

    public function get_get($post_id){
        $post = $this->_model->findActive($post_id);
        if( $post ){
            $this->response($this->norm_response($post), SELF::HTTP_OK);
        }
        $this->response_no_record();
    }

    public function list_get(){
        $page = $this->get('page');
        $page_size = $this->get('page_size');
        $category_id = $this->get('category_id');

        if( !$page || !is_numeric($page) || (int)$page < 1 ) $page = 1;
        if( !$page_size || !is_numeric($page_size) || (int)$page_size < 1 ) $page_size = 20;

        if( !$category_id || !is_numeric($category_id) || (int)$category_id < 1 ){
            $this->response([
                    'status' => FALSE,
                    'error' => __('Missing parametter: category_id.')
                ], SELF::HTTP_BAD_REQUEST);
        }

        $posts = $this->_model->listActive($category_id, $page, $page_size);
        $response_data = [];
        foreach ($posts as $post) {
            $response_data[] = $this->norm_response($post);
        }

        $this->response($response_data, SELF::HTTP_OK);
    }

    private function norm_cat_response($cat){
        unset($cat->cat_modified_date);
        unset($cat->cat_user_id);

        return $cat;
    }

    public function category_get($param){
        if( is_numeric($param) ){
            $cat = $this->_model->findCategoryActive((int)$param);
            if( $cat ){
                $this->response($this->norm_response($cat), SELF::HTTP_OK);
            }
            $this->response_no_record();
        }else if($param === 'list'){
            $page = $this->get('page');
            $page_size = $this->get('page_size');

            if( !$page || !is_numeric($page) || (int)$page < 1 ) $page = 1;
            if( !$page_size || !is_numeric($page_size) || (int)$page_size < 1 ) $page_size = 20;

            $cats = $this->_model->listCategoryActive($page, $page_size);
            $response_data = [];
            foreach ($cats as $cat) {
                $response_data[] = $this->norm_cat_response($cat);
            }

            $this->response($response_data, SELF::HTTP_OK);
        }

        $this->response([
            $this->config->item('rest_status_field_name') => FALSE,
            $this->config->item('rest_message_field_name') => __('Unknown method.')
        ], self::HTTP_NOT_FOUND);
    }
}
