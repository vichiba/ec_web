<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require_once dirname(__FILE__) . '/../libraries/REST_Controller.php';

class Product extends REST_Controller {

    const ACCESS_TOKEN_LENGTH = 30;

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        $this->load->model('customer/customer_model');
        $this->load->model('product/product_model', '_model');
        $this->currency_list = $this->config->item('currency');
    }

    private function norm_response($product){
        unset($product->created_date);
        unset($product->modified_date);
        unset($product->created_user_id);
        unset($product->modified_user_id);
        unset($product->raw_data);
        unset($product->click_count);
        unset($product->buy_count);
        unset($product->status);
        $product->price         = number_format($product->price);
        if($product->sale_price > 0 ){
            $product->sale_price  = number_format($product->sale_price);
        }else{
            $product->sale_price = $product->price;
        }
        $product->retail_price  = number_format($product->retail_price);
        // Remove all illegal characters from a url
        $url = filter_var($product->image_url, FILTER_SANITIZE_URL);
        // Validate url
        if (!filter_var($url, FILTER_VALIDATE_URL) === false) {
            //echo("$url is a valid URL");
        } else {
            //echo("$url is not a valid URL");
            $product->image_url = site_url($product->image_url);
        }

        if( $this->isValidAccessToken() ){
            $customer_id = $this->session->userdata('customer_id');
            //$product->is_like = $this->customer_model->isLikeProduct($customer_id, $product->product_id);
            if( isset($product->brand) ){
                $product->brand->is_follow = $this->customer_model->isFollowBrand($customer_id, $product->brand->brand_id);
            }
        }

        if( !$product->currency || is_null($product->currency) ){
            $symbol = isset($this->currency_list[$product->currency])?$this->currency_list[$product->currency]:'¥';
            $product->currency = $symbol;
        }else{
            $product->currency = '¥';
        }

        return $product;
    }

    public function get_get($product_id){
        $product = $this->_model->findActive($product_id);
        if( $product ){
            $this->_model->incClickCount($product_id);
            $this->response($this->norm_response($product), SELF::HTTP_OK);
        }
        $this->response_no_record();
    }

    public function search_get(){
        $keyword            = $this->get('keyword');
        $page               = $this->get('page');
        $page_size          = $this->get('page_size');
        $category_id        = $this->get('category_id');
        $brand_id           = $this->get('brand_id');
        $sort_type          = $this->get('sort_type');
        $price_min          = $this->get('price_min');
        $price_max          = $this->get('price_max');

        if( !$page || !is_numeric($page) || (int)$page < 1 ) $page = 1;
        if( !$page_size || !is_numeric($page_size) || (int)$page_size < 1 ) $page_size = 20;

        // if( !$category_id || !is_numeric($category_id) || (int)$category_id < 1 ){
        //     $this->response([
        //             'status' => FALSE,
        //             'error' => 'Missing parametter: category_id.'
        //         ], SELF::HTTP_BAD_REQUEST);
        // }

        // if( $brand_id && (!is_numeric($brand_id) || (int)$brand_id < 1) ){
        //     $this->response([
        //             'status' => FALSE,
        //             'error' => '[brand_id] value is not correct.'
        //         ], SELF::HTTP_BAD_REQUEST);
        // }

        $price_range = [];
        if( $price_min && is_numeric($price_min) && (int)$price_min > 0 ){
            $price_range['min'] = $price_min;
        }else{
            $price_min = 0;
        }

        if( $price_max && is_numeric($price_max) && (int)$price_max > (int)$price_min ){
            $price_range['max'] = $price_max;
        }

        $products = $this->_model->searchActive($keyword, $category_id, $brand_id, $price_range, $sort_type, $page, $page_size, [], TRUE);
        foreach ($products->items as $k => $product) {
            $response_data->items[$k] = $this->norm_response($product);
        }

        $this->response($products, SELF::HTTP_OK);
    }

    public function like_post(){
        if( $this->isValidAccessToken() && $this->isAccountActive() ){
            $this->load->model('customer/customer_model');
            $customer_id = $this->session->userdata('customer_id');
            $product_id = $this->post('product_id');
            if( $this->customer_model->addProductFavourite($customer_id, $product_id) ){
                $this->response([
                        'status' => TRUE,
                        'message' => __('Like product successfully.')
                    ], SELF::HTTP_OK);
            }else{
                $this->response([
                    'status' => FALSE,
                    'error' => __('This produt has ready liked.')
                ], SELF::HTTP_BAD_REQUEST);
            }
        }
        $this->response_no_auth();
    }

    public function unlike_post(){
        if( $this->isValidAccessToken() && $this->isAccountActive() ){
            $this->load->model('customer/customer_model');
            $customer_id = $this->session->userdata('customer_id');
            $product_id = $this->post('product_id');
            if( $this->customer_model->removeProductFavourite($customer_id, $product_id) ){
                $this->response([
                        'status' => TRUE,
                        'message' => __('Unlike product successfully.')
                    ], SELF::HTTP_OK);
            }else{
                $this->response([
                    'status' => FALSE,
                    'error' => __('This produt has ready unliked.')
                ], SELF::HTTP_BAD_REQUEST);
            }
        }
        $this->response_no_auth();
    }

    public function norm_response_category($category){
        unset($category->created_date);
        unset($category->created_user_id);
        unset($category->modified_date);
        unset($category->modified_user_id);
        unset($category->display_in_home);
        unset($category->status);
        unset($category->search_keyword);
        unset($category->skimlinks_associated);
        unset($category->cj_associated);
        unset($category->rakuten_associated);

        // Remove all illegal characters from a url
        $url = filter_var($category->image_url, FILTER_SANITIZE_URL);
        // Validate url
        if (!filter_var($url, FILTER_VALIDATE_URL) === false) {
            //echo("$url is a valid URL");
        } else {
            //echo("$url is not a valid URL");
            $category->image_url = site_url($category->image_url);
        }

        return $category;
    }

    public function hot_keywords_get(){
        $page               = $this->get('page');
        $page_size          = $this->get('page_size');

        if( !$page || !is_numeric($page) || (int)$page < 1 ) $page = 1;
        if( !$page_size || !is_numeric($page_size) || (int)$page_size < 1 ) $page_size = 20;

        $this->load->model('product/search_keywords_model');
        $result = $this->search_keywords_model->findHotKeywords($page, $page_size);
        foreach ($result->items as $k => $item) {
            $result->items[$k] = $this->norm_response_category($item);
        }
        $this->response($result, SELF::HTTP_OK);
    }
}
