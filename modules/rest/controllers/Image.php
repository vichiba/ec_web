<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require_once dirname(__FILE__) . '/../libraries/REST_Controller.php';

class Image extends REST_Controller {

    const ACCESS_TOKEN_LENGTH = 30;

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        $this->load->model('image/image_model', '_model');
    }

    private function norm_response($post){
        unset($post->post_created_date);
        unset($post->post_modified_date);
        unset($post->post_user_id);

        return $post;
    }

    public function get_get($position, $id = 0){
        
        $ref_type = $position;
        $ref_id = (int)$position == Image_model::TYPE_HOMEPAGE ? 0: $id;

        /*$validation_rules = array(
            array(
                    'field' => 'position',
                    'label' => __('Position'),
                    'rules' => 'trim|required|integer'
            ),
            array(
                    'field' => 'id',
                    'label' => __('ID'),
                    'rules' => 'trim|integer'
            )
        );
        $this->form_validation->set_data(['position' => $ref_type, 'id' => $ref_id]);

        $this->set_validation_rules($validation_rules);*/

        if( !is_numeric($ref_type) ) $this->response_no_record();
        if( $ref_id && !is_numeric($ref_id) )  $this->response_no_record();

        $images = $this->_model->listActive( (int)$ref_id, (int)$ref_type );

        if( $images ){
            $this->response($images, SELF::HTTP_OK);
        }

        $this->response_no_record();
    }
}
