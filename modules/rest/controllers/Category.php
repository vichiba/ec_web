<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require_once dirname(__FILE__) . '/../libraries/REST_Controller.php';

class Category extends REST_Controller {

    const ACCESS_TOKEN_LENGTH = 30;

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        $this->load->model('category/category_model', '_model');
    }

    public function norm_response($category){
        unset($category->created_date);
        unset($category->created_user_id);
        unset($category->modified_date);
        unset($category->modified_user_id);
        unset($category->display_in_home);
        unset($category->status);
        unset($category->search_keyword);
        unset($category->skimlinks_associated);
        unset($category->cj_associated);
        unset($category->rakuten_associated);

        // Remove all illegal characters from a url
        $url = filter_var($category->image_url, FILTER_SANITIZE_URL);
        // Validate url
        if (!filter_var($url, FILTER_VALIDATE_URL) === false) {
            //echo("$url is a valid URL");
        } else {
            //echo("$url is not a valid URL");
            $category->image_url = site_url($category->image_url);
        }

        return $category;
    }

    public function get_get($category_id){
        $category = $this->_model->findActive($category_id);
        if( $category ){
            $this->response($this->norm_response($category), SELF::HTTP_OK);
        }
        $this->response_no_record();
    }

    private function norm_recursive($list){
        foreach ($list as $k => $item) {
            $list[$k] = $this->norm_response($item);
            if(isset($item->has_child) && $item->has_child === TRUE){
                $list[$k]->childs = $this->norm_recursive($list[$k]->childs);
            }
        }
        return $list;
    }

    public function list_get(){
        $page = $this->get('page');
        $page_size = $this->get('page_size');
        $recursive = $this->get('recursive');

        if( !$page || !is_numeric($page) || (int)$page < 1 ) $page = "1";
        if( !$page_size || !is_numeric($page_size) || (int)$page_size < 1 ) $page_size = "20";
        if( !is_numeric($recursive) ) $recursive = "1";

        $category_list = $this->_model->listActive((int)$recursive === 1 ? 0 : NULL, $page, $page_size, (int)$recursive === 1);
        $response_data = $this->norm_recursive($category_list);

        $this->response($response_data, SELF::HTTP_OK);
    }
}
