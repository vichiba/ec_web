<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require_once dirname(__FILE__) . '/../libraries/REST_Controller.php';

class Homepage extends REST_Controller {


    function __construct()
    {
        // Construct the parent class
        parent::__construct();
    }

    public function hot_category_get(){
        $this->load->model('category/category_model', '_model');
        $this->load->module('rest/category');
        $category_id = 1;
        $category_entity = $this->_model->findActive($category_id);
        if( $category_entity ){
            $this->response($this->category->norm_response($category_entity), SELF::HTTP_OK);
        }
        $this->response_no_record();
    }
}
