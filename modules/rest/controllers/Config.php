<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require_once dirname(__FILE__) . '/../libraries/REST_Controller.php';

class Config extends REST_Controller {


    function __construct(){
        // Construct the parent class
        parent::__construct();

        $this->load->model('admin/config_model', '_model');
    }

    public function get_get($config_key){
        $value = $this->_model->findByKey(strtoupper($config_key));
        if($value){

            $this->response(json_decode($value->conf_value), SELF::HTTP_OK);
        }else{
            $this->response([
                'status' => FALSE,
                'error' => __('Config key is not exists')
            ], SELF::HTTP_BAD_REQUEST);
        }
    }

}
