<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require_once dirname(__FILE__) . '/../libraries/REST_Controller.php';

class Brand extends REST_Controller {

    const ACCESS_TOKEN_LENGTH = 30;

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        $this->load->model('brand/brand_model', '_model');
        $this->load->model('customer/customer_model');
    }

    public function norm_response($brand){
        unset($brand->created_user_id);
        unset($brand->modified_user_id);
        unset($brand->status);
        unset($brand->raw_data);
        unset($brand->ref_id);
        unset($brand->created_date);
        unset($brand->modified_date);
        unset($brand->position);

        // Remove all illegal characters from a url
        $url = filter_var($brand->image_url, FILTER_SANITIZE_URL);
        // Validate url
        if (!filter_var($url, FILTER_VALIDATE_URL) === false) {
            //echo("$url is a valid URL");
        } else {
            //echo("$url is not a valid URL");
            $brand->image_url = site_url($brand->image_url);
        }

        if( $this->isValidAccessToken() ){
            $customer_id = $this->session->userdata('customer_id');
            $brand->is_follow = $this->customer_model->isFollowBrand($customer_id, $brand->brand_id);
        }


        return $brand;
    }

    public function get_get($brand_id){
        $brand = $this->_model->findActive($brand_id);
        $is_recrusive = $this->get('recrusive');
        if( $brand ){
            $data = $this->norm_response($brand);
            $list_category = $this->_model->listActiveCategory($data->brand_id);
            $this->load->module('rest/category');
            $this->load->model('category/category_model');
            $catalogs = $this->category_model->listActive(NULL, 1, 100, TRUE);

            $data->categories = [];
            $checked_category = [];
            foreach ($list_category as $item) {
                $root = $this->getParentClosest(
                                $this->category_model->findActive($item->category_id), 
                                $catalogs
                        );
                if( $root && !isset($checked_category[$root->category_id]) ){
                    $checked_category[$root->category_id] = $root->name;
                    $root = $this->category->norm_response($root);
                    if( isset($root->has_child) && isset($root->childs) ){
                        unset($root->has_child);
                        unset($root->childs);
                    }
                    $data->categories[] = $root;
                }
            }
            $this->response($data, SELF::HTTP_OK);
        }
        $this->response_no_record();
    }

    private function getParentClosest($current, $list){
        if($current->parent_id == 0) return $current;
        foreach ($list as $item) {
            if( (int)$item->category_id === (int)$current->parent_id ) return $this->getParentClosest($item, $list);
        }
    }

    public function list_get(){
        $page = $this->get('page');
        $page_size = $this->get('page_size');

        if( !$page || !is_numeric($page) || (int)$page < 1 ) $page = "1";
        if( !$page_size || !is_numeric($page_size) || (int)$page_size < 1 ) $page_size = "20";

        $brands = $this->_model->listActive($page, $page_size);
        
        foreach ($brands as $k => $brand) {
            $brands[$k] = $this->norm_response($brand);
        }

        $this->response($brands, SELF::HTTP_OK);
    }

    public function search_get(){
        $keyword = $this->get('keyword');
        $page = $this->get('page');
        $page_size = $this->get('page_size');
        $category_id = $this->get('category_id');

        if( !$page || !is_numeric($page) || (int)$page < 1 ) $page = "1";
        if( !$page_size || !is_numeric($page_size) || (int)$page_size < 1 ) $page_size = "20";

        $brands = $this->_model->searchActive($keyword, $category_id, $page, $page_size);
        
        foreach ($brands->items as $k => $brand) {
            $brands->items[$k] = $this->norm_response($brand);
        }

        $this->response($brands, SELF::HTTP_OK);
    }

    public function follow_post(){
        if( $this->isValidAccessToken() && $this->isAccountActive() ){
            $customer_id = $this->session->userdata('customer_id');
            $brand_id = $this->post('brand_id');
            $this->_model->doCustomerFollow($customer_id, $brand_id);
            $this->response([
                    'status' => TRUE,
                    'message' => __('Follow brand successfully.')
                ], SELF::HTTP_OK);
        }
        $this->response_no_auth();
    }

    public function unfollow_post(){
        if( $this->isValidAccessToken() && $this->isAccountActive() ){
            $customer_id = $this->session->userdata('customer_id');
            $brand_id = $this->post('brand_id');
            $this->_model->doCustomerUnfollow($customer_id, $brand_id);
            $this->response([
                    'status' => TRUE,
                    'message' => __('Unfollow brand successfully.')
                ], SELF::HTTP_OK);
        }
        $this->response_no_auth();
    }
}
