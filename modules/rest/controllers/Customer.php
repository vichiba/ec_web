<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require_once dirname(__FILE__) . '/../libraries/REST_Controller.php';

class Customer extends REST_Controller {

    const ACCESS_TOKEN_LENGTH   = 30;
    const RESETPW_TOKEN_LENGTH  = 30;
    const ACTIVE_TOKEN_LENGTH   = 30;

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model('customer/customer_model', '_model');
    }

    public function index_get(){
        if( $this->isValidAccessToken() ){
            $customer_id = $this->session->userdata('customer_id');
            $customer = $this->_model->find($customer_id);
            if( $customer ){
                unset($customer->customer_id);
                unset($customer->password);
                unset($customer->status);
                unset($customer->active_token);
                unset($customer->refresh_token);
                unset($customer->resetpw_token);

                // Remove all illegal characters from a url
                $url = filter_var($customer->avatar, FILTER_SANITIZE_URL);
                // Validate url
                if (!filter_var($url, FILTER_VALIDATE_URL) === false) {
                    //echo("$url is a valid URL");
                } else {
                    //echo("$url is not a valid URL");
                    $customer->avatar = site_url($customer->avatar);
                }
                $customer->brand_followed = $this->_model->countFollowBrand($customer_id);
                $customer->product_liked = $this->_model->countLikeProduct($customer_id);
                $this->response($customer, SELF::HTTP_OK); 
            }
        }
        $this->response_no_auth();
    }

    public function is_password_strong($str){
        if( preg_match('#[0-9]#', $str) && preg_match('#[a-z]#', $str)  && preg_match('#[A-Z]#', $str) ){
            return TRUE;
        }
        $this->form_validation->set_message('is_password_strong', __('Password not secure'));
        return FALSE;
    }

    public function login_post(){
        $email          = $this->post('email');
        $password       = $this->post('password');

        $validation_rules = array(
            array(
                'field' => 'email',
                'label' => __('Email'),
                'rules' => 'trim|required|valid_email'
            ),
            array(
                'field' => 'password',
                'label' => __('Password'),
                'rules' => 'trim|required|min_length[8]|callback_is_password_strong'
            )
        );


        $this->set_validation_rules($validation_rules);
        $this->run_validation();

        $refresh_token  = generate_token(SELF::ACCESS_TOKEN_LENGTH);

        $customer_id    = $this->_model->login($email, $password, $refresh_token);
        if( $customer_id && is_numeric($customer_id) ){
            $access_token = generate_token(SELF::ACCESS_TOKEN_LENGTH);

            $this->session->set_userdata('customer_id', $customer_id);
            $this->session->set_userdata('customer_access_token', $access_token);

            $this->response([
                    'status' => TRUE,
                    'message' => __('Login successfully.'),
                    'access_token' => $access_token,
                    'refresh_token' => $refresh_token
                ], SELF::HTTP_OK); 
        }else{
            $customer_id = $this->_model->exists(['email' => $email]);
            if( $customer_id && is_numeric($customer_id)  && !$this->_model->isActive($customer_id) ){
                $this->response([
                    'status' => FALSE,
                    'error' => __('Your account has not active yet.')
                    ], SELF::HTTP_BAD_REQUEST);     
            }else{
                $this->response([
                    'status' => FALSE,
                    'error' => __('Username/password not match.')
                    ], SELF::HTTP_BAD_REQUEST); 
            }
        }
    }


    public function is_unique($str, $field)
    {
        if( is_null($str) ) return TRUE; //Xu ly cho TH Goole+, Facebook, Twitter
        sscanf($field, '%[^.].%[^.]', $table, $field);
        return $this->db->limit(1)->get_where($table, array($field => $str))->num_rows() === 0;

    }

    public function register_post()
    {
        $email          = $this->post('email');
        $password       = $this->post('password');
        $fullname       = $this->post('fullname');
        $google_id      = $this->post('google_id');
        $facebook_id    = $this->post('facebook_id');
        $twitter_id     = $this->post('twitter_id');
        $gender         = Customer_model::GENDER_NONE;

        $validation_rules = array(
            array(
                    'field' => 'email',
                    'label' => __('Email'),
                    'rules' => 'trim|required|valid_email|callback_is_unique[customers.email]'
            ),
            array(
                    'field' => 'password',
                    'label' => __('Password'),
                    'rules' => 'trim|required|min_length[8]|callback_is_password_strong'
            ),
            array(
                    'field' => 'fullname',
                    'label' => __('Fullname'),
                    'rules' => 'trim|required|min_length[3]|max_length[20]'
            ),
            array(
                    'field' => 'google_id',
                    'label' => __('Google ID'),
                    'rules' => 'trim|callback_is_unique[customers.google_id]'
            ),
            array(
                    'field' => 'facebook_id',
                    'label' => __('Facebook ID'),
                    'rules' => 'trim|callback_is_unique[customers.facebook_id]'
            ),
            array(
                    'field' => 'twitter_id',
                    'label' => __('Twitter ID'),
                    'rules' => 'trim|callback_is_unique[customers.twitter_id]'
            ),
        );

        $this->set_validation_rules($validation_rules);
        $this->run_validation();

        if( $this->_model->exists(['email' => $email]) ){
            $this->response([
                    'status' => FALSE,
                    'error' => __('User ready exists.')
                    ], SELF::HTTP_BAD_REQUEST);    
        }else{
            $active_token = generate_token(SELF::ACTIVE_TOKEN_LENGTH);
            if( $this->_model->regsiter($email, $password, $fullname, $gender, $google_id, $facebook_id, $twitter_id, $active_token) ){
                
                $this->sendRegisterConfirmationEmail($fullname, $email, $active_token);

                $this->response([
                    'status' => TRUE,
                    'message' => __('Register new account successfully.')
                    //'active_token' => $active_token
                    ], SELF::HTTP_OK); 
            }
        }
    }

    public function sendRegisterConfirmationEmail($fullname, $email, $active_token){
        $data = [
            'fullname' => $fullname,
            'active_link' => site_url('customer/do_active/?token='.$active_token)
        ];

        $this->load->library('email');

        $this->email->initialize(['mailtype' => 'html']);

        $this->email->from('ecsystem.ytasia@gmail.com', 'ecsystem.com');
        
        $this->email->to($email);

        $this->email->subject(sprintf(__('Register new account Confirmation for %s'), $fullname));
        
        $this->email->message($this->load->view('customer/email/register', $data, TRUE));

        $this->email->send();
    }

    public function sendRequestPasswordEmail($fullname, $email, $resetpw_token){

        $data = [
            'fullname' => $fullname,
            'reset_link' => site_url('customer/reset_password/?token='.$resetpw_token)
        ];

        $this->load->library('email');

        $this->email->initialize(['mailtype' => 'html']);

        $this->email->from('ecsystem.ytasia@gmail.com', 'ecsystem.com');
        
        $this->email->to($email);

        $this->email->subject(sprintf(__('Password Reset Confirmation for %s'), $fullname));
        
        $this->email->message($this->load->view('customer/email/reset_password', $data, TRUE));

        $this->email->send();
    }

    public function profile_post()
    {
        if( $this->isValidAccessToken() ){
            $customer_id = $this->session->userdata('customer_id');

            $fullname               = $this->post('fullname');

            $password_cr            = $this->post('password_cr');
            $password_new           = $this->post('password_new');
            $password_cf            = $this->post('password_cf');
            $avatar                 = $this->post('avatar');
            $gender                 = $this->post('gender');

            $validation_rules = array(
                array(
                        'field' => 'fullname',
                        'label' => __('Fullname'),
                        'rules' => 'trim|min_length[3]|max_length[20]'
                ),
                array(
                        'field' => 'gender',
                        'label' => __('Gender'),
                        'rules' => 'trim|integer|in_list[0,1,2,3]'
                ),
                array(
                        'field' => 'password_new',
                        'label' => __('New Password'),
                        'rules' => 'trim|min_length[8]' //Khong can is_password_strong o day
                ),
                array(
                        'field' => 'password_cf',
                        'label' => __('Confirm Password'),
                        'rules' => 'trim|min_length[8]|matches[password_new]'
                )
            );

            $this->set_validation_rules($validation_rules);
            $this->run_validation();
            $is_exists = FALSE;
            if( !empty($password_new) ){
                $is_exists = $this->_model->exists(['customer_id' => $customer_id, 'password' => md5($password_cr)]);
            }else{
                $is_exists = $this->_model->exists(['customer_id' => $customer_id]);
            }

            if( $is_exists ){
                
                $data = [];
                if( !empty($fullname) ){
                    $data['fullname'] = $fullname;
                }
                if( !empty($gender) ){
                    $data['gender'] = $gender;
                }
                if( !empty($password_new) ){
                    $data['password'] = md5($password_new);
                    if( !$this->is_password_strong($password_new) ){
                        $this->response([
                            'status' => FALSE,
                            'error' => __('Password not secure')
                        ], SELF::HTTP_BAD_REQUEST);
                    }
                }

                $_file = isset($_FILES['avatar'])?$_FILES['avatar']:NULL;
                if( isset($_file) && $_file && isset($_file['name']) && $_file['name'] ){
                    //upload image
                    $upload_path = '/uploads/profiles/';
                    $config                         = [];
                    $config['upload_path']          = '.'.$upload_path;
                    $config['allowed_types']        = 'gif|jpg|png';
                    $config['max_size']             = 2048;
                    $config['max_width']            = 1020;
                    $config['max_height']           = 1020;
                    $config['file_ext_tolower']     = TRUE;

                    $this->load->library('upload', $config);

                    if ( $this->upload->do_upload('avatar')){
                        $result = $this->upload->data();
                        //debug($result, TRUE);
                        if( $result && isset($result['file_name']) ){
                            
                            //remove old avatar file
                            $customer = $this->_model->find($customer_id);
                            if($customer) unlink('.'.$customer->avatar);

                            $data['avatar'] = $upload_path.$result['file_name'];
                        }
                    }else{
                        $this->response([
                            'status' => FALSE,
                            'error' => $this->upload->display_errors('', '\n'),
                        ], SELF::HTTP_BAD_REQUEST);
                    }
                }


                if( $this->_model->update($customer_id, $data) !== FALSE ){
                    $this->response([
                        'status' => TRUE,
                        'message' => __('Your profile has been updated successfully.')
                    ], SELF::HTTP_OK);
                }
                $this->response([
                    'status' => FALSE,
                    'error' => __('Update Failed')
                    ], SELF::HTTP_BAD_REQUEST);
            }else{
                $this->response([
                    'status' => FALSE,
                    'error' => __('Current password not match')
                    ], SELF::HTTP_BAD_REQUEST);
            }
            
        }
        $this->response_no_auth();
    }

    public function active_get(){
        $email  = $this->get('email');
        $token = $this->get('token');

        $validation_rules = array(
            array(
                    'field' => 'email',
                    'label' => __('Email'),
                    'rules' => 'trim|required|valid_email'
            ),
            array(
                    'field' => 'token',
                    'label' => __('Token'),
                    'rules' => 'trim|required|min_length[5]'
            )
        );

        $this->set_validation_rules($validation_rules);
        //$this->run_validation(FALSE);
        $customer_id = $this->_model->exists(['email' => $email]);
        if($customer_id && is_numeric($customer_id)){
            if($this->_model->isActive($customer_id)){
                $this->response([
                        'status' => FALSE,
                        'message' => __('Account had been ready actived before.')
                        ], SELF::HTTP_OK); 
            }else{
                if( $this->_model->active($email, $token) ){
                    $this->response([
                            'status' => TRUE,
                            'message' => __('Account has been actived.')
                            ], SELF::HTTP_OK); 
                }else{
                    $this->response([
                            'status' => FALSE,
                            'error' => __('Active action was wrong.')
                            ], SELF::HTTP_BAD_REQUEST); 
                }
            }
        }else{
            $this->response([
                    'status' => FALSE,
                    'error' => __('Customer does not exists.')
                    ], SELF::HTTP_BAD_REQUEST); 
        }
    }

    public function is_active_get(){
        if( $this->isValidAccessToken() ){
            $customer_id = $this->session->userdata('customer_id');
            $active = $this->_model->isActive($customer_id);

            $this->response([
                'status' => $active,
                'message' => sprintf(__('Account was%s actived.'), ($active?"":" not"))
                ], SELF::HTTP_OK); 
        }
        $this->response_no_auth();
    }

    public function social_login_post(){
        $vendor  = $this->post('vendor');
        $access_token = $this->post('access_token');
        $access_token_secret = $this->post('access_token_secret'); //For Twitter Only
        $twitter_email = $this->post('email');

        $email = NULL;
        $fullname = NULL;
        $google_id = NULL;
        $facebook_id = NULL;
        $twitter_id = NULL;
        $customer_id = NULL;

        if( $vendor && $access_token ){
            switch ($vendor) {
                case 'facebook':
                    $content = @file_get_contents("https://graph.facebook.com/me?fields=id,name,email,gender&access_token=$access_token");
                    if( $content && ($content = json_decode($content)) ){
                        $facebook_id = $content->id;
                        $email = $content->email;
                        $fullname = $content->name;                 
                        $gender = $content->gender;
                        switch ($gender) {
                            case 'male':
                                $gender = Customer_model::GENDER_MAN;
                                break;
                            case 'female':
                                $gender = Customer_model::GENDER_WOMEN;
                                break;
                            default:
                                $gender = Customer_model::GENDER_NONE;
                                break;
                        }
                        $password = $facebook_id;
                        $avatar = 'http://graph.facebook.com/'.$facebook_id.'/picture?type=large';

                        $customer_id = $this->_model->exists(['email' => $email, 'facebook_id' => $facebook_id]);

                        if( $customer_id && is_numeric($customer_id) ){

                        }else{
                            $customer_id = $this->_model->exists(['email' => $email]);
                            if( $customer_id && is_numeric($customer_id) ){
                                $data = [
                                    'facebook_id' => $facebook_id
                                ];

                                $this->_model->update($customer_id, $data);
                            }else{
                                $customer_id = $this->_model->regsiter($email, $password, $fullname, $gender, $google_id, $facebook_id, $twitter_id, '', Customer_model::STATUS_OK, $avatar);
                            }
                        }

                        if( $customer_id && is_numeric($customer_id) ){
                            $customer = $this->_model->find($customer_id);
                            $this->session->set_userdata('customer_id', $customer_id);
                            $this->session->set_userdata('customer_name', $customer->fullname);
                            $this->session->set_userdata('customer_avatar', $customer->avatar);
                            $this->session->set_userdata('customer_email', $customer->email);

                            $this->session->set_userdata('customer_fid', $customer->facebook_id);
                            $this->session->set_userdata('customer_gi', $customer->google_id);
                            $this->session->set_userdata('customer_tid', $customer->twitter_id);
                        }
                    }
                    break;
                case 'twitter':
                    $this->load->module('customer/twitter_auth');
                    
                    // If twitter does not response email and query string does not include email parametter
                    if( empty($twitter_email) && ($twitter_email = $this->twitter_auth->has_email($access_token, $access_token_secret)) === FALSE ){
                        $this->response([
                            'status' => FALSE,
                            'message' => __('Twitter email is invalid.')
                        ], SELF::HTTP_BAD_REQUEST); 
                    }
                    
                    $customer_id = $this->twitter_auth->do_login($twitter_email, $access_token, $access_token_secret);
                    
                break;
                case 'google':
                    $this->load->module('customer/oauth2');
                    $customer_id = $this->oauth2->do_login($access_token);
                break;
            }

            if( $customer_id && is_numeric($customer_id) ){
                
                $access_token = generate_token(SELF::ACCESS_TOKEN_LENGTH);
                $refresh_token  = generate_token(SELF::ACCESS_TOKEN_LENGTH);

                $this->session->set_userdata('customer_access_token', $access_token);
                
                //Update refresh token to DB
                $this->_model->update($customer_id, ['refresh_token' => $refresh_token]);

                $this->response([
                    'status' => TRUE,
                    'message' => __('Login successfully.'),
                    'access_token' => $access_token,
                    'refresh_token' => $refresh_token
                ], SELF::HTTP_OK); 
            } else{
                $this->response([
                    'status' => FALSE,
                    'message' => __('Login Failed.'),
                ], SELF::HTTP_BAD_REQUEST); 
            }
        }else{
            $this->response([
                    'status' => FALSE,
                    'error' => __('Missing params.')
                    ], SELF::HTTP_BAD_REQUEST); 
        }
    }

    public function refresh_token_get(){
        $refresh_token          = $this->get('refresh_token');

        $customer_id    = $this->_model->checkRefreshToken($refresh_token);
        if( $customer_id && is_numeric($customer_id) ){
            $access_token = generate_token(SELF::ACCESS_TOKEN_LENGTH);

            $this->session->set_userdata('customer_id', $customer_id);
            $this->session->set_userdata('customer_access_token', $access_token);

            $this->response([
                    'status' => TRUE,
                    'message' => __('Access Token refresh successfully.'),
                    'access_token' => $access_token
                ], SELF::HTTP_OK); 
        }
        $this->response([
                'status' => FALSE,
                'error' => __('Refresh token is not match.')
                ], SELF::HTTP_BAD_REQUEST); 
    }

    public function logout_post(){
        if( $this->isValidAccessToken() ){
            $this->session->unset_userdata('customer_id');
            $this->session->unset_userdata('customer_access_token');
            $this->response([
                    'status' => TRUE,
                    'message' => __('Logout successfully.')
                ], SELF::HTTP_OK); 
        }
        $this->response_no_auth();
    }

    public function is_logon_get(){
        $access_token = $this->session->userdata('customer_access_token');
        if( $access_token ){
            $this->response([
                    'status' => TRUE,
                    'message' => __('Login successfully.'),
                    'access_token' => $access_token,
                ], SELF::HTTP_OK); 
        }else{
            $this->response([
                    'status' => FALSE,
                    'error' => __('Not login yet.')
                ], SELF::HTTP_BAD_REQUEST); 
        }
    }

    public function brand_get(){
        if( $this->isValidAccessToken() && $this->isAccountActive() ){
            $this->load->model('brand/brand_model');
            $this->load->module('rest/brand');
            $customer_id = $this->session->userdata('customer_id');
            
            $page = $this->get('page');
            $page_size = $this->get('page_size');

            if( !$page || !is_numeric($page) || (int)$page < 1 ) $page = "1";
            if( !$page_size || !is_numeric($page_size) || (int)$page_size < 1 ) $page_size = "20";

            $list = $this->brand_model->listBeFollowed($customer_id, $page, $page_size);
            foreach ($list->items as $k => $item) {
                $list->items[$k] = $this->brand->norm_response($this->brand_model->findActive($item->brand_id));
            }
            $this->response($list, SELF::HTTP_OK);
        }
        $this->response_no_auth();
    }

    public function forgotpassword_post(){
        $email          = $this->post('email');
        $validation_rules = array(
            array(
                    'field' => 'email',
                    'label' => __('Email'),
                    'rules' => 'trim|required|valid_email'
            )
        );

        $this->set_validation_rules($validation_rules);
        $this->run_validation();
        $cust_id = $this->_model->exists(['email' => $email]);
        if( $cust_id && is_numeric($cust_id) ){
            $resetpw_token = generate_token(SELF::RESETPW_TOKEN_LENGTH);
            $this->_model->setResetPWToken($email, $resetpw_token);
            
            //Send email
            $cust = $this->_model->find($cust_id);
            $this->sendRequestPasswordEmail($cust->fullname, $email, $resetpw_token);
            
            $this->response([
                'status' => TRUE,
                'reset_token' => $resetpw_token
                ], SELF::HTTP_OK);
        }
        $this->response([
            'status' => FALSE,
            'error' => __('Email does not exists.')
            ], SELF::HTTP_BAD_REQUEST); 
    }

    public function resetpassword_post(){
        $email          = $this->post('email');
        $new_password   = $this->post('new_password');
        $reset_token    = $this->post('reset_token');

        $validation_rules = array(
            array(
                    'field' => 'email',
                    'label' => __('Email'),
                    'rules' => 'trim|required|valid_email'
            ),
            array(
                    'field' => 'new_password',
                    'label' => __('New Password'),
                    'rules' => 'trim|required|min_length[5]'
            ),
            array(
                    'field' => 'reset_token',
                    'label' => __('Reset Token'),
                    'rules' => 'trim|required|min_length[5]'
            )
        );

        $this->set_validation_rules($validation_rules);
        $this->run_validation();

        if( $this->_model->exists(['email' => $email, 'resetpw_token' => $reset_token]) ){
            if( $this->_model->resetPassword($email, $new_password) ){
                $this->response([
                'status' => TRUE,
                'message' => __('Password has been reset successfully.')
                ], SELF::HTTP_OK);
            }
        }
        $this->response(NULL, SELF::HTTP_NOT_FOUND); 
    }

    public function product_get(){
        if( $this->isValidAccessToken() && $this->isAccountActive() ){
            $page = $this->get('page');
            $page_size = $this->get('page_size');

            if( !$page || !is_numeric($page) || (int)$page < 1 ) $page = "1";
            if( !$page_size || !is_numeric($page_size) || (int)$page_size < 1 ) $page_size = "20";

            $this->load->model('product/product_model');
            $customer_id = $this->session->userdata('customer_id');
            
            $list = $this->_model->listProductFavourite($customer_id, $page, $page_size);
            
            foreach ($list->items as $k => $item) {
                $product_item = $this->product_model->findActive($item->product_id);
                if($product_item){
                    $product_item->price        = number_format($product_item->price);
                    $product_item->sale_price   = number_format($product_item->sale_price);
                    $product_item->retail_price = number_format($product_item->retail_price);

                    $list->items[$k] = $product_item;

                }
            }
            $this->response($list, SELF::HTTP_OK);
        }
        $this->response_no_auth();
    }
}
