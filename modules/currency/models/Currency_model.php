<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Currency_model extends MY_Model{
 	 	
 	public $table = 'currency';
	public $primary_key = 'id';

	static $CURRENCY_LIST = [];
	

	public function change($currency_code, $value){
		$rate = 1;
		if( empty($currency_code) ) return $value;
		if( isset(self::$CURRENCY_LIST[$currency_code]) ){
			$rate = self::$CURRENCY_LIST[$currency_code];
		}else{
			$list = $this->find_all([], 'created_date desc');
			foreach ($list as $item){
				if($item->currency === $currency_code){
					$rate = (double)$item->value;
					self::$CURRENCY_LIST[$currency_code] = $rate;
					break;
				}
			}
		}
		$result = $rate * (double)$value;
		return $result;
	}

}
