<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends MX_Controller 
{
    function __construct($foo = null)
    {
        $this->load->library('pagination');

        parent::__construct();
    }
    public function index(){

    	$keyword            = $this->input->get('keyword');
        $page               = $this->input->get('page');
        $page_size          = $this->input->get('page_size');
        $category_id        = $this->input->get('category_id');
        $brand_id           = $this->input->get('brand_id');
        $sort_type          = $this->input->get('sort_type');
        $price              = $this->input->get('price_range');
        $price_conds        = explode('-', $price);
        $price_min          = isset($price_conds[0]) ? $price_conds[0] : 0;
        $price_max          = isset($price_conds[1]) ? $price_conds[1] : 0;
        //$price_min          = $this->input->get('price_min');
        //$price_max          = $this->input->get('price_max');

        if( !$page || !is_numeric($page) || (int)$page < 1 ) $page = 1;
        if( !$page_size || !is_numeric($page_size) || (int)$page_size < 1 || (int)$page_size > 20 ) $page_size = 20;

        $price_range = [];
        if( $price_min && is_numeric($price_min) && (int)$price_min > 0 ){
            $price_range['min'] = $price_min;
        }else{
            $price_min = 0;
        }

        if( $price_max && is_numeric($price_max) && (int)$price_max > (int)$price_min ){
            $price_range['max'] = $price_max;
        }

        $products = $this->product_model->searchActive($keyword, $category_id, $brand_id, $price_range, $sort_type, $page, $page_size);

        $breadcrumbs = [
        	'page_title' => __('Search').': '.$keyword,
        	'items' => [
        		['label' => __('Home'), 'link' => base_url()],
                ['label' => __('Search'), 'link' => base_url()],
        		['label' => $keyword, 'link' => site_url('search?keyword='.$keyword)],
        	]
        ];

        $brands = $this->brand_model->listActive(1, 1000);
        $categories = $this->category_model->listActive(NULL, 1, 1000);

    	$data_render = [
    		'products' => $products,
            'brands' => $brands,
            'categories' => $categories,
    		'breadcrumbs' => $breadcrumbs,
            'sort_type' => $sort_type,
            'price_range' => $price_range,
            'page_title' => __('Search').': '.$keyword . ' | ' . sprintf(__("About %s results."),number_format($products->total)),
            'page_keywords' => $keyword,
    	];


        $this->load->view('search', $data_render);
    }
}