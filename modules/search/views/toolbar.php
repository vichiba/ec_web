<!--Toolbar-->
<div class="box-select toolbar row">
    <div class="col-xs-6 col-md-3">
        <div class="box-select-items">
            <label></label>
            <select class="selectpicker" name="category_id">
                <option value=""><?php echo __('All') ?></option>
                <?php foreach ($categories as $item): ?>
                    <option value="<?php echo $item->category_id ?>" <?php echo $this->input->get('category_id') == $item->category_id ? 'selected' : '' ?>><span><?php echo $item->name ?></span></option>
                <?php endforeach ?>
                <option class="hidden"><span>hidden-option-fix</span> </option>
            </select>
        </div>
    </div>
    <div class="col-xs-6 col-md-3">
        <div class="box-select-items">
            <label></label>
            <select class="selectpicker" name="brand_id">
                <option value=""><?php echo __('All') ?></option>
                <?php foreach ($brands as $item): ?>
                    <option value="<?php echo $item->brand_id ?>" <?php echo $this->input->get('brand_id') == $item->brand_id ? 'selected' : '' ?>><span><?php echo $item->name ?></span></option>
                <?php endforeach ?>
                <option class="hidden"><span>hidden-option-fix</span> </option>
            </select>
        </div>
    </div>
    <div class="col-xs-6 col-md-3">
        <div class="box-select-items">
            <label><?php echo __('Price') ?>:</label>
            <select class="selectpicker" name="price_range">
                <option value=""><?php echo __('All') ?></option>
                <?php 
                $price_range_list = json_decode(get_config_value('PRICE_SEARCH_RANGE'));
                foreach ($price_range_list as $item): 
                $price_range_value = $item->min_price . ($item->max_price?'-'.$item->max_price:'');
                ?>
                <option value="<?php echo $price_range_value ?>" <?php echo $this->input->get('price_range') == $price_range_value ? 'selected' : '' ?>>
                    <span><?php echo $item->display ?></span>
                </option>
                <?php endforeach ?>
            </select>
        </div>
    </div>
    <div class="col-xs-6 col-md-3">
        <div class="box-select-items">
            <label><?php echo __('Sort') ?> :</label>
            <select class="selectpicker" name="sort_type">
                <option value="1" <?php echo $this->input->get('sort_type') == '1' ? 'selected' : '' ?>><span><?php echo __('Newest') ?></span> </option>
                <option value="2" <?php echo $this->input->get('sort_type') == '2' ? 'selected' : '' ?>><span><?php echo __('Polular') ?></span> </option>
                <option value="3" <?php echo $this->input->get('sort_type') == '3' ? 'selected' : '' ?>><span><?php echo __('On Sale') ?></span> </option>
            </select>
        </div>
    </div>
</div>
<!--End of Toolbar-->