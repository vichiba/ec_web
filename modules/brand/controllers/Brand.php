<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Brand extends MX_Controller 
{
    function __construct($foo = null)
    {
        $this->load->library('pagination');

        parent::__construct();
    }
    public function index($uri){

        $brand = $this->brand_model->findActiveByUri($uri);
    	if( !$brand ){
    		show_404();
    		return;
    	}

    	$keyword            = NULL;
        $page               = $this->input->get('page');
        $page_size          = $this->input->get('page_size');
        $category_id        = $this->input->get('category_id');
        $brand_id           = $brand->brand_id;
        $sort_type          = $this->input->get('sort_type');
        $price          	= $this->input->get('price_range');
        $price_conds = explode('-', $price);
        $price_min = isset($price_conds[0]) ? $price_conds[0] : 0;
        $price_max = isset($price_conds[1]) ? $price_conds[1] : 0;

        if( !$page || !is_numeric($page) || (int)$page < 1 ) $page = 1;
        if( !$page_size || !is_numeric($page_size) || (int)$page_size < 1 ) $page_size = 20;

        $price_range = [];
        if( $price_min && is_numeric($price_min) && (int)$price_min > 0 ){
            $price_range['min'] = $price_min;
        }else{
            $price_min = 0;
        }

        if( $price_max && is_numeric($price_max) && (int)$price_max > (int)$price_min ){
            $price_range['max'] = $price_max;
        }

        $products = $this->product_model->searchActive($keyword, $category_id, $brand_id, $price_range, $sort_type, $page, $page_size);

        $breadcrumbs = [
        	'page_title' => $brand->name,
        	'items' => [
        		['label' => __('Home'), 'link' => base_url()],
        		['label' => __('Brand'), 'link' => '#'],
        		['label' => $brand->name, 'link' => site_url('brand/'.$brand->uri_path.'.html')],
        	]
        ];

        $list_active_category = $this->brand_model->listActiveCategory($brand->brand_id);
        $catalogs = $this->category_model->listActive(NULL, 1, 100, TRUE);
        $sidebar_categories = [];
        foreach ($list_active_category as $item ){
            $item = $this->category_model->findActive($item->category_id);
        	$root = $this->getParentClosest($item, $catalogs);
            if($root) $sidebar_categories[$root->category_id] = $root;
        }
    	$data_render = [
    		'sidebar_categories' => $sidebar_categories,
    		'brand' => $brand,
            'list_active_category' => $list_active_category,
    		'products' => $products,
            'category_id' => $category_id,
    		'carousel' => $brand->images,
    		'breadcrumbs' => $breadcrumbs,
            'sort_type' => $sort_type,
            'price_range' => $price_range,
            'page_title' => $brand->name,
            'page_description' => strip_tags($brand->long_description),
    	];


        $this->load->view('brand', $data_render);
    }

    private function getParentClosest($current, $list){
        if($current->parent_id == NULL){
            foreach ($list as $item) {
                if($item->category_id == $current->category_id) return $item;
            }
        }else{
            foreach ($list as $item) {
                if( $item->has_child && $this->isExistChild( (int)$current->category_id, $item->childs) ) return $item;
            }
        }
    }

    private function isExistChild($category_id, $list){
        foreach ($list as $item) {
            if( (int)$item->category_id === $category_id ) return TRUE;
            if( $item->has_child && $this->isExistChild($category_id, $item->childs) ) return TRUE;
        }
        return FALSE;
    }

    public function list(){
        $breadcrumbs = [
            'page_title' => __('Brand'),
            'items' => [
                ['label' => __('Home'), 'link' => base_url()],
                ['label' => __('Brand'), 'link' => '#']
            ]
        ];

        $data_render = [
            'breadcrumbs' => $breadcrumbs
        ];

    	$this->load->view('brand_list', $data_render);
    }
}