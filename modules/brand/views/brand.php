<?php $this->load->view('theme/header'); ?>
<?php $this->load->view('theme/page_top'); ?>
<?php $this->load->view('theme/navigation'); ?>
<?php $this->load->view('theme/breadcrumbs'); ?>
<!--products list-->
<div class="products-list-page">
    <div class="container">
        <div class="row">
            <!--content list-products-->
            <div class="col-xs-12">
                <!--about brands-->
                <div class="brands-about">
                    <div class="col-xs-12 col-sm-6 col-md-6">
                        <img src="<?php echo $brand->image_url ?>" class="img-responsive">
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 text-brands">
                        <h4><?php echo $brand->name ?></h4><hr>
                        <div class="readmore" more-text="Show more" less-text="Show less">
                            <?php echo $brand->long_description ?>
                            <p><?php echo $brand->short_description ?></p>
                        </div>
                        <a href="<?php echo $brand->domain ?>" title="<?php echo $brand->domain ?>" target="_blank">
                            <?php echo $brand->domain ?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!--e about brands-->
        <div class="row">
            <?php $this->load->view('sidebar'); ?>
            <div class="col-xs-12 col-sm-9 col-md-9">
                <?php $this->load->view('widget/product_list') ?>
            </div>
        </div>
    </div>
    <!--products list-->
</div>
<?php $this->load->view('theme/footer'); ?>