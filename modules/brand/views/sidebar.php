<?php
$list_active_category_id =[];
foreach($list_active_category as $item){
    $list_active_category_id[] = $item->category_id;
}
function hasChild($category_id_list, $list){
    foreach($list as $item){
        if( in_array($item->category_id, $category_id_list) ) return TRUE;
    }
    return FALSE;
}
?>
<!--menu list-products-->
<div class="col-xs-12 col-sm-3 col-md-3">
    <div class="nav-side-menu">
        <p><a href="<?php echo site_url('brand/'.$brand->uri_path.'.html') ?>"><?php echo __('All') ?></a></p>
        <?php foreach ($sidebar_categories as $category):  ?>
            <div class="brand"><b><?php echo $category->name ?></b></div>
            <i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"></i>
            <div class="menu-list">
                <ul id="menu-content" class="menu-content collapse out">
                    <?php
                        if($category->has_child):
                        foreach ($category->childs as $item):
                            if( !$item->has_child && !in_array($item->category_id, $list_active_category_id) ) continue;
                            if( $item->has_child && !hasChild($list_active_category_id, $item->childs)) continue;
                    ?>
                    <li <?php echo $item->has_child ? 'data-toggle="collapse" data-target="#sidemenu-'.$item->category_id.'"':'' ?> 
                        class="<?php echo $category_id == $item->category_id ? 'active': '' ?>">
                        <a href="<?php echo site_url('brand/'.$brand->uri_path.'.html?category_id='.$item->category_id) ?>">
                            <?php echo $item->name?>
                        </a>
                        <?php echo $item->has_child ? '<i class="fa fa-angle-down" aria-hidden="true"></i>' : '' ?>
                    </li>
                    <?php if( $item->has_child ):
                            $html_item = '';
                            $has_active = FALSE;
                            foreach ($item->childs as $child): 
                                if( !$has_active ){
                                    $has_active = $category_id == $child->category_id;
                                }
                                if( !in_array($child->category_id, $list_active_category_id) ) continue;
                                $html_item .= '
                                <li class="'.($category_id == $child->category_id ? 'active': '').'">
                                    <a href="'.site_url('brand/'.$brand->uri_path.'.html?category_id='.$child->category_id).'" title="'.$child->name.'">'.$child->name.'</a>
                                </li>';
                            endforeach 
                            ?>
                        <ul class="sub-menu collapse <?php echo $has_active ? 'in': '' ?>" id="sidemenu-<?php echo $item->category_id?>">
                            <?php echo $html_item; ?>
                        </ul>
                    <?php endif ?>
                    <?php endforeach ?>
                    <?php endif ?>
                </ul>
            </div>
            <hr/>
        <?php endforeach ?>
    </div>
</div><!--e menu list-products-->
