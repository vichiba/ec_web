<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Brand_model extends MY_Model{
 	 	
 	public $table = 'brands';
	public $primary_key = 'brand_id';

	//0:Delete, 1: inactive, 2: OK
	const STATUS_DELETED = 0;
	const STATUS_DEACTIVE = 1;
	const STATUS_ACTIVE = 2;

	public function findActiveByUri($uri){
		$catalog_id = $this->exists(['uri_path' => $uri]);
		if( $catalog_id && is_numeric($catalog_id) ){
			return $this->findActive($catalog_id);
		}
		return NULL;
	}

	public function findActive($brand_id){
		$brand = $this->find($brand_id);
		if($brand && (int)$brand->status === SELF::STATUS_ACTIVE){
			$this->load->model('image/image_model');
			$images = $this->image_model->listActive($brand_id, Image_model::TYPE_BRAND);
			$brand->images = [];
			foreach ($images as $item) {
				$brand->images[] = $item;
			}
			return $brand;
		}
		return NULL;
	}

	public function listActive($page = 1, $page_size = 20){
		$conds = ['status' => SELF::STATUS_ACTIVE];
		$order_conds = 'position ASC, modified_date DESC, created_date DESC';
		$this->db->limit($page_size, ( (int)$page - 1 ) * (int)$page_size );
		return $this->find_all($conds, $order_conds);
	}

	private function getChildCategoryIds($category_id){
		
		return $this->implodeAllCategoryIds($list);	
	}

	private function implodeAllCategoryIds($list){
		$return = [];
		foreach ($list as $item) {
			$return[] = $item->category_id;
			if($item->has_child) $return = array_merge($return, $this->implodeAllCategoryIds($item->childs));
		}
		return $return;
	}

	public function searchActive($keyword = '', $category_id = NULL, $page = 1, $page_size = 20){
		$brand_list_id = NULL;
		if( $category_id && is_numeric($category_id) ){
			$this->load->model('category/category_model');
			$list = $this->category_model->listActive($category_id, 1, 1000, TRUE);
			$category_id_list = [$category_id];
			$category_id_list = array_merge($category_id_list, $this->implodeAllCategoryIds($list));
			$brand_list_id = [];
			$this->db->select('DISTINCT(brand_id)');
			$this->db->from('products');
			$this->db->where_in('category_id', $category_id_list);
			$brand_list = $this->db->get()->result();
			foreach ($brand_list as $item) {
				$brand_list_id[] = $item->brand_id;
			}
		}

		$conds = ['status' => SELF::STATUS_ACTIVE];
		$order_conds = 'position ASC, modified_date DESC, created_date DESC';

		if($keyword) $this->db->like('name', $keyword);

		if( !is_null($brand_list_id) ){
			if( !empty($brand_list_id) ) $this->db->where_in($this->primary_key, $brand_list_id);
			else $this->db->where($this->primary_key, '0');
		}

		$total = $this->count_all($conds, $order_conds);

		if($keyword) $this->db->like('name', $keyword);
		if( !is_null($brand_list_id) ){
			if( !empty($brand_list_id) ) $this->db->where_in($this->primary_key, $brand_list_id);
			else $this->db->where($this->primary_key, '0');
		}
		$this->db->limit($page_size, ( (int)$page - 1 ) * (int)$page_size );
		$results = $this->find_all($conds, $order_conds);

		$brands = new stdClass();
		$brands->total = $total;
		$brands->page = $page;
		$brands->page_size = $page_size;
		$brands->items = $results;

		return $brands;
	}

	public function doCustomerFollow($customer_id, $brand_id){
		if( is_array($brand_id) ){
			$data = [];
			foreach ($brand_id as $id) {
				$data[] = [
					'customer_id' => $customer_id,
					'brand_id' => $id
				];
			}
			return $this->db->insert_batch('customer_brand', $data); //return affected
		}else{
			$data = [
				'customer_id' => $customer_id,
				'brand_id' => $brand_id
			];
			return $this->db->insert('customer_brand', $data);
		}
		
	}

	public function doCustomerUnfollow($customer_id, $brand_id){
		if( is_array($brand_id) ){
			$this->db->where('customer_id', $customer_id);
			$this->db->where_in('brand_id', $brand_id);
		}else{
			$conds = [
				'customer_id' => $customer_id,
				'brand_id' => $brand_id
			];
			$this->db->where($conds);
		}
		$this->db->delete('customer_brand');
		return TRUE;
	}

	public function listBeFollowed($customer_id, $page = 1, $page_size = 20){
		$list = new stdClass();
		$conds = [
			'customer_id' => $customer_id
		];
		$this->db->from('customer_brand');
		$this->db->where($conds);
		
		$list->total = $this->db->count_all_results();
		$list->page = $page;
		$list->page_size = $page_size;

		$this->db->from('customer_brand');
		$this->db->where($conds);
		$this->db->limit($page_size, ( (int)$page - 1 ) * (int)$page_size );
		$this->db->order_by('created_date', 'DESC');
		
		$list->items = $this->db->get()->result();


		return $list; 
	}

	public function listActiveCategory($brand_id){
		$this->db->select('DISTINCT(category_id)');
		$this->db->from('products');
		$this->db->where('brand_id', $brand_id);

		return $this->db->get()->result();
	}
}
