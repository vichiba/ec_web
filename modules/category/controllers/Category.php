<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends MX_Controller 
{
    function __construct($foo = null)
    {
        $this->load->library('pagination');

        parent::__construct();
    }
    public function index($uri1 = NULL, $uri2 = NULL, $uri3){
        $uri = $uri3;
        if($uri2) $uri = $uri2 . '/' . $uri;
        if($uri1) $uri = $uri1 . '/' . $uri;

    	$catalog = $this->category_model->findActiveByUri($uri);
    	if( !$catalog ){
    		show_404();
    		return;
    	}

    	$keyword            = NULL;
        $page               = $this->input->get('page');
        $page_size          = $this->input->get('page_size');
        $category_id        = $catalog->category_id;
        $brand_id           = NULL;
        $sort_type          = $this->input->get('sort_type');
        $price          = $this->input->get('price_range');
        $price_conds = explode('-', $price);
        $price_min = isset($price_conds[0]) ? $price_conds[0] : 0;
        $price_max = isset($price_conds[1]) ? $price_conds[1] : 0;
        //$price_min          = $this->input->get('price_min');
        //$price_max          = $this->input->get('price_max');

        if( !$page || !is_numeric($page) || (int)$page < 1 ) $page = 1;
        if( !$page_size || !is_numeric($page_size) || (int)$page_size < 1 || (int)$page_size > 28 ) $page_size = 28;

        $price_range = [];
        if( $price_min && is_numeric($price_min) && (int)$price_min > 0 ){
            $price_range['min'] = $price_min;
        }else{
            $price_min = 0;
        }

        if( $price_max && is_numeric($price_max) && (int)$price_max > (int)$price_min ){
            $price_range['max'] = $price_max;
        }

        $products = $this->product_model->searchActive($keyword, $category_id, $brand_id, $price_range, $sort_type, $page, $page_size);
        
        $breadcrumb_items = [['label' => __('Home'), 'link' => base_url()]];
        $page_title = [];
        if($uri1){
            $catalog_tmp = $this->category_model->findActiveByUri($uri1);
            $breadcrumb_items[] = ['label' => $catalog_tmp->name, 'link' => site_url($uri1.'.html')];
            $page_title[] = $catalog_tmp->name;
        }
        if($uri2){
            $uri_tmp = $uri2;
            if($uri1) $uri_tmp = $uri1 . '/' . $uri_tmp;
            $catalog_tmp = $this->category_model->findActiveByUri($uri_tmp);
            $breadcrumb_items[] = ['label' => $catalog_tmp->name, 'link' => site_url($uri_tmp.'.html')];
            $page_title[] = $catalog_tmp->name;
        }
        $breadcrumb_items[] = ['label' => $catalog->name, 'link' => site_url($uri.'.html')];
        $page_title[] = $catalog->name;
        $breadcrumbs = [
        	'page_title' => $catalog->name,
        	'items' => $breadcrumb_items,
        ];

        $catalog->uri_path = $uri;
        $childs = $this->category_model->listActiveWithURI($catalog->category_id, $uri, 1, 100, TRUE);
        $catalogs = $this->category_model->listActiveWithURI(0, '', 1, 1000, TRUE);
        $catalog->has_child = count($childs) > 0;
        $catalog->childs = $childs;

        $root_catalogs = $catalog->parent_id == 0 ? $catalog : $this->hasChild($catalog->category_id, $catalogs);
        #$root_catalogs = count($childs) !== 0 ? $catalog : $this->hasChild($catalog->parent_id, $catalogs);
    	$data_render = [
            'catalogs' => $catalogs,
    		'catalog' => $catalog,
            'root_catalogs' => $root_catalogs,
    		'products' => $products,
    		'carousel' => $catalog->images,
    		'breadcrumbs' => $breadcrumbs,
            'sort_type' => $sort_type,
            'price_range' => $price_range,
            'page_title' =>implode(' » ', $page_title),
            'page_keywords' => $catalog->search_keyword,
            'page_description' => strip_tags($catalog->short_description),
    	];


        $this->load->view('category', $data_render);
    }

    private function hasChild($item_id, $list){
        foreach ($list as $item) {
            if( (int)$item->category_id === (int)$item_id ) return $item;
            
            if( $item->has_child && $this->hasChild($item_id, $item->childs)){
                return $item;
            }
        }
        return FALSE;
    }
}