<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Category_model extends MY_Model{
 	 	
 	public $table = 'catalogs';
	public $primary_key = 'category_id';

	public $second_table = 'product_catalogs';
	public $mapping_table = 'catalog_mapping';

	//0:Delete, 1: inactive, 2: OK
	const STATUS_INACTIVE = 1;
	const STATUS_ACTIVE = 2;

	public function findActiveByUri($uri){
		$catalogs = $this->listActiveWithURI(0, '', 1, 1000, TRUE);
		
		$uri = '/'.$uri;
		$catalog_id = $this->loopCategortByURI($uri, $catalogs);
		if( $catalog_id > 0){
			return $this->findActive($catalog_id);
		}
		return NULL;
	}

	private function loopCategortByURI($value, $list){
        foreach ($list as $item) {
        	if( $item->uri_path === $value ) return $item->category_id;
            if( $item->has_child === TRUE ){
            	$category_id = $this->loopCategortByURI($value, $item->childs);
            	if( $category_id && is_numeric($category_id) ) return $category_id;
            }
        }
    }

	public function listActiveWithURI($parent_id = NULL, $parent_uri = '', $page = 1, $page_size = 20, $is_recrusive = FALSE){
		$conds = ['status' => SELF::STATUS_ACTIVE];
		
		if( !is_null($parent_id) ){
			$conds['parent_id'] = $parent_id;
		}

		$order_conds = 'position ASC, modified_date DESC, created_date DESC';
		$this->db->limit($page_size, ( (int)$page - 1 ) * (int)$page_size );
		$response = $this->find_all($conds, $order_conds);
		if( $is_recrusive === TRUE ){
			foreach ($response as $k => $item) {
				$item->uri_path = $parent_uri.'/'.$item->uri_path;
				$childs = $this->listActiveWithURI($item->category_id, $item->uri_path, 1, 100, $is_recrusive);
				$response[$k]->has_child = count($childs) > 0;
				if( $response[$k]->has_child === TRUE ){
					$response[$k]->childs = $childs;
				}
			}
		}
		return $response;
	}

	public function findActive($catalog_id){
		$catalog = $this->find($catalog_id);
		if($catalog && (int)$catalog->status === SELF::STATUS_ACTIVE){
			$this->load->model('image/image_model');
			$images = $this->image_model->listActive($catalog_id, Image_model::TYPE_CATEGORY);
			$catalog->images = [];
			foreach ($images as $item) {
				$item->label = $item->label == NULL ? '': $item->label;
				$item->description = $item->description == NULL ? '': $item->description;
				$catalog->images[] = $item;
			}
			return $catalog;
		}
		return NULL;
	}

	public function listActive($parent_id = NULL, $page = 1, $page_size = 20, $is_recrusive = FALSE){
		$conds = ['status' => SELF::STATUS_ACTIVE];
		
		if( !is_null($parent_id) ){
			$conds['parent_id'] = $parent_id;
		}

		$order_conds = 'position ASC, modified_date DESC, created_date DESC';
		$this->db->limit($page_size, ( (int)$page - 1 ) * (int)$page_size );
		$response = $this->find_all($conds, $order_conds);
		if( $is_recrusive === TRUE ){
			foreach ($response as $k => $item) {
				$childs = $this->listActive($item->category_id, 1, 100, $is_recrusive);
				$response[$k]->has_child = count($childs) > 0;
				if( $response[$k]->has_child === TRUE ) $response[$k]->childs = $childs;
			}
		}
		return $response;
	}

	public function getHomeActive(){
		$conds = ['status' => SELF::STATUS_ACTIVE, 'display_in_home' => STATUS_ACTIVE];
		$order_conds = 'position ASC, modified_date DESC, created_date DESC';
		$this->db->limit($page_size, ( (int)$page - 1 ) * (int)$page_size );
		return $this->find_all($conds, $order_conds);
	}

	public function findCategoryByMapping($category_mapping_name, $vendor = 'CJ'){
		$this->db->where("vendor", $vendor);
		$this->db->where("category_mapping_name", $category_mapping_name);
		$this->db->select('category_id');
		$this->db->from($this->mapping_table);

		return $this->db->get()->result();
	}

	public function hasCategoryByMapping($category_mapping_name, $vendor = 'CJ'){
		$list = $this->findCategoryByMapping($category_mapping_name, $vendor);
		foreach ($list as $item) {
			if( !is_null($item->category_id) ) return TRUE;
		}
		return FALSE;
	}

	public function removeProductRelative($product_id){
		$this->db->where('product_id', $product_id);
		return $this->db->delete($this->second_table);
	}

	public function addProductRelative($category_id, $product_id){
		$this->db->where('category_id', $category_id);
		$this->db->where('product_id', $product_id);
		$this->db->from($this->second_table);
		if( $this->db->count_all_results() <= 0 ){
			$this->db->insert($this->second_table, ['category_id' => $category_id, 'product_id' => $product_id]);
			return $this->db->insert_id();
		}
		return NULL;
	}

	public function savePostion($category_id, $position){
		return $this->update($category_id, ["position" => $position]);
	}
}