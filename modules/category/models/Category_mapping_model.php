<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Category_mapping_model extends MY_Model{
 	public $table = 'catalog_mapping';

 	public function delete_all(){
 		$this->db->truncate($this->table);
 	}
}