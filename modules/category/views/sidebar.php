<!--sidebar-->
<div class="col-xs-12 col-sm-3 col-md-3">
    <div class="nav-side-menu">
        <div class="brand"><b><?php echo $root_catalogs->name ?></b></div>
        <i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"></i>
        <div class="menu-list">
            <ul id="menu-content" class="menu-content collapse out">
                <?php if($root_catalogs->has_child):
                    foreach ($root_catalogs->childs as $item): 
                $uri = $item->uri_path.'.html';
                ?>
                <li <?php echo $item->has_child ? 'data-toggle="collapse" data-target="#sidemenu-'.$item->category_id.'"':'' ?> 
                    class="<?php echo $catalog->category_id == $item->category_id ? 'active': '' ?>">
                    <a href="<?php echo site_url($uri) ?>"><?php echo $item->name ?></a>
                    <?php echo $item->has_child ? '<i class="fa fa-angle-down" aria-hidden="true"></i>' : '' ?>
                </li>
                <?php if ($item->has_child):
                        $html_item = '';
                        $has_active = FALSE;
                        foreach ($item->childs as $child): 
                            $uri = $child->uri_path.'.html';
                            if( !$has_active ){
                                $has_active = $catalog->category_id == $child->category_id;
                            }
                            $html_item .= '
                            <li class="'.($catalog->category_id == $child->category_id ? 'active': '').'">
                                <a href="'.site_url($uri).'" title="'.$child->name.'">'.$child->name.'</a>
                            </li>';
                        endforeach 
                        ?>
                    <ul class="sub-menu collapse <?php echo $has_active ? 'in': '' ?>" id="sidemenu-<?php echo $item->category_id?>">
                        <?php echo $html_item; ?>
                    </ul>
                <?php endif ?>
                <?php endforeach ?>
                <?php endif ?>
            </ul>
        </div>
    </div>
</div>
<script type="text/javascript">
$(function(){
    $('.nav-side-menu .menu-content').find('ul li.active').closest('ul').prev().addClass('active');
});
</script>  
<!--end of sidebar-->