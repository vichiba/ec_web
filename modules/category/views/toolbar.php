<!--Toolbar-->
<div class="box-select toolbar">
    <div class="box-select-items">
        <label><?php echo __('Price') ?>:</label>
        <select class="selectpicker" name="price_range">
            <option value=""><?php echo __('All') ?></option>
            <?php 
            $price_range_list = json_decode(get_config_value('PRICE_SEARCH_RANGE'));
            foreach ($price_range_list as $item): 
            $price_range_value = $item->min_price . ($item->max_price?'-'.$item->max_price:'');
            ?>
            <option value="<?php echo $price_range_value ?>" <?php echo $this->input->get('price_range') == $price_range_value ? 'selected' : '' ?>>
                <span><?php echo $item->display ?></span>
            </option>
            <?php endforeach ?>
        </select>
    </div>

    <div class="box-select-items">
        <label><?php echo __('Sort') ?> :</label>
        <select class="selectpicker" name="sort_type">
            <option value=""><?php echo __('All') ?></option>
            <option value="1" <?php echo $this->input->get('sort_type') == '1' ? 'selected' : '' ?>><span><?php echo __('Newest') ?></span> </option>
            <option value="2" <?php echo $this->input->get('sort_type') == '2' ? 'selected' : '' ?>><span><?php echo __('Polular') ?></span> </option>
            <option value="3" <?php echo $this->input->get('sort_type') == '3' ? 'selected' : '' ?>><span><?php echo __('On Sale') ?></span> </option>
        </select>
    </div>
</div>
<!--End of Toolbar-->