<?php $this->load->view('theme/header'); ?>
<?php $this->load->view('theme/page_top'); ?>
<?php $this->load->view('theme/navigation'); ?>
<?php //$this->load->view('theme/slider'); ?>
<?php $this->load->view('theme/breadcrumbs'); ?>

<!--products list-->
<div class="products-list-page">
    <div class="container">
        <div class="row">


            <?php $this->load->view('sidebar') ?>

            <!--content list-products-->
            <div class="col-xs-12 col-sm-9 col-md-9">
                <!--top seleact-->
                <?php $this->load->view('toolbar') ?>

                <!--e top select-->
                <!--content products-->
                <div class="content-products-list">
                    <?php foreach ($products->items as $k => $item):
                    $item_link = site_url($item->product_id.'.html');;
                    ?>
                    <!--items-->
                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <div class="item">
                            <li class="img-products">
                                <div class="img-wrapper">
                                    <img src="<?php echo $item->image_url?>" alt="<?php echo $item->name?>" class="img-responsive" />
                                    <div class="caption-img">
                                        <a href="<?php echo trackurl('Category detail page', 'PRODUCT', $item->product_id, uri_string().'?'.$this->input->server('QUERY_STRING'), $item->buy_url) ?>"><i class="fa fa-cart-plus" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                                <div style="padding:10px 0px;">
                                    <a href="<?php echo $item_link ?>" class="product_title"><?php echo $item->name ?></a>
                                    <hr>
                                    <strong style=" color:#5bc0de;"><?php echo display_price($item)?></strong>

                                    <a href="<?php echo site_url('customer/product_like?product_id='.$item->product_id)?>" 
                                        class="text-muted pull-right ajax_get <?php echo isset($item->is_like) && (int)$item->is_like > 0 ? 'active' : ''?>">
                                        <i class="fa fa-heart<?php echo (int)$item->is_like > 0 ? '' : '-o'?>" aria-hidden="true"></i>
                                        <small class="text-muted"><?php echo (int)$item->favourite_count > 0 ? number_format($item->favourite_count) : ''?></small>
                                    </a>


                                </li>
                            </div>
                        </div><!--e items-->
                    <?php endforeach ?>

                </div> <!--e content products-->


                <div class="pagilation-box">
                    <nav>
                        <?php 
                        $config['base_url'] = site_url(uri_string());
                        $config['total_rows'] = $products->total;
                        $config['per_page'] = $products->page_size;
                        $config['use_page_numbers'] = TRUE;
                        $config['page_query_string'] = TRUE;
                        $config['enable_query_strings'] = TRUE;
                        $config['reuse_query_string'] = TRUE;
                        $config['query_string_segment'] = 'page';
                        $config['full_tag_open'] = '<ul class="pagination">';
                        $config['full_tag_close'] = '</ul>';
                        $config['first_tag_open'] = '<li class="hide">';
                        $config['first_tag_close'] = '</li>';
                        $config['last_tag_open'] = '<li class="hide">';
                        $config['last_tag_close'] = '</li>';
                        $config['next_link'] = '&raquo;';
                        $config['next_tag_open'] = '<li><span aria-hidden="true">';
                        $config['next_tag_close'] = '</span></li>';
                        $config['prev_link'] = '&laquo;';
                        $config['prev_tag_open'] = '<li><span aria-hidden="true">';
                        $config['prev_tag_close'] = '</span></li>';
                        $config['num_tag_open'] = '<li>';
                        $config['num_tag_close'] = '</li>';
                        $config['cur_tag_open'] = '<li class="active"><a href="#">';
                        $config['cur_tag_close'] = '</a></li>';

                        $this->pagination->initialize($config);

                        echo $this->pagination->create_links();
                        ?>
                    </nav>
                </div>

            </div><!--e content list-products-->

        </div>
    </div>
</div>
<!--products list-->
<?php $this->load->view('theme/footer'); ?>