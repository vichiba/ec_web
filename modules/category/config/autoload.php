<?php defined('BASEPATH') OR exit('No direct script access allowed');

$autoload['helper'] = array('language');
$autoload['language'] = array();
$autoload['model'] = array('admin/config_model', 'category/category_model', 'product/product_model', 'image/image_model', 'currency/currency_model');