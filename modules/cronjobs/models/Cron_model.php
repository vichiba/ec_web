<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cron_model extends MY_Model{
	const STATUS_WAIT = '0';
	const STATUS_RUNNING = '1';
	const STATUS_DONE = '2';
 	 	
 	public $table = 'crontab_jobs';
	public $primary_key = 'log_id';

}
