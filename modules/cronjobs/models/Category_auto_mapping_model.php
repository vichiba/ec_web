<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Category_auto_mapping_model extends MY_Model{
 	 	
 	public $table = 'category_auto_mapping';
	public $primary_key = 'mapping_id';

	public function getMappedCategories($keyword){
		$this->db->select('category_id');
		$this->db->from($this->table);
		$this->db->where("status", self::STATUS_ACTIVE);

		$this->db->where("( keyword1 is null OR (keyword1 is not null AND ".$this->db->escape($keyword)." REGEXP concat('^(.*', keyword1, ')')) )", NULL, FALSE);
		$this->db->where("( keyword2 is null OR (keyword2 is not null AND ".$this->db->escape($keyword)." REGEXP concat('^(.*', keyword2, ')')) )", NULL, FALSE);
		$this->db->where("( keyword3 is null OR (keyword3 is not null AND ".$this->db->escape($keyword)." REGEXP concat('^(.*', keyword3, ')')) )", NULL, FALSE);
		$this->db->where("( keyword4 is null OR (keyword4 is not null AND ".$this->db->escape($keyword)." REGEXP concat('^(.*', keyword4, ')')) )", NULL, FALSE);
		$this->db->where("( keyword5 is null OR (keyword5 is not null AND ".$this->db->escape($keyword)." REGEXP concat('^(.*', keyword5, ')')) )", NULL, FALSE);
		$this->db->where("( keyword6 is null OR (keyword6 is not null AND ".$this->db->escape($keyword)." REGEXP concat('^(.*', keyword6, ')')) )", NULL, FALSE);

		$results = $this->db->get()->result();
		/*$values = [];
		foreach ($results as $item) {
			$values[] = $item->category_id;
		}

		return $values;*/
		return $results;
	}
}
