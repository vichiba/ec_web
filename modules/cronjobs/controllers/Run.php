<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Run CLI command
 * visudo %apache ALL=(apache)NOPASSWD:/usr/bin/crontab
 * Example: php index.php cronjobs run index "CJ" "12345"
 * Example: php index.php cronjobs run index "RAKUTEN" "12345"
 */

class Run extends MX_Controller{
	public function __construct(){
		parent::__construct();
		$this->load->model('admin/config_model');
		$this->load->model('brand/brand_model');
		$this->load->model('product/product_model');
		$this->load->model('category/category_model');
		$this->load->model('category_auto_mapping_model', 'auto_mapping');
	}

	public function index($vendor, $brand_id){
		if(!$this->input->is_cli_request()){
         	//echo "greet my only be accessed from the command line";
         	return;
     	}
     	echo "Vendor: $vendor - Brand: $brand_id ".PHP_EOL;
     	switch ($vendor) {
     		case 'CJ':
     			$this->importFromCJ($brand_id);
     			break;

     		case 'RAKUTEN':
     			$this->importFromRakuten($brand_id);
     			break;
     		default:
     			echo 'UnSupport';
     			break;
     	}
	}

	public function brands($vendor){
		$brand_list = $this->brand_model->find_all(['vendor' => $vendor, 'status' => Brand_model::STATUS_ACTIVE]);
		$brand_id_list = [];
		foreach($brand_list as $brand){
			$brand_id_list[] = $brand->ref_id;
		}
		echo implode(',', $brand_id_list);
		exit;
	}

	private function importFromRakuten($brand_id){
		$this->load->library('admin/RakutenConnector', NULL, '_rakuten');
		
		$page_number = 1;
		$records_per_page = 100;
		$max_page_number = 100;
		$return_product = 0;
		$total_matched = 0;
		$total_page = 0;

		$count_insert 	= 0;
     	$count_update 	= 0;
     	$count_nothing 	= 0;

		$this->db->trans_start();
		do{
			$products = $this->_rakuten->searchProduct('', $brand_id, '', $records_per_page, $page_number);
			$return_product = count($products->item);
			$total_matched = (float)$products->TotalMatches;
			$total_page = (float)$products->TotalPages;
			//echo "Page: ".$page_number."/".$products->TotalPages."\n";
			if($page_number < 2) echo "Total products: ".number_format($total_matched). " ( ".number_format($total_page > 20 ? 20*$records_per_page : $total_matched)." readable)".PHP_EOL;
			if($products->TotalPages > 0) $this->show_status($page_number*$records_per_page, $total_page > 20 ? 20*$records_per_page : $total_page*$records_per_page);
			$save_cats =[];
			foreach($products->item as $item){
				$category_name = (string)$item->category->primary;
				$second_cat = (string)$item->category->secondary;
				if($second_cat){
					$category_name .= '>>'.$second_cat;
				}
				//$log = $item->mid . '|' . $item->merchantname . '|' . $category_name;
				//$save_cats[$log] = $page_number . '|' . $log;
				$action = $this->forceRAKUTENUpdateProduct($item, $category_name);
				switch ($action) {
					case 'INSERT':
						$count_insert += 1;
						break;
					case 'UPDATE':
						$count_update += 1;
						break;
					default:
						$count_nothing += 1;
						break;
				}
			}
			//echo "Save Product: ".$return_product."\n";
			//file_put_contents(APPPATH.'/../logs/catalog_rakuten_'.date("j.n.Y").'.txt', implode(PHP_EOL, $save_cats).PHP_EOL, FILE_APPEND);
			$page_number++;
		}while($return_product > 0 && $page_number < $max_page_number);
		$this->db->trans_complete();

		if( $this->db->trans_status() === FALSE ){
		    echo PHP_EOL."Database ERROR!!".PHP_EOL;
		}else{
			echo PHP_EOL."Transaction SUCCESS.".PHP_EOL;
		}
		/*echo "TOTAL RECORDS: ".number_format($record_returned).PHP_EOL;
		echo "   INSERT : ".$count_insert.PHP_EOL;
		echo "   UPDATE : ".$count_update.PHP_EOL;
		echo "   NOTHING: ".$count_nothing.PHP_EOL;*/
		echo "================================================".PHP_EOL;
	}

	private function importFromCJ($brand_id){
		$record_returned 	= 0;
		$total_return 		= 0;
     	$total_matched 		= 0;
     	$page_number 		= 1;
     	$records_per_page 	= 500;

     	$count_insert 	= 0;
     	$count_update 	= 0;
     	$count_nothing 	= 0;

     	$brand 			= $this->brand_model->exists(['vendor' => 'CJ', 'ref_id' => $brand_id]);
     	if(!$brand){
     		echo PHP_EOL."NO MAPPING FOR $brand_id.".PHP_EOL;
     		return;
     	}

     	$this->db->trans_start();
     	do{
	     	//Start command 
	     	//echo $page_number;
	     	$products = $this->getProductCJ($page_number++, $records_per_page, $brand_id);
	     	$total_matched = (float)$products[0]['total-matched'];
	     	$record_returned = (float)$products[0]['records-returned'];
	     	$total_return += $record_returned;
	     	if($page_number <= 2) echo "Total products: ".number_format($total_matched).($total_matched > 10000 ? " (10.000 readable)": "").PHP_EOL;
	     	//echo  '('.number_format($total_return).'/'.number_format($total_matched).')'."\n";
	     	if($total_matched > 0) $this->show_status($total_return, $total_matched > 10000 ? 10000 : $total_matched);
	     	if( (int)$total_matched > 0 ){
			    foreach ($products->product as $item) {
					$action = $this->forceCJUpdateProduct($item);
					switch ($action) {
						case 'INSERT':
							$count_insert += 1;
							break;
						case 'UPDATE':
							$count_update += 1;
							break;
						default:
							$count_nothing += 1;
							break;
					}
				}
		    }
		}while( $record_returned > 0);
		$this->db->trans_complete();

		if( $this->db->trans_status() === FALSE ){
		    echo PHP_EOL."Database ERROR!!".PHP_EOL;
		}else{
			echo PHP_EOL."Transaction SUCCESS.".PHP_EOL;
		}
		/*echo "TOTAL RECORDS: ".number_format($record_returned).PHP_EOL;
		echo "   INSERT : ".$count_insert.PHP_EOL;
		echo "   UPDATE : ".$count_update.PHP_EOL;
		echo "   NOTHING: ".$count_nothing.PHP_EOL;*/
		echo "================================================".PHP_EOL;
	}

	private function getProductCJ($page_number, $records_per_page, $advertiser_ids){
		$this->load->library('admin/CJConnector', NULL, '_cj');

		$page_number = $page_number ? $page_number : '1';
		$records_per_page = $records_per_page ? $records_per_page : '1000';
		$advertiser_ids = $advertiser_ids ? $advertiser_ids : 'joined';

		$keywords = '';
		$isbn = '';
		$upc = '';
		$advertiser_sku = '';
		$low_price = '';
		$high_price = '';
		$low_sale_price = '';
		$high_sale_price = '';
		$currency = '';
		$sort_by = '';
		$sort_order = '';


		$products = $this->_cj->productSearch($advertiser_ids, $keywords, $isbn, $upc, $advertiser_sku, 
												$low_price, $high_price, $low_sale_price, $high_sale_price, 
												$currency, $sort_by, $sort_order, $page_number, $records_per_page);

		return $products;
	}

	/**
	 * Mapping lai toan bo san pham cho CJ
	 * php index.php cronjobs run recatalog_cj
	 */
	public function recatalog_cj(){
		debug_mode();
		echo "Mapping Product for CJ Vendor.".PHP_EOL;
		$list = [];
		$page = 1;
		$page_size = 1000;
		$success_mapping = 0;
		$total_mapping = 0;
		do{
			$this->db->limit($page_size, ( (int)$page - 1 ) * (int)$page_size );
			$this->db->from('products');
			$list = $this->db->get()->result();

			$total_mapping += count($list);
			echo $total_mapping.PHP_EOL;
			$this->db->trans_start();
			foreach ($list as $item) {
				$raw_data = $item->raw_data;
				if($raw_data){
					$json = json_decode($raw_data);
					if( is_object($json->{"advertiser-category"}) ){
						continue;
					}
					$category_id_list 	= $this->category_model->findCategoryByMapping($json->{"advertiser-category"}, 'CJ');
					if( !empty($category_id_list) ){
						$this->category_model->removeProductRelative($item->product_id);
						$is_item_mapping = FALSE;
						foreach ($category_id_list as $cat_item) {
							if( !is_null($cat_item->category_id) ){
								$is_item_mapping = TRUE;
								$this->category_model->addProductRelative($cat_item->category_id, $item->product_id);
							}
						}
						if($is_item_mapping) $success_mapping++;
					}
					$json = NULL;
					$category_id_list = NULL;
				}
			}
			$this->db->trans_complete();
			if( $this->db->trans_status() === FALSE ){
				echo "\nERROR PAGE $page!!\n";
			}
			$page++;
		}while(count($list) > 0);

		//if( $this->db->trans_status() === FALSE ){
		//    echo "\nDatabase ERROR!!\n";
		//}else{
			echo "Transaction SUCCESS.".PHP_EOL;
			echo "Mapping: ".number_format($success_mapping)."/".number_format($total_mapping)." (".ceil($success_mapping*100/$total_mapping)."%)".PHP_EOL;
		//}
	}

	/**
	 * Update/Insert san pham cho CJ
	 * @param  [type] $data [description]
	 * @return [type]       [description]
	 */
	private function forceCJUpdateProduct($data){
		if( empty($data) ) return;

		$product_sku 	= $data->{"sku"}."-".$data->{"advertiser-id"}."-".$data->{"ad-id"};
		$product_id 	= $this->product_model->exists(['sku' => $product_sku, 'vendor' => 'CJ']);

		$category_id_list 	= $this->category_model->findCategoryByMapping($data->{"advertiser-category"}, 'CJ');
		if( empty($category_id_list) ){
			$category_id_list 	= $this->auto_mapping->getMappedCategories($data->{"advertiser-category"});
		}

		$brand_id 		= $this->brand_model->exists(['vendor' => 'CJ', 'ref_id' => $data->{"advertiser-id"}]);
		$raw_data 		= json_encode($data);
		
		#$brand_id = 1;
		#$category_id = 1;

		if( $brand_id && is_numeric($brand_id) 
			&& !empty($category_id_list) 
			){
			$action = '';
			$category_id_str = [];
			foreach ($category_id_list as $cat_item) {
				$category_id_str[] = $cat_item->category_id;
			}
			$category_id_str = implode(",", $category_id_str);
			
			if( !$category_id_str ){ return "NOTHING"; }

			if( $product_sku && $product_id && is_numeric($product_id) ){
				$data_update = [
					'sku' => $product_sku,
					'name' => (string)$data->{"name"},
					'long_description' => (string)$data->{"description"},
					'raw_data' => $raw_data,
					'image_url' => (string)$data->{"image-url"},
					'buy_url' => (string)$data->{"buy-url"},
					'price' => (string)$data->{"price"},
					'sale_price' => (string)$data->{"sale-price"},
					'retail_price' => (string)$data->{"retail-price"},
					'category_id' => $category_id_str,
					'currency' => trim($data->{"currency"}),
					'brand_id' => $brand_id,
					'click_count' => 0,
					'buy_count' => 0,
					'vendor' => 'CJ',
					'status' => Product_model::STATUS_ACTIVE,
					'modified_user_id' => 1
				];
				$this->product_model->update($product_id, $data_update);
				$action = "UPDATE";
			}else{
				$data_insert = [
					'sku' => $product_sku,
					'name' => (string)$data->{"name"},
					'short_description' => '',
					'long_description' => (string)$data->{"description"},
					'raw_data' => $raw_data,
					'image_url' => (string)$data->{"image-url"},
					'buy_url' => (string)$data->{"buy-url"},
					'price' => (string)$data->{"price"},
					'sale_price' => (string)$data->{"sale-price"},
					'retail_price' => (string)$data->{"retail-price"},
					'category_id' => $category_id_str,
					'currency' => trim($data->{"currency"}),
					'brand_id' => $brand_id,
					'vendor' => 'CJ',
					'created_user_id' => 1,
					'modified_date' => date('Y-m-d H:m:s')
				];
				$product_id = $this->product_model->insert($data_insert);
				$action = "INSERT";
			}

			//Add product and category relative
			$this->category_model->removeProductRelative($product_id);
			foreach ($category_id_list as $cat_item) {
				if( !is_null($cat_item->category_id) ){
					$this->category_model->addProductRelative($cat_item->category_id, $product_id);
				}
			}
			return $action;
		}

		//return $product_id;
		return "NOTHING";
	}

	/**
	 * Update/Insert san pham cho RAKUTEN
	 * @param  [type] $data [description]
	 * @return [type]       [description]
	 */
	private function forceRAKUTENUpdateProduct($data, $category_mapping_name){
		if( empty($data) ) return;
		$product_sku 	= $data->mid."-".$data->sku;
		$product_id 	= $this->product_model->exists(['sku' => $product_sku, 'vendor' => 'RAKUTEN']);
		
		$category_id_list 	= $this->category_model->findCategoryByMapping($category_mapping_name, 'RAKUTEN');
		if( empty($category_id_list) ){
			$category_id_list 	= $this->auto_mapping->getMappedCategories($category_mapping_name);
		}

		$brand_id 		= $this->brand_model->exists(['vendor' => 'RAKUTEN', 'ref_id' => $data->mid]);
		$raw_data 		= json_encode($data);
		
		#$brand_id = 1;
		#$category_id = 1;

		if( $brand_id && is_numeric($brand_id) 
			&& !empty($category_id_list) 
			){
			$action = '';
			$category_id_str = [];
			foreach ($category_id_list as $cat_item) {
				$category_id_str[] = $cat_item->category_id;
			}
			$category_id_str = implode(",", $category_id_str);
			
			if( !$category_id_str ){ return "NOTHING"; }

			if( $product_sku && $product_id && is_numeric($product_id) ){
				$data_update = [
					'sku' => $product_sku,
					'name' => (string)$data->productname,
					'short_description' => (string)$data->description->short,
					'long_description' => (string)$data->description->long,
					'raw_data' => $raw_data,
					'image_url' => (string)$data->imageurl,
					'buy_url' => (string)$data->linkurl,
					'price' => (string)$data->price,
					'sale_price' => (string)$data->saleprice,
					'retail_price' => '',
					'category_id' => $category_id_str,
					'currency' => trim((string)$data->saleprice['currency']),
					'brand_id' => $brand_id,
					'click_count' => 0,
					'buy_count' => 0,
					'vendor' => 'RAKUTEN',
					'status' => Product_model::STATUS_ACTIVE,
					'modified_user_id' => 1
				];
				$this->product_model->update($product_id, $data_update);
				$action = "UPDATE";
			}else{
				$data_insert = [
					'sku' => $product_sku,
					'name' => (string)$data->productname,
					'short_description' => (string)$data->description->short,
					'long_description' => (string)$data->description->long,
					'raw_data' => $raw_data,
					'image_url' => (string)$data->imageurl,
					'buy_url' => (string)$data->linkurl,
					'price' => (string)$data->price,
					'sale_price' => (string)$data->saleprice,
					'retail_price' => '',
					'category_id' => $category_id_str,
					'currency' => trim((string)$data->saleprice['currency']),
					'brand_id' => $brand_id,
					'vendor' => 'RAKUTEN',
					'created_user_id' => 1,
					'modified_date' => date('Y-m-d H:m:s')
				];
				$product_id = $this->product_model->insert($data_insert);
				$action = "INSERT";
			}

			//Add product and category relative
			$this->category_model->removeProductRelative($product_id);
			foreach ($category_id_list as $cat_item) {
				if( !is_null($cat_item->category_id) ){
					$this->category_model->addProductRelative($cat_item->category_id, $product_id);
				}
			}
			return $action;
		}

		//return $product_id;
		return "NOTHING";
	}

	/**
	 * RUN all RAKUTEN Brand importer
	 * @param  [type] $item [description]
	 * @return [type]       [description]
	 */
	public function rakuten($item){
		$this->db->from('brands');
		$this->db->select('name, event_name, event_value');
		$this->db->where('vendor', 'RAKUTEN');
		$this->db->where('ref_id', $item);
		$brand = $this->db->get()->row();
		if($brand){
			echo "BRAND: $brand->name".PHP_EOL;
			echo "Event: $brand->event_name $brand->event_value".PHP_EOL;
			

			$this->db->from('catalog_mapping');
			$this->db->where('brand', $brand->name);
			$this->db->where('category_id is not null');
			if( $this->db->count_all_results() <= 0 ){
	     		echo PHP_EOL."!!! NO CATALOGS MAPPING FOR $brand->name.".PHP_EOL;
	     		return;
	     	}

			$this->importFromRakuten($item);
		}else{
			echo "BRAND does not exists.".PHP_EOL;
		}
	}

	/**
	 * RUN all CJ Brand importer
	 * @return [type] [description]
	 */
	public function cj($item){
		//foreach($brand_list as $item){
			$this->db->from('brands');
			$this->db->select('name, event_name, event_value');
			$this->db->where('vendor', 'CJ');
			$this->db->where('ref_id', $item);
			$brand = $this->db->get()->row();
			if($brand){
				echo "BRAND: $brand->name".PHP_EOL;
				echo "Event: $brand->event_name $brand->event_value".PHP_EOL;


				$this->db->from('catalog_mapping');
				$this->db->where('brand', $brand->name);
				$this->db->where('category_id is not null');
				if( $this->db->count_all_results() <= 0 ){
		     		echo PHP_EOL."!!! NO CATALOGS MAPPING FOR $brand->name.".PHP_EOL;
		     		return;
		     	}

				$this->importFromCJ($item);
			}
		//	usleep(2000000);
		//}
	}

	public function reset(){
		$this->db->truncate('tracks');
		$this->db->truncate('customer_product');
		$this->db->truncate('product_catalogs');
		$this->db->truncate('products');
	}

	/**
	 * show a status bar in the console
	 * 
	 * <code>
	 * for($x=1;$x<=100;$x++){
	 * 
	 *     show_status($x, 100);
	 * 
	 *     usleep(100000);
	 *                           
	 * }
	 * </code>
	 *
	 * @param   int     $done   how many items are completed
	 * @param   int     $total  how many items are to be done total
	 * @param   int     $size   optional size of the status bar
	 * @return  void
	 *
	 */
	private function show_status($done, $total, $size=30) {
	 
	    static $start_time;
	 
	    // if we go over our bound, just ignore it
	    if($done > $total) return;
	 
	    if(empty($start_time)) $start_time=time();
	    $now = time();
	 
	    $perc=(double)($done/$total);
	 
	    $bar=floor($perc*$size);
	 
	    $status_bar="[";
	    $status_bar.=str_repeat("=", $bar);
	    if($bar<$size){
	        $status_bar.=">";
	        $status_bar.=str_repeat(" ", $size-$bar);
	    } else {
	        $status_bar.="=";
	    }
	 
	    $disp=number_format($perc*100, 0);
	 
	    $status_bar.="] $disp%  ".number_format($done)."/".number_format($total);
	 
	    $rate = ($now-$start_time)/$done;
	    $left = $total - $done;
	    $eta = round($rate * $left, 2);
	 
	    $elapsed = $now - $start_time;
	 
	    $status_bar.= " remaining: ".number_format($eta)." sec.  elapsed: ".number_format($elapsed)." sec.";
	 
	 	echo "\033[".(strlen($status_bar)+2)."D";      // Move 5 characters backward
	    echo "$status_bar  ";
	 
	    flush();
	 
	    // when done, send a newline
	    if($done == $total) {
	        echo "\n";
	    }
	 
	}

	public function test(){
		$data = "CASA>>COMPLEMENTI D'ARREDO>>VASI.";
		$category_id_list 	= $this->category_model->findCategoryByMapping($data, 'CJ');
		if( empty($category_id_list) ){
			echo "empty: ";
			$category_id_list 	= $this->auto_mapping->getMappedCategories($data);
			//echo "[".$this->db->last_query()."]";
			debug($category_id_list);
		}else{
			echo "not empty: ";
			debug($category_id_list);
		}
		
		return;
	}

	public function test_cache(){
		$logo = $this->config_model->findByKey('LOGO', TRUE);
		$this->config_model->
		debug($logo);
	}
	
	public function start($file){
		$file = str_replace('"', '', $file);
		if($file){
			$this->load->model('cron_model');
			echo $this->cron_model->insert(['log_str' => $file, 'status' => Cron_model::STATUS_RUNNING]);
		}
	}

	public function stop($log_id){
		if($log_id){
			$this->load->model('cron_model');
			$this->cron_model->update($log_id, ['status' => Cron_model::STATUS_DONE]);
			echo "ALL DONE.";
		}
	}
}