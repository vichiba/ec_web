<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Catalog extends MX_Controller{
	public function __construct(){
		parent::__construct();
		$this->load->model('admin/config_model');
		$this->load->model('brand/brand_model');
		$this->load->model('product/product_model');
		$this->load->model('category/category_model');
		$this->load->model('category_auto_mapping_model', 'auto_mapping');
	}

	public function rakuten($brand_id){
		$this->load->library('admin/RakutenConnector', NULL, '_rakuten');
		
		$page_number = 1;
		$records_per_page = 100;
		$max_page_number = 100;
		$return_product = 0;
		$total_matched = 0;
		$total_page = 0;

		do{
			$products = $this->_rakuten->searchProduct('', $brand_id, '', $records_per_page, $page_number);
			$return_product = count($products->item);
			$total_matched = (float)$products->TotalMatches;
			$total_page = (float)$products->TotalPages;
			//echo "Page: ".$page_number."/".$products->TotalPages."\n";
			if($page_number < 2) echo "Total products: ".number_format($total_matched). " ( ".number_format($total_page > 20 ? 20*$records_per_page : $total_matched)." readable)".PHP_EOL;
			if($products->TotalPages > 0) $this->show_status($page_number*$records_per_page, $total_page > 20 ? 20*$records_per_page : $total_page*$records_per_page);
			$save_cats =[];
			foreach($products->item as $item){
				$category_name = (string)$item->category->primary;
				$second_cat = (string)$item->category->secondary;
				if($second_cat){
					$category_name .= '>>'.$second_cat;
				}
				$log = $item->mid . '|' . $item->merchantname . '|' . $category_name;
				$save_cats[$log] = $page_number . '|' . $log;

			}
			echo "Save Product: ".$return_product."\n";
			file_put_contents(APPPATH.'/../logs/catalog_rakuten_'.date("j.n.Y").'.txt', implode(PHP_EOL, $save_cats).PHP_EOL, FILE_APPEND);
			$page_number++;
		}while($return_product > 0 && $page_number < $max_page_number);
		
		echo "================================================".PHP_EOL;
	}

	/**
	 * show a status bar in the console
	 * 
	 * <code>
	 * for($x=1;$x<=100;$x++){
	 * 
	 *     show_status($x, 100);
	 * 
	 *     usleep(100000);
	 *                           
	 * }
	 * </code>
	 *
	 * @param   int     $done   how many items are completed
	 * @param   int     $total  how many items are to be done total
	 * @param   int     $size   optional size of the status bar
	 * @return  void
	 *
	 */
	private function show_status($done, $total, $size=30) {
	 
	    static $start_time;
	 
	    // if we go over our bound, just ignore it
	    if($done > $total) return;
	 
	    if(empty($start_time)) $start_time=time();
	    $now = time();
	 
	    $perc=(double)($done/$total);
	 
	    $bar=floor($perc*$size);
	 
	    $status_bar="[";
	    $status_bar.=str_repeat("=", $bar);
	    if($bar<$size){
	        $status_bar.=">";
	        $status_bar.=str_repeat(" ", $size-$bar);
	    } else {
	        $status_bar.="=";
	    }
	 
	    $disp=number_format($perc*100, 0);
	 
	    $status_bar.="] $disp%  ".number_format($done)."/".number_format($total);
	 
	    $rate = ($now-$start_time)/$done;
	    $left = $total - $done;
	    $eta = round($rate * $left, 2);
	 
	    $elapsed = $now - $start_time;
	 
	    $status_bar.= " remaining: ".number_format($eta)." sec.  elapsed: ".number_format($elapsed)." sec.";
	 
	 	echo "\033[".(strlen($status_bar)+2)."D";      // Move 5 characters backward
	    echo "$status_bar  ";
	 
	    flush();
	 
	    // when done, send a newline
	    if($done == $total) {
	        echo "\n";
	    }
	 
	}
}