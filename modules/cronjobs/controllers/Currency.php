<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Run CLI command
 * visudo %apache ALL=(apache)NOPASSWD:/usr/bin/crontab
 * Example: php index.php cronjobs run index "CJ" "12345"
 * Example: php index.php cronjobs run index "RAKUTEN" "12345"
 */

class Currency extends MX_Controller{
	const API  = "https://query.yahooapis.com/v1/public/yql?format=json&env=store://datatables.org/alltableswithkeys&callback=&q=";
	public function __construct(){
		parent::__construct();
		$this->load->model('admin/config_model');
		include( dirname(__FILE__).'/../../admin/libraries/httpful.phar' );
	}

	private function getRestURL(){
		$currency_convert_code = get_config_value("CURRENCY_CONVERT_CODE");
		$query = "select * from yahoo.finance.xchange where pair in ($currency_convert_code)";
		return self::API.rawurlencode($query);
	}

	public function run(){
		echo "START CRAWLING..".PHP_EOL;
	    $response = \Httpful\Request::get($this->getRestURL())
					->expectsJson()
	    			->send();
		$body = $response->body;
		if($body){
			$rates = $body->query->results->rate;
			$this->load->model('currency/currency_model');
			foreach($rates as $rate){
				$code 	= str_replace('JPY', '', $rate->id);
				$name 	= $rate->Name;
				$value 	= $rate->Rate;
				$date 	= $rate->Date;

				//convert
				$date = date('Y-m-d', strtotime($date));

				$data = [
					'name' => $name,
					'currency' => $code,
					'value' => $value,
					'created_date' => $date,
				];

				$exists_id = $this->currency_model->exists(['currency' => $code, 'created_date' => $date]);
				if( $exists_id ){
					$this->currency_model->update($exists_id, $data);
				}else{
					$this->currency_model->insert($data);
				}
			}
		}
		echo "DONE!".PHP_EOL;
	}
}