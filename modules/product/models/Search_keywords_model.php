<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Search_keywords_model extends MY_Model{
 	 	
 	public $table = 'search_keywords';
	public $primary_key = 'id';

	public $second_table = 'catalogs';

	public function findHotKeywords($page = 1, $page_size = 20){
		$keywords = new stdClass();

		$this->applyCondition();
		$keywords->total = $this->db->count_all_results();

		$this->applyCondition();
		$this->db->order_by('search_times', 'DESC');
		$this->db->limit($page_size, ( (int)$page - 1 ) * (int)$page_size );
		$keywords->items = $this->db->get()->result();
		$keywords->page  = $page;
		$keywords->page_size  = $page_size;

		return $keywords;
	}

	private function applyCondition(){
		$this->db->select($this->table.'.keyword, search_times, '.$this->second_table.'.*');
		$this->db->from($this->table);
		$this->db->join($this->second_table, $this->table.'.category_id = '.$this->second_table.'.category_id');
		//$this->db->group_by($this->table.'.category_id');
	}
}