<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Product_model extends MY_Model{
 	 	
 	public $table = 'products';
	public $primary_key = 'product_id';

	public $second_ref_table = 'product_catalogs';
	public $second_table = 'catalogs';
	public $second_key = 'category_id';

	public $thirth_table = 'brands';
	public $thirth_key = 'brand_id';

	//0:Delete, 1: inactive, 2: OK
	const STATUS_INACTIVE = 1;
	const STATUS_ACTIVE = 2;

	const SORT_NEWEST = 1;
	const SORT_POLULAR = 2;
	const SORT_SALE = 3;
	const SORT_SALEOFF = 4;
	const SORT_PRICE_DESC = 5;
	const SORT_PRICE_ASC = 6;
	
	function __construct(){
		$this->load->model('category/category_model');
		$this->load->model('brand/brand_model');
	}

	public function findActive($product_id){
		$this->db->select('products.product_id, products.sku, products.name, products.short_description, products.long_description, products.raw_data, 
							products.image_url, products.buy_url, products.category_id,
							IFNULL(products.price * currency_latest.value, products.price) as price, 
							IFNULL(products.sale_price * currency_latest.value, products.sale_price) as sale_price, 
							IFNULL(products.retail_price * currency_latest.value, products.retail_price) as retail_price, 
							IF(currency_latest.currency IS NULL, products.currency, \'JPY\') as currency, 
							products.brand_id, products.favourite_count, products.vendor, products.status,
							IF(products.sale_price = 0, 0, ROUND(( (products.price-products.sale_price)/products.price * 100 ), 0) ) sale_off', 
							FALSE);
		$this->db->join('currency_latest', $this->table.'.currency = currency_latest.currency', 'LEFT');
		$product = $this->find($product_id);
		if($product && (int)$product->status === SELF::STATUS_ACTIVE){
			$this->load->model('image/image_model');
			$images = $this->image_model->listActive($product_id, Image_model::TYPE_PRODUCT);
			$product->images = [];
			foreach ($images as $item) {
				$product->images[] = $item;
			}
			$brand = $this->brand_model->findActive($product->brand_id);
			$category = $this->category_model->findActive($product->category_id);
			if($brand){
				unset($brand->created_date);
				unset($brand->modified_date);
				unset($brand->long_description);
				unset($brand->position);
				unset($brand->status);
				unset($brand->created_user_id);
				unset($brand->modified_user_id);
				$url = filter_var($brand->image_url, FILTER_SANITIZE_URL);
		        if (!filter_var($url, FILTER_VALIDATE_URL) === false) {} else {
		            $brand->image_url = site_url($brand->image_url);
		        }
				$product->brand = $brand;
			}
			if($category){
				unset($category->created_date);
				unset($category->modified_date);
				unset($category->long_description);
				unset($category->position);
				unset($category->status);
				unset($category->created_user_id);
				unset($category->modified_user_id);
				$url = filter_var($category->image_url, FILTER_SANITIZE_URL);
		        if (!filter_var($url, FILTER_VALIDATE_URL) === false) {} else {
		            $category->image_url = site_url($category->image_url);
		        }
				$product->category = $category;
			}

			$customer_id = $this->session->userdata('customer_id');
			if( $customer_id && is_numeric($customer_id) ){
				$this->db->from('customer_product');
				$this->db->where('customer_id', $customer_id);
				$this->db->where('product_id', $product_id);
				$product->is_like = $this->db->count_all_results() > 0 ? "1" : "0";
			}

			//$product->price         = number_format($product->price);
        	//$product->sale_price    = number_format($product->sale_price);
        	//$product->retail_price  = number_format($product->retail_price);

			return $product;
		}
		return NULL;
	}

	/**
	 * Search active Product following conditions
	 * 
	 * @param  string  	$keyword     	Product Name Keyword
	 * @param  int  	$category_id 	category id
	 * @param  int  	$brand_id    	brand id
	 * @param  array 	$price_range 	khoang gia
	 * @param  array  	$sort_type  	sort type; 1: Newest, 2:Polular, 3:Sale
	 * @param  int 		$page        	page display
	 * @param  int 		$page_size   	number record on 1 page
	 * @return mix               		List of products
	 */
	public function searchActive($keyword, $category_id, $brand_id, $price_range, $sort_type, $page = 1, $page_size = 20, $extra_conds = array(), $is_restful_request = FALSE){
		$products = new stdClass();

		$this->load->model('category/category_model');
		if( $category_id && is_numeric($category_id) ){
			$childs_categories = $this->category_model->listActive($category_id, 1, 100, TRUE);
			//debug($childs_categories, TRUE);
			$childCats = $this->getAllChildCatId($childs_categories);
			if( count($childCats) > 0 ){
				$category_id = array_merge([$category_id], $childCats);
			}
		}

 		$this->applyCondition($keyword, $category_id, $brand_id, $price_range, $sort_type, $extra_conds, $is_restful_request);
 		$query = $this->db->query($this->db->get_compiled_select());
		$products->total = $query->num_rows();

		//debug($this->db->last_query(), TRUE);
		
		$this->applyCondition($keyword, $category_id, $brand_id, $price_range, $sort_type, $extra_conds, $is_restful_request);
		$this->db->limit($page_size, ( (int)$page - 1 ) * (int)$page_size );
		$items = $this->db->get()->result();

		/*array_walk($items, function(&$item, $key){
			unset($item->search_price); //remove search_price from result sets
		});*/

		$products->items = $items;
		$products->page  = $page;
		$products->page_size  = $page_size;
		return $products;
	}

	private function getAllChildCatId($childs_recursive){
		$list = [];
		foreach ($childs_recursive as $item) {
			$list[] = $item->category_id;
			if( $item->has_child ){
				$list = array_merge($list, $this->getAllChildCatId($item->childs));
			}
		}
		return $list;
	}

	/**
	 * Search Price conditions
	select a.product_id, a.name, price, a.sale_price, a.retail_price, 
	case 
		when a.sale_price > 0 then a.sale_price * b.value
		else a.price * b.value
	end search_price
	from products a
	left join (
		select c1.currency, c1.value from currency c1 
		where c1.created_date = (select max(c2.created_date) from currency c2 where c1.currency = c2.currency)
		group by currency 
	) b on a.currency = b.currency
	where match(a.name, a.short_description) AGAINST("3d short blazer soft")
	having search_price >= 1000 and search_price <= 2000
	;
	 */

	private function applyCondition($keyword, $category_id, $brand_id, $price_range, $sort_type, $extra_conds, $is_restful_request = FALSE){
		if( !empty($category_id) ) {
			if( is_numeric($category_id) ){
				$this->db->where($this->second_table.'.category_id', $category_id);
			}elseif( is_array($category_id) ){
				$this->db->where_in($this->second_table.'.category_id', $category_id);
			}
		}
		
		if( !empty($brand_id) && is_numeric($brand_id) ) {
			$this->db->where($this->table.'.brand_id', $brand_id);
		}

		if( !empty($keyword) ){
			//$this->db->like($this->table.'.name', $keyword);
			$this->db->where('MATCH ('.$this->table.'.name,'.$this->table.'.short_description) AGAINST ('.$this->db->escape($keyword).')', NULL, FALSE);
		}
		if( count($extra_conds) ){
			$this->db->where($extra_conds);
		}

		if( is_array($price_range) ){
			if( isset($price_range['min']) && is_numeric($price_range['min']) ){
				$this->db->having('IF(sale_price = 0, price, sale_price) >=', $price_range['min'], FALSE);
			}
			if( isset($price_range['max']) && is_numeric($price_range['max']) ){
				$this->db->having('IF(sale_price = 0, price, sale_price) <=', $price_range['max'], FALSE);	
			}
		}
		
		$this->db->where($this->table.'.status', SELF::STATUS_ACTIVE);
		$this->db->where($this->second_table.'.status', SELF::STATUS_ACTIVE);

		$select_is_like = '';
		$select_is_brand_follow = '';
		$customer_id = $this->session->userdata('customer_id');
		if( $is_restful_request === TRUE && !$this->isValidAccessToken() ){
			$customer_id = NULL;
		}
		if( $customer_id && is_numeric($customer_id) ){
			$select_is_like = ', (SELECT COUNT(1) FROM customer_product where customer_product.product_id = '.$this->table.'.product_id AND customer_product.customer_id = '.$customer_id.') is_like';

			//FIX issue #319
			$select_is_brand_follow = ', (SELECT COUNT(1) FROM customer_brand WHERE customer_id='.$customer_id.' AND brand_id = products.brand_id) is_brand_followed';
			$this->db->order_by('is_brand_followed', 'DESC'); //must place before switch($sort_type)
		}

		//sort type
		switch ($sort_type) {
			case SELF::SORT_POLULAR:
				$this->db->order_by($this->table.'.click_count', 'DESC');	
				break;
			case SELF::SORT_SALE:
				$this->db->order_by($this->table.'.buy_count', 'DESC');	
				break;
			case SELF::SORT_SALEOFF:
				$this->db->order_by('sale_off', 'DESC');
				break;
			case SELF::SORT_PRICE_DESC:
				$this->db->order_by('IF(sale_price = 0, price, sale_price)', 'DESC', FALSE);
				break;
			case SELF::SORT_PRICE_ASC:
				$this->db->order_by('IF(sale_price = 0, price, sale_price)', 'ASC', FALSE);
				break;
			case SELF::SORT_NEWEST:
			default:
				$this->db->order_by($this->table.'.modified_date DESC, '.$this->table.'.created_date DESC');
				break;
		}

		$this->db->select('products.product_id, products.sku, products.name, products.short_description, products.long_description, 
							products.image_url, products.buy_url, '.$this->second_table.'.category_id, 
							IFNULL(products.price * currency_latest.value, products.price) as price, 
							IFNULL(products.sale_price * currency_latest.value, products.sale_price) as sale_price, 
							IFNULL(products.retail_price * currency_latest.value, products.retail_price) as retail_price, 
							IF(currency_latest.currency IS NULL, products.currency, \'JPY\') as currency, 
							products.brand_id, products.favourite_count, products.vendor,'.
						$this->second_table.'.uri_path as category_uri_path, '.
						'IF(products.sale_price = 0, 0, ROUND(( (products.price-products.sale_price)/products.price * 100 ), 0) ) sale_off, '.
						$this->second_table.'.name category_name, '.
						$this->thirth_table.'.uri_path brand_uri_path, '.
						$this->thirth_table.'.name brand_name'.$select_is_like.$select_is_brand_follow, FALSE);
		$this->db->from($this->table);
		$this->db->join($this->second_ref_table, $this->table.'.product_id = '.$this->second_ref_table.'.product_id');
		$this->db->join($this->second_table, $this->second_ref_table.'.category_id = '.$this->second_table.'.'.$this->second_key);
		$this->db->join($this->thirth_table, $this->table.'.brand_id = '.$this->thirth_table.'.'.$this->thirth_key);
		$this->db->join('currency_latest', $this->table.'.currency = currency_latest.currency', 'LEFT');
	}

	public function incBuyCount($product_id){
		$this->db->where($this->primary_key, $product_id);
		$this->db->set('buy_count', 'buy_count + 1', FALSE);
		return $this->db->update($this->table);
	}

	public function incClickCount($product_id){
		$this->db->where($this->primary_key, $product_id);
		$this->db->set('click_count', 'click_count + 1', FALSE);
		return $this->db->update($this->table);
	}
}