<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends MX_Controller 
{
	function __construct()
	{
		parent::__construct();
	}

    private function hasChild($item_id, $list){
        foreach ($list as $k => $item) {
            if( (int)$item->category_id === (int)$item_id ){

                if( $item->has_child )
                foreach($item->childs as $k => $tmp){
                    if($tmp->category_id != $item_id) unset($item->childs[$k]);
                }
                return $item;
            }
            
            if( $item->has_child && $this->hasChild($item_id, $item->childs)){
                foreach($item->childs as $k => $tmp){
                    if($tmp->category_id != $item_id) unset($item->childs[$k]);
                }
                return $item;
            }
        }
        return FALSE;
    }

    private function getCategoryArray($root){
        $list = isset($root->has_child) && $root->has_child ? $root->childs : [];
        unset($root->has_child);
        unset($root->childs);
        $array = [$root];
        foreach ($list as $item){
            $array[] = $item;
            if( isset($item->has_child) && $item->has_child ){
                $array = array_merge( $array, $this->getCategoryArray($item->childs) );
            }
        }
        return $array;
    }

    public function index($product_id){
        if( !is_numeric($product_id) ){
            show_404();
        }

    	$product = $this->product_model->findActive($product_id);
        if( !$product ){
            show_404();
        }

        $this->product_model->incClickCount($product_id);

        $category_id = $product->category->category_id;

        $catalogs = $this->category_model->listActiveWithURI(0, '', 1, 1000, TRUE);
        $root_catalogs = $this->hasChild($category_id, $catalogs);

        $breadcrumbs = [
            'page_title' => $product->name,
            'items' => [
                ['label' => __('Home'), 'link' => base_url()]
            ]
        ];
        $list = $this->getCategoryArray($root_catalogs);
        foreach( $list as $item ){
            if($item){
                $breadcrumbs['items'][] = ['label' => $item->name, 'link' => site_url($item->uri_path.'.html')];
            }
        }

        $breadcrumbs['items'][] = ['label' => $product->name, 'link' => site_url($product->product_id.'.html')];

        $related_products = $this->product_model->searchActive($product->name, $category_id, NULL, NULL, product_model::SORT_POLULAR, 1, 20, 'products.product_id != '.$product->product_id);

    	$data_render = [
    		'product' => $product,
    		'breadcrumbs' => $breadcrumbs,
    		'related_products' => $related_products,
            'page_title' => $product->name,
            'page_keywords' => $product->name,
            'page_description' => strip_tags($product->short_description),
    	];

        $this->load->view('product', $data_render);
    }
}