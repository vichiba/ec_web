<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['sample'] = 'sample';
$lang['Product details'] = '製品詳細情報';
$lang['Buy now'] = 'すぐ買う';
$lang['Save for late'] = 'お気に入り';
$lang['Related products'] = '関連商品';