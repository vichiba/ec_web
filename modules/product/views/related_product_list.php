<!-- more product-->
<div class="container more-products" >
    <div class="row">
        <div class="header-more-products"><h4><?php echo __('Related products') ?></h4></div>                   
        <div id="owl-demo" class="owl-carousel">
            <?php 
            foreach($related_products->items as $item){?>
                <div class="item">
                    <div class="img-products">
                        <div class="img-wrapper">
                            <img src="<?php echo $item->image_url?>" alt="<?php echo $item->name?>" class="img-responsive">
                            <div class="caption-img">
                                <a href="<?php echo trackurl('Related block on product detail page', 'PRODUCT', $item->product_id, uri_string().'?'.$this->input->server('QUERY_STRING'), $item->buy_url) ?>" target="_blank" title="Buy now"><i class="fa fa-cart-plus" aria-hidden="true"></i></a>
                            </div>
                        </div>
                        <div style="padding:10px 10px 0px 10px;">
                            <a href="<?php echo site_url($item->product_id.'.html') ?>" class="product_title">
                                <?php echo $item->name ?>
                            </a>
                            <hr>
                            <strong style=" color:#5bc0de;"><?php echo display_price($item)?></strong>

                            <a href="<?php echo site_url('customer/product_like?product_id='.$item->product_id)?>" 
                                class="text-muted pull-right ajax_get <?php echo isset($item->is_like) && (int)$item->is_like > 0 ? 'active' : ''?>">
                                <i class="fa fa-heart<?php echo (int)$item->is_like > 0 ? '' : '-o'?>" aria-hidden="true"></i>
                                <small class="text-muted"><?php echo (int)$item->favourite_count > 0 ? number_format($item->favourite_count) : ''?></small>
                            </a>
                        </div>

                    </div>
                </div>
                <?php } ?>  
            </div>
        </div>
    </div>
</div>
<!--e more product-->