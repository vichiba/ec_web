<?php $this->load->view('theme/header'); ?>
<?php $this->load->view('theme/page_top'); ?>
<?php $this->load->view('theme/navigation'); ?>

<?php $this->load->view('theme/breadcrumbs'); ?>

<link rel="stylesheet" href="/public/css/zoom/pygments.css" />
<link rel="stylesheet" href="/public/css/zoom/easyzoom.css" />

<!--products view-->
<div class="products-view zoom-img-items">
    <div class="container">
        <div class="row">
            <!-- left show products--> 
            <div class="col-xs-12 col-sm-7 col-md-7">
                <div class="col-xs-2 col-sm-2 col-md-2">  
                    <ul class="thumbnails">
                        <li>
                            <a href="<?php echo $product->image_url ?>" data-standard="<?php echo $product->image_url ?>">
                                <img src="<?php echo $product->image_url ?>" alt="" />
                            </a>
                        </li>
                        <?php foreach ($product->images as $item): ?>
                            <li>
                                <a href="<?php echo $item->image_url ?>" data-standard="<?php echo $item->image_url ?>">
                                    <img src="<?php echo $item->image_url ?>" alt="" />
                                </a>
                            </li>
                        <?php endforeach ?>
                    </ul>
                </div>

                <div class="col-xs-10 col-sm-10 col-md-10">
                    <div class="easyzoom easyzoom--overlay easyzoom--with-thumbnails">
                        <a href="<?php echo $product->image_url; ?>" title="<?php echo $product->name ?>">
                            <img src="<?php echo $product->image_url; ?>" alt="<?php echo $product->name ?>"  class="img-responsive"/>
                        </a>
                    </div>
                </div>
            </div><!-- e left show products-->
            <!-- right show products--> 
            <div class="col-xs-12 col-sm-5 col-md-5">
                <div class="product-view-detail">
                    <div class="name-products">
                        <h4><?php echo $product->name ?></h4>
                        <small><?php echo $product->short_description ?></small>
                    </div>
                    <div class="brand-name-text">
                        <small>by</small>
                        <a href="<?php echo site_url('brand/'.$product->brand->uri_path.'.html') ?>" title="<?php echo $product->brand->name ?>"><?php echo $product->brand->name ?></a>
                        <hr>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <strong class="rice"><?php echo display_price($product)?></strong>
                        </div>
                        <div class="col-md-6 text-right">
                            <a href="<?php echo site_url('customer/product_like?product_id='.$product->product_id)?>" 
                                class="text-muted ajax_get <?php echo isset($product->is_like) && (int)$product->is_like > 0 ? 'active' : ''?>">
                                <i class="fa fa-heart<?php echo (int)$product->is_like > 0 ? '' : '-o'?>" aria-hidden="true"></i>
                                <small class="text-muted"><?php echo (int)$product->favourite_count > 0 ? number_format($product->favourite_count) : ''?></small>
                            </a>
                        </div>
                    </div>
                    <div class="buttons-group">
                        <a href="<?php echo trackurl('Product detail page', 'PRODUCT', $product->product_id, uri_string().'?'.$this->input->server('QUERY_STRING'), $product->buy_url) ?>" target="_blank" title="Buy now" style="width: 100%">
                            <i class="fa fa-cart-plus" aria-hidden="true"></i>
                            <?php echo __('Buy now') ?>
                        </a>
                    </div>
                    <div class="detail-info">
                        <h4><?php echo __('Product details') ?></h4>
                        <?php echo $product->long_description ?>
                    </div>
                </div>
            </div>  <!-- e right show products-->
        </div>
        <div class="row">
            <div class="fb-comments" data-numposts="6" data-width="100%"></div>
        </div>
    </div>
</div> <!--end products view-->

<?php $this->load->view('related_product_list') ?>
<script type="text/javascript" src="/public/js/zoom/easyzoom.js"></script>
<script type="text/javascript">
// Instantiate EasyZoom instances
var $easyzoom = $('.easyzoom').easyZoom();

// Setup thumbnails example
var api1 = $easyzoom.filter('.easyzoom--with-thumbnails').data('easyZoom');

$('.thumbnails').on('click', 'a', function(e) {
    var $this = $(this);

    e.preventDefault();

// Use EasyZoom's `swap` method
api1.swap($this.data('standard'), $this.attr('href'));
});

// Setup toggles example
var api2 = $easyzoom.filter('.easyzoom--with-toggle').data('easyZoom');

$('.toggle').on('click', function() {
    var $this = $(this);

    if ($this.data("active") === true) {
        $this.text("Switch on").data("active", false);
        api2.teardown();
    } else {
        $this.text("Switch off").data("active", true);
        api2._init();
    }
});
</script>
<div id="fb-root"></div>
<script>(function(d, s, id) {   
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.7&appId=440542806073540";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<?php $this->load->view('theme/footer'); ?>