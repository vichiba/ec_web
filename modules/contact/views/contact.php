<?php $this->load->view('theme/header'); ?>
<?php $this->load->view('theme/page_top'); ?>
<?php $this->load->view('theme/navigation'); ?>
<?php $this->load->view('theme/breadcrumbs'); ?>
<!--    Chuỗi khai báo lấy tham số của google maps   -->
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false&language=ja"></script>
<script type="text/javascript">
var map;
function initialize(lat, lng) {
    var myLatlng = new google.maps.LatLng(lat, lng);
    var myOptions = {
        zoom: 16,
        center: myLatlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    map = new google.maps.Map(document.getElementById("div_id"), myOptions); 
    // Biến text chứa nội dung sẽ được hiển thị
    var text;
    text= "<div id='map-info'>" + 
    "<p style='text-align:left; font-weight: bold'><?php echo get_config_value('COMPANY_NAME') ?></p>" +
    "<img src='/public/images/logo.png' height=60 align='left'/>"+
    '<span><i class="fa fa-map-marker" aria-hidden="true"></i><?php echo get_config_value('COMPANY_ADDRESS') ?></span><br/>'+
    '<span><i class="fa fa-phone" aria-hidden="true"></i><?php echo get_config_value('COMPANY_PHONE') ?></span><br/>'+
    '<span><i class="fa fa-envelope" aria-hidden="true"></i><?php echo get_config_value('COMPANY_EMAIL') ?></span><br/>'+
    '</div>';
    var infowindow = new google.maps.InfoWindow(
        { content: text,
            size: new google.maps.Size(100,50),
            position: myLatlng
        });
    infowindow.open(map);    
    var marker = new google.maps.Marker({
        position: myLatlng, 
        map: map,
        title:"<?php echo get_config_value('COMPANY_NAME') ?>"
    });
}

$(document).ready(function(){
    initialize(<?php echo get_config_value('COMPANY_ADDRESS_LAT') ?>, <?php echo get_config_value('COMPANY_ADDRESS_LNG') ?>);
});

</script>

<div class="contact-page">
    <div class="container">
        <div class="row">

            <div class="top-contact">
                <div class="col-xs-12 col-sm-4 col-md-4" style="margin-bottom:30px;">
                    <h4><?php echo get_config_value('COMPANY_NAME') ?></h4><br>
                    <p><i class="fa fa-map-marker" aria-hidden="true"></i><?php echo get_config_value('COMPANY_ADDRESS') ?></p>
                    <p><i class="fa fa-phone" aria-hidden="true"></i><?php echo __('Call us now'); ?>: <?php echo get_config_value('COMPANY_PHONE') ?></p>

                    <p><i class="fa fa-envelope" aria-hidden="true"></i><?php echo __('Email'); ?>: <?php echo get_config_value('COMPANY_EMAIL') ?></p>

                </div>
                <div class="col-xs-12 col-sm-8 col-md-8" >

                    <div id="div_id" style="width:100%; height: 300px;"></div>
                </div>

            </div>

        </div>
    </div>
</div>
<!--products list-->

<div class="input-contact">
    <div class="container">
        <div class="row">
            <form action="<?php echo site_url('contact/save') ?>" method="POST">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <?php echo $flashdata; ?>   
                    </div>
                </div>
                <div class="tittle-box">
                    <h4><?php echo __('SENT TO MESSENGER') ?></h4>
                    <p><?php echo __('Please send your messenger to me....') ?></p>
                </div>
                <hr>
                <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="form-group">
                        <input type="text" name="name" class="form-control" placeholder="<?php echo __('Your Name') ?>">
                    </div>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="form-group">
                        <input type="email" name="email" class="form-control" placeholder="<?php echo __('Your Email') ?>">
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12  col-md-12 ">
                    <div class="form-group">
                        <input type="text" name="subject" class="form-control" placeholder="<?php echo __('Subject') ?>">
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12  col-md-12 ">
                    <div class="form-group">
                        <textarea class="form-control" rows="2" id="comment" name="message" placeholder="<?php echo __('Message') ?>"></textarea>
                    </div>

                </div>
                <div class="col-xs-6 col-sm-6  col-md-6 col-xs-offset-6 col-sm-offset-6 col-md-offset-6">
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary" "><?php echo __('Send') ?></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<?php $this->load->view('theme/footer'); ?>