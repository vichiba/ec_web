<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['Contact'] = 'お問い合わせ';
$lang['Send'] = '送信する';
$lang['SENT TO MESSENGER'] = 'お問い合わせ';
$lang['Please send your messenger to me....'] = '以下のフォムに入力してください。';
$lang['Your Name'] = 'お名前';
$lang['Your Email'] = 'メールアドレス';
$lang['Subject'] = '件名';
$lang['Message'] = 'お問い合わせ内容';
$lang['Message has been sent. We will answers as soon as posible.'] = 'メールを送信しました。確認次第、返事いたします。';