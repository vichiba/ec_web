<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends MX_Controller 
{
    public function index(){
    	$breadcrumbs = [
        	'page_title' => __('Contact'),
        	'items' => [
        		['label' => __('Home'), 'link' => base_url()],
                ['label' => __('Contact'), 'link' => base_url(uri_string())]
        	]
        ];

    	$data_render = [
    		'breadcrumbs' => $breadcrumbs,
            'page_title' => __('Contact'),
            'flashdata' => $this->session->flashdata('CONTACT')
    	];
        $this->load->view('contact', $data_render);
    }

    public function save(){
        if( $_SERVER['REQUEST_METHOD'] === 'POST' ){
            $name = $this->input->post('name');
            $email = $this->input->post('email');
            $subject = $this->input->post('subject');
            $message = $this->input->post('message');

            $validation_rules = array(
                array(
                        'field' => 'name',
                        'label' => __('Your name'),
                        'rules' => 'trim|required|min_length[3]',
                        'errors' => array(
                            'required'      => 'You have not provided %s.'
                        )
                ),
                array(
                        'field' => 'email',
                        'label' => __('Your Email'),
                        'rules' => 'trim|required|valid_email',
                        'errors' => array(
                            'required'      => 'You have not provided %s.'
                        )
                ),
                array(
                        'field' => 'subject',
                        'label' => __('Subject'),
                        'rules' => 'trim|required|min_length[5]',
                        'errors' => array(
                                'required' => 'You must provide a %s.',
                        )
                ),
                array(
                        'field' => 'message',
                        'label' => __('Message'),
                        'rules' => 'trim|required|min_length[10]',
                        'errors' => array(
                                'required' => 'You must provide a %s.',
                        )
                )
            );

            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run() == FALSE){
                $error = implode('<br/>', explode(PHP_EOL, trim(strip_tags($this->form_validation->error_string()), PHP_EOL)));
                $this->session->set_flashdata('CONTACT', $error);
            }else{

                $data = [
                    'name' => $name,
                    'email' => $email,
                    'subject' => $subject,
                    'message' => $message,
                    'is_read' => 1
                ];
                $this->load->model('contact_model');
                $contact_id = $this->contact_model->insert($data);
                if( $contact_id && is_numeric($contact_id) ){
                    $this->session->set_flashdata('CONTACT', __('Message has been sent. We will answers as soon as posible.'));
                }
            }
        }
        redirect(site_url('contact'));
    }
}