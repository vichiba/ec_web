<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Track extends MX_Controller 
{
    public function index(){
    	$source_content = $this->input->get('source_content');
        $source_type    = $this->input->get('source_type');
        $source_id      = $this->input->get('source_id');
        $source_url     = $this->input->get('source_url');
        $dest_url       = $this->input->get('dest_url');

        $data = [
            'source_content' => $source_content,
            'source_type' => $source_type,
            'source_id' => $source_id,
            'source_url' => $source_url,
            'dest_url' => $dest_url
        ];

        $this->load->view('track', $data);
    }

    public function redirect(){
        $source_content = $this->input->get('source_content');
        $source_type    = $this->input->get('source_type');
        $source_id      = $this->input->get('source_id');
        $source_url     = $this->input->get('source_url');
        $dest_url       = $this->input->get('dest_url');

        if( $source_id && is_numeric($source_id) && $source_type && $dest_url){
            $data = [
                'source_content' => $source_content,
                'source_type' => $source_type,
                'source_id' => $source_id,
                'source_url' => $source_url,
                'dest_url' => $dest_url,
                'source_ip' => $this->input->ip_address(),
            ];
            $this->load->model('track_model');

            $track_id = $this->track_model->insert($data);
            if( $track_id ){
                if( $source_type == 'PRODUCT' ){
                    $this->load->model('product/product_model');
                    $this->product_model->incBuyCount($source_id);
                }
            }
            redirect($dest_url);
        }

        if($source_url) redirect($source_url);
        else redirect(base_url());
    }
}