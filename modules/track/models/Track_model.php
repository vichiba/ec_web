<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Track_model extends MY_Model{
 	 	
 	public $table = 'tracks';
	public $primary_key = 'track_id';

	const TYPE_PRODUCT = "PRODUCT";

	function __construct(){
		$this->load->model('category/category_model');
		$this->load->model('brand/brand_model');
	}
	
}