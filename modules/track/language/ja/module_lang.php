<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['Redirecting you to Bird...'] = 'リダイレクトしています...';
$lang['If you are not automatically redirected, <a href="%s">click here</a>'] = '自動的にリダイレクトしない場合、ここを<a href="%s">クリックして</a>ください。';
