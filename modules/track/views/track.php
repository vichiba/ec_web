<!DOCTYPE html>
<html>
<head>
	<title><?php echo __('Redirecting you to Bird...') ?></title>
	
	<style type="text/css">
		@font-face {
		    font-family: 'MSGothic';
		    src: url('/public/fonts/font/MSGothic.ttf');
		}
		body{
			background: #F5F5F5;
			color: #333;
			font-family: 'MSGothic';
			font-size: 14px;
		}
		.container{
			text-align: center;
			position: fixed;
			left: 0;
			top: 40%;
			width: 100%;
		}
		a{
			text-decoration: none;
			color: #5bc0de;
		}
	</style>
</head>
<body>
	<div class="container">
		<form action="<?php echo site_url('track/redirect') ?>" method="GET">
			<input type="hidden" name="source_content" value="<?php echo $source_content?>" />
            <input type="hidden" name="source_type" value="<?php echo $source_type?>" />
            <input type="hidden" name="source_id" value="<?php echo $source_id?>" />
            <input type="hidden" name="source_url" value="<?php echo $source_url?>" />
            <input type="hidden" name="dest_url" value="<?php echo $dest_url?>" />
		</form>
		<h1><?php echo __('Redirecting you to Bird...') ?></h1>
		<span><?php echo sprintf(__('If you are not automatically redirected, <a href="%s">click here</a>'), $dest_url); ?></span>
	</div>
	<script type="text/javascript" src="<?php echo site_url('public/js/jquery-1.9.1.min.js')?>"></script>
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			setTimeout(function(){
				$('form').submit();
			}, 1000);
		});
	</script>
</body>
</html>