<?php 
if( !isset($catalogs) ){
    $catalogs = $this->category_model->listActiveWithURI(0, '', 1, 6, FALSE);
}
?>
<!--footer-->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-3 info">
                <h3><?php echo get_config_value('COMPANY_NAME') ?></h3>
                <p> <i class="fa fa-map-marker" aria-hidden="true"></i><?php echo get_config_value('COMPANY_ADDRESS') ?></p>
                <p> <i class="fa fa-phone" aria-hidden="true"></i><?php echo __('Call us now'); ?>: <?php echo get_config_value('COMPANY_PHONE') ?></p>
                <p> <i class="fa fa-envelope-o" aria-hidden="true"></i><?php echo __('Email'); ?>: <?php echo get_config_value('COMPANY_EMAIL') ?></p>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-3 link-shop">
                <nav>
                    <h3><?php echo __('SHOP LINK') ?></h3>
                    <ul>
                        <?php foreach ($catalogs as $item): ?>
                        <li><a href="<?php echo site_url($item->uri_path) ?>.html" title="<?php echo $item->name ?>"><?php echo $item->name ?></a></li>
                        <?php endforeach ?>
                    </ul>
                </nav>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-3 link-shop">
                <!-- <h3><?php echo __('GET A NEWSLETTER') ?></h3>
                <p><?php echo __('Make sure that you never miss our interesting news by joining our newsletter program.') ?></p>
                <form class="form_newsletter" action="" method="post" id="" name="" target="_blank">
                    <input type="email" value="" placeholder="<?php echo __('Your email ...') ?>" name="EMAIL" id="mail" class="newsletter-input form-control" aria-label="Your email ...">
                    <button id="subscribe" class="button_mini btn" type="submit">
                        <span>Subscribe →</span>
                    </button>
                </form> -->
                <nav>
                    <h3><?php echo __('Information') ?></h3>
                    <ul>
                        <li><a href="/about"><?php echo __('About us') ?></a></li>
                        <li><a href="/faq"><?php echo __('FAQ') ?></a></li>
                        <li><a href="/terms-of-service"><?php echo __('Terms of Service') ?></a></li>
                        <li><a href="/privacy"><?php echo __('Privacy Policy') ?></a></li>
                        <li><a href="/contact"><?php echo __('Contact us') ?></a></li>
                    </ul>
                </nav>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-3">
                <h3><?php echo __('FOLLOW US ON SOCIAL') ?></h3>
                <p><?php echo __('We are now on social network. Connect with us to update the larest news and promotion') ?></p>
                <ul class="list-unstyled clearfix">

                    <li class="facebook">
                        <a target="_blank" href="<?php echo get_config_value('FACEBOOK_LINK') ?>" title="Follow us on Facebook" class="btn-tooltip" data-original-title="Facebook">
                            <i class="fa fa-facebook"></i>
                            <span>Facebook</span>
                        </a>
                    </li>


                    <li class="twitter">
                        <a target="_blank" href="<?php echo get_config_value('TWITTER_LINK') ?>" title="Follow us on Twitter" class="btn-tooltip" data-original-title="Twitter">
                            <i class="fa fa-twitter"></i>
                            <span>Twitter</span>
                        </a>
                    </li>

                    <li class="instagram">
                        <a target="_blank" href="<?php echo get_config_value('INSTAGRAM_LINK') ?>" title="Follow us on Instagram" class="btn-tooltip" data-original-title="Instagram">
                            <i class="fa fa-instagram"></i>
                            <span>Instagram</span>
                        </a>
                    </li>
                    <!-- <li class="youtube">
                        <a target="_blank" href="<?php echo get_config_value('YOUTUBE_LINK') ?>" title="Follow us on Instagram" class="btn-tooltip" data-original-title="Youtube">
                            <i class="fa fa-youtube"></i>
                            <span>Youtube</span>
                        </a>
                    </li> -->

                    <li class="skype">
                        <a target="_blank" href="<?php echo get_config_value('GOOGLE_LINK') ?>" title="Follow us on Google Plus" class="btn-tooltip" data-original-title="Skype">
                            <i class="fa fa-google-plus"></i>
                            <span>Google flus</span>
                        </a>
                    </li>
                </ul>
            </div>

        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 copyright text-center">
                    Copyright © design by <a href="/"><?php echo get_config_value('COMPANY_NAME') ?></a> 2016. All rights reserved.
                </div>
                <!-- <div class="col-xs-12 col-sm-12 col-md-6 link-bottom">

                    <ul>
                        <li><a href="/contact"><?php echo __('Contact us') ?></a></li>
                        <li><a href="/terms-of-service"><?php echo __('Terms of Service') ?></a></li>
                        <li><a href="/privacy"><?php echo __('Privacy Policy') ?></a></li>
                        <li><a href="/about"><?php echo __('About us') ?></a></li>
                    </ul>

                </div> -->
            </div>
        </div>
    </div>
</footer> 


<!--end footer-->
<!-- slide slider products-->
<script src="<?php echo site_url('public/js/owl.carousel.js')?>"></script>


<script src="<?php echo site_url('public/js/menu-sub/cbpHorizontalMenu.min.js')?>"></script>
<script>
    $(function() {
        cbpHorizontalMenu.init();
    });
</script>

<script src="<?php echo site_url('public/js/jquery.dlmenu.js')?>"></script>
<script>
    $(function() {
        $( '#dl-menu' ).dlmenu({
            animationClasses : { classin : 'dl-animate-in-5', classout : 'dl-animate-out-5' }
        });
    });
</script>
</body>
</html>