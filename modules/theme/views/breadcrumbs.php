<?php 
if( isset($breadcrumbs) && is_array($breadcrumbs) ):
?>
<!--breadcrumbs-->
<div class="banner-page">
    <div class="container">
        <div class="tittle-banner-page"><h3><?php echo $breadcrumbs['page_title'] ?></h3></div>
        <ol class="breadcrumb">
        	<?php
        	$breadcrumbIdx = 1;
        	foreach ($breadcrumbs['items'] as $item): ?>
            <li <?php echo $breadcrumbIdx++ == count($breadcrumbs['items']) ? 'class="active"': '' ?>class="active">
            	<a href="<?php echo $item['link'] ?>" title="<?php echo $item['label'] ?>"><?php echo $item['label'] ?></a>
            </li>
        	<?php endforeach ?>
        </ol>
    </div>
</div>   
<!--end of breadcrumbs-->
<?php endif; ?>