<?php if (isset($carousel) && is_array($carousel)): ?>
<!--slider-->   
<div class="carousel slide" data-ride="carousel" id="miniCarousel" style="width: 100%; ">  
    <div class="carousel-inner" role="listbox" style="height: 250px;">  
        <?php 
        $carouselActive = false;
        foreach ($carousel as $item): 
            $link = "";
            switch ($item->link_type) {
                case Image_model::CLICK_TYPE_PRODUCT:
                    $product_id = $item->link_value;
                    $product = $this->product_model->findActive($product_id);
                    $link = site_url($product->catalog_uri_path.'/'.$product->product_id.'.html');
                    break;
                case Image_model::CLICK_TYPE_CATEGORY
                    $category_id = $item->link_value;
                    $catalog = $this->category_model->findActive($category_id);
                    $link = site_url($catalog->uri_path.'.html');
                    break;
                case Image_model::CLICK_TYPE_BRAND
                    $brand_id = $item->link_value;
                    $brand = $this->brand_model->findActive($brand_id);
                    $link = site_url('brand/'.$brand->uri_path.'.html');
                    break;
                case Image_model::CLICK_TYPE_LINK
                    $link = $item->link_value;
                    break;
            }
        ?>
        <div class="item <?php echo !$carouselActive ? 'active': ''; $carouselActive = TRUE; ?>">
            <img alt="<?php echo $item->label ?>" src="<?php echo site_url($item->image_url)?>">  
            <div class="carousel-caption hidden-xs">  
                <?php if ($item->label): ?><h2><?php echo $item->label ?></h2><?php endif ?>
                <?php if ($item->description): ?><p><?php echo $item->description ?></p><?php endif ?>
                <?php if ($link): ?><a href="<?php echo site_url($link); ?>">Go to shop <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a><?php endif ?>
            </div> 
            
        </div>
        <?php endforeach ?>
    </div> 
</div>  
<!--end slider-->
<?php endif ?>