<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php echo isset($page_title) ? $page_title : get_config_value('SEO_TITLE') ?></title>
    
    <meta name="keywords" content="<?php echo isset($page_keywords) ? $page_keywords : get_config_value('SEO_KEYWORDS') ?>" />
    <meta name="description" content="<?php echo isset($page_description) ? $page_description : get_config_value('SEO_DESCRIPTION') ?>" />
    <meta name="resource-type" content="document" />
    <meta name="robots" content="all, index, follow"/>
    <meta name="googlebot" content="all, index, follow" />

    <!-- Bootstrap -->

    <link rel="stylesheet" type="text/css" href="<?php echo site_url('public/css/bootstrap.min.css')?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('public/css/style.css')?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('public/css/font-awesome.min.css')?>"/>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script type="text/javascript" src="<?php echo site_url('public/js/jquery-1.9.1.min.js')?>"></script>
    <script type="text/javascript" src="<?php echo site_url('public/js/readmore.min.js')?>"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo site_url('public/js/bootstrap.min.js')?>"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('public/css/component.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('public/css/responsive.css')?>">
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('public/css/page.css')?>">
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('public/css/menu-desktop-css.css')?>">


    <link rel="stylesheet" type="text/css" href="<?php echo site_url('public/css/owl.carousel.css')?>">
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('public/css/owl.theme.css')?>" rel="stylesheet">

    <!-- Prettify -->
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('public/css/prettify.css')?>" rel="stylesheet">

    <script type="text/javascript" src="<?php echo site_url('public/js/modernizr.custom.js')?>"></script>
    
    <script type="text/javascript" src="<?php echo site_url('public/js/app.js')?>"></script>

    <script type="text/javascript">
        // img effect on tab
        $(document).ready(function () {
            var play = 0;
            $("ul[data-liffect] li").shuffleElements().each(function (i) {
                $(this).attr("style", "-webkit-animation-delay:" + i * 300 + "ms;"
                    + "-moz-animation-delay:" + i * 300 + "ms;"
                    + "-o-animation-delay:" + i * 300 + "ms;"
                    + "animation-delay:" + i * 300 + "ms;");
                play++;
                if (play == $("ul[data-liffect] li").size()) {
                    $("ul[data-liffect]").addClass("play")
                }
            });
        });

        jQuery.fn.shuffleElements = function () {
            var o = $(this);
            for (var j, x, i = o.length; i; j = parseInt(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
                return o;
        };

        $(window).scroll(function(){
            // Nếu cuộn được hơn 100px rồi
            if($(this).scrollTop()>100){
                //  Tiến hành show menu ra    
                $(".navbar-inverse").css("background-color", "rgba(0, 0, 0, 0.7)");

                $(".navbar-inverse").css("top", "0");

                $(".navbar-inverse").css("position", "fixed");
            }

            else{
                //    Ngược lại, nhỏ hơn 100px thì hide menu đi.

                $(".navbar-inverse").css("background-color", "rgba(0, 0, 0, 0.3)");
                $(".navbar-inverse").css("position", "absolute");
                $(".navbar-inverse").css("top", "100px");
            }
        });

    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $(".owl-carousel").owlCarousel({
                items : 4,
                lazyLoad : true,
                navigation : true,
                navigationText: ['<?php echo __('Prev') ?>', '<?php echo __('Next') ?>']
            });

            $('.box-select.toolbar .box-select-items select.selectpicker').change(function(){
                var val = $(this).val();
                insertParam($(this).attr('name'), val);
            });
        });

        function insertParam(key, value){
            key = encodeURI(key); value = encodeURI(value);

            var kvp = document.location.search.substr(1).split('&');

            var i=kvp.length; var x; while(i--) 
            {
                x = kvp[i].split('=');

                if (x[0]==key)
                {
                    x[1] = value;
                    kvp[i] = x.join('=');
                    break;
                }
            }

            if(i<0) {kvp[kvp.length] = [key,value].join('=');}

            //this will reload the page, it's likely better to store this until finished
            document.location.search = kvp.join('&'); 
        }
    </script>
    <script type="text/javascript">var LOGGED_IN = '<?php echo $this->session->userdata('customer_id') ?>', NOT_LOGGED_IN_MSG = '<?php echo __('NOT_LOGGED_IN_MSG') ?>';</script>
</head>
<body>