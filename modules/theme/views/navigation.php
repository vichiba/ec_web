<?php 
if( !isset($catalogs) ){
    $catalogs = $this->category_model->listActiveWithURI(0, '', 1, 1000, TRUE);
}
?>
<!-- menu-->
<nav class="navbar navbar-inverse" >
    <!-- <div class="usd-exchange">
        <a href="#" data-toggle="modal" data-target=".bs-example-modal-lg-1" style="border-right:1px solid #f4f4f4;">$ USD</a>
        <a href="#" data-toggle="modal" data-target=".bs-example-modal-lg-1">¥ JP</a>
    </div> -->
    <!--menu responsive-->
    <div class="column hidden-sm  hidden-md hidden-lg" >
        <div id="dl-menu" class="dl-menuwrapper">
            <button class="dl-trigger">Open Menu</button>
            <!--ul main-->
            <ul class="dl-menu">
                <!--li items-->
                <li class="level0"><a href="<?php echo base_url() ?>"><?php echo __('Home') ?></a></li><!--end li items-->
                <?php foreach ($catalogs as $item): ?>
                <!--li items-->
                <li class="level0"><a href="<?php echo site_url($item->uri_path.'.html'); ?>" title="<?php echo $item->name ?>"><?php echo $item->name ?></a>
                    <?php if($item->has_child): ?>
                    <!--ul sub 1-->
                    <ul class="dl-submenu">
                        <?php foreach ($item->childs as $child1): ?>
                        <li><a href="<?php echo site_url($child1->uri_path.'.html'); ?>" title="<?php echo $child1->name ?>"><?php echo $child1->name ?></a>
                            <!--ul sub 2-->
                            <?php if($child1->has_child): ?>
                            <ul class="dl-submenu">
                                <?php foreach ($child1->childs as $child2): ?>
                                <li><a href="<?php echo site_url($child2->uri_path.'.html'); ?>" title="<?php echo $child2->name ?>"><?php echo $child2->name ?></a></li>
                                <?php endforeach ?>
                            </ul><!--e ul sub 2-->
                            <?php endif ?> 
                        </li>
                        <?php endforeach; ?>
                    </ul><!--e ul sub 1-->
                    <?php endif ?> 
                </li><!--end li items-->
                <?php endforeach ?>
            </ul><!--end ul main-->
        </div><!-- /dl-menuwrapper -->
    </div><!-- menu responsive-->
    <!--logo-->
    <div class="cbp-hrmenu navbar-collapse collapse" id="cbp-hrmenu" >
        <ul>
            <!--li level 0-->
            <li class="level0"><a href="<?php echo base_url() ?>"><?php echo __('Home') ?></a></li>
            <!--end li level 0-->
            <?php foreach ($catalogs as $item): ?>
            <!--li level 0-->
            <li class="level0"><a href="<?php echo site_url($item->uri_path.'.html'); ?>" title="<?php echo $item->name ?>"><?php echo $item->name ?></a>
                <?php if($item->has_child): ?>
                <i class="fa fa-angle-down" aria-hidden="true"></i>
                <!--div chứa menu sub level 1-->
                <ul class="cbp-hrsub container-fluid">
                    <ul class="cbp-hrsub-inner"> 
                        <?php foreach ($item->childs as $child1): ?>
                        <!-- noi dung của bên trong menu sub bắt đầu từ đây-->
                        <ul class="ul-submenu">
                            <!--li items level 1 -->
                            <li class="<?php echo $child1->has_child ? 'has-child' : ''?>"><a href="<?php echo site_url($child1->uri_path.'.html'); ?>" title="<?php echo $child1->name ?>"><?php echo $child1->name ?></a>
                                <?php if($child1->has_child): ?>
                                <!--ul sub 2, ul level 2-->
                                <ul class="ul-submenu-sub">
                                    <?php foreach ($child1->childs as $child2): ?>
                                    <li><a href="<?php echo site_url($child2->uri_path.'.html'); ?>" title="<?php echo $child2->name ?>"><?php echo $child2->name ?></a></li>
                                    <?php endforeach ?>
                                </ul>
                                <!--e ul sub 2,ul level 2-->
                                <?php endif ?>
                            </li>
                            <!--end li items level 1 -->
                        </ul>
                        <?php endforeach ?>
                        <!-- end noi dung của bên tron menu sub bắt đầu từ đây-->
                    </ul><!-- /cbp-hrsub-inner -->
                </ul><!-- /cbp-hrsub -->
                <!--end div chứa menu sub level 1-->
                <?php endif ?>
            </li>
            <?php endforeach ?>
            <!--end li level 0-->
        </ul>
    </div>
</nav>
<!-- end menu-->
<script type="text/javascript">
$(function(){
    $('nav.navbar a[href="<?php echo base_url(uri_string()) ?>"]').closest('.level0').addClass('active');
});
</script>