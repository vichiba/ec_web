<!--header-->
<header>
    <!--search box hidden-->
    <?php 
    $this->load->model('product/search_keywords_model');
    $hot_keywords = $this->search_keywords_model->findHotKeywords(1, 8);
    $hot_keyword_html = '';
    foreach($hot_keywords->items as $item){
        $hot_keyword_html.= '<p><a href="'.site_url('search?keyword='.urlencode($item->keyword)).'" title="'.$item->keyword.'"><span>'.$item->keyword.'</span></a> 
                    in <a href="'.site_url('search?category_id='.$item->category_id.'&keyword='.urlencode($item->keyword)).'">'.$item->name.'</a>
                    </p>';
    }
    ?>
    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <form action="<?php echo site_url('search') ?>" method="GET">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>

                    <div class="input-group">
                        <input type="text" class="form-control" name="keyword" placeholder="<?php echo __('Product Search') ?>" >
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="submit">
                                <i class="fa fa-search" style="margin-right:10px;"></i>
                            </button>
                        </span>
                    </div><!-- /input-group -->
                    </form>
                </div>
                <div class="modal-body top-search-keywords">
                    <i class="fa fa-map-marker" style="margin-right:10px;"></i><span style=" color:#c2c2c2; font-size:12px;"><?php echo __('Top searches') ?></span> <hr>
                    <?php echo $hot_keyword_html; ?>
                </div>
            </div>
        </div>
    </div><!--search box-->


    <!--box change exchange-->
    <div class="modal  modal-exchange fade bs-example-modal-lg-1" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>

                    <div class="form-group">
                        <label for="sel1">Set Your Country</label><hr>
                        <select class="form-control" id="sel1">
                            <option>India</option>
                            <option>Japan</option>
                            <option>USA</option>
                            <option>Viet Nam</option>
                        </select><hr>
                        <button type="button" class="btn btn-deflautlt">Go !</button>
                    </div>

                    <!-- /input-group -->

                </div>
                <div class="modal-body">

                </div>
            </div>
        </div>
    </div> <!--box change exchange-->

    <!-- header top-->
    <div class="header-top">
        <div class="container">
            <div class="row">
                <div class="col-xs-3 col-sm-3 col-md-3">
                    <a href="<?php echo base_url(); ?>" title="">
                        <div class="logo">
                            <img src="/public/images/logo.png">
                        </div>
                    </a>
                </div>
                <div class="col-xs-9 col-sm-9 col-md-9 right-box">
                    <!-- /search-group -->
                    <div class="box-icon box-icon-small login">
                        <?php 
                        $customer_id = $this->session->userdata('customer_id');
                        if ( $customer_id && is_numeric($customer_id) ) :?>
                        <a href="<?php echo site_url('customer') ?>">
                            <i class="fa fa-user hidden-md hidden-lg" aria-hidden="true"></i>
                            <span class="hidden-xs hidden-sm">
                                <img src="<?php echo $this->session->userdata('customer_avatar') ?>" class="pull-left" style="max-height: 100%">
                                <?php echo $this->session->userdata('customer_name') ?>
                            </span>
                        </a>
                         <div class="user-box">
                            <p><strong>マイアカウント</strong></p><hr>
                            <ul class="list-user">
                              <li><a href="<?php echo site_url('customer/profile') ?>"><?php echo __('Profile') ?></a></li>
                              <li><a href="<?php echo site_url('customer/favourite') ?>"><?php echo __('My Favourites') ?></a></li>
                              <li><a href="<?php echo site_url('customer/brand') ?>"><?php echo __('My Brands') ?></a></li>
                              <li><a href="<?php echo site_url('customer/brand') ?>"><?php echo __('Update Password') ?></a></li>
                            </ul><hr>
                            <p><a href="<?php echo site_url('customer/logout') ?>"><?php echo __('Logout') ?></a></p>
                        </div>
                        <?php else: ?>
                            <a href="<?php echo site_url('customer') ?>"><i class="fa fa-user hidden-md hidden-lg" aria-hidden="true"></i><span class="hidden-xs hidden-sm">ログイン</span></a>
                        <?php endif; ?>
                    </div>
                    <form action="<?php echo site_url('search') ?>" method="GET">
                    <div class="input-group">
                        <input type="text" class="form-control hidden-xs" name="keyword" placeholder="<?php echo __('Product Search') ?>" value="<?php echo $this->input->get('keyword') ?>">
                        <span class="input-group-btn">
                            <button class="btn btn-default hidden-sm hidden-md hidden-lg" type="button" data-toggle="modal" data-target=".bs-example-modal-lg">
                                <i class="fa fa-search" aria-hidden="true"></i>
                            </button>
                            <button class="btn btn-default hidden-xs" type="submit">
                                <i class="fa fa-search" aria-hidden="true"></i>
                            </button>
                        </span>
                    </div>
                    </form>
                    <!-- /search-group -->

                </div>


            </div>
        </div>
        </div> <!--end header top-->
</header> <!--end header-->