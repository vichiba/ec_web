<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['Home'] = 'ホーム';

$lang['My Account'] = 'マイアカウント';
$lang['Account'] = 'アカウント';
$lang['Profile'] = 'プロファイル';
$lang['My Favourites'] = 'お気に入り';
$lang['My Brands'] = 'マイブランド';
$lang['Update Password'] = 'パスワード更新';
$lang['Logout'] = 'ログアウト';
$lang['Update Password'] = 'パスワード更新';

$lang['Top searches'] = 'トップキーワード';

$lang['Price'] = '値段';
$lang['Sort'] = 'ソート';
$lang['All'] = 'すべて';

$lang['Information'] = '情報';

$lang['Category'] = 'カタログ';
$lang['Newest'] = '最新';
$lang['Polular'] = '人気';
$lang['On Sale'] = '発売中';
$lang['Product Search'] = '商品検索';

$lang['Prev'] = '前';
$lang['Next'] = '次';

$lang['Read More'] = '詳細を見る';

$lang['Contact us'] = 'お問い合わせ';
$lang['Terms of Service'] = '利用規約';
$lang['Privacy Policy'] = 'プライバシーポリシー';
$lang['About us'] = '会社情報';
$lang['GET A NEWSLETTER'] = 'お問い合わせ';
$lang['FOLLOW US ON SOCIAL'] = 'SNSでフォローする';
$lang['SHOP LINK'] = 'ショップリンク';
$lang['STORE INFOMATION'] = '会社情報';

$lang['Call us now'] = '問い合わせ番号';
$lang['Email'] = 'メール';
$lang['Your email ...'] = 'あなたのメール';

$lang['We are now on social network. Connect with us to update the larest news and promotion'] = 'SNSでフォローすることで最新情報やキャンペーンがすぐ通知します。';
$lang['Make sure that you never miss our interesting news by joining our newsletter program.'] = 'ニュースレターを受けることにすれば、良いニュースを飛ばしません。';

$lang['NOT_LOGGED_IN_MSG'] = 'ログインすることが必要です。ログインください。';