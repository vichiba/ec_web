<?php defined('BASEPATH') OR exit('No direct script access allowed');

$autoload['packages'] = array();
$autoload['libraries'] = array();
$autoload['drivers'] = array();
$autoload['helper'] = array('language');
$autoload['config'] = array();
$autoload['language'] = array();
$autoload['model'] = array('admin/config_model', 'category/category_model', 'currency/currency_model');