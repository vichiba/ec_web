<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if( !function_exists('admin_url') ){
	function admin_url($uri = ''){
		$CI = &get_instance();
		$admin_url = $CI->config->item('admin_url');
		return site_url("$admin_url/$uri");
	}
}

if( !function_exists('get_error_block') ){
	function get_error_block($content){
		return 
		'<div class="alert alert-danger fade in block-inner">
		  	<button type="button" class="close" data-dismiss="alert">×</button>
		    <i class="icon-cancel-circle"></i> '.$content.'
		</div>';
	}
}


if( !function_exists('generate_hash_token') ){
	/**
	 * generate_hash_token
	 * Generates a new hashed password / token.
	 *
	 * @return string
	 * @author Rob Hussey
	 */
	function generate_hash_token($token, $database_salt = FALSE, $is_password = FALSE)
	{
	    if (empty($token))
	    {
	    	return FALSE;
	    }
		
		// Get static salt if set via config file.
		$static_salt = $this->auth->auth_security['static_salt'];
		
		if ($is_password)
		{
			require_once(APPPATH.'libraries/phpass/PasswordHash.php');				
			$phpass = new PasswordHash(8, FALSE);
			
			return $phpass->HashPassword($database_salt . $token . $static_salt);
		}
		else
		{
			return sha1($database_salt . $token . $static_salt);
		}
	}
}

if( !function_exists('show_403') ){
	function show_403(){
		$CI = &get_instance();
		Modules::run($CI->router->routes['403_page']);
	}
}