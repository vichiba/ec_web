<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Admin configuration
*/
$config['project_name'] = 'Eternal Tokyo';

/**
* Upload module config (elFinder-v2.1.5)
*/
$config['upload_folder'] = 'uploads'; // Example: uploads => 'uploads' is root folder
$config['upload_thumb_folder'] = '../cache/uploads/thumbs';
$config['upload_URL'] = site_url('uploads'); // http://example.com/uploads
$config['upload_max_size'] = '5M';
$config['upload_allow_type'] = array('image', 'text/plain');