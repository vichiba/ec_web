<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* Admin Sidebar configurations
*/
$config['sidebar'] = array(
	__('Dashboard') => array(
		'key'=> 'dashboard', 'href' => admin_url('dashboard'), 'icon' => 'icon-screen2'
	),
	__('Slider') => array( //CMS module
		'key'=> 'slider_homepage', 'href' => admin_url('slider'), 'icon' => 'icon-newspaper', 
		'childs' => array(
			__('Homepage') => array('key' => 'slider_homepage', 'href' => admin_url('slider')),
			__('Mobile Category') => array('key' => 'slider_mobile', 'href' => admin_url('slider/mobile'))
		)
	),
	__('Notification') => array( //CMS module
		'key'=> 'notify_mobile', 'href' => admin_url('notification'), 'icon' => 'icon-newspaper', 
		'childs' => array(
			__('Mobile Notifies') => array('key' => 'notify_mobile', 'href' => admin_url('notification'))
		)
	),
	__('Import Data') => array( //CMS module
		'key'=> 'import_data', 'href' => admin_url('brands'), 'icon' => 'icon-newspaper', 
		'childs' => array(
			__('Category Auto Mapping') => array('key' => 'content_auto_mapping', 'href' => admin_url('category_auto_mapping')),
			__('Currency Converter') => array('key' => 'content_currency', 'href' => admin_url('currency')),
			__('CronTAB') => array('key' => 'manage_crontab', 'href' => admin_url('crontab')),
			__('Thirtparty Key') => array('key' => 'import_accesskey', 'href' => admin_url('import/accesskey')),
			__('Brand') => array('key' => 'import_brand', 'href' => admin_url('import/brand')),
			__('Product') => array('key' => 'import_product', 'href' => admin_url('import/product')),
		)
	),
	__('Customer') => array( //CMS module
		'key'=> 'customer', 'href' => admin_url('customer'), 'icon' => 'icon-newspaper', 
		'childs' => array(
			__('Management') => array('key' => 'customer', 'href' => admin_url('customer')),
		)
	),
	__('Report') => array( //CMS module
		'key'=> 'report_tracking', 'href' => admin_url('report'), 'icon' => 'icon-newspaper', 
		'childs' => array(
			__('Click Action') => array('key' => 'report_tracking', 'href' => admin_url('report')),
			__('CJ') => array('key' => 'report_cj', 'href' => admin_url('report/cj')),
			__('Rakuten') => array('key' => 'report_rakuten', 'href' => admin_url('report/rakuten')),
		)
	),
	__('Brand') => array( //CMS module
		'key'=> 'brands', 'href' => admin_url('brands'), 'icon' => 'icon-newspaper', 
		'childs' => array(
			__('New Brand') => array('key' => 'content_brand_add', 'href' => admin_url('brands/index/add')),
			__('Brand Management') => array('key' => 'content_brand', 'href' => admin_url('brands')),
		)
	),
	__('Catalog') => array( //CMS module
		'key'=> 'catalogs', 'href' => admin_url('catalogs'), 'icon' => 'icon-newspaper', 
		'childs' => array(
			__('New Catalog') => array('key' => 'content_catalog_add', 'href' => admin_url('catalogs/index/add')),
			__('Catalog Management') => array('key' => 'content_catalog', 'href' => admin_url('catalogs')),
			__('Catalog Mapping') => array('key' => 'content_catalog_mapping', 'href' => admin_url('catalogs/mapping')),
		)
	),
	__('Product') => array( //CMS module
		'key'=> 'products', 'href' => admin_url('products'), 'icon' => 'icon-newspaper', 
		'childs' => array(
			__('New Product') => array('key' => 'content_product_add', 'href' => admin_url('products/index/add')),
			__('Product Management') => array('key' => 'content_product', 'href' => admin_url('products')),
			__('Search Keywords') => array('key' => 'content_search_keywords', 'href' => admin_url('search_keywords')),
		)
	),
	__('Blog') => array( //CMS module
		'key'=> 'posts', 'href' => admin_url('posts'), 'icon' => 'icon-newspaper', 
		'childs' => array(
			__('New Post') => array('key' => 'content_post_add', 'href' => admin_url('posts/index/add')),
			__('Posts') => array('key' => 'content_post', 'href' => admin_url('posts')),
			__('Categories') => array('key' => 'content_category', 'href' => admin_url('categories')),
			__('FAQ Page') => array('key' => 'content_post_faq', 'href' => admin_url('posts/faq')),
			__('About Us Page') => array('key' => 'content_post_aboutus', 'href' => admin_url('posts/aboutus')),
			__('Privacy & Policy Page') => array('key' => 'content_post_privacy', 'href' => admin_url('posts/privacy')),
			__('Terms & Service Page') => array('key' => 'content_post_terms', 'href' => admin_url('posts/terms')),
		)
	),
	__('Admin Panel') => array( //Admin module
		'key'=> 'admin_panel', 'href' => admin_url('admin_panel'), 'icon' => 'icon-paragraph-justify2', 
		'childs' => array(
			__('Logo Configuration') => array('key'=> 'manage_logo', 'href' => admin_url('config/logo')),
			__('Email Configuration') => array('key'=> 'manage_email', 'href' => admin_url('email')),
			__('Manage Users') => array('key'=> 'manage_users', 'href' => admin_url('user')),
			__('Manage Groups') => array('key'=> 'manage_groups', 'href' => admin_url('group')),
			__('Manage Privileges') => array('key'=> 'manage_privileges', 'href' => admin_url('privileges')),
			__('Manage Files') => array('key'=> 'manage_files', 'href' => admin_url('files')),
			__('System config') => array('key'=> 'manage_system_config', 'href' => admin_url('config/system')),
			__('Website config') => array('key'=> 'manage_website_config', 'href' => admin_url('config/')),
//			__('System Logs') => array('key'=> 'system_activity', 'href' => admin_url('system_activity')),
		)
	),
	
);