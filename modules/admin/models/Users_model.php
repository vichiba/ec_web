<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users_model extends MY_Model{
 	 	
 	public $table = 'users';
	public $primary_key = 'user_id';

	public function login($username, $password){
		$conds = array(
			'user_login' => $username,
			'user_password' => md5($password)
		);
		return $this->exists($conds);
	}

	public function find($id){
		$this->db->select($this->table.'.*, GROUP_CONCAT(roles.role_filter) as role_filter');
		$this->db->from($this->table);
		$this->db->join('user_group', $this->table.'.user_id = user_group.user_id');
		$this->db->join('group_role', 'group_role.group_id = user_group.group_id');
		$this->db->join('roles', 'roles.role_id = group_role.role_id');
		$this->db->where($this->table.'.'.$this->primary_key, $id);
		return $this->db->get()->row();
	}

	public function set_remember_token($user_id, $remember_token, $expired_time){
		$user_data = array(
			'remember_token' => $remember_token,
			'remember_token_expired' => $expired_time,
		);
		return $this->update($user_id, $user_data);
	}
}
