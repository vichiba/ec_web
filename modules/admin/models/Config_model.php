<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Config_model extends MY_Model {
	public $table = 'config';
	public $primary_key = 'conf_id';

	const TYPE_SYSTEM 	= 1;
	const TYPE_WEBSITE 	= 2;
	const TYPE_3PARTY 	= 3;

	public function findByKey($key, $cache = FALSE){
		if($cache){
			$cached_object = $this->getCache($key);
			if($cached_object){
				return $cached_object;
			}
		}
		if( $conf_id = $this->exists(['conf_key' => $key, 'conf_status' => SELF::STATUS_ACTIVE]) ){
			$cached_object = $this->find($conf_id);
			if($cache){
				echo 'set to cache';
				$this->setCache($key, $cached_object);
			}
			return $cached_object;
		}
		return NULL;
	}

	public function updateByKey($key, $value){
		$this->db->where('conf_key', $key);
		$this->db->set('conf_value', trim($value));
		return $this->db->update($this->table);
	}
}