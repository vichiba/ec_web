<?php $this->load->view('header');?>
<?php $this->load->view('navbar');?>

<!-- Page container -->
<div class="page-container">

	<?php $this->load->view('sidebar')?>

	<!-- Page content -->
 	<div class="page-content">

 		<?php $this->load->view('page_header');?>
		<?php $this->load->view('breadcrumbs');?>

		<?php echo $this->load->view('modules/'.$module, '', TRUE)?>

        <?php $this->load->view('copyright');?>
	</div>
	<!-- /page content -->
</div>
<!-- /page container -->
<?php $this->load->view('footer');?>