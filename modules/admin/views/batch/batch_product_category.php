<!DOCTYPE html>
<html>
<head>
	<title>Import Batch</title>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
</head>
<body>
<?php 
/*
<h4>Page: <?php echo $page_number ?></h4>
<h4>Current: <?php echo number_format( (int)$page_number * (int)$records_per_page ).'/'.number_format($total) ?></h4>
<h3>Redirecting <label style="color: red"></label></h3>
<script type="text/javascript">
$(function(){
	<?php if( (int)$result == 0 ): 
			$page_number -=1;
	?>
	var page = <?php echo $page_number ?>;
	var page_size = 1000;
	var status = 'joined';
	var max_wait = 2*60;
	var max_request = 10;
	$('h3').hide();
	setTimeout(function(){
		window.location.href = "<?php echo site_url(uri_string())?>?status="+status+"&page_number="+(page+1)+"&records_per_page="+page_size;
	}, max_wait*1000 );

	var count = max_wait;
	$('h3').show();
	$('h3 label').text(count--);

	setInterval(function(){
		$('h3 label').text(count--);
	}, 1000 );

	<?php else:?>
	
	var page = <?php echo $page_number ?>;
	var page_size = 1000;
	var status = 'joined';
	var max_wait = 5*60;
	var max_request = 10;
	$('h3').hide();
	setTimeout(function(){
		window.location.href = "<?php echo site_url(uri_string())?>?status="+status+"&page_number="+(page+1)+"&records_per_page="+page_size;
	}, ( (page % max_request == 0) ? max_wait*1000 : 1000 ) );

	if( (page % max_request) == 0 ){
		var count = max_wait;
		$('h3').show();
		$('h3 label').text(count--);

		setInterval(function(){
			$('h3 label').text(count--);
		}, 1000 );
	}

	<?php endif;?>
});
</script>
*/
 ?>
 <br/>
 <br/>
 <br/>
 <div class="container">
 	<form action="" method="POST">
 		<textarea cols="30" rows="20" class="form-control" name="xml"></textarea>
 		<p></p>
 		<button type="submit">Submit</button>
 	</form>
 </div>
</body>
</html>