<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-migrate/1.2.1/jquery-migrate.min.js"></script>

<script type="text/javascript" src="<?php echo res('js/plugins/charts/sparkline.min.js') ?>"></script>

<script type="text/javascript" src="<?php echo res('js/plugins/forms/uniform.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo res('js/plugins/forms/select2.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo res('js/plugins/forms/inputmask.js') ?>"></script>
<script type="text/javascript" src="<?php echo res('js/plugins/forms/autosize.js') ?>"></script>
<script type="text/javascript" src="<?php echo res('js/plugins/forms/inputlimit.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo res('js/plugins/forms/listbox.js') ?>"></script>
<script type="text/javascript" src="<?php echo res('js/plugins/forms/multiselect.js') ?>"></script>
<script type="text/javascript" src="<?php echo res('js/plugins/forms/validate.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo res('js/plugins/forms/tags.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo res('js/plugins/forms/switch.min.js') ?>"></script>

<script type="text/javascript" src="<?php echo res('js/plugins/forms/uploader/plupload.full.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo res('js/plugins/forms/uploader/plupload.queue.min.js') ?>"></script>

<script type="text/javascript" src="<?php echo res('js/plugins/forms/wysihtml5/wysihtml5.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo res('js/plugins/forms/wysihtml5/toolbar.js') ?>"></script>

<script type="text/javascript" src="<?php echo res('js/plugins/interface/daterangepicker.js') ?>"></script>
<script type="text/javascript" src="<?php echo res('js/plugins/interface/fancybox.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo res('js/plugins/interface/moment.js') ?>"></script>
<script type="text/javascript" src="<?php echo res('js/plugins/interface/jgrowl.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo res('js/plugins/interface/datatables.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo res('js/plugins/interface/colorpicker.js') ?>"></script>
<script type="text/javascript" src="<?php echo res('js/plugins/interface/fullcalendar.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo res('js/plugins/interface/timepicker.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo res('js/bootstrap.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo res('js/admin.application.js') ?>"></script>

<?php 
if (! empty($js_files)) {
	foreach($js_files as $file){
		echo '<script type="text/javascript"  src="'.$file.'""></script>';
	}
}?>

<?php
if( !empty($js_inline) ){
	echo '<script type="text/javascript">'.$js_inline.'</script>';
}
?>

</body>
</html>