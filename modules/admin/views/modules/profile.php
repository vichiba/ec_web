<div class="row">
	<div class="col-md-6 col-md-offset-2">
		<form class="form-horizontal" action="" method="POST" autocomplete="OFF">
			<div class="form-group">
				<?php echo $message ? '<div class="callout callout-info fade in">'.$message.'</div>' : ''; ?>
				<?php echo $error_message ? '<div class="callout callout-danger fade in">'.$error_message.'</div>' : ''; ?>
			</div>
		  	<div class="form-group">
		    	<label for="inputPassword3" class="col-sm-3 control-label"><?php echo __('Current password') ?></label>
		    	<div class="col-sm-9">
		      		<input type="password" name='user_password' class="form-control" placeholder="<?php echo __('Enter your current password') ?>"/>
		    	</div>
		  	</div>
		  	<div class="form-group">
		    	<label for="inputPassword3" class="col-sm-3 control-label"><?php echo __('New password') ?></label>
		    	<div class="col-sm-9">
		      		<input type="password" name='new_password' class="form-control" placeholder="<?php echo __('Enter your new password') ?>"/>
		    	</div>
		  	</div>
		  	<div class="form-group">
		    	<label for="inputPassword3" class="col-sm-3 control-label"><?php echo __('Confirm password') ?></label>
		    	<div class="col-sm-9">
		      		<input type="password" name='confirm_password' class="form-control" placeholder="<?php echo __('Confirm your password') ?>"/>
		    	</div>
		  	</div>
		  	<div class="form-group">
		    	<div class="col-sm-9 col-md-offset-3">
		      		<button type="submit" class="btn btn-xs btn-primary"><?php echo __('Save') ?></button>
		    	</div>
		  	</div>
		</form>
	</div>
</div>