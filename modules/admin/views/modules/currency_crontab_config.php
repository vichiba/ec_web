<?php if (! empty($message)) { ?>
<div id="message">
	<?php echo $message; ?>
</div>
<?php } ?>

<div class="row text-center">
	<div class="btn-group">
		<a href="?tab=management" class="btn btn-<?php echo $tab == "management" ? "success" : "default" ?>"><?php echo __('Management') ?></a>
		<a href="?tab=auto" class="btn btn-<?php echo $tab == "auto" ? "success" : "default" ?>"><?php echo __('Automatic Schedule') ?></a>
	</div>
</div>
<br/><br/>
<div class="row">
	<div class="col-md-10 col-md-offset-2">
		<form class="form-horizontal" action="" method="POST">
			<input type="hidden" name="cronjob" value="<?php echo $cronjob?>"/>
			<div class="form-group">
				<?php echo $message ? '<div class="callout callout-info fade in">'.$message.'</div>' : ''; ?>
				<!-- <pre><?php echo $joblists ?></pre> -->
			</div>
		  	<div class="form-group">
		    	<label for="inputPassword3" class="col-sm-2 control-label"><?php echo __('Choose Times') ?></label>
		    	<div class="col-sm-9">
		      		<p class="form-control-static"><div id="cronjob"></div></p>
		    	</div>
		  	</div>
		  	<div class="form-group">
		    	<div class="col-sm-9 col-md-offset-2">
		      		<button type="submit" class="btn btn-xs btn-primary"><?php echo __('Save configuration') ?></button>
		    	</div>
		  	</div>
		</form>
	</div>
</div>