<h2>Manage Privileges</h2>
				<a href="auth_admin/insert_privilege">Insert New Privilege</a>

			<?php if (! empty($message)) { ?>
				<div id="message">
					<?php echo $message; ?>
				</div>
			<?php } ?>
				
				<?php echo form_open(current_url());	?>  	
				<div class="datatable-tasks">
					<table class="table table-bordered table-hover" data-searching="true" data-page-length='5' data-processing="true" data-server-side="true" data-ajax="/admin/privileges/ajax">
						<thead>
							<tr>
								<th><?php echo __('Privilege Name'); ?></th>
								<th><?php echo __('Description'); ?></th>
								<th width="20"><?php echo __('Delete'); ?></th>
							</tr>
						</thead>
						<tbody>
						<?php foreach ($privileges as $privilege) { ?>
							<tr>
								<td>
									<a href="auth_admin/update_privilege/<?php echo $privilege[$this->flexi_auth->db_column('user_privileges', <?php echo __('id'))];?>">
										<?php echo $privilege[$this->flexi_auth->db_column('user_privileges', <?php echo __('name'))];?>
									</a>
								</td>
								<td><?php echo $privilege[$this->flexi_auth->db_column('user_privileges', <?php echo __('description'))];?></td>
								<td class="text-center">
								<?php if ($this->flexi_auth->is_privileged('Delete Users')) { ?>
									<input type="checkbox" name="delete_privilege[<?php echo $privilege[$this->flexi_auth->db_column('user_privileges', 'id')];?>]" value="1"/>
								<?php } else { ?>
									<input type="checkbox" disabled="disabled"/>
									<small>Not Privileged</small>
									<input type="hidden" name="delete_privilege[<?php echo $privilege[$this->flexi_auth->db_column('user_privileges', 'id')];?>]" value="0"/>
								<?php } ?>
								</td>
							</tr>
						<?php } ?>
						</tbody>
						<tfoot>
							<td colspan="3">
								<?php $disable = (! $this->flexi_auth->is_privileged('Update Privileges') && ! $this->flexi_auth->is_privileged('Delete Privileges')) ? 'disabled="disabled"' : NULL;?>
								<input type="submit" name="submit" value="Delete Checked Privileges" class="link_button large" <?php echo $disable; ?>/>
							</td>
						</tfoot>
					</table>
					</div>
				<?php echo form_close();?>