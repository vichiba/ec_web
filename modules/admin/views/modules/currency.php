<?php if (! empty($message)) { ?>
<div id="message">
	<?php echo $message; ?>
</div>
<?php } ?>

<div class="row text-center">
	<div class="btn-group">
		<a href="?tab=management" class="btn btn-<?php echo $tab == "management" ? "success" : "default" ?>"><?php echo __('Management') ?></a>
		<a href="?tab=auto" class="btn btn-<?php echo $tab == "auto" ? "success" : "default" ?>"><?php echo __('Automatic Schedule') ?></a>
	</div>
</div>
<br/><br/>
<?php echo $output;?>