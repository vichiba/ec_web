<div class="row">
	<div class="col-md-8 col-md-offset-1">
		<form class="form-horizontal" action="" method="GET">
		  	<div class="form-group">
		    	<label for="inputEmail3" class="col-sm-3 control-label"><?php echo __('Date Type') ?></label>
		    	<div class="col-sm-9">
		      		<select name='date_type' class="form-control" >
						<option value="event" <?php echo $this->input->get('date_type') === "event" ? "selected" : "" ?> ><?php echo __('Event') ?></option>
						<option value="posting" <?php echo $this->input->get('date_type') === "posting" ? "selected" : "" ?> ><?php echo __('Posting') ?></option>
					</select>
		    	</div>
		  	</div>
		  	<div class="form-group">
		    	<label for="inputPassword3" class="col-sm-3 control-label"><?php echo __('Advertiser CID') ?></label>
		    	<div class="col-sm-9">
		      		<input type="text" name='cids' class="form-control" value="<?php echo $this->input->get('advertiser_name') ?>" placeholder="Advertiser CID"/>
		    	</div>
		  	</div>
		  	<div class="form-group">
		  		<div class="col-sm-3">
		    		<label for="inputPassword3" class="control-label"><?php echo __('Date Range') ?></label>
		    		<br/>
					<small>YYYY-MM-DD</small>
				</div>
		    	<div class="col-sm-9">
		    		<div class="row">
			      		<div class="col-sm-6">
					    	<input type="text" name='start_date' class="form-control" value="<?php echo $this->input->get('start_date') ? $this->input->get('start_date') : '' ?>" placeholder="<?php echo __('Start Date') ?>"/>
					  	</div>
					  	<div class="col-sm-6">
					    	<input type="text" name='end_date' class="form-control" value="<?php echo $this->input->get('end_date') ? $this->input->get('end_date') : '' ?>" placeholder="<?php echo __('End Date') ?>"/>
					  	</div>
				  	</div>
		    	</div>
		  	</div>
		  	<div class="form-group">
		    	<label for="inputPassword3" class="col-sm-3 control-label"><?php echo __('Commission ID') ?></label>
		    	<div class="col-sm-9">
		      		<input type="text" name='commission_id' class="form-control" value="<?php echo $this->input->get('commission_id') ?>" placeholder="<?php echo __('Commissions CID') ?>"/>
		    	</div>
		  	</div>
		  	<div class="form-group">
		    	<label for="inputPassword3" class="col-sm-3 control-label"><?php echo __('Action Type') ?></label>
		    	<div class="col-sm-9">
		    		<div class="row">
			      		<div class="col-sm-6">
				      		<select name="action_types" class="form-control">
				      			<option value=""><?php echo __('All Type') ?></option>
								<option value="bonus"><?php echo __('Bonus') ?></option>
								<option value="click"><?php echo __('Click') ?></option>
								<option value="impression"><?php echo __('Impression') ?></option>
								<option value="sale"><?php echo __('Sale') ?></option>
								<option value="lead"><?php echo __('Lead') ?></option>
								<option value="advanced sale"><?php echo __('Advanced Sale') ?></option>
								<option value="advanced lead"><?php echo __('Advanced Lead') ?></option>
								<option value="performance incentive"><?php echo __('Performance Incentive') ?></option>
				      		</select>
				      	</div>
				      	<div class="col-sm-6">
				      		<select name="action_status" class="form-control">
				      			<option value=""><?php echo __('All Status') ?></option>
								<option value="new"><?php echo __('New') ?></option>
								<option value="locked"><?php echo __('Locked') ?></option>
								<option value="extended"><?php echo __('Extended') ?></option>
								<option value="closed"><?php echo __('Closed') ?></option>
				      		</select>
				      	</div>
				      </div>
		    	</div>
		  	</div>
		  	<div class="form-group">
		    	<label for="inputPassword3" class="col-sm-3 control-label"><?php echo __('Ad ID') ?></label>
		    	<div class="col-sm-9">
		      		<input type="text" name='aids' class="form-control" value="<?php echo $this->input->get('aids') ?>" placeholder="<?php echo __('Ad ID') ?>"/>
		    	</div>
		  	</div>
		  	<div class="form-group">
		    	<div class="col-sm-9 col-md-offset-3">
		      		<button type="submit" class="btn btn-xs btn-primary"><?php echo __('Search') ?></button>
		    	</div>
		  	</div>
		  </form>
		</form>
	</div>
</div>
<hr/>
<div class="row">
	<div class="col-md-12">
		<table class="data-table">
			<thead>
				<th><?php echo __('Commission ID') ?></th>
				<th><?php echo __('Advertiser Name') ?></th>
				<th><?php echo __('Action Type') ?></th>
				<th><?php echo __('Sale Amount (USD)') ?></th>
				<th><?php echo __('Order Discount (USD)') ?></th>
				<th><?php echo __('Publisher Commission (USD)') ?></th>
				<th><?php echo __('Status') ?></th>
				<th><?php echo __('Action Name') ?></th>
				<th><?php echo __('Ad ID') ?></th>
				<th><?php echo __('Website ID') ?></th>
				<th><?php echo __('Event Date') ?></th>
			</thead>
			<tbody>
				<?php foreach ($commissions as $item): ?>
				<tr>
					<td><?php echo $item->{'commission-id'} ?></td>
					<td><?php echo $item->{'advertiser-name'} ?></td>
					<td><?php echo $item->{'action-type'} ?></td>
					<td><?php echo $item->{'sale-amount'} ?></td>
					<td><?php echo $item->{'order-discount'} ?></td>
					<td><?php echo $item->{'commission-amount'} ?></td>
					<td><?php echo $item->{'action-status'} ?></td>
					<td><?php echo $item->{'action-tracker-name'} ?></td>
					<td><?php echo $item->{'aid'} ?></td>
					<td><?php echo $item->{'website-id'} ?></td>
					<td><?php echo $item->{'event-date'} ?></td>
				</tr>
				<?php endforeach ?>
			</tbody>
		</table>
	</div>
</div>
