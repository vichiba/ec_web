<div class="row">
	<div class="col-md-8 col-md-offset-1">
		<form class="form-horizontal" action="" method="GET">
		  	<div class="form-group">
		    	<label for="inputEmail3" class="col-sm-3 control-label">Report Type</label>
		    	<div class="col-sm-9">
		      		<select name='reportid' class="form-control" >
						<option value="4" <?php echo $this->input->get('reportid') === "4" ? "selected" : "" ?> ><?php echo __('Sales & Activity') ?></option>
						<option value="5" <?php echo $this->input->get('reportid') === "5" ? "selected" : "" ?> ><?php echo __('Revenue') ?></option>
						<option value="6" <?php echo $this->input->get('reportid') === "6" ? "selected" : "" ?> ><?php echo __('Link Type') ?></option>
						<option value="7" <?php echo $this->input->get('reportid') === "7" ? "selected" : "" ?> ><?php echo __('Individual Item') ?></option>
						<option value="8" <?php echo $this->input->get('reportid') === "8" ? "selected" : "" ?> ><?php echo __('Product Success') ?></option>
						<option value="9" <?php echo $this->input->get('reportid') === "9" ? "selected" : "" ?> ><?php echo __('Program Level') ?></option>
						<option value="10" <?php echo $this->input->get('reportid') === "10" ? "selected" : "" ?> ><?php echo __('Non-Commissionable Sales Report') ?></option>
						<option value="11" <?php echo $this->input->get('reportid') === "11" ? "selected" : "" ?> ><?php echo __('Signature Activity Report') ?></option>
						<option value="12" <?php echo $this->input->get('reportid') === "12" ? "selected" : "" ?> ><?php echo __('Signature Order') ?></option>
						<option value="14" <?php echo $this->input->get('reportid') === "14" ? "selected" : "" ?> ><?php echo __('Media Optimization Report') ?></option>
					</select>
		    	</div>
		  	</div>
		  	<div class="form-group">
		    	<label for="inputPassword3" class="col-sm-3 control-label"><?php echo __('Network') ?></label>
		    	<div class="col-sm-9">
		      		<select name='nid' class="form-control" >
		      			<option value="1"><?php echo __('for US') ?></option>
						<option value="5"><?php echo __('for Canada')?></option>
						<option value="8"><?php echo __('for Brazil')?></option>
						<option value="41"><?php echo __('for Australia')?></option>
						<option value="3"><?php echo __('for UK')?></option>
						<option value="7"><?php echo __('for France')?></option>
						<option value="9"><?php echo __('for Germany')?></option>
		      		</select>
		    	</div>
		  	</div>
		  	<div class="form-group">
		    	<label for="inputPassword3" class="col-sm-3 control-label"><?php echo __('Merchant ID') ?></label>
		    	<div class="col-sm-9">
		      		<input type="text" name='mid' class="form-control" value="<?php echo $this->input->get('mid') ?>" placeholder="<?php echo __('Merchant CID') ?>"/>
		    	</div>
		  	</div>
		  	<div class="form-group">
		  		<div class="col-sm-3">
		    		<label for="inputPassword3" class="control-label"><?php echo __('Date Range') ?></label>
		    		<br/>
					<small>YYYY-MM-DD</small>
				</div>
		    	<div class="col-sm-9">
		    		<div class="row">
			      		<div class="col-sm-6">
					    	<input type="text" name='bdate' class="form-control" value="<?php echo $this->input->get('bdate') ? $this->input->get('bdate') : '' ?>" placeholder="<?php echo __('Start Date') ?>"/>
					  	</div>
					  	<div class="col-sm-6">
					    	<input type="text" name='edate' class="form-control" value="<?php echo $this->input->get('edate') ? $this->input->get('edate') : '' ?>" placeholder="<?php echo __('End Date') ?>"/>
					  	</div>
				  	</div>
		    	</div>
		  	</div>
		  	<div class="form-group">
		    	<div class="col-sm-9 col-md-offset-3">
		      		<button type="submit" class="btn btn-xs btn-primary"><?php echo __('Search') ?></button>
		    	</div>
		  	</div>
		  </form>
		</form>
	</div>
</div>
<hr/>
<div class="row">
	<div class="col-md-12">
		<table class="data-table">
			<thead>
				<th><?php echo __('Commission ID') ?></th>
				<th><?php echo __('Advertiser Name'); ?></th>
				<th><?php echo __('Action Type'); ?></th>
				<th><?php echo __('Sale Amount (USD)'); ?></th>
				<th><?php echo __('Order Discount (USD)'); ?></th>
				<th><?php echo __('Publisher Commission (USD)'); ?></th>
				<th><?php echo __('Status'); ?></th>
				<th><?php echo __('Action Name'); ?></th>
				<th><?php echo __('Ad ID'); ?></th>
				<th><?php echo __('Website ID'); ?></th>
				<th><?php echo __('Event Date'); ?></th>
			</thead>
			<tbody>
				<?php foreach ($commissions as $item): ?>
				<tr>
					<td><?php echo $item->{'commission-id'} ?></td>
					<td><?php echo $item->{'advertiser-name'} ?></td>
					<td><?php echo $item->{'action-type'} ?></td>
					<td><?php echo $item->{'sale-amount'} ?></td>
					<td><?php echo $item->{'order-discount'} ?></td>
					<td><?php echo $item->{'commission-amount'} ?></td>
					<td><?php echo $item->{'action-status'} ?></td>
					<td><?php echo $item->{'action-tracker-name'} ?></td>
					<td><?php echo $item->{'aid'} ?></td>
					<td><?php echo $item->{'website-id'} ?></td>
					<td><?php echo $item->{'event-date'} ?></td>
				</tr>
				<?php endforeach ?>
			</tbody>
		</table>
	</div>
</div>
