<div class="row">
	<div class="col-md-4 col-md-offset-2">
		<form class="form-horizontal" action="" method="POST">
			<div class="form-group">
				<?php echo $message ? '<div class="callout callout-info fade in">'.$message.'</div>' : ''; ?>
			</div>
		  	<div class="form-group">
		    	<label for="inputPassword3" class="col-sm-3 control-label"><?php echo __('SMTP Host') ?></label>
		    	<div class="col-sm-9">
		      		<input type="text" name='smtp_host' class="form-control" value="<?php echo $smtp_host?>" placeholder="ssl://smtp.googlemail.com"/>
		    	</div>
		  	</div>
		  	<div class="form-group">
		    	<label for="inputPassword3" class="col-sm-3 control-label"><?php echo __('SMTP Port') ?></label>
		    	<div class="col-sm-9">
		      		<input type="number" name='smtp_port' class="form-control" value="<?php echo $smtp_port?>" placeholder="465"/>
		    	</div>
		  	</div>
		  	<div class="form-group">
		    	<label for="inputPassword3" class="col-sm-3 control-label"><?php echo __('User Name') ?></label>
		    	<div class="col-sm-9">
		      		<input type="text" name='smtp_user' class="form-control" value="<?php echo $smtp_user?>" placeholder="Email username"/>
		    	</div>
		  	</div>
		  	<div class="form-group">
		    	<label for="inputPassword3" class="col-sm-3 control-label"><?php echo __('User Password') ?></label>
		    	<div class="col-sm-9">
		      		<input type="text" name='smtp_pass' class="form-control" value="<?php echo $smtp_pass?>" placeholder="Email password"/>
		    	</div>
		  	</div>
		  	<div class="form-group">
		    	<div class="col-sm-9 col-md-offset-3">
		      		<button type="submit" class="btn btn-xs btn-primary"><?php echo __('Save configuration') ?></button>
		    	</div>
		  	</div>
		</form>
	</div>
</div>