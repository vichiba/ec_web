<!-- Info blocks -->
<ul class="info-blocks">
	<li class="bg-primary">
		<div class="top-info">
			<a href="<?php echo admin_url('posts/index/add'); ?>"><?php echo __('New Post') ?></a>
			<small><?php echo __('Posts') ?></small>
		</div>
		<a href="<?php echo admin_url('posts/index/add'); ?>"><i class="icon-pencil"></i></a>
		<span class="bottom-info bg-danger"><?php echo __('New Post') ?></span>
	</li>
	<li class="bg-success">
		<div class="top-info">
			<a href="<?php echo admin_url('config'); ?>"><?php echo __('Website configs') ?></a>
			<small><?php echo __('Site parameters') ?></small>
		</div>
		<a href="<?php echo admin_url('config'); ?>"><i class="icon-cogs"></i></a>
		<span class="bottom-info bg-primary"><?php echo __('Site parameters') ?></span>
	</li>
	<li class="bg-danger">
		<div class="top-info">
			<a href="<?php echo admin_url('report'); ?>"><?php echo __('Report') ?></a>
			<small><?php echo __('Click Action') ?></small>
		</div>
		<a href="<?php echo admin_url('report'); ?>"><i class="icon-stats2"></i></a>
		<span class="bottom-info bg-primary"><?php echo __('Click Action') ?></span>
	</li>
</ul>
<!-- /info blocks -->