<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>elFinder 2.0</title>

		<!-- jQuery and jQuery UI (REQUIRED) -->
		<link rel="stylesheet" type="text/css" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
		<style type="text/css">
		body{margin: 0;}
		</style>
		<!-- elFinder CSS (REQUIRED) -->

		<link rel="stylesheet" type="text/css" href="/assets/elfinder/css/elfinder.min.css">
		<link rel="stylesheet" type="text/css" href="/assets/elfinder/themes/windows-10/css/theme.css">

		<!-- elFinder JS (REQUIRED) -->
		<script src="/assets/elfinder/js/elfinder.min.js"></script>


	</head>
	<body>

		<!-- Element where elFinder will be created (REQUIRED) -->
		<div id="elfinder"></div>

		<script type="text/javascript" charset="utf-8">
		    // Helper function to get parameters from the query string.
		    function getUrlParam(paramName) {
		        var reParam = new RegExp('(?:[\?&]|&amp;)' + paramName + '=([^&]+)', 'i') ;
		        var match = window.location.search.match(reParam) ;

		        return (match && match.length > 1) ? match[1] : '' ;
		    }

		    $().ready(function() {
		        var funcNum = getUrlParam('CKEditorFuncNum');

		        var elf = $('#elfinder').elfinder({
		            url : '<?php echo admin_url('files/action')?>',
		            getFileCallback : function(file) {
		                try{
		                	window.opener.CKEDITOR.tools.callFunction(funcNum, file.url);
		                }catch(e){}
		                try{
		                	window.opener.elFinderPopup.callBack(file.url);
		                }catch(e){}
		                window.close();
		            },
		            resizable: true
		        }).elfinder('instance');


		        $elfinder = $('#elfinder');
				var $window = $(window);
				$window.resize(function(){
				    var win_height = $window.height();
				    if( $elfinder.height() != win_height ){
				        $elfinder.height(win_height-5).resize();
				    }
				})
		    });
		</script>
	</body>
</html>
