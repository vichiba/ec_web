<?php if (! empty($message)) { ?>
	<div id="message">
		<?php echo $message; ?>
	</div>
<?php } ?>
<div class="tabbable page-tabs">
	<ul class="nav nav-tabs">
		<li <?php echo $child_module === 'active_user' ? 'class="active"':''?> >
			<a href="/admin/user_activity/list_user_active"><i class="icon-checkmark"></i> <?php echo __('Active users') ?></a>
		</li>
		<li <?php echo $child_module === 'inactive_user' ? 'class="active"':''?> >
			<a href="/admin/user_activity/list_user_inactive"><i class="icon-checkmark2"></i> <?php echo __('Inactive users') ?></a>
		</li>
		<li <?php echo $child_module === 'unactive_user' ? 'class="active"':''?> >
			<a href="/admin/user_activity/list_user_unactive"><i class="icon-checkbox-unchecked"></i> <?php echo __('Unactivated users') ?></a>
		</li>
		<li <?php echo $child_module === 'failed_login_user' ? 'class="active"':''?> >
			<a href="/admin/user_activity/list_failed_login_users"><i class="icon-warning"></i> <?php echo __('High number of failed login users') ?></a>
		</li>
	</ul>
	<div class="tab-content">
		<!-- First tab content -->
		<div class="tab-pane active fade in">
			<!-- Default datatable inside panel -->
			<div class="panel panel-default">
				<?php $this->load->view('modules/user_activity/'.$child_module);?>
			</div>
		</div>
	</div>
</div>