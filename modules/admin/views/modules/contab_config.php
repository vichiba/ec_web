<div class="row">
	<div class="col-md-10 col-md-offset-2">
		<form class="form-horizontal" action="" method="POST">
			<input type="hidden" name="cronjob" value="<?php echo $cronjob?>"/>
			<div class="form-group">
				<?php echo $message ? '<div class="callout callout-info fade in">'.$message.'</div>' : ''; ?>
				<!-- <pre><?php echo $joblists ?></pre> -->
			</div>
		  	<div class="form-group">
		    	<label for="inputPassword3" class="col-sm-2 control-label"><?php echo __('Choose Times') ?></label>
		    	<div class="col-sm-9">
		      		<p class="form-control-static"><div id="cronjob"></div></p>
		    	</div>
		  	</div>
		  	<div class="form-group">
		    	<div class="col-sm-9 col-md-offset-2">
		      		<button type="submit" class="btn btn-xs btn-primary"><?php echo __('Save configuration') ?></button>
		    	</div>
		  	</div>
		</form>
	</div>
</div>
<hr/>
<div class="row">
	<div class="list-group col-md-2">
	<?php foreach ($listFiles as $filename): ?>
		<a href="<?php echo admin_url('crontab?f='.$filename->log_str) ?>" class="list-group-item <?php echo $this->input->get('f') == $filename->log_str ? 'active': '' ?>"><?php echo $filename->log_str ?></a>
	<?php endforeach ?>
	</div>
	<div class="col-md-10">
		<div class="terminal_loadmore"><a href="#"><?php echo __('Loadmore') ?></a></div>
		<div id="terminal">
		</div>
	</div>
</div>
<br/><br/><br/><br/><br/><br/>