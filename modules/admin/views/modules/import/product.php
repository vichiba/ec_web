<?php 
/*

[ad-id] => 10405970
[advertiser-id] => 1599858
[advertiser-name] => SuperJeweler
[advertiser-category] => Jewelry
[buy-url] => http://www.kqzyfj.com/click-7984980-10405970-1434739860496?url=http%3A%2F%2Fwww.superjeweler.com%2FDetails%2FIndex%2F16364%2F2ct-halo-diamond-engagement-ring-crafted-in-14-karat-rose-gold.html&cjsku=16364
[catalog-id] => cjo:1166
[currency] => USD
[description] => For a classically beautiful diamond engagement ring, try this engagement ring crafted in 14 karat rose gold.   This over the top ring boasts a .75 carat center surrounded by a halo of diamonds, and accented with an additional artistic border of shimmering diamonds.  The ring has a total diamond weight of 2.01 carats.  There are 129 diamonds total in I/J color and I1/I2 clarity.  The ring is crafted in solid 14 karat white gold and is available in ring sizes 4-9.
[image-url] => http://www.superjeweler.com/images/products/700X700/pic16364-1.jpg
[in-stock] => true
[isbn] => SimpleXMLElement Object
    (
    )

[manufacturer-name] => SimpleXMLElement Object
    (
    )

[manufacturer-sku] => SimpleXMLElement Object
    (
    )

[name] => 2ct Halo Diamond Engagement Ring Crafted in 14 Karat Rose Gold
[price] => 2999.0
[retail-price] => 4999.0
[sale-price] => 2499.0
[sku] => 16364
[upc] => SimpleXMLElement Object
    (
    )
 */
?>
<div class="row">
	<div class="col-md-12">
		<div class="btn-group" role="group">
			<a href="<?php echo site_url('admin/import/product/skimlinks') ?>" class="btn btn-<?php echo $vendor === 'skimlinks' ? 'primary' : 'default'?>" disabled>Skimlinks</a>
			<a href="<?php echo site_url('admin/import/product/cj') ?>" class="btn btn-<?php echo $vendor === 'cj' ? 'primary' : 'default'?>">CJ</a>
			<a href="<?php echo site_url('admin/import/product/rakuten') ?>" class="btn btn-<?php echo $vendor === 'rakuten' ? 'primary' : 'default'?>">Rakuten</a>
		</div>
	</div>
</div>
<p>&nbsp;</p>
<div class="row">
	<div class="col-md-6 col-md-offset-2">
		<form class="form-horizontal" action="" method="GET">
		<?php switch ($vendor) {
			case 'cj':
		?>
		  	<div class="form-group">
		    	<label for="inputEmail3" class="col-sm-3 control-label"><?php echo __('Advertiser Type');?></label>
		    	<div class="col-sm-9">
		      		<select name='advertiser_ids' class="form-control" >
						<option value="notjoined" <?php echo $this->input->get('advertiser_ids') === "notjoined" ? "selected" : "" ?> ><?php echo __('Not Joined');?></option>
						<option value="joined" <?php echo $this->input->get('advertiser_ids') === "joined" ? "selected" : "" ?> ><?php echo __('Joined');?></option>
						<option value="CIDs" <?php echo !in_array($this->input->get('advertiser_ids'), ["", "joined", "notjoined"]) ? "selected" : "" ?> ><?php echo __('CIDs');?></option>
					</select>
					<input type="text" class="form-control" name="advertiser_ids" value="<?php echo !in_array($this->input->get('advertiser_ids'), ["joined", "notjoined"]) ? $this->input->get('advertiser_ids') : "" ?>" style="display: none">
		    	</div>
		  	</div>
		  	<div class="form-group">
		    	<label class="col-sm-3 control-label">Keywords</label>
		    	<div class="col-sm-9">
		      		<input type="text" name='keywords' class="form-control" value="<?php echo $this->input->get('keywords') ?>" placeholder="<?php echo __('Keyword for searching');?>"/>
		    	</div>
		  	</div>
		  	<div class="form-group">
		    	<label class="col-sm-3 control-label">ISBN/UPC</label>
		    	<div class="col-sm-9">
		    		<div class="row">
			      		<div class="col-sm-6">
					    	<input type="text" name='isbn' class="form-control" value="<?php echo $this->input->get('isbn') ?>" placeholder="<?php echo __('ISBN Number');?>"/>
					  	</div>
					  	<div class="col-sm-6">
					    	<input type="text" name='upc' class="form-control" value="<?php echo $this->input->get('upc') ?>" placeholder="<?php echo __('UPC Number');?>"/>
					  	</div>
				  	</div>
		    	</div>
		  	</div>
		  	<div class="form-group">
		    	<label class="col-sm-3 control-label"><?php echo __('Price');?></label>
		    	<div class="col-sm-9">
		    		<div class="row">
			      		<div class="col-sm-6">
					    	<input type="text" name='low_price' class="form-control" value="<?php echo $this->input->get('low_price') ?>" placeholder="<?php echo __('Low Price');?>"/>
					  	</div>
					  	<div class="col-sm-6">
					    	<input type="text" name='high_price' class="form-control" value="<?php echo $this->input->get('high_price') ?>" placeholder="<?php echo __('High Price');?>"/>
					  	</div>
				  	</div>
		    	</div>
		  	</div>
		  	<div class="form-group">
		    	<label class="col-sm-3 control-label"><?php echo __('Sale Price');?></label>
		    	<div class="col-sm-9">
		    		<div class="row">
			      		<div class="col-sm-6">
					    	<input type="text" name='low_sale_price' class="form-control" value="<?php echo $this->input->get('low_sale_price') ?>" placeholder="<?php echo __('Low Sale Price');?>"/>
					  	</div>
					  	<div class="col-sm-6">
					    	<input type="text" name='high_sale_price' class="form-control" value="<?php echo $this->input->get('high_sale_price') ?>" placeholder="<?php echo __('High Sale Price');?>"/>
					  	</div>
				  	</div>
		    	</div>
		  	</div>
		  	<div class="form-group">
		    	<label class="col-sm-3 control-label"><?php echo __('Sort by');?></label>
		    	<div class="col-sm-9">
		    		<div class="row">
			      		<div class="col-sm-6">
					    	<select name='sort_by' class="form-control">
					    		<option value="<?php echo CJConnector::SORT_TYPE_DEFAULT ?>" 
					    			<?php echo $this->input->get('sort_by') == CJConnector::SORT_TYPE_DEFAULT ? "selected" : "" ?>
					    			><?php echo __('Default');?></option>
								<option value="<?php echo CJConnector::SORT_TYPE_NAME ?>"
									 <?php echo $this->input->get('sort_by') == CJConnector::SORT_TYPE_DEFAULT ? "selected" : "" ?>
									 ><?php echo __('Name');?></option>
								<option value="<?php echo CJConnector::SORT_TYPE_ADVERTISER_ID ?>"
									 <?php echo $this->input->get('sort_by') == CJConnector::SORT_TYPE_ADVERTISER_ID ? "selected" : "" ?>
									 ><?php echo __('Advertiser ID');?></option>
								<option value="<?php echo CJConnector::SORT_TYPE_ADVERTISER_NAME ?>"
									 <?php echo $this->input->get('sort_by') == CJConnector::SORT_TYPE_ADVERTISER_NAME ? "selected" : "" ?>
									 ><?php echo __('Advertiser Name');?></option>
								<option value="<?php echo CJConnector::SORT_TYPE_CURRENCY ?>"
									 <?php echo $this->input->get('sort_by') == CJConnector::SORT_TYPE_CURRENCY ? "selected" : "" ?>
									 ><?php echo __('Currency');?></option>
								<option value="<?php echo CJConnector::SORT_TYPE_PRICE ?>"
									 <?php echo $this->input->get('sort_by') == CJConnector::SORT_TYPE_PRICE ? "selected" : "" ?>
									 ><?php echo __('Price');?></option>
								<option value="<?php echo CJConnector::SORT_TYPE_SALEPRICE ?>"
									 <?php echo $this->input->get('sort_by') == CJConnector::SORT_TYPE_SALEPRICE ? "selected" : "" ?>
									 ><?php echo __('salePrice');?></option>
								<option value="<?php echo CJConnector::SORT_TYPE_MANUFACTURER ?>"
									 <?php echo $this->input->get('sort_by') == CJConnector::SORT_TYPE_MANUFACTURER ? "selected" : "" ?>
									 ><?php echo __('Manufacturer');?></option>
								<option value="<?php echo CJConnector::SORT_TYPE_SKU ?>"
									 <?php echo $this->input->get('sort_by') == CJConnector::SORT_TYPE_SKU ? "selected" : "" ?>
									 ><?php echo __('SKU');?></option>
								<option value="<?php echo CJConnector::SORT_TYPE_UPC ?>"
									 <?php echo $this->input->get('sort_by') == CJConnector::SORT_TYPE_UPC ? "selected" : "" ?>
									 ><?php echo __('UPC');?></option>
					    	</select>
					  	</div>
					  	<div class="col-sm-6">
					    	<select name='sort_order' class="form-control">
					    		<option value="<?php echo CJConnector::SORT_ORDER_ASC ?>"
					    			 <?php echo $this->input->get('sort_order') == CJConnector::SORT_ORDER_ASC ? "selected" : "" ?>
					    			 ><?php echo __('ASC');?></option>
								<option value="<?php echo CJConnector::SORT_ORDER_DEC ?>"
									 <?php echo $this->input->get('sort_order') == CJConnector::SORT_ORDER_DEC ? "selected" : "" ?>
									 ><?php echo __('DEC');?></option>
					    	</select>
					  	</div>
				  	</div>
		    	</div>
		  	</div>
		  	<div class="form-group">
		    	<label class="col-sm-3 control-label"><?php echo __('Paging');?></label>
		    	<div class="col-sm-9">
		    		<div class="row">
			      		<div class="col-sm-6">
					    	<input type="text" name='page_number' class="form-control" value="<?php echo $this->input->get('page_number') ?>" placeholder="<?php echo __('Page number');?>"/>
					  	</div>
					  	<div class="col-sm-6">
					    	<input type="text" name='records_per_page' class="form-control" value="<?php echo $this->input->get('records_per_page') ?>" placeholder="<?php echo __('Record per page');?>"/>
					  	</div>
				  	</div>
		    	</div>
		  	</div>
		  	<?php break;?>
		  	<?php case 'rakuten':?>
		  	<div class="form-group">
		    	<label class="col-sm-3 control-label"><?php echo __('Keywords');?></label>
		    	<div class="col-sm-9">
		      		<input type="text" name='keywords' class="form-control" value="<?php echo $this->input->get('keywords') ?>" placeholder="<?php echo __('Keyword for searching');?>"/>
		    	</div>
		  	</div>
		  	<div class="form-group">
		    	<label class="col-sm-3 control-label"><?php echo __('Merchant ID');?></label>
		    	<div class="col-sm-9">
		      		<input type="text" name='mid' class="form-control" value="<?php echo $this->input->get('mid') ?>" placeholder="<?php echo __('Merchant ID');?>"/>
		    	</div>
		  	</div>
		  	<div class="form-group">
		    	<label class="col-sm-3 control-label"><?php echo __('Category ID');?></label>
		    	<div class="col-sm-9">
		      		<input type="text" name='category' class="form-control" value="<?php echo $this->input->get('category') ?>" placeholder="<?php echo __('Category ID');?>"/>
		    	</div>
		  	</div>
		  	<div class="form-group">
		    	<label class="col-sm-3 control-label"><?php echo __('Paging');?></label>
		    	<div class="col-sm-9">
		    		<div class="row">
			      		<div class="col-sm-6">
					    	<input type="text" name='pagenumber' class="form-control" value="<?php echo $this->input->get('pagenumber') ?>" placeholder="<?php echo __('Page number');?>"/>
					  	</div>
					  	<div class="col-sm-6">
					    	<input type="text" name='max' class="form-control" value="<?php echo $this->input->get('max') ?>" placeholder="<?php echo __('Record per page');?>"/>
					  	</div>
				  	</div>
		    	</div>
		  	</div>
		  	<div class="form-group">
		    	<label class="col-sm-3 control-label"><?php echo __('Sort by');?></label>
		    	<div class="col-sm-9">
		    		<div class="row">
			      		<div class="col-sm-6">
					    	<select name='sort' class="form-control">
					    		<option value="<?php echo RakutenConnector::SORT_RETAILPRICE ?>" 
					    			<?php echo $this->input->get('sort') == RakutenConnector::SORT_RETAILPRICE ? "selected" : "" ?>
					    			><?php echo __('Retail Price');?></option>
								<option value="<?php echo RakutenConnector::SORT_PRODUCTNAME ?>"
									 <?php echo $this->input->get('sort') == RakutenConnector::SORT_PRODUCTNAME ? "selected" : "" ?>
									 ><?php echo __('Product Name');?></option>
								<option value="<?php echo RakutenConnector::SORT_CATEGORYNAME ?>"
									 <?php echo $this->input->get('sort') == RakutenConnector::SORT_CATEGORYNAME ? "selected" : "" ?>
									 ><?php echo __('Category Name');?></option>
								<option value="<?php echo RakutenConnector::SORT_MID ?>"
									 <?php echo $this->input->get('sort') == RakutenConnector::SORT_MID ? "selected" : "" ?>
									 ><?php echo __('Merchant ID');?></option>
					    	</select>
					  	</div>
					  	<div class="col-sm-6">
					    	<select name='sorttype' class="form-control">
					    		<option value="<?php echo RakutenConnector::SORT_TYPE_ASC ?>"
					    			 <?php echo $this->input->get('sorttype') == RakutenConnector::SORT_TYPE_ASC ? "selected" : "" ?>
					    			 ><?php echo __('ASC');?></option>
								<option value="<?php echo RakutenConnector::SORT_TYPE_DES ?>"
									 <?php echo $this->input->get('sorttype') == RakutenConnector::SORT_TYPE_DES ? "selected" : "" ?>
									 ><?php echo __('DEC');?></option>
					    	</select>
					  	</div>
				  	</div>
		    	</div>
		  	</div>
		  	<?php break;?>
		  	<?php }?>
		  	<div class="form-group">
		    	<div class="col-sm-9 col-md-offset-3">
		      		<button type="submit" class="btn btn-xs btn-primary"><?php echo __('Search');?></button>
		    	</div>
		  	</div>
		  </form>
		</form>
	</div>
</div>
<hr/>
<div class="row">
	<div class="col-md-12">
		<?php switch ($vendor) {
			case 'cj':
			if ( isset($products->product) ) {
		?>
			<table class="data-table text-center" >
				<thead>
					<th><?php echo __('Image');?></th>
					<th><?php echo __('Product');?></th>
					<th><?php echo __('Price');?></th>
					<th><?php echo __('Brand');?></th>
					<th><?php echo __('Currency');?></th>
					<th><?php echo __('In Stock');?></th>
					<th><?php echo __('Action');?></th>
				</thead>
				<tbody>
					<?php foreach ($products->product as $item):
					$product_sku = $item->{"sku"}."-".$item->{"advertiser-id"}."-".$item->{"ad-id"};
					$category_id = $this->category_model->hasCategoryByMapping($item->{"advertiser-category"}, 'CJ');

					$ref_brand_id = $item->{"advertiser-id"};
					$brand_id = $this->brand_model->exists(['vendor' => 'CJ', 'ref_id' => $ref_brand_id]);

					$itemded = $this->product_model->exists(['sku' => $product_sku, 'vendor' => 'CJ']);
					?>
					<tr>
						<td>
							<img src="<?php echo $item->{"image-url"} ?>" class="img-responsive" width="80"/>
						</td>
						<td>
							<div class="row text-left">
								<div class="col-md-4">
									<label class="label label-default"><?php echo __('ID');?></label> <?php echo $item->{"ad-id"} ?><br/>
									<label class="label label-info"><?php echo __('SKU');?></label> <?php echo $item->{"sku"} ?><br/>
									<label class="label label-warning"><?php echo __('UPC');?></label> <?php echo $item->{"upc"} ?>
								</div>
								<div class="col-md-8">
									<a href="<?php echo $item->{"buy-url"} ?>" target="_blank"><?php echo $item->{"name"} ?></a></td>
								</div>
							</div>
						</td>
						<td class="text-left">
							<label class="label label-danger"><?php echo $item->{"price"} ?></label><br/>
							<label class="label label-warning"><?php echo __('Retail');?>: <?php echo $item->{"retail-price"} ?></label><br/>
							<label class="label label-info"><?php echo __('Sale');?>: <?php echo $item->{"sale-price"} ?></label>
						</td>
						<td class="text-left">
							<label class="label label-default"><?php echo __('ID');?></label> <?php echo $item->{"advertiser-id"} ?><br/>
							<label class="label label-info"><?php echo __('NAME');?></label> <?php echo $item->{"advertiser-name"} ?><br/>
							<label class="label label-warning"><?php echo __('CAT');?>: <?php echo $item->{"catalog-id"} ?></label> <?php echo $item->{"advertiser-category"} ?>
						</td>
						<td><?php echo $item->{"currency"} ?></td>
						<td><?php echo $item->{"in-stock"} ?></td>
						<td class="">
							<div class="btn-group text-center" role="group">
								<?php if( $category_id && is_array($category_id) && !empty($category_id)
										&& $brand_id && is_numeric($brand_id) ):?>
								<form action="<?php echo admin_url('import/product/'.$vendor.'/'.$item->{"ad-id"}.'?action=update&'.http_build_query($this->input->get()))?>" method="POST">
									<input type="hidden" name="product_id" value="<?php echo $itemded; ?>"/>
									<input type="hidden" name="product_sku" value="<?php echo $product_sku; ?>"/>
									<input type="hidden" name="category_id" value="<?php echo $item->{"advertiser-category"}; ?>"/>
									<input type="hidden" name="brand_id" value="<?php echo $brand_id; ?>"/>
									<input type="hidden" name="data" value="<?php echo htmlspecialchars(json_encode($item)); ?>"/>
									<button type="button"  class="btn btn-xs btn-primary ajax_post">
										<?php echo $itemded ? "Update" : "Add"?>
									</button>
								</form>
								<?php else:?>
								
									<?php if( !($brand_id && is_numeric($brand_id)) ):?>
										<a href="<?php echo admin_url("import/brand/$vendor?advertiser_ids=CIDs&advertiser_ids=".$item->{"advertiser-id"})?>" class="btn btn-xs btn-danger" target="_blank">Import Brand</a>
									<?php endif;?>
									<?php if( !($category_id && is_array($category_id) && !empty($category_id)) ):?>
									<a href="<?php echo admin_url("catalogs")?>" target="_blank" class="btn btn-xs btn-warning">Add Category</a>
									<?php endif;?>
									<p><a href="#" class="btn btn-xs btn-primary" disabled><?php echo __('Add');?></a></p>
								<?php endif;?>
							</div>
						</td>
					</tr>
					<?php endforeach ?>
				</tbody>
			</table>
		<?php 
				}
				break;
			case 'rakuten':
		?>
			<table class="data-table">
				<thead>
					<th><?php echo __('Image');?></th>
					<th><?php echo __('Product');?></th>
					<th><?php echo __('Price');?></th>
					<th><?php echo __('Brand');?></th>
					<th><?php echo __('Currency');?></th>
					<th><?php echo __('In Stock');?></th>
					<th><?php echo __('Action');?></th>
				</thead>
				<tbody>
				<?php foreach ($products as $item) { 
					$product_sku = $item->sku."-".$item->mid."-".$item->linkid;
					$category_name = (string)$item->category->primary;
					$second_cat = (string)$item->category->secondary;
					if($second_cat){
						$category_name .= '>>'.$second_cat;
					}
					$category_id = $this->category_model->hasCategoryByMapping($category_name, 'RAKUTEN');

					$ref_brand_id = $item->mid;
					$brand_id = $this->brand_model->exists(['vendor' => 'RAKUTEN', 'ref_id' => $ref_brand_id]);

					$itemded = $this->product_model->exists(['sku' => $product_sku, 'vendor' => 'RAKUTEN']);
				?>
				<tr>
					<td>
						<img src="<?php echo $item->imageurl ?>" class="img-responsive" width="80"/>
					</td>
					<td>
						<label class="label label-info"><?php echo $item->sku?></label>
						<?php echo $item->productname?>
					</td>
					<td>
						Sale Price: <label class="label label-info"><?php echo $item->saleprice?></label><br/>
						Price<label class="label label-info"><?php echo $item->price?></label>
					</td>
					<td>
						<?php echo $item->merchantname?>
					</td>
					<td>-</td>
					<td>-</td>
					<td>
						<div class="btn-group text-center" role="group">
							<?php if( $category_id && is_array($category_id) && !empty($category_id)
									&& $brand_id && is_numeric($brand_id) ):?>
							<form action="<?php echo admin_url('import/product/'.$vendor.'/'.$item->{"ad-id"}.'?action=update&'.http_build_query($this->input->get()))?>" method="POST">
								<input type="hidden" name="product_id" value="<?php echo $itemded; ?>"/>
								<input type="hidden" name="product_sku" value="<?php echo $product_sku; ?>"/>
								<input type="hidden" name="category_id" value="<?php echo $category_name; ?>"/>
								<input type="hidden" name="brand_id" value="<?php echo $brand_id; ?>"/>
								<input type="hidden" name="data" value="<?php echo htmlspecialchars(json_encode($item)); ?>"/>
								<button type="button"  class="btn btn-xs btn-primary ajax_post">
									<?php echo __($itemded ? "Update" : "Add")?>
								</button>
							</form>
							<?php else:?>
							
								<?php if( !($brand_id && is_numeric($brand_id)) ):?>
									<a href="<?php echo admin_url("import/brand/$vendor?merchantname=".$item->merchantname)?>" class="btn btn-xs btn-danger" target="_blank"><?php echo __('Import Brand');?></a>
								<?php endif;?>
								<?php if( !($category_id && is_array($category_id) && !empty($category_id)) ):?>
								<a href="<?php echo admin_url("catalogs")?>" target="_blank" class="btn btn-xs btn-warning">Add Category</a>
								<?php endif;?>
								<p><a href="#" class="btn btn-xs btn-primary" disabled><?php echo __('Add');?></a></p>
							<?php endif;?>
						</div>
					</td>
				</tr>
				<?php } ?>
				</tbody>
			</table>
		<?php 
				break;
			default:
		?>

		<?php 
				break;
		}?>
	</div>
</div>
<br/><br/><br/><br/><br/><br/>