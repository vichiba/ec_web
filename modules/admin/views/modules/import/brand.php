<?php 
	/*
	
[advertiser-id] => 1599858
[account-status] => Active
[seven-day-epc] => 21.53
[three-month-epc] => 34.59
[language] => en
[advertiser-name] => SuperJeweler
[program-url] => http://www.superjeweler.com
[relationship-status] => joined
[mobile-tracking-certified] => true
[network-rank] => 4
[primary-category] => SimpleXMLElement Object
    (
        [parent] => Accessories
        [child] => Jewelry
    )

[performance-incentives] => false
[actions] => SimpleXMLElement Object
    (
        [action] => SimpleXMLElement Object
            (
                [name] => SuperJeweler.com Purchase Item
                [type] => advanced sale
                [id] => 359647
                [commission] => SimpleXMLElement Object
                    (
                        [default] => 12.00%
                    )

            )

    )

[link-types] => SimpleXMLElement Object
    (
        [link-type] => Array
            (
                [0] => Text Link
                [1] => Banner
                [2] => OtherDeepLink
                [3] => DeepLink
                [4] => AutoMoneyDeepLink
            )

    )

	 */
	
function getCJEventInfo($actions){
	$return = [];
	if( isset($actions->action) ){
		$actions = $actions->action;
		if( !is_array($actions) ){
			$actions = [$actions];
		}
		foreach ($actions as $action){
			$return[] = [$action->name, $action->type, $action->commission->default];
		}
	}
	return $return;
}
	
?>
<div class="row">
	<div class="col-md-12">
		<div class="btn-group" role="group">
			<a href="<?php echo site_url('admin/import/brand/skimlinks') ?>" class="btn btn-<?php echo $vendor === 'skimlinks' ? 'primary' : 'default'?>" disabled>Skimlinks</a>
			<a href="<?php echo site_url('admin/import/brand/cj') ?>" class="btn btn-<?php echo $vendor === 'cj' ? 'primary' : 'default'?>">CJ</a>
			<a href="<?php echo site_url('admin/import/brand/rakuten') ?>" class="btn btn-<?php echo $vendor === 'rakuten' ? 'primary' : 'default'?>">Rakuten</a>
		</div>
	</div>
</div>
<p>&nbsp;</p>
<div class="row">
	<div class="col-md-4 col-md-offset-2">
		<form class="form-horizontal" action="" method="GET">
		<?php switch ($vendor) {
			case 'cj':
		?>
		  	<div class="form-group">
		    	<label for="inputEmail3" class="col-sm-3 control-label"><?php echo __('Search Type')?></label>
		    	<div class="col-sm-9">
		      		<select name='advertiser_ids' class="form-control" >
						<option value="notjoined" <?php echo $this->input->get('advertiser_ids') === "notjoined" ? "selected" : "" ?> ><?php echo __('Not Joined')?></option>
						<option value="joined" <?php echo $this->input->get('advertiser_ids') === "joined" ? "selected" : "" ?> ><?php echo __('Joined')?></option>
						<option value="CIDs" <?php echo !in_array($this->input->get('advertiser_ids'), ["", "joined", "notjoined"]) ? "selected" : "" ?> ><?php echo __('CIDs')?></option>
					</select>
					<input type="text" class="form-control" name="advertiser_ids" value="<?php echo !in_array($this->input->get('advertiser_ids'), ["joined", "notjoined"]) ? $this->input->get('advertiser_ids') : "" ?>" style="display: none">
		    	</div>
		  	</div>
		  	<div class="form-group">
		    	<label for="inputPassword3" class="col-sm-3 control-label"><?php echo __('Name')?></label>
		    	<div class="col-sm-9">
		      		<input type="text" name='advertiser_name' class="form-control" value="<?php echo $this->input->get('advertiser_name') ?>" placeholder="<?php echo __('Advertiser name for searching')?>"/>
		    	</div>
		  	</div>
		  	<div class="form-group">
		    	<label for="inputPassword3" class="col-sm-3 control-label"><?php echo __('Keywords')?></label>
		    	<div class="col-sm-9">
		      		<input type="text" name='keywords' class="form-control" value="<?php echo $this->input->get('keywords') ?>" placeholder="<?php echo __('Keyword for searching')?>"/>
		    	</div>
		  	</div>
		  	<div class="form-group">
		    	<label for="inputPassword3" class="col-sm-3 control-label"><?php echo __('Paging')?></label>
		    	<div class="col-sm-9">
		    		<div class="row">
			      		<div class="col-sm-6">
					    	<input type="text" name='page_number' class="form-control" value="<?php echo $this->input->get('page_number') ? $this->input->get('page_number') : '1' ?>" placeholder="<?php echo __('Page number')?>"/>
					  	</div>
					  	<div class="col-sm-6">
					    	<input type="text" name='records_per_page' class="form-control" value="<?php echo $this->input->get('records_per_page') ? $this->input->get('records_per_page') : '25' ?>" placeholder="<?php echo __('Record per page')?>"/>
					  	</div>
				  	</div>
		    	</div>
		  	</div>
		  <?php break;?>
		  <?php case 'rakuten':?>
		  	<div class="form-group">
		    	<label for="inputPassword3" class="col-sm-3 control-label"><?php echo __('Merchant Name')?></label>
		    	<div class="col-sm-9">
		      		<input type="text" name='merchantname' class="form-control" value="<?php echo $this->input->get('merchantname') ?>" placeholder="<?php echo __('Advertiser name for searching')?>"/>
		    	</div>
		  	</div>
		  <?php break;?>
		  <?php }?>
		  <div class="form-group">
		    	<div class="col-sm-9 col-md-offset-3">
		      		<button type="submit" class="btn btn-xs btn-primary"><?php echo __('Search') ?></button>
		    	</div>
		  	</div>
		</form>
	</div>
</div>
<hr/>
<div class="row">
	<div class="col-md-12">
		<?php switch ($vendor) {
			case 'cj':
			if ( isset($brands->advertisers) && isset($brands->advertisers->advertiser) ) {
		?>
			<table class="data-table">
				<thead>
					<th><?php echo __('Advertister ID') ?></th>
					<th><?php echo __('Name') ?></th>
					<th><?php echo __('Category') ?></th>
					<th><?php echo __('Rank') ?></th>
					<th><?php echo __('Event') ?></th>
					<th><?php echo __('Seven day EPC') ?></th>
					<th><?php echo __('Three day EPC') ?></th>
					<th><?php echo __('Relationship Status') ?></th>
					<th><?php echo __('Account Status') ?></th>
					<th><?php echo __('Action') ?></th>
				</thead>
				<tbody>
					<?php foreach ($brands->advertisers->advertiser as $ad): 
					$added = $this->brand_model->exists(['vendor' => 'CJ', 'ref_id' => $ad->{"advertiser-id"}]);
					?>
					<tr>
						<td><?php echo $ad->{"advertiser-id"} ?></td>
						<td><a href="<?php echo $ad->{"program-url"} ?>" target="_blank"><?php echo $ad->{"advertiser-name"} ?></a></td>
						<td>
							<?php 
							$cats = $ad->{"primary-category"};
							if( isset($cats->parent) ) echo $cats->parent;
							if( isset($cats->child) ){
								if( isset($cats->parent) && !empty($cats->parent) ) echo ' > ';
								echo $cats->child;
							}
							?></td>
						</td>
						<td><?php echo $ad->{"network-rank"} ?></td>
						<td><?php
							$actions = getCJEventInfo($ad->actions);
							foreach ($actions as $event) {
								echo $event[0]." <br/><b>".$event[1]."</b> <label class=\"label label-success\">".$event[2]."</label>";
							}
						?></td>
						<td><?php echo $ad->{"seven-day-epc"} ?></td>
						<td><?php echo $ad->{"three-month-epc"} ?></td>
						<td><?php echo $ad->{"relationship-status"} ?></td>
						<td><?php echo $ad->{"account-status"} ?></td>
						<td>
							<?php if($added):?>
								<a href="<?php echo admin_url('import/brand/'.$vendor.'/'.$ad->{"advertiser-id"}.'?action=update&'.http_build_query($this->input->get()))?>" 
									class="btn btn-xs btn-primary ajax_get"
									>Update</a>
							<?php else:?>
								<a href="<?php echo admin_url('import/brand/'.$vendor.'/'.$ad->{"advertiser-id"}.'?action=add&'.http_build_query($this->input->get()))?>" 
									class="btn btn-xs btn-primary ajax_get"
									>Add</a>
							<?php endif;?>
						</td>
					</tr>
					<?php endforeach ?>
				</tbody>
			</table>
		<?php 
				}
				break;
			case 'rakuten':
		?>
			<table class="data-table">
				<thead>
					<th><?php echo __('Logo') ?></th>
					<th><?php echo __('Advertister ID') ?></th>
					<th><?php echo __('Name') ?></th>
					<th><?php echo __('Action') ?></th>
				</thead>
				<tbody>
					<?php foreach ($brands->midlist->merchant as $merchant): 
					$added = $this->brand_model->exists(['vendor' => 'RAKUTEN', 'ref_id' => $merchant->mid]);
					?>
					<tr>
						<td><img src="http://merchant.linksynergy.com/fs/logo/lg_<?php echo $merchant->mid ?>.jpg" width="120"/></td>
						<td><?php echo $merchant->mid ?></td>
						<td><?php echo $merchant->merchantname?></td>
						<td>
							<?php if($added):?>
								<a href="<?php echo admin_url('import/brand/'.$vendor.'/'.$merchant->mid.'?action=update&'.http_build_query($this->input->get()))?>" 
									class="btn btn-xs btn-primary ajax_get"
									><?php echo __('Update') ?></a>
							<?php else:?>
								<a href="<?php echo admin_url('import/brand/'.$vendor.'/'.$merchant->mid.'?action=add&'.http_build_query($this->input->get()))?>" 
									class="btn btn-xs btn-primary ajax_get"
									><?php echo __('Add') ?></a>
							<?php endif;?>
						</td>
					</tr>
					<?php endforeach ?>
				</tbody>
			</table>
		<?php 
				break;
			default:
		?>

		<?php 
				break;
		}?>
	</div>
</div>
<br/><br/><br/><br/><br/><br/>