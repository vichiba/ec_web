<!-- Page tabs -->
<div class="tabbable page-tabs page-messages">
    <ul class="nav nav-tabs">
        <?php 
        $user_count = 0;
        $active_user = 0;
        foreach ($users as $user) {
            $user_count++;
            $active_user = ($user_count === 1 ? $user->msg_user_id : $active_user);
            echo '<li class="'.($user_count === 1 ? 'active' : '').'">
                    <a href="#user-'.$user->msg_user_id.'" data-toggle="tab" title="'.$user->msg_user_created_date.'">
                        <img src="http://placehold.it/300" alt="" class="tab-img">
                        '.$user->msg_user_fullname,' 
                        <span class="status status-'.($user->msg_user_alive === '1' ? 'info': 'danger').'"></span>
                    </a>
                </li>';
        }?>
    </ul>
    <div class="tab-content">
    <?php 
    $user_count = 0;
    foreach ($users as $user):
        $user_count++;
    ?>
    	<!-- Message tab : user-<?php echo $user->msg_user_id?> -->
        <div class="tab-pane fade <?php echo ($active_user === $user->msg_user_id ? 'active in' : '')?>" id="user-<?php echo $user->msg_user_id?>">

            <!-- Eugene -->
            <div class="block">
            	<div class="chat-member-heading clearfix">
					<h6 class="pull-left"><i class="icon-bubble6"></i> <?php echo $user->msg_user_fullname?> <?php if( $user->msg_user_phonenumber ){?><small>&nbsp;/&nbsp; <?php echo $user->msg_user_phonenumber?></small><?php }?></h6>
					<div class="pull-right">
						Start at: <b><?php echo $user->msg_user_created_date?></b>
					</div>
            	</div>
            	
                <div class="chat">
                </div>

                <textarea name="enter-message" class="form-control" rows="3" cols="1" placeholder="Enter your message..."></textarea>
                <div class="message-controls">
                    <span class="pull-left"><i class="icon-checkmark-circle"></i> Some basic <a href="#" title="">HTML</a> is OK</span>
                    <div class="pull-right">
                    	<div class="upload-options">
                        	<a href="#" title="" class="tip" data-original-title="Smileys"><i class="icon-smiley"></i></a>
                            <a href="#" title="" class="tip" data-original-title="Upload photo"><i class="icon-camera3"></i></a>
                            <a href="#" title="" class="tip" data-original-title="Attach file"><i class="icon-attachment"></i></a>
                        </div>
                        <button type="button" class="btn btn-danger btn-loading" data-loading-text="<i class='icon-spinner7 spin'></i> Processing">Submit</button>
                    </div>
                </div>
            </div>
            <!-- /eugene -->

        </div>
        <!-- /message tab -->
    <?php endforeach;?>
        </div>
        <!-- /fifth tab -->
    </div>

</div>
<!-- /page tabs -->