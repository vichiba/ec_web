<!-- Page header -->
<div class="page-header">
	<div class="page-title">
		<h3>
			<?php echo empty($page_title)? '' : $page_title;?>
			<?php if(!empty($page_desc) ) echo '<small>'.$page_desc.'</small>' ?>
		</h3>
	</div>
</div>
<!-- /page header -->