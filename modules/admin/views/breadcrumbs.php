<!-- Breadcrumbs line -->
<div class="breadcrumb-line">
	<ul class="breadcrumb">
		<li><a href="/<?php echo $this->config->item('admin_url') ?>">Home</a></li>
	</ul>
	<?php
	if( !empty($breadcrumb_buttons) && is_array($breadcrumb_buttons) ):
	?>
	<ul class="breadcrumb-buttons collapse">
		<li class="language dropdown">
			<?php
			$data_list = '';
			foreach ($breadcrumb_buttons as $btn) {
				$data_list .= '<li><a href="'.$btn['href'].'">'.$btn['label'].'</a></li>';
			}?>
			<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span><?php echo $breadcrumb_buttons_selected_label?></span> <b class="caret"></b></a>
			<ul class="dropdown-menu dropdown-menu-right icons-right">
				<?php echo $data_list?>
			</ul>
		</li>
	</ul>
	<?php 
	endif;
	?>
	<div class="visible-xs breadcrumb-toggle">
		<a class="btn btn-link btn-lg btn-icon" data-toggle="collapse" data-target=".breadcrumb-buttons"><i class="icon-menu2"></i></a>
	</div>
</div>
<!-- /breadcrumbs line -->