<?php $this->load->view('header');?>
<?php $this->load->view('navbar');?>

<!-- Page container -->
<div class="page-container">


 		<?php $this->load->view('page_header');?>

		<!-- Error wrapper -->
	<div class="error-wrapper text-center">
	    <h1>403</h1>
        <h6>- <?php echo __('Oops, an error has occurred. Forbidden!') ?> -</h6>

        <!-- Error content -->
        <div class="error-content">
	    	<form class="block-inner" action="#">
	    		<div class="input-group">
					<input type="text" class="form-control" placeholder="Search...">
					<span class="input-group-btn">
						<button class="btn btn-primary" type="button"><?php echo __('Search') ?></button>
					</span>
				</div>
	    	</form>

	        <div class="row">
		        <div class="col-md-6">
		            <a href="<?php echo admin_url('dashboard')?>" class="btn btn-danger btn-block"><?php echo __('Back to dashboard') ?></a>
	            </div>
	            <div class="col-md-6">
		            <a href="<?php echo base_url()?>" class="btn btn-success btn-block"><?php echo __('Back to the website') ?></a>
	            </div>
	        </div>
        </div>
        <!-- /error content -->
        
	</div>  
	<!-- /error wrapper -->

</div>
<!-- /page container -->
<?php $this->load->view('footer');?>