<?php $this->load->view('header');?>

<!-- Page container -->
<div class="page-container login-page">

	<!-- Login wrapper -->
	<div class="login-wrapper">
		<?php echo !empty($message) ? $message : '';?>
		<form role="form" method="POST" autocomplete="OFF">
			<div class="popup-header">
				<a href="#" class="pull-left"></a>
				<span class="text-semibold"><?php echo __('User Login') ?></span>
				<div class="btn-group pull-right">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-cogs"></i></a>
	                <ul class="dropdown-menu icons-right dropdown-menu-right">
						<li><a href="#"><i class="icon-info"></i> <?php echo __('Forgot password?') ?></a></li>
						<li><a href="#"><i class="icon-support"></i> <?php echo __('Contact admin') ?></a></li>
	                </ul>
				</div>
			</div>
			<div class="well">
				<div class="form-group has-feedback">
					<label><?php echo __('Username') ?></label>
					<input type="text" class="form-control" name="username" placeholder="Username">
					<i class="icon-users form-control-feedback"></i>
				</div>

				<div class="form-group has-feedback">
					<label><?php echo __('Password') ?></label>
					<input type="password" class="form-control" name="password" placeholder="Password">
					<i class="icon-lock form-control-feedback"></i>
				</div>

				<div class="row form-actions">
					<div class="col-xs-6">
						<div class="checkbox checkbox-success">
						<label>
							<input type="checkbox" class="styled" name="remember_me" value="1" checked="checked">
							<?php echo __('Remember me') ?>
						</label>
						</div>
					</div>

					<div class="col-xs-6">
						<button type="submit" class="btn btn-warning pull-right"><i class="icon-menu2"></i> <?php echo __('Sign in') ?></button>
					</div>
				</div>
			</div>
		</form>
		<?php $this->load->view('copyright');?>
	</div>  
	<!-- /login wrapper -->

</div>
<!-- /page container -->
<?php $this->load->view('footer');?>