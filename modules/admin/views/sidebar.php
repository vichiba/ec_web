<!-- Sidebar -->
<div class="sidebar">
	<div class="sidebar-content">
		<!-- Main navigation -->
		<ul class="navigation">
			<?php
				$this->config->load('sidebar');
				$sidebar = $this->config->item('sidebar');
				$sidebar_active = empty($sidebar_active)? '' : $sidebar_active;
				function display_sidebar($sidebar, $sidebar_active){
					foreach ($sidebar as $label => $item) {
						$active_class = '';
						if( $item['key'] == $sidebar_active ) $active_class = 'active';

						echo '<li class='.$active_class.'>';
						echo '<a href="'.(empty($item['href']) ? '#': $item['href']).'"><span>'.$label.'</span>';
						if( !empty($item['icon']) ){
							echo ' <i class="'.$item['icon'].'"></i>';
						}
						echo '</a>';
						if( !empty($item['childs']) && count($item['childs']) ){
							echo '<ul>';
							display_sidebar($item['childs'], $sidebar_active);
							echo '</ul>';
						}
						echo '</li>';
					}
				}
				display_sidebar($sidebar, $sidebar_active);
			?>
		</ul>
		<!-- /main navigation -->
		
	</div>
</div>
<!-- /sidebar -->