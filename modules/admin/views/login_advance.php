<?php $this->load->view('header');?>

<!-- Page container -->
<div class="page-container login-page">

	<!-- Login wrapper -->
	<div class="login-wrapper">
		<?php echo !empty($message) ? $message : '';?>
    	<form role="form" method="POST" autocomplete="OFF">
    		<input type="hidden" name="username" value="<?php echo $user->user_login?>"/>
			<div class="well">
			    <div class="thumbnail">
			    	<div class="thumb">
						<img alt="<?php echo $user->user_fullname?>" src="<?php $avatar = trim($user->user_photo); echo $avatar ? $avatar : 'http://placehold.it/300'?>" style="width: 150px; height: 150px;">
				    </div>
			    
			    	<div class="caption text-center">
			    		<h6><?php echo $user->user_fullname?> <small>&laquo; <?php echo $user->user_login?> &raquo;</small></h6>
			    	</div>
		    	</div>

				<div class="form-group has-feedback has-feedback-no-label">
					<input type="password" class="form-control" placeholder="Password" name="password">
					<i class="icon-lock form-control-feedback"></i>
				</div>

				<div class="row form-actions">
					<div class="col-xs-6">
						<div class="checkbox checkbox-success">
						<label>
							<input type="checkbox" class="styled" checked="true" name="remember_me">
							<?php echo __('Remember me') ?>
						</label>
						</div>
					</div>

					<div class="col-xs-6">
						<button type="submit" class="btn btn-danger pull-right"><i class="icon-menu2"></i> <?php echo __('Sign in') ?></button>
					</div>
				</div>
			</div>
			<div class="text-center">
				<p></p><a href="<?php echo admin_url('signin_with_different_account');?>"><?php echo __('Sign in with a different account') ?></a>
			</div>
    	</form>
	</div>  
	<!-- /login wrapper -->
    
</div>
<!-- /page container -->
<?php $this->load->view('footer');?>