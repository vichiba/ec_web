<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
<title><?php echo empty($page_title) ? 'Admin Page' : $page_title ?></title>

<!-- Latest compiled and minified CSS -->
<link href="<?php echo res('css/bootstrap.min.css') ?>" rel="stylesheet" type="text/css">
<link href="<?php echo res('css/londinium-theme.css') ?>" rel="stylesheet" type="text/css">
<link href="<?php echo res('css/styles.css') ?>" rel="stylesheet" type="text/css">
<link href="<?php echo res('css/icons.css') ?>" rel="stylesheet" type="text/css">
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">

<?php 
if( !empty($css_files) ){
	foreach($css_files as $file){
		echo '<link type="text/css" rel="stylesheet" href="'.$file.'" />';
	}
}
?>
<script>BASE_URL='<?php echo admin_url()?>';</script>

</head>
<body>