<!-- Navbar -->
<div class="navbar navbar-inverse" role="navigation">
	<div class="navbar-header">
		<a class="navbar-brand" href="<?php echo admin_url()?>"><?php echo $this->config->item('project_name');?></a>
		<a class="sidebar-toggle"><i class="icon-paragraph-justify2"></i></a>
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-icons">
			<span class="sr-only">Toggle navbar</span>
			<i class="icon-grid3"></i>
		</button>
		<button type="button" class="navbar-toggle offcanvas">
			<span class="sr-only">Toggle navigation</span>
			<i class="icon-paragraph-justify2"></i>
		</button>
	</div>

	<ul class="nav navbar-nav navbar-right collapse" id="navbar-icons">
		<li>
			<div id="reportrange" class="range">
				<div class="date-range"></div>
			</div>
		</li>

		<!-- <li class="dropdown">
			<a data-toggle="dropdown" class="dropdown-toggle">
				<i class="icon-grid"></i>
			</a>
			<div class="popup dropdown-menu dropdown-menu-right">
				<div class="popup-header">
					<a href="#" class="pull-left"><i class="icon-spinner7"></i></a>
					<span>Import Tasks list</span>
					<a href="#" class="pull-right"><i class="icon-new-tab"></i></a>
				</div>
				<table class="table table-hover">
					<thead>
						<tr>
							<th>Description</th>
							<th>Category</th>
							<th class="text-center">Priority</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td><span class="status status-success item-before"></span> <a href="#">Frontpage fixes</a></td>
							<td><span class="text-smaller text-semibold">Bugs</span></td>
							<td class="text-center"><span class="label label-success">87%</span></td>
						</tr>
						<tr>
							<td><span class="status status-danger item-before"></span> <a href="#">CSS compilation</a></td>
							<td><span class="text-smaller text-semibold">Bugs</span></td>
							<td class="text-center"><span class="label label-danger">18%</span></td>
						</tr>
						<tr>
							<td><span class="status status-info item-before"></span> <a href="#">Responsive layout changes</a></td>
							<td><span class="text-smaller text-semibold">Layout</span></td>
							<td class="text-center"><span class="label label-info">52%</span></td>
						</tr>
						<tr>
							<td><span class="status status-success item-before"></span> <a href="#">Add categories filter</a></td>
							<td><span class="text-smaller text-semibold">Content</span></td>
							<td class="text-center"><span class="label label-success">100%</span></td>
						</tr>
						<tr>
							<td><span class="status status-success item-before"></span> <a href="#">Media grid padding issue</a></td>
							<td><span class="text-smaller text-semibold">Bugs</span></td>
							<td class="text-center"><span class="label label-success">100%</span></td>
						</tr>
					</tbody>
				</table>
			</div>
		</li> -->

		<li class="user dropdown">
			<a class="dropdown-toggle" data-toggle="dropdown">
				<img src="<?php $avatar = trim($this->session->userdata('user_photo')); echo $avatar ? $avatar : 'http://placehold.it/300'?>">
				<span><?php echo $this->session->userdata('user_fullname')?></span>
				<i class="caret"></i>
			</a>
			<ul class="dropdown-menu dropdown-menu-right icons-right">
				<li><a href="<?php echo admin_url('profile');?>"><i class="icon-user"></i> <?php echo __('Profile') ?></a></li>
				<!-- <li><a href="#"><i class="icon-bubble4"></i> Messages</a></li>
				<li><a href="#"><i class="icon-cog"></i> Settings</a></li> -->
				<li><a href="<?php echo admin_url('logout')?>"><i class="icon-exit"></i> <?php echo __('Logout') ?></a></li>
			</ul>
		</li>
	</ul>
</div>
<!-- /navbar -->