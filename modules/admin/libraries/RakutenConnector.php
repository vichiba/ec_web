<?php
class RakutenConnector{

	const R_TOKEN_API = 'https://api.rakutenmarketing.com/token';
	const R_PRODUCT_API = 'https://api.rakutenmarketing.com/productsearch/1.0';
	const R_ADVERTISER_API = 'https://api.rakutenmarketing.com/advertisersearch/1.0';
	const R_ADVERTISER_CATEGORY_API = 'https://api.rakutenmarketing.com/linklocator/1.0/getCreativeCategories/';
	const R_ADVERTISER_INFO_API = 'https://api.rakutenmarketing.com/linklocator/1.0/getMerchByID/';
	const R_ADVERTISER_BANNER_API = 'https://api.rakutenmarketing.com/linklocator/1.0/getBannerLinks/';
	const R_ADVANCE_REPORT_API = 'https://api.rakutenmarketing.com/advancedreports/1.0';

	const R_MID = 'RAKUTEN_MID';
	const R_AUTH_TOKEN = 'RAKUTEN_AUTH_TOKEN';
	const R_SECURITY_TOKEN = 'RAKUTEN_SECURITY_TOKEN';
	const RAKUTEN_PASSWORD = 'RAKUTEN_PASSWORD';
	const RAKUTEN_USERNAME = 'RAKUTEN_USERNAME';

	private $R_MID;
	private $R_AUTH_TOKEN;
	private $R_SECURITY_TOKEN;
	private $R_USERNAME;
	private $R_PASSWORD;

	const SORT_RETAILPRICE = 'retailprice';
	const SORT_PRODUCTNAME = 'productname';
	const SORT_CATEGORYNAME = 'categoryname';
	const SORT_MID = 'mid';
	const SORT_TYPE_ASC = 'asc';
	const SORT_TYPE_DES = 'dsc';

	function __construct(){
		include(dirname(__FILE__).'/httpful.phar');

		$this->R_MID = get_config_value(SELF::R_MID);
		$this->R_AUTH_TOKEN = get_config_value(SELF::R_AUTH_TOKEN);
		$this->R_SECURITY_TOKEN = get_config_value(SELF::R_SECURITY_TOKEN);
		$this->R_USERNAME = get_config_value(SELF::RAKUTEN_USERNAME);
		$this->R_PASSWORD = get_config_value(SELF::RAKUTEN_PASSWORD);
	}
	
	private function requestToken(){
		$response = \Httpful\Request::post(SELF::R_TOKEN_API)
					->addHeader('Authorization', $this->R_AUTH_TOKEN)
					->body('<xml><grant_type>password</grant_type><username>'.$this->R_USERNAME.'</username><password>'.$this->R_PASSWORD.'</password><scope>'.$this->R_MID.'</scope></xml>')
	    			->sendsXml()
	    			->send();
		$body = $response->body;
		return $body;
	}

	private function refreshToken($refresh_token){
		$response = \Httpful\Request::post(SELF::R_TOKEN_API)
					->addHeader('Authorization', SELF::R_AUTH_TOKEN)
					->body('<xml><grant_type>refresh_token</grant_type><refresh_token>'.$refresh_token.'</refresh_token><scope>PRODUCTION</scope></xml>')
	    			->sendsXml()
	    			->send();
		$body = $response->body;
		return $body;
	}

	private function getAccessToken(){
		$response = $this->requestToken();
		$access_token = $response->access_token;
		$access_token_bearer = 'Bearer '.$access_token;
		return $access_token_bearer;
	}

	/**
	* Search Product API
	*
	* @param int $mid Filter by an Advertiser ID
	* @param string $keyword Search Term to find products with all words specified (e.g. 'DVD' and 'Player')
	* @param string $category Filter by category (e.g. 'Electronics')
	* @param int $max Maximum results per page (Range: 0-100, Default: 20)
	* @param int $pagenumber Page number of the results (Default: 1)
	* @param string $sort Sort by a particular attribute (Retail Price ['retailprice'], Product Name ['productname'], Primary Category ['categoryname'], Advertiser ID ['mid'])
	* @param string $sorttype Sort Order (Ascending ['asc'] or Descending ['dsc'])
	* @return mix Array of products
	*/
	public function searchProduct($keyword = '', $mid = '', $category = '', $max = 20, $pagenumber = 1, $sort = 'productname', $sorttype = 'asc'){
		$params = array();
		if($mid && is_numeric($mid)){
			$params['mid'] = $mid;
		}
		if($keyword){
			$params['keyword'] = $keyword;
		}
		if($category){
			$params['cat'] = $category;
		}
		if($max && is_numeric($max)){
			$params['max'] = $max;
		}
		if($pagenumber && is_numeric($pagenumber)){
			$params['pagenumber'] = $pagenumber;
		}
		if($sort){
			$params['sort'] = $sort;
		}

		if($sorttype){
			$params['sorttype'] = $sorttype;
		}

		$response = \Httpful\Request::get(SELF::R_PRODUCT_API.'?'.http_build_query($params))
					->addHeader('Authorization', $this->getAccessToken())
	    			->send();
		$body = $response->body;
		return $body;
	}


	/**
	* Search Advertiser
	*
	* @param string merchantname Filter by Advertiser Name
	*
	* @return mix 
	*/
	public function searchAdvertiser($merchantname = ''){
		$response = \Httpful\Request::get(SELF::R_ADVERTISER_API.'?'.http_build_query(array('merchantname' => $merchantname)))
					->addHeader('Authorization', $this->getAccessToken())
	    			->send();
		$body = $response->body;
		return $body;
	}

	public function getAdvertiserInfo($advertiserId){
		$response = \Httpful\Request::get(SELF::R_ADVERTISER_INFO_API.$advertiserId)
					->addHeader('Authorization', $this->getAccessToken())
	    			->send();
		$body = $response->raw_body;
		return new SimpleXMLElement($body);
	}

	public function searchAdvertisterCategory($advertiserId){
		$response = \Httpful\Request::get(SELF::R_ADVERTISER_CATEGORY_API.$advertiserId)
					->addHeader('Authorization', $this->getAccessToken())
	    			->send();
		$body = $response->raw_body;
		return $body;
	}

	public function searchAdvertisterBanner($mid, $creativeCategory, $startDate = '', $endDate = '', $size = '-1', $page = '1'){
		$query = "${mid}/${creativeCategory}/${startDate}/${endDate}/${size}/-1/${page}";
		$response = \Httpful\Request::get(SELF::R_ADVERTISER_BANNER_API.$query)
					->expectsXml()
					->addHeader('Authorization', $this->getAccessToken())
	    			->send();
		$body = $response->raw_body;
		return $body;
	}

	/**
	 * Advance Rakuten Report
	 * @param  string $bdate    Begin Date (YYYYMMDD)
	 * @param  string $edate    End Date (YYYYMMDD)
	 * @param  int $nid      Network ID
	 *                       + nid=1 for US 
	 *                       + nid=5 for Canada 
	 *                       + nid=8 for Brazil 
	 *                       + nid=41 for Australia
	 *                       + nid=3 for UK 
	 *                       + nid=7 for France 
	 *                       + nid=9 for Germany
	 * @param  int $reportid Report Type
	 *                       + Sales & Activity=4
	 *						 + Revenue=5
	 *						 + Link Type=6
	 *						 + Individual Item=7
	 *						 + Product Success=8
	 *						 + Program Level=9
	 *						 + Non-Commissionable Sales Report=10
	 *						 + Signature Activity Report=11
	 *						 + Signature Order=12
	 *						 + Media Optimization Report=14
	 * @param  int $mid      merchant ID
	 * @return mix           report data
	 */
	public function advanceReport($bdate, $edate, $reportid, $nid = '', $mid = ''){
		
		$params = array();
		if($bdate){
			$params['bdate'] = $bdate;
		}
		if($edate){
			$params['edate'] = $edate;
		}
		$params['token'] = $this->R_SECURITY_TOKEN;

		if($nid && is_numeric($nid)){
			$params['nid'] = $nid;
		}
		if($reportid && is_numeric($reportid)){
			$params['reportid'] = $reportid;
		}
		if($mid && is_numeric($mid)){
			$params['mid'] = $mid;
		}

		//$query = "${mid}/${creativeCategory}/${startDate}/${endDate}/${size}/-1/${page}";
		$response = \Httpful\Request::get(SELF::R_ADVANCE_REPORT_API.'?'.http_build_query($params))
					->addHeader('Authorization', $this->getAccessToken())
	    			->send();
	    //debug($response, TRUE);
		$body = $response->raw_body;
		return $body;
	}
}