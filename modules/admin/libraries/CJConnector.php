<?php
class CJConnector{

	const CJ_KEY_CONF = 'CJ_ACCESS_KEY' ;

	const CJ_BRAND_API = 'https://advertiser-lookup.api.cj.com/v3/advertiser-lookup';
	const CJ_LINK_API = 'https://link-search.api.cj.com/v2/link-search';
	const CJ_PRODUCT_API = 'https://product-search.api.cj.com/v2/product-search';
	const CJ_COMMISSION_API = 'https://commission-detail.api.cj.com/v3/commissions';
	const CJ_WEBSITE_ID_CONF = 'CJ_WEBSITE_ID';

	private $CJ_KEY ;
	private $CJ_WEBSITE_ID;

	const SORT_TYPE_DEFAULT = '';
	const SORT_TYPE_NAME = 'Name';
 	const SORT_TYPE_ADVERTISER_ID = 'Advertiser ID';
 	const SORT_TYPE_ADVERTISER_NAME = 'Advertiser Name';
 	const SORT_TYPE_CURRENCY = 'Currency';
 	const SORT_TYPE_PRICE = 'Price';
 	const SORT_TYPE_SALEPRICE = 'salePrice';
 	const SORT_TYPE_MANUFACTURER = 'Manufacturer';
 	const SORT_TYPE_SKU = 'SKU';
 	const SORT_TYPE_UPC = 'UPC';

 	const SORT_ORDER_ASC = 'asc';
 	const SORT_ORDER_DEC = 'dec';

	function __construct(){
		include(dirname(__FILE__).'/httpful.phar');
		
		$this->CJ_KEY = get_config_value(SELF::CJ_KEY_CONF);
		$this->CJ_WEBSITE_ID = get_config_value(SELF::CJ_WEBSITE_ID_CONF);

	}

	/**
	 * Search Advertister
	 *
	 * @param  advertiser-ids 	Limits the results to a set of particular advertisers (CIDs) using one of the following four values.
     *                          CIDs: You may provide list of one or more advertiser CIDs, separated by commas, to limit the results to a specific sub-set of merchants.
 	 *							joined: This value restricts the search to advertisers with which you have a relationship.
	 *							notjoined: This value restricts the search to advertisers with which you do not have a relationship.
 	 *							Empty String: Leaving this field blank returns results for non-joined advertisers only. Using the Empty String value for 'advertiser-ids' is currently behaving the same as using the 'non-joined' value.
 	 * @param  advertiser-name 		Limits the results to a set of particular advertisers (CIDs) using one of the following values.
 	 *							Advertiser name: Limits results to advertisers matching specified program name.
 	 *							Program URL: Limits results to advertisers matching specified program URL.
 	 * @param  keywords 	This value restricts the search results based on keywords found in the advertiser’s name, the product name or the product description. This parameter may be left blank. You may use simple Boolean logic operators (’+’, ’-’) to obtain more relevant search results. By default, the system assumes basic OR logic. The examples below illustrate how these operators affect search results.
 	 *							Query				Results
 	 *							"kitchen sink"		Any product with the word "kitchen" or "sink"
 	 *							"+kitchen +sink" 	Any product with the words "kitchen" and "sink"
 	 *							"+kitchen -sink" 	Any product with "kitchen" and without "sink"
 	 *							"kitchen +sink" 	All the products with the word "sink"; if they also contain "kitchen", it increases the product’s relevancy.
	 * @param page-number 		Specifies the page of the results set that is currently being viewed.
	 * @param records-per-page 		Specifies the number of records to be viewed per page.
 	 *							If this parameter is not given, the response will return 25 records per page. The maximum value for this parameter is 100.
 	 * @param mobile-tracking-certified 	Limits the results to a set of particular advertisers (CIDs) using one of the following two values:
 	 *							True: This special value restricts the search to .
 	 *							False: This special value restricts the search to advertisers that have not been certified for mobile tracking
	 * @return mix
	 */
	public function getBrand($advertiser_ids = 'joined', $advertiser_name = '', $keywords = '', $page_number = 1, $records_per_page = 25){
		$params = [];
		$params['advertiser-ids'] = $advertiser_ids;
		$params['advertiser-name'] = $advertiser_name;
		$params['keywords'] = $keywords;
		$params['page-number'] = $page_number;
		$params['records-per-page'] = $records_per_page;
		//debug(SELF::CJ_BRAND_API.'?'.http_build_query($params), TRUE);
		$response = \Httpful\Request::get(SELF::CJ_BRAND_API.'?'.http_build_query($params))
					->addHeader('authorization', $this->CJ_KEY)
					->send();
		$body = $response->body;
		return $body;
	}


	public function getLinkAds(){
		$response = \Httpful\Request::get(SELF::CJ_LINK_API.'?website-id='.$this->CJ_WEBSITE_ID.'&link-type=banner&advertiser-ids=joined')
					->addHeader('authorization', $this->CJ_KEY)
					->send();
		$body = $response->body;
		return $body;
	}

	/**
	 * Search Product
	 *
	 * @param  advertiser-ids 		Limits the results to a set of particular advertisers (CIDs) using one of the following four values.
     *                          CIDs: You may provide list of one or more advertiser CIDs, separated by commas, to limit the results to a specific sub-set of merchants.
 	 *							joined: This value restricts the search to advertisers with which you have a relationship.
	 *							notjoined: This value restricts the search to advertisers with which you do not have a relationship.
 	 *							Empty String: You may provide an empty string to remove any advertiser-specific restrictions on the search.
 	 * @param  advertiser-name 		Limits the results to a set of particular advertisers (CIDs) using one of the following values.
 	 *							Advertiser name: Limits results to advertisers matching specified program name.
 	 *							Program URL: Limits results to advertisers matching specified program URL.
 	 * @param  keywords 	This value restricts the search results based on keywords found in the advertiser’s name, the product name or the product description. This parameter may be left blank. You may use simple Boolean logic operators (’+’, ’-’) to obtain more relevant search results. By default, the system assumes basic OR logic. The examples below illustrate how these operators affect search results.
 	 *							Query				Results
 	 *							"kitchen sink"		Any product with the word "kitchen" or "sink"
 	 *							"+kitchen +sink" 	Any product with the words "kitchen" and "sink"
 	 *							"+kitchen -sink" 	Any product with "kitchen" and without "sink"
 	 *							"kitchen +sink" 	All the products with the word "sink"; if they also contain "kitchen", it increases the product’s relevancy.
 	 *
 	 * @param serviceable-area		Limits the results to a specific set of advertisers’ targeted areas.
 	 * @param isbn 	Limits the results to a specific product from multiple merchants identified by the appropriate unique identifier; ISBN.
 	 * @param upc 	Limits the results to a specific product from multiple merchants identified by the appropriate unique identifier; UPC.
 	 * @param manufacturer-name 	Limits the results to a particular manufacturer's name.
 	 * @param manufacturer-sku 		Limits the results to a particular manufacturer's SKU number.
 	 * @param advertiser-sku 		Limits the results to a particular advertiser SKU.
 	 * @param low-price 		Limits the results to products with a price greater than or equal to the 'low-price'.
 	 *							Tip: Use in conjunction with the 'high-price' to specify a specific range of prices.
 	 *							Note: Only whole numbers are supported for this request parameter. The 'low-price' parameter is inclusive, whereas the 'high-price' parameter is exclusive. 
 	 *							For example, using a low price of 10 and a high price of 20 will return everything from 10 to 19.99.
 	 *
 	 * @param high-price		Limits the results to products with a price less than or equal to the 'high-price'.
 	 *							Tip: Use in conjunction with the 'low-price' to specify a specific range of prices.
 	 *							Note: Only whole numbers are supported for this request parameter. The 'low-price' parameter is inclusive, whereas the 'high-price' parameter is exclusive. 
 	 *							For example, using a low price of 10 and a high price of 20 will return everything from 10 to 19.99.
 	 * @param low-sale-price 	Limits the results to products with a price greater than or equal to the Advertiser offered 'low-sale-price'.
 	 *							Note: Only whole numbers are supported for this request parameter. The 'low-sale-price' parameter is inclusive, whereas the 'high-sale-price' parameter is exclusive. 
 	 *							For example, using a low sale price of 10 and a high sale price of 20 will return everything from 10 to 19.99.
 	 *
 	 * @param high-sale-price 	Limits the results to products with a price less than or equal to the Advertiser offered 'high-sale-price'.
 	 *							Note: Only whole numbers are supported for this request parameter. The 'low-sale-price' parameter is inclusive, whereas the 'high-sale-price' parameter is exclusive. 
 	 *							For example, using a low sale price of 10 and a high sale price of 20 will return everything from 10 to 19.99.
 	 * @param currency 			Limits the results to one of the CJ supported tracking currencies.
 	 * @param sort-by			Sort the results in the response by one of the following values.
 	 *							Name
 	 *							Advertiser ID
 	 *							Advertiser Name
 	 *							Currency
 	 *							Price
 	 *							salePrice
 	 *							Manufacturer
 	 *							SKU
 	 *							UPC
 	 *							
 	 *							Note: Only the results returned in the particular request are sorted by the value of this parameter. 
 	 *							The system automatically sorts all matching results in the index (not just the results in the specific request) by relevance to keyword value sent in the request. *							
 	 *							Example
 	 *							Sample search parameter values for this example:
 	 *							keywords = shoes
 	 *							start-at = 0
 	 *							max-results = 500
 	 *							sort-by = price
 	 *							Suppose the index contains 5,432 possible results. The system automatically sorts those results based on their relevance to the keywords value (shoes). 
 	 *							Then, the system pulls the top 500 results and sorts those 500 results based on your 'sort-by' value ('price'). 
 	 *							Thus, the response includes the 500 most relevant matches, sorted by price.
 	 *
 	 * @param sort-order		Specifies the order in which the results are sorted; the following case-insensitive values are acceptable.
 	 *							asc: ascending (default value)
 	 *							dec: descending
	 * @param page-number 		Specifies the page of the results set that is currently being viewed.
	 * @param records-per-page 		Specifies the number of records to be viewed per page.
 	 *							If this parameter is not given, the response will return 25 records per page. The maximum value for this parameter is 100.
 	 * @param mobile-tracking-certified 	Limits the results to a set of particular advertisers (CIDs) using one of the following two values:
 	 *							True: This special value restricts the search to .
 	 *							False: This special value restricts the search to advertisers that have not been certified for mobile tracking
	 */
	public function productSearch($advertiser_ids = 'joined', $keywords = '', $isbn = '', $upc = '', $advertiser_sku = '', 
			$low_price = '', $high_price = '', $low_sale_price = '', $high_sale_price = '', $currency = '',
			$sort_by = SELF::SORT_TYPE_DEFAULT, $sort_order = SELF::SORT_ORDER_ASC,
			$page_number = 1, $records_per_page = 25){
		$params = [];
		$params['website-id'] = $this->CJ_WEBSITE_ID;
		$params['advertiser-ids'] = $advertiser_ids;
		$params['keywords'] = $keywords;

		//$params['serviceable-area'] = '';
		$params['isbn'] = $isbn;
		$params['upc'] = $upc;
		//$params['manufacturer-name'] = '';
		//$params['manufacturer-sku'] = '';
		//$params['advertiser-sku'] = '';
		$params['low-price'] = $low_price;
		$params['high-price'] = $high_price;
		$params['low-sale-price'] = $low_sale_price;
		$params['high-sale-price'] = $high_sale_price;
		$params['currency'] = $currency;
		$params['sort-by'] = $sort_by;
		$params['sort-order'] = $sort_order;
		$params['page-number'] = $page_number;
		$params['records-per-page'] = $records_per_page;
		$response = \Httpful\Request::get(SELF::CJ_PRODUCT_API.'?'.http_build_query($params))
					->addHeader('authorization', $this->CJ_KEY)
					->send();
		$body = $response->body;
		return isset($body->products)?$body->products:$body;
	}


	public function report($date_type, $start_date, $end_date, $cids, $action_types, $aids, $action_status, $commission_id, $website_ids){

		$params = ['date-type' => $date_type,
				'start-date' => $start_date,
				'end-date' => $end_date,
				'cids' => $cids,
				'action-types' => $action_types,
				'aids' => $aids,
				'action-status' => $action_status,
				'commission-id' => $commission_id,
				'website-ids' => $website_ids];

		$response = \Httpful\Request::get(SELF::CJ_COMMISSION_API.'?'.http_build_query($params))
					->addHeader('authorization', $this->CJ_KEY)
					->send();
		$body = $response->body;
		return isset($body->commision)?$body->commision:$body;
	}

}