<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Files extends MY_Controller{
	public function __construct(){
		parent::__construct();

	}

	public function index(){
		$css_files = array(
			'//ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css',
			'/assets/elfinder/css/elfinder.min.css',
			'/assets/elfinder/themes/windows-10/css/theme.css'
		);

		$js_files = array(
			'/assets/elfinder/js/elfinder.min.js',
			//'/assets/elfinder/js/i18n/elfinder.vi.js',
		);

		$js_inline = "
		$(document).ready(function(){
			//===== elFinder plugin =====//
			$('#elfinder').elfinder({
				url : '".admin_url('files/action')."',  // connector URL (REQUIRED)
				//lang: 'vi',                    // language (OPTIONAL)
				resizable: true
			});
		});
		";
	    
		$data_render = array(
			'css_files' => $css_files,
			'js_files' => $js_files,
			'js_inline' => $js_inline,
    		'page_title' =>  __('File Management'),
    		'page_desc' =>  __('File manage'),
    		'sidebar_active' => 'manage_files',
    		'module' => 'file_manager',
    		'message' => $this->session->flashdata('message'),
    	);
    	$this->load->view('index', $data_render);
	}

	public function action(){
		//$this->load->library('elfinder/connector');
		include dirname(__FILE__).'/../libraries/elfinder/connector.php';
	}

	public function popup(){
		$this->load->view('modules/file_manager_popup');
	}
}