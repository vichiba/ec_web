<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Catalogs extends MY_Controller{

	public function __construct(){
		parent::__construct();

		$this->load->model('category/category_model');
		
		$catalogs = $this->category_model->listActiveWithURI(0, '', 1, 1000, TRUE);
		$this->clear_catalog_list = $this->getClearCategoryWithLevel($catalogs);

		usort($this->clear_catalog_list, function ($a, $b) {
		    return strcmp($a->name_with_level, $b->name_with_level);
		});
	}

	function unique_field_name($field_name) {
    	return 's'.substr(md5($field_name),0,8); //This s is because is better for a string to begin               with a letter and not with a number
	}


	public function index(){
		$crud = new grocery_CRUD();
 		$crud->unset_jquery();
 		$crud->set_theme('datatables');

	    $crud->set_table('catalogs')
	        ->set_subject('Catalog');
	    $crud->order_by('position', 'asc');

	    $crud->columns('category_id', 'image_url', 'name', 'uri_path', 'parent_id', 'search_keyword', 'short_description', 
	    	'position', 'status', 'modified_date');
	    $crud->set_column_width('60px', '80px', '60px', '0', '120px', '80px');

	    $crud->fields('name', 'uri_path', 'parent_id', 'image_url', 'short_description', 'search_keyword', 'position', 'status', 'created_user_id', 'modified_user_id', 'modified_date');

		$crud
			->display_as('name', __('Title'))
			->display_as('uri_path', __('Friendly URL'))
			->display_as('parent_id', __('Parent'))
			->display_as('search_keyword', __('Search Keyword'))
			->display_as('short_description', __('Description'))
			->display_as('position', __('Position'))
			->display_as('status', __('Status'))
			->display_as('image_url', __('Image'));

	 	$crud->field_type('status', 'dropdown', array(SELF::STATUS_ACTIVE => 'Active', SELF::STATUS_INACTIVE => 'Inactive'));
	 	
    	$crud->field_type('image_url', 'image');

    	$crud->callback_field('parent_id',array($this,'callback_parent_category'));
    	$crud->callback_column('position',array($this,'callback_col_position'));
	 	$crud->callback_column('name',array($this,'callback_col_category_mapping'));
	 	$crud->callback_column('parent_id',array($this,'callback_col_parent_id'));
    	
    	$crud->callback_before_insert(array($this,'change_uri_path'));
    	$crud->callback_before_update(array($this,'change_uri_path'));

	 	$state = $crud->getState();
    	$state_info = $crud->getStateInfo();

    	if($state !== 'add'){
    		$crud->field_type('modified_date', 'hidden', get_current_time());
    		$crud->field_type('created_user_id', 'hidden');
    		$crud->field_type('modified_user_id', 'hidden', $this->session->userdata('user_id'));
    	}else{
    		$crud->field_type('modified_user_id', 'hidden');
    		$crud->field_type('modified_date', 'hidden');
    		$crud->field_type('created_user_id', 'hidden', $this->session->userdata('user_id'));
    	}


	    $crud->unset_read();
	 
	    $output = $crud->render();
	    $js_files = ['https://cdnjs.cloudflare.com/ajax/libs/jquery-csv/0.71/jquery.csv-0.71.min.js'];
	    $js_inline = '
	    $(document).ready(function(){
	    	function browserSupportFileUpload() {
		        var isCompatible = false;
		        if (window.File && window.FileReader && window.FileList && window.Blob) {
		        isCompatible = true;
		        }
		        return isCompatible;
		    }
		    function upload(evt, callback) {
			    if ( browserSupportFileUpload() ){
		            var data = null;
		            var file = evt.target.files[0];
		            var reader = new FileReader();
		            reader.readAsText(file);
		            reader.onload = function(event) {
		                var csvData = event.target.result;
		                console.log(csvData);
		                data = $.csv.toArrays(csvData);
		                if (data && data.length > 0) {
		                	callback(data.join(","));
		                }
		            };
		            reader.onerror = function() {
		                alert("Unable to read " + file.fileName);
		            };
		        }
			}
			$(".btn-upload-csv input").change(function(e){
				var dest = $($(this).parent().attr("dest"));
				upload(e, function(data){
					dest.text(data);
				});
			});

			$(".toogle_editable .value").click(function(e){
				var self = $(this).parent();
				var value = self.find(".value");
				var editable = self.find(".editable");
				if( value.is(":visible") ){
					value.hide();
					editable.show();
				}
			});

			$(".btn-save-position").click(function(e){
				var self = $(this);
				var input = $(this).prev();
				var val = input.val();

				var parent = self.closest(".toogle_editable");
					

				var category_id = input.attr("ref");
				self.attr("disabled", "disabled");
				$.post("'.admin_url('catalogs/save_position').'", {id: category_id, position: val}, function(success){
					if(success){
						self.removeAttr("disabled");
						parent.find(".value span").text(val);
					}
				}).always(function(){
					self.removeAttr("disabled");
					parent.find(".value").show();
					parent.find(".editable").hide();
				});
			});
	    });
	    ';
		$data_render = array(
			'js_files' => $js_files,
			'js_inline' => $js_inline,
    		'page_title' => __('Product\'s Catalog Management'),
    		'page_desc' => __('Manage catalog of product'),
    		'sidebar_active' => ($state == 'add' ? 'content_catalog_add': 'content_catalog'),
    		'module' => 'grocery',
    		'message' => $this->session->flashdata('message'),
    	);
    	$this->load->view('index', $this->getDataOuput($data_render, $output));
	}

	public function save_position(){
		$category_id = $this->input->post('id');
		$position = $this->input->post('position');

		echo $this->category_model->savePostion($category_id, $position);
	}

	function callback_parent_category($value = '', $primary_key = null){
		$html = '<select name="parent_id" id="field-parent_id" class="chosen-select">';
		$html.= '<option value="0">ROOT Level</option>';
		foreach ($this->clear_catalog_list as $item) {
			$html.= '<option value="'.$item->category_id.'" '.($value == $item->category_id ? 'selected' : '').'>'.$item->name_with_level.'</option>';
		}
		$html.= '</select>';
		return $html;
	}

	function callback_uri_path_field($value = '', $primary_key = null){
		return '<input id="field-uri_path" class="form-control friendly-url" ref="#field-name" name="uri_path" type="text" value="'.$value.'" />';
	}

	function change_uri_path($post_array, $primary_key = null) {
		$uri = strtolower(trim($post_array['uri_path']));
		$uri = preg_replace('/\s+/', '_', $uri);
		$post_array['uri_path'] = $uri;
		return $post_array;
	}

	function callback_col_parent_id($value, $row){
		foreach ($this->clear_catalog_list as $item) {
			if( $row->parent_id == $item->category_id) return $item->name;
		}
		return $row->parent_id == 0 ? '' : $row->parent_id;
	}

	function callback_col_position($value, $row){
		$html.= '<div class="toogle_editable">';
		$html.= '<div class="value"><span style="display: inline-block; width: 30px;">'.$value.'</span> <i class="icon-pencil" aria-hidden="true" style="color: #999"></i></div>';
		$html.= '<div class="editable" style="display: none">
					<input type="text" class="form-control text-center pull-left" value="'.$value.'" style="width: 70%; height: 31px" ref="'.$row->category_id.'"/>
					<button class="btn-save-position btn btn-xs btn-warning pull-right" style="width: 30%"><i class="icon-checkmark" aria-hidden="true"></i></button>
				</div>';
		$html.= '</div>';
		return $html;
	}

	public function mapping(){
		
		$crud = new grocery_CRUD();
 		$crud->unset_jquery();

	    $crud->set_table('catalog_mapping')
	        ->set_subject('Catalog Mapping');
	    $crud->order_by('id', 'asc');

	    $crud->columns('id', 'category_id', 'vendor', 'category_mapping_name', 'brand');
	    $crud->display_as('id', __('ID'));
	    $crud->display_as('category_id', __('Category ID'));
	    $crud->display_as('vendor',  'Vendor');
	    $crud->display_as('category_mapping_name', __('Mapping Name'));
	    $crud->display_as('brand', __('Brand'));
	    $crud->fields('category_id', 'vendor', 'category_mapping_name', 'brand');
	    $crud->set_column_width('60px', '80px', '60px', '0', '120px');

	    $crud->field_type('category_mapping_name','readonly');
	    $crud->field_type('brand','readonly');
	    $crud->field_type('vendor','readonly');
	 	//$crud->field_type('vendor','dropdown', array('CJ' => 'CJ', 'RAKUTEN' => 'RAKUTEN'));
	 	
	 	$crud->callback_field('category_id',array($this,'callback_category_mapping'));
	 	$crud->callback_column('category_id',array($this,'callback_col_category_mapping'));

	    $crud->unset_read();
	    $crud->unset_add();
	    $crud->unset_delete();
	 
	    $output = $crud->render();
	    
		$data_render = array(
    		'page_title' => __('Catalog Mapping'),
    		'page_desc' => __('Catalog Mapping Management'),
    		'sidebar_active' => 'content_catalog_mapping',
    		'module' => 'grocery',
    		'message' => $this->session->flashdata('message'),
    	);
    	$this->load->view('index', $this->getDataOuput($data_render, $output));
	}

	function callback_category_mapping($value = '', $primary_key = null){
		$html = '<select name="category_id" id="field-category_id" class="chosen-select">';
		foreach ($this->clear_catalog_list as $item) {
			$html.= '<option value="'.$item->category_id.'" '.($value == $item->category_id ? 'selected' : '').'>'.$item->name_with_level.'</option>';
		}
		$html.= '</select>';
		return $html;
	}

	function callback_col_category_mapping($value, $row){
		foreach ($this->clear_catalog_list as $item) {
			if( $row->category_id == $item->category_id) return $item->name_with_level;
		}
		return $row->category_id;
	}

	private function getClearCategoryWithLevel($list, $parent = ''){
		foreach ($list as $k => $item) {
			$list[$k]->name_with_level = ($parent ? $parent.' &raquo; ':'').$item->name;
			if($item->has_child){
				$list = array_merge($list, $this->getClearCategoryWithLevel($list[$k]->childs, $list[$k]->name_with_level));
				unset($list[$k]->childs);
			}
		}
		return $list;
	}
}