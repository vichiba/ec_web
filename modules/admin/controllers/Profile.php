<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends MY_Controller{

	public function __construct(){
		parent::__construct();

		$this->load->model('users_model', '_model');
	}

	private function is_password_strong($str){
        if( preg_match('#[0-9]#', $str) && preg_match('#[a-z]#', $str) ){
            return TRUE;
        }
        $this->session->set_flashdata('error_message',  __('Password not secure. Please try again with other secure password.<br/>New Password must contain number (0-9),  least one alphabet (a-z).'));
        return FALSE;
    }

	public function index(){

		$user_id 			= $this->session->userdata('user_id');
		$user_password 		= $this->input->post('user_password');
		$new_password 		= $this->input->post('new_password');
		$confirm_password 	= $this->input->post('confirm_password');

		if ($_SERVER['REQUEST_METHOD'] == 'POST'){
			if( $user_password && $new_password && $confirm_password ){
				if( $this->_model->exists(['user_id' => $user_id, 'user_password' => md5($user_password)]) ){
					if( strlen($new_password) > 6 && $this->is_password_strong($new_password) ){
						if($new_password == $confirm_password){
							$data = [
								'user_password' => md5($confirm_password)
							];
							if( $this->_model->update($user_id, $data) !== FALSE ){
								$this->session->set_flashdata('message',  __('New password has been changed.'));
							}else{
								$this->session->set_flashdata('error_message',  __('Error while changing new password. Please try again latter.'));
							}
						}else{
							$this->session->set_flashdata('error_message',  __('Confirm password is not match.'));
						}
					}
				}else{
					$this->session->set_flashdata('error_message',  __('Current password is not match.'));
				}
			}else{
				$this->session->set_flashdata('error_message',  __('Enter your password to change profile.'));
			}
			redirect(admin_url('profile'));
			return;
		}

		$data_render = array(
    		'page_title' =>  __('User Profile'),
    		'page_desc' =>  __('User Profile'),
    		'sidebar_active' => 'manage_profile',
    		'module' => 'profile',
    		'message' => $this->session->flashdata('message'),
    		'error_message' => $this->session->flashdata('error_message'),
    	);
    	
    	$this->load->view('index', $data_render);
	}
}