<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once dirname(__FILE__) . '/../libraries/php-crontab-manager/src/CronEntry.php';
require_once dirname(__FILE__) . '/../libraries/php-crontab-manager/src/CliTool.php';
require_once dirname(__FILE__) . '/../libraries/php-crontab-manager/src/CrontabManager.php';

use php\manager\crontab\CrontabManager;

class Crontab extends MY_Controller{

	#const CMD = 'php '.FCPATH.'/index.php cronjobs run test >> '.FCPATH.'/cron_logs/cronjobs_$(date +"%Y-%m-%d").log 2>&1';
	const CMD = 'sh '.FCPATH.'run.sh ALL  >> '.FCPATH.'cron_logs/cronjobs_$(date +"\%Y-\%m-\%d").log 2>&1';

	public function __construct(){
		parent::__construct();
	}

	public function index(){

		$cronjob = $this->input->post('cronjob');

		if( $cronjob ){
			$this->load->model('config_model');
			$old_cronjob = get_config_value('CRONJOB');

			if( $this->config_model->updateByKey('CRONJOB', $cronjob) !== FALSE ){

				//Save CronJob to file
				$this->saveCronJob($old_cronjob, $cronjob);

				$this->session->set_flashdata('message', __('Crontab configurations has been save.'));
			}else{
				$this->session->set_flashdata('message', __('Error while saving Crontab configurations. Please try again latter.'));
			}
			redirect(admin_url('crontab'));
			return;
		}


		$js_files = [
			'/assets/cron/jquery-cron-min.js'
		];
		$css_files = [
			'/assets/cron/jquery-cron.css'
		];

		$cronjob = get_config_value('CRONJOB');

		$js_inline = '
		$(document).ready(function(){
			$("#cronjob").cron({
			    initial: "'.$cronjob.'",
			    customValues: {
			        "'.__('15 Minutes').'" : "*/15 * * * *",
			        "'.__('30 Minutes').'" : "*/30 * * * *",
			        "'.__('2 Hours').'" : "* */2 * * *",
			        "'.__('4 Hours').'" : "* */2 * * *",
			        "'.__('6 Hours').'" : "* */6 * * *",
			        "'.__('10 Hours').'" : "* */10 * * *",
			    },
			    onChange: function() {
			    	var $val = $(this).cron("value");
			    	console.log($val);
			        $("input[name=cronjob]").val($val);
			    }
			});

			if("'.$this->input->get('f').'" !== "" && $("#terminal").length){
				console.log("START MONITORING...");
				var countBlank = 0;
				var interval = setInterval(function(){
					$.ajax({
						url: "'.admin_url('crontab/monitor').'",
						data: {f: "'.$this->input->get('f').'"},
					})
					.done(function(data) {
						if( !data ){
							countBlank++;
						}
						countBlank = 0;
						if( countBlank >= 15 ) clearInterval(interval);
						var terminalIndex = data.lastIndexOf(\'\\033[\');
						var isProcessBar = false;
						if( terminalIndex  > -1){
							isProcessBar = true;
							data = data.substr(terminalIndex);
							var lastDiv = $("#terminal div:last");
							if(lastDiv.hasClass("processing")) lastDiv.remove();
							data = data.replace(/\\033\\[([0-9]+)D/ig, "");
							data.replace(" ", "&nbsp;");
						}
						if($("#terminal div:last").text() !== data){
							var html = "<div "+(isProcessBar ? "class=\"processing\"" : "")+">"+data+"</div>";
							$("#terminal").append(html);

							$("#terminal").animate({
							    scrollTop: $("#terminal")[0].scrollHeight
							}, 500);
						}
					})
					.fail(function() {
						console.log("error");
					});
				},1000);
			}
			var LOAD_MORE_LINE = 0;
			$(".terminal_loadmore a").click(function(e){
				e.preventDefault();
				var self = $(this);
				$.ajax({
					url: "'.admin_url('crontab/monitor').'/"+(LOAD_MORE_LINE+=10),
					data: {f: "'.$this->input->get('f').'"},
				})
				.done(function(data) {
					$("#terminal .prepend").remove();
					data = data.replace(/\\033\\[([0-9]+)D/ig, "<br/>");
					$("#terminal").prepend("<div class=\"prepend\">"+data+"</div>");
					$("#terminal").animate({
					    scrollTop: 0
					}, 500);
				})
				.fail(function() {
					console.log("error");
				});
			});
		});
		';

		$crontab = $this->getCrontabManager();

		$data_render = array(
    		'page_title' => __('Crontab configuration'),
    		'page_desc' => __('Crontab configuration'),
    		'sidebar_active' => 'manage_crontab',
    		'module' => 'contab_config',
    		'js_files' => $js_files,
    		'css_files' => $css_files,
    		'js_inline' => $js_inline,
			'cronjob' => $cronjob,
			'joblists' => $crontab->listJobs(),
			'listFiles' => $this->listFiles(),
    		'message' => $this->session->flashdata('message'),
    	);
    	
    	$this->load->view('index', $data_render);
	}

	private function getCrontabManager(){
		$crontab = new CrontabManager();
		return $crontab;
	}

	private function saveCronJob($old_data, $data){
		$crontab = $this->getCrontabManager();
		
		//Delete old job
		$old_job = $crontab->newJob();
		$old_job->on($old_data)->doJob(self::CMD);
		$crontab->deleteJob($old_job);

		//add new job
		$job = $crontab->newJob();
		$job->on($data)->doJob(self::CMD);
		$crontab->add($job);
		
		//save
		$crontab->save(false);

	}

	public function listFiles(){
		$this->db->from('crontab_jobs');
		$this->db->order_by('log_id', 'DESC');
		$this->db->group_by('log_str');
		return $this->db->get()->result();
	}

	public function monitor($lines = 1, $adaptive = true) {
		$filepath = $this->input->get('f');

		$filepath = FCPATH.'/cron_logs/'.$filepath;

		// Open file
		$f = @fopen($filepath, "rb");
		if ($f === false) return false;

		// Sets buffer size
		if (!$adaptive) $buffer = 4096;
		else $buffer = ($lines < 2 ? 64 : ($lines < 10 ? 512 : 4096));

		// Jump to last character
		fseek($f, -1, SEEK_END);

		// Read it and adjust line number if necessary
		// (Otherwise the result would be wrong if file doesn't end with a blank line)
		if (fread($f, 1) != "\n") $lines -= 1;
		
		// Start reading
		$output = '';
		$chunk = '';

		// While we would like more
		while (ftell($f) > 0 && $lines >= 0) {

			// Figure out how far back we should jump
			$seek = min(ftell($f), $buffer);

			// Do the jump (backwards, relative to where we are)
			fseek($f, -$seek, SEEK_CUR);

			// Read a chunk and prepend it to our output
			$output = ($chunk = fread($f, $seek)) . $output;

			// Jump back to where we started reading
			fseek($f, -mb_strlen($chunk, '8bit'), SEEK_CUR);

			// Decrease our line counter
			$lines -= substr_count($chunk, "\n");

		}

		// While we have too many lines
		// (Because of buffer size we might have read too many)
		while ($lines++ < 0) {

			// Find first newline and remove all text before that
			$output = substr($output, strpos($output, "\n") + 1);

		}

		// Close file and return
		fclose($f);
		echo nl2br(trim($output));
	}
}