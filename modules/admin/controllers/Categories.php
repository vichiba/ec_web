<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Categories extends MY_Controller{
	
	public function __construct(){
		parent::__construct();
	}

	public function index(){
		$crud = new grocery_CRUD();
 		$crud->unset_jquery();

	    $crud->set_table('categories')
	        ->set_subject('Category');
	    $crud->order_by('cat_modified_date', 'desc');

	    $crud->columns('cat_title', 'cat_url', 'cat_status', 'cat_modified_date');
	    $crud->set_column_width('200px', '0', '80px', '120px', '80px');

	    $crud->fields('cat_title', 'cat_url', 'cat_seo_title', 'cat_seo_description', 'cat_seo_keyword', 'cat_status', 'cat_user_id', 'cat_modified_date');

	    $crud->wrap_fields(
	    	array(
	    		array('label' => __('General'), 'icon' => 'icon-info'),
	    		array('label' => __('SEO'), 'icon' => 'icon-search3'),
	    	),
	    	array(
		    	array('cat_title', 'cat_url' , 'cat_status'),
		    	array('cat_seo_title', 'cat_seo_description', 'cat_seo_keyword'),
	    	)
	    );

		$crud
			->display_as('cat_title', __('Title'))
			->display_as('cat_url', __('Friendly URL'))
			->display_as('cat_status', __('Status'))
			->display_as('cat_modified_date', __('Updated date'))
			->display_as('cat_seo_title', __('Title'))
			->display_as('cat_seo_description', __('Description'))
			->display_as('cat_seo_keyword', __('Keyword'));

	 	$crud->field_type('cat_status','dropdown', array(SELF::STATUS_ACTIVE => 'Active', SELF::STATUS_INACTIVE => 'InActive'));
	 	$crud->field_type('cat_title', 'string');
	 	$crud->field_type('cat_url', 'string');
    	$crud->field_type('cat_user_id', 'hidden', $this->session->userdata('user_id'));

    	$crud->callback_field('cat_url',array($this,'callback_cat_url_field'));

	 	$state = $crud->getState();
    	$state_info = $crud->getStateInfo();
    	$crud->field_type('cat_modified_date', 'hidden', get_current_time());
	 	
	 	$crud->unset_fields('cat_created_date');
	    $crud->unset_read();

	    $crud->set_js_config('assets/js/admin.friendly_url.js');
	 
	    $output = $crud->render();
	    
		$data_render = array(
    		'page_title' =>  __('Category Management'),
    		'page_desc' =>  __('Category manage'),
    		'sidebar_active' => 'content_category',
    		'module' => 'grocery',
    		'message' => $this->session->flashdata('message'),
    	);
    	$this->load->view('index', $this->getDataOuput($data_render, $output));
	}

	function callback_cat_url_field($value = '', $primary_key = null){
		return '<input id="field-cat_url" class="form-control friendly-url" ref="#field-cat_title" name="cat_url" type="text" value="'.$value.'" />';
	}
}