<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Category_auto_mapping extends MY_Controller{

	public function __construct(){
		parent::__construct();

		$this->load->model('category/category_model');
		
		$catalogs = $this->category_model->listActiveWithURI(0, '', 1, 1000, TRUE);
		$this->clear_catalog_list = $this->getClearCategoryWithLevel($catalogs);

		usort($this->clear_catalog_list, function ($a, $b) {
		    return strcmp($a->name_with_level, $b->name_with_level);
		});
	}

	function unique_field_name($field_name) {
    	return 's'.substr(md5($field_name),0,8); //This s is because is better for a string to begin               with a letter and not with a number
	}


	public function index(){
		$crud = new grocery_CRUD();
 		$crud->unset_jquery();

	    $crud->set_table('category_auto_mapping')
	        ->set_subject('Mapping');
	    $crud->order_by('created_date', 'desc');

	    $crud->columns('category_id', 'keyword1', 'keyword2', 'keyword3', 'keyword4', 'keyword5', 'keyword6', 'status', 'created_date');

	    $crud->fields('category_id', 'keyword1', 'keyword2', 'keyword3', 'keyword4', 'keyword5', 'keyword6', 'status');

		$crud
			->display_as('keyword1',  __('Keyword 1'))
			->display_as('keyword2',  __('Keyword 2'))
			->display_as('keyword3',  __('Keyword 3'))
			->display_as('keyword4',  __('Keyword 4'))
			->display_as('keyword5',  __('Keyword 5'))
			->display_as('keyword6',  __('Keyword 6'))
			->display_as('category_id',  __('Category'));

		$crud->field_type('status', 'dropdown', array(SELF::STATUS_ACTIVE => 'Active', SELF::STATUS_INACTIVE => 'Inactive'));

    	$crud->callback_field('category_id',array($this,'callback_category'));
	 	$crud->callback_column('category_id',array($this,'callback_col_category_id'));

	    $crud->unset_read();
	 
	    $output = $crud->render();
	    
		$data_render = array(
    		'page_title' => __('Category auto mapping Management'),
    		'page_desc' => __('Category mapping by 6 keywords'),
    		'sidebar_active' => 'content_auto_mapping',
    		'module' => 'grocery',
    		'message' => $this->session->flashdata('message'),
    	);
    	$this->load->view('index', $this->getDataOuput($data_render, $output));
	}

	function callback_category($value = '', $primary_key = null){
		$html = '<select name="category_id" id="field-category_id" class="chosen-select">';
		foreach ($this->clear_catalog_list as $item) {
			$html.= '<option value="'.$item->category_id.'" '.($value == $item->category_id ? 'selected' : '').'>'.$item->name_with_level.'</option>';
		}
		$html.= '</select>';
		return $html;
	}

	function callback_col_category_id($value, $row){
		foreach ($this->clear_catalog_list as $item) {
			if( $row->category_id == $item->category_id) return $item->name;
		}
		return $row->category_id == 0 ? '' : $row->category_id;
	}

	private function getClearCategoryWithLevel($list, $parent = ''){
		foreach ($list as $k => $item) {
			$list[$k]->name_with_level = ($parent ? $parent.' &raquo; ':'').$item->name;
			if($item->has_child){
				$list = array_merge($list, $this->getClearCategoryWithLevel($list[$k]->childs, $list[$k]->name_with_level));
				unset($list[$k]->childs);
			}
		}
		return $list;
	}
}