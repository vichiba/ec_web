<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends MY_Controller{
	public function __construct(){
		parent::__construct();
	}

	public function index(){

		$crud = new grocery_CRUD();
 		$crud->unset_jquery();

	    $crud->set_table('customers')
	        ->set_subject(__('Customer'));
	    $crud->order_by('created_time', 'desc');

	    $crud->columns('avatar', 'email', 'fullname', 'active_time', 'block_time', 'last_logon_time', 'created_time', 'status');
	    $crud->unset_fields('password', 'refresh_token', 'active_token', 'resetpw_token', 'active_time', 'block_time', 'last_logon_time', 'created_time');

	    $crud->display_as('avatar', __('Avatar'));
	    $crud->display_as('email', __('Email'));
		$crud->display_as('fullname', __('Fullname'));
		$crud->display_as('active_time', __('Active Time'));
		$crud->display_as('block_time', __('Block Time'));
		$crud->display_as('last_logon_time', __('Last Logon Time'));
		$crud->display_as('created_time', __('Created Time'));
		$crud->display_as('status', __('Status'));

		$crud->field_type('notice', 'text');
		$crud->unset_texteditor('notice');
	 	$crud->field_type('status','dropdown', array(SELF::STATUS_ACTIVE => 'Active', SELF::STATUS_INACTIVE => 'Inactive'));	
    	$crud->field_type('avatar', 'image');

	 	$state = $crud->getState();
    	$state_info = $crud->getStateInfo();

	    $crud->unset_read();
	 
	    $output = $crud->render();
	    
		$data_render = array(
    		'page_title' => __('Customer\'s Management'),
    		'page_desc' =>  __('Manage customers'),
    		'sidebar_active' => 'customer',
    		'module' => 'grocery',
    		'message' => $this->session->flashdata('message'),
    	);
    	$this->load->view('index', $this->getDataOuput($data_render, $output));
	}

}