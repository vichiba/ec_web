<?php defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Controller{
	public function __construct(){
		parent::__construct();
	}

	public function index(){
		$crud = new grocery_CRUD();
 		$crud->unset_jquery();
 		$crud->where('user_id !=', 1);

	    $crud->set_table('users')
	        ->set_subject(__('Account'));
	    $crud->order_by('user_timestamp', 'desc');

	    $crud->columns('user_login', 'user_fullname', 'user_photo', 'user_status', 'user_timestamp');
	    $crud->set_column_width('80px', '0', '100px', '80px', '80px', '80px');

		$crud->unset_columns('remember_token', 'remember_token_expired');
		$crud->display_as('user_login', __('Username'))
			->display_as('user_password', __('Password'))
			->display_as('user_fullname', __('Fullname'))
			->display_as('user_photo', __('Avatar'))
			->display_as('user_status', __('Status'))
			->display_as('user_timestamp', __('Created date'));
	 	$crud->field_type('user_status','dropdown',
            array(SELF::STATUS_ACTIVE => 'Active', SELF::STATUS_INACTIVE => 'Inactive'));

	 	$crud->field_type('user_photo', 'image');
	 	$crud->field_type('user_password', 'password');
        $crud->callback_before_insert(array($this,'encrypt_password_callback'));
        $crud->callback_before_update(array($this,'encrypt_password_callback'));
	 	
	 	$crud->unset_fields('user_timestamp', 'remember_token', 'remember_token_expired');
	 	
	    //$crud->unset_read();
	 
	    $output = $crud->render();
	    
		$data_render = array(
    		'page_title' => __('User Management'),
    		'page_desc' => __('User manage'),
    		'sidebar_active' => 'manage_users',
    		'module' => 'grocery',
    		'message' => $this->session->flashdata('message'),
    	);
    	$this->load->view('index', $this->getDataOuput($data_render, $output));
	}

	function encrypt_password_callback($post_array, $primary_key = null){
        $post_array['user_password'] = md5($post_array['user_password']);
        return $post_array;
    }
}