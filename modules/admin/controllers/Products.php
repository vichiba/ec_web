<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends MY_Controller{

	public function __construct(){
		parent::__construct();
	}

	public function index(){
		$crud = new grocery_CRUD();
 		$crud->unset_jquery();

	    $crud->set_table('products')
	        ->set_subject('Product');
	    $crud->order_by('modified_date', 'desc');

	    $crud->columns('image_url', 'sku', 'name', 'short_description', 'vendor', 'price', 'click_count', 'buy_count', 'brand_id', 'category_id', 'status', 'modified_date');
//	    $crud->set_column_width('80px', '60px', '0', '120px', '80px');

	    $crud->fields('sku', 'name','image_url', 'short_description', 'long_description', 'vendor', 'buy_url', 'price', 'sale_price', 'retail_price', 'click_count', 'buy_count', 'brand_id', 'category_list', 'status', 'created_user_id', 'modified_user_id', 'modified_date');

		$crud
			->display_as('name', __('Title'))
			->display_as('uri_path', __('Friendly URL'))
			->display_as('parent_id', __('Parent'))
			->display_as('search_keyword', __('Search Keyword'))
			->display_as('short_description', __('Description'))
			->display_as('position', __('Position'))
			->display_as('status', __('Status'));

		//$crud->set_relation('category_id', 'catalogs', 'name');
		$crud->set_relation_n_n('category_list', 'product_catalogs', 'catalogs', 'product_id', 'category_id', 'name', 'priority');
		$crud->set_relation('brand_id', 'brands', 'name');

	 	$crud->field_type('status','dropdown', array(SELF::STATUS_ACTIVE => 'Active', SELF::STATUS_INACTIVE => 'Inactive'));
	 	
    	$crud->field_type('image_url', 'image');

	 	$state = $crud->getState();
    	$state_info = $crud->getStateInfo();

    	if($state !== 'add'){
    		$crud->field_type('modified_date', 'hidden', get_current_time());
    		$crud->field_type('created_user_id', 'hidden');
    		$crud->field_type('modified_user_id', 'hidden', $this->session->userdata('user_id'));
    	}else{
    		$crud->field_type('modified_user_id', 'hidden');
    		$crud->field_type('modified_date', 'hidden');
    		$crud->field_type('created_user_id', 'hidden', $this->session->userdata('user_id'));
    	}


	    $crud->unset_read();
	 
	    $output = $crud->render();
	    
		$data_render = array(
    		'page_title' => __('Product Management'),
    		'page_desc' => __('Manage product'),
    		'sidebar_active' => ($state == 'add' ? 'content_product_add': 'content_product'),
    		'module' => 'grocery',
    		'message' => $this->session->flashdata('message'),
    	);
    	$this->load->view('index', $this->getDataOuput($data_render, $output));
	}
}