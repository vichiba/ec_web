<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends MY_Controller{

	public function __construct(){
		parent::__construct();
		
		$this->load->model('product/product_model');
	}

	public function index(){
		$crud = new grocery_CRUD();
 		$crud->unset_jquery();

	    $crud->set_table('tracks')
	        ->set_subject(__('Click Tracker'));
	    $crud->order_by('track_id', 'desc');

	    $crud->columns('source_content', 'source_type', 'source_id', 'source_url', 'source_ip', 'created_date');

		$crud->display_as('source_content', __('From Page'));
		$crud->display_as('source_type', __('Click Type'));
		$crud->display_as('source_id', __('Value'));
		$crud->display_as('source_url', __('Source'));
		$crud->display_as('dest_url', __('Destination'));
		$crud->display_as('source_ip', __('IP Address'));

		$crud->field_type('source_url', 'text');
		$crud->unset_texteditor('source_url');
		$crud->field_type('dest_url', 'text');
		$crud->unset_texteditor('dest_url');

	 	$crud->field_type('source_type','dropdown', array('PRODUCT' => 'Product'));
	 	
	 	$crud->callback_column('source_id', array($this, '_callback_col_source_id'));
	 	$crud->callback_column('source_url', array($this, '_callback_col_url'));

	 	$crud->callback_read_field('source_id', array($this, '_callback_read_source_id'));
	 	$crud->callback_read_field('source_url', array($this, '_callback_read_source_url'));
	 	$crud->callback_read_field('dest_url', array($this, '_callback_read_dest_url'));

	    $crud->unset_add();
	    $crud->unset_edit();
	    $crud->unset_delete();
	 
	    $output = $crud->render();
	    
		$data_render = array(
    		'page_title' => __('Tracking\'s Management'),
    		'page_desc' => __('Manage click actions'),
    		'sidebar_active' => 'report_tracking',
    		'module' => 'grocery',
    		'message' => $this->session->flashdata('message'),
    	);
    	$this->load->view('index', $this->getDataOuput($data_render, $output));
	}

	function _callback_col_source_id($value, $row){
		if( strtolower($row->source_type) == 'product' && ($product = $this->product_model->find($value)) ){
			return '<a href="'.site_url($value.'.html').'" target="_blank">'.$product->name.'</a>';
		}
		return $value;
	}

	function _callback_read_source_id($value, $primary_key, $field_info, $row){
		if( strtolower($row->source_type) == 'product' && ($product = $this->product_model->find($value)) ){
			return '<a href="'.site_url($value.'.html').'" target="_blank">'.$product->name.'</a>';
		}
		return $value;
	}

	function _callback_col_url($value, $row){
		return utf8_decode(urldecode($value));
	}


	function _callback_read_source_url($value, $row){
		$link = utf8_decode(urldecode($value));
		return $link.'<br/><a target="_blank" href="/'.$link.'" class="btn btn-success btn-xs">Test this link</a>';
	}

	function _callback_read_dest_url($value, $row){
		$link = utf8_decode(urldecode($value));
		return '<textarea class="form-control" readonly>'.$link.'</textarea><br/><a target="_blank" href="'.$link.'" class="btn btn-warning btn-xs">Test this link</a>';
	}

	public function cj(){
		$this->load->library('CJConnector', NULL, '_cj');

		$date_type = $this->input->get('date_type');
		$start_date = $this->input->get('start_date');
		$end_date = $this->input->get('end_date');
		$cids = $this->input->get('cids');
		$action_types = $this->input->get('action_types');
		$aids = $this->input->get('aids');
		$action_status = $this->input->get('action_status');
		$commission_id = $this->input->get('commission_id');
		$website_ids = $this->input->get('website_ids');

		$commissions = $this->_cj->report($date_type, $start_date, $end_date, $cids, $action_types, $aids, $action_status, $commission_id, $website_ids);

    	$js_files = [
			'//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js'
		];

		$css_files = [
			'//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css'
		];

		$js_inline = '
		$(document).ready(function(){
			$oTable = $(".data-table").DataTable({
		    	
		    });
		});
		';

		//debug($brands, TRUE);

		$data_render = array(
			'commissions' => $commissions,
			'js_files' => $js_files,
			'css_files' => $css_files,
			'js_inline' => $js_inline,
    		'page_title' => __('CJ Commissons Report'),
    		'page_desc' => __('CJ Commissons Report'),
    		'sidebar_active' => 'report_cj',
    		'module' => 'report/cj',
    		'message' => $this->session->flashdata('message'),
    	);
    	$this->load->view('index', $data_render);
	}

	public function rakuten(){
		$this->load->library('RakutenConnector', NULL, '_rakuten');

		$bdate = $this->input->get('bdate');
		$edate = $this->input->get('edate');
		$reportid = $this->input->get('reportid');
		$nid = $this->input->get('nid');
		$mid = $this->input->get('mid');

		$commissions = $this->_rakuten->advanceReport($bdate, $edate, $reportid, $nid, $mid);

		//debug($commissions, TRUE);

    	$js_files = [
			'//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js'
		];

		$css_files = [
			'//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css'
		];

		$js_inline = '
		$(document).ready(function(){
			$oTable = $(".data-table").DataTable({
		    	
		    });
		});
		';

		//debug($brands, TRUE);

		$data_render = array(
			'commissions' => $commissions,
			'js_files' => $js_files,
			'css_files' => $css_files,
			'js_inline' => $js_inline,
    		'page_title' => __('Rakuten Commissons Report'),
    		'page_desc' => __('Rakuten Commissons Report'),
    		'sidebar_active' => 'report_rakuten',
    		'module' => 'report/rakuten',
    		'message' => $this->session->flashdata('message'),
    	);
    	$this->load->view('index', $data_render);
	}
}