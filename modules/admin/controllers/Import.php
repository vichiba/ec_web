<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Import extends MY_Controller{
	const TYPE_SYSTEM 	= 1;
	const TYPE_WEBSITE 	= 2;
	const TYPE_3PARTY 	= 3;

	public function __construct(){
		parent::__construct();
		//$this->lang->load('import', 'ja');
	}

	public function accesskey(){
		$thirhparty_config = $this->getConfigUI(self::TYPE_3PARTY);
	    
		$data_render = array(
    		'page_title' =>  __('Thirth Party Config'),
    		'page_desc' =>  __('Thirth Party Access Key config'),
    		'sidebar_active' => 'import_accesskey',
    		'module' => 'grocery',
    		'message' => $this->session->flashdata('message'),
    	);
    	$this->load->view('index', $this->getDataOuput($data_render, $thirhparty_config));
	}

	/**
	* Get config UI by type
	* @param $config_type:
	* 		1: System Config
	* 		2: Website Config
	* 		3: Thirtparty Config
	*/
	public function getConfigUI($config_type = self::TYPE_WEBSITE){
		$crud = new grocery_CRUD();
 		$crud->unset_jquery();

	    $crud->set_table('config')
	        ->set_subject('Config');
	    $crud->where('conf_type', $config_type);

	    $crud->columns('conf_key', 'conf_value', 'conf_desc', 'conf_status');
	    $crud->fields('conf_desc', 'conf_value', 'conf_status');

	    $crud->display_as('conf_key',  __('Key'))
	    	->display_as('conf_value',  __('Value'))
	    	->display_as('conf_status',  __('Status'))
	    	->display_as('conf_desc',  __('Description'));
	   	$crud->field_type('conf_key', 'readonly');
	   	$crud->field_type('conf_desc', 'readonly');
	   	$crud->field_type('conf_status','dropdown',
            array('1' => 'Active', '0' => 'Inactive'));

	   	$state = $crud->getState();
    	$state_info = $crud->getStateInfo();
    	if($state == 'edit'){
    		$this->load->model('config_model');
	        $primary_key = $state_info->primary_key;
    		$conf_obj = $this->config_model->find($primary_key);
	        if($conf_obj->conf_richeditor !== '1'){
	        	$crud->unset_texteditor('conf_value');
	        }
	    }


	    $crud->unset_fields('updated_date');

	    $crud->unset_read();
	    $crud->unset_add();
	    $crud->unset_delete();
	 
	    return $crud->render();
	}

	public function getCJEventInfo($actions){
		$return = [];
		if( isset($actions->action) ){
			$actions = $actions->action;
			if( !is_array($actions) ){
				$actions = [$actions];
			}
			foreach ($actions as $action){
				$return[] = [$action->name, $action->type, $action->commission->default];
			}
		}
		return $return;
	}

	public function brand($vendor = 'cj', $brand_id = NULL){
		debug_mode();
		$action = $this->input->get('action');
		$js_inline = "";
		$brands = [];
		$this->load->model('brand/brand_model');
		switch ($vendor) {
			case 'cj':
				$this->load->library('CJConnector', NULL, '_cj');

				$advertiser_ids 	= $this->input->get('advertiser_ids');
				$advertiser_name 	= $this->input->get('advertiser_name');
				$keywords 			= $this->input->get('keywords');
				$page_number 		= $this->input->get('page_number');
				$records_per_page 	= $this->input->get('records_per_page');
				if($brand_id){
					$advertiser_ids = $brand_id;
				}
				$brands = $this->_cj->getBrand($advertiser_ids, $advertiser_name, $keywords, $page_number, $records_per_page);
				if($brand_id){
					$brand = NULL;
					if( isset($brands->advertisers) && isset($brands->advertisers->advertiser) ) $brand = $brands->advertisers->advertiser;

					if($action == 'add' || $action == 'update'){
						if($brand){
							$db_id = $this->brand_model->exists(['vendor' => 'CJ', 'ref_id' => $brand->{"advertiser-id"}]);
							$actions = $this->getCJEventInfo($brand->actions);
							$event_name = NULL;
							$event_type = NULL;
							$event_value = NULL;
							foreach ($actions as $event) {
								$event_name = $event[0];
								$event_type = $event[1];
								$event_value = $event[2];
							}
							if( !$db_id ){
								$data = ['name' => $brand->{"advertiser-name"},
										'uri_path' => 'cj_'.$brand->{"advertiser-id"},
										'image_url' => '',
										'search_keyword' => $brand->{"advertiser-name"},
										'domain' => $brand->{"program-url"},
										'short_description' => '',
										'long_description' => '',
										'vendor' => 'CJ',
										'event_name' => $event_name,
										'event_type' => $event_type,
										'event_value' => $event_value,
										'ref_id' => $brand->{"advertiser-id"},
										'raw_data' => json_encode($brand),
										'created_user_id' => $this->session->userdata('user_id'),
										'status' => Brand_model::STATUS_ACTIVE];
								$db_id = $this->brand_model->insert($data);
							}else{
								$data = ['name' => $brand->{"advertiser-name"},
										'uri_path' => 'cj_'.$brand->{"advertiser-id"},
										'search_keyword' => $brand->{"advertiser-name"},
										'domain' => $brand->{"program-url"},
										'vendor' => 'CJ',
										'event_name' => $event_name,
										'event_type' => $event_type,
										'event_value' => $event_value,
										'ref_id' => $brand->{"advertiser-id"},
										'raw_data' => json_encode($brand),
										'modified_user_id' => $this->session->userdata('user_id'),
										'modified_date' => date('Y-m-d H:m:s')];
								$this->brand_model->update($db_id, $data);
							}
							//$params = $this->input->get();
							//unset($params['action']);
							//redirect(admin_url("import/brand/$vendor?".http_build_query($params)));
							echo $db_id;
							return;
						}else{
							return;
						}
					}
				}

				$js_inline .= '
				jQuery(document).ready(function($) {
					$("select[name=advertiser_ids]").change(function(){
						if( $(this).val() == "CIDs" ){
							$("input[name=advertiser_ids]").removeAttr("disabled").show();
						}else{
							$("input[name=advertiser_ids]").attr("disabled", "true").hide();
						}
					});
					$("select[name=advertiser_ids]").trigger("change");
				});
				';

				break;
			case 'rakuten':
				$this->load->library('RakutenConnector', NULL, '_rakuten');
				$merchant_name 	= $this->input->get('merchantname');
				if($brand_id){
					if($action == 'add' || $action == 'update'){
						$brand = $this->_rakuten->getAdvertiserInfo($brand_id);
						$brand = $brand->xpath('//ns1:return')[0];						
						$event_name = NULL;
						$event_type = NULL;
						$event_value = NULL;

						$event_name = (string)$brand->xpath('ns1:offer')[0]->xpath('ns1:offerName')[0];
						$event_type = NULL;
						$event_value = (string)$brand->xpath('ns1:offer')[0]->xpath('ns1:commissionTerms')[0];

						$db_id = $this->brand_model->exists(['vendor' => 'RAKUTEN', 'ref_id' => $brand_id]);
						if( !$db_id ){
							$data = ['name' => (string)$brand->xpath('ns1:name')[0],
									'uri_path' => 'rk_'.(string)$brand->xpath('ns1:mid')[0],
									'image_url' => 'http://merchant.linksynergy.com/fs/logo/lg_'.(string)$brand->xpath('ns1:mid')[0].'.jpg',
									'search_keyword' => (string)$brand->xpath('ns1:name')[0],
									'domain' => '',
									'short_description' => '',
									'long_description' => '',
									'vendor' => 'RAKUTEN',
									'event_name' => $event_name,
									'event_type' => $event_type,
									'event_value' => $event_value,
									'ref_id' => (string)$brand->xpath('ns1:mid')[0],
									'raw_data' => $brand->asXML(),
									'created_user_id' => $this->session->userdata('user_id'),
									'status' => Brand_model::STATUS_ACTIVE];
							$db_id = $this->brand_model->insert($data);
						}else{
							$data = ['name' => (string)$brand->xpath('ns1:name')[0],
									'uri_path' => 'rk_'.(string)$brand->xpath('ns1:mid')[0],
									'search_keyword' => (string)$brand->xpath('ns1:name')[0],
									'domain' => '',
									'vendor' => 'RAKUTEN',
									'event_name' => $event_name,
									'event_type' => $event_type,
									'event_value' => $event_value,
									'ref_id' => (string)$brand->xpath('ns1:mid')[0],
									'raw_data' => $brand->asXML(),
									'modified_user_id' => $this->session->userdata('user_id'),
									'modified_date' => date('Y-m-d H:m:s')];
							$this->brand_model->update($db_id, $data);
						}
						echo $db_id;
						return;
					}else{
						return;
					}
				}
				$brands = $this->_rakuten->searchAdvertiser(is_null($merchant_name) ? '' : $merchant_name);
				break;
			default:
				# code...
				break;
		}
		
		$js_files = [
			'//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js'
		];

		$css_files = [
			'//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css'
		];

		$js_inline .= '
		$(document).ready(function(){
			function listener(){
			    $(".data-table a.ajax_get").on("click", function(e){
			    	e.preventDefault();
					var self = $(this);
					var text = self.text();
					self.attr("disabled", "disabled");
					self.text("Processing..");
					$.ajax({
					    type: "GET",
					    url: self.attr("href"),
					    success: function(data){
					    	if(data){
					    		self.removeAttr("disabled");
								self.text(text == "Add" ? "Update" : text);
					    	}
					    },
					    failure: function(errMsg) {
					    	self.removeAttr("disabled");
					        alert("Error.");
					    }
					});
			    });
		    }

		    $oTable = $(".data-table").DataTable({
		    	"fnDrawCallback": function( oSettings ) {
			      listener();
			    }
		    });
		});
		';

		//debug($brands, TRUE);

		$data_render = array(
			'vendor' => $vendor,
			'brands' => $brands,
			'js_files' => $js_files,
			'css_files' => $css_files,
			'js_inline' => $js_inline,
    		'page_title' => __('Brand import'),
    		'page_desc' => __('Brand importer'),
    		'sidebar_active' => 'import_brand',
    		'module' => 'import/brand',
    		'message' => $this->session->flashdata('message'),
    	);
    	$this->load->view('index', $data_render);
	}

	public function product($vendor = 'cj'){
		$action = $this->input->get('action');
		$this->load->model('product/product_model');
		$this->load->model('brand/brand_model');
		$products = NULL;
		
		$js_files = [];
		$css_files = [];
		$js_inline = '';

		switch ($vendor) {
			case 'cj':
				$this->load->library('CJConnector', NULL, '_cj');

				if($action == 'update'){
					$raw_data = $this->input->post('data');

					if($raw_data){
						$data = json_decode($raw_data);
						$product_sku = $this->input->post('product_sku');
						$product_id = $this->input->post('product_id');
						$category_id_list 	= $this->category_model->findCategoryByMapping($this->input->post('category_id'), 'CJ');
						$brand_id = $this->input->post('brand_id');

						if( $brand_id && is_numeric($brand_id) ){
							if( $product_sku && $product_id && is_numeric($product_id) 
								&& $category_id && is_numeric($category_id) ){
								$data_update = [
									'sku' => $product_sku,
									'name' => $data->{"name"},
									'long_description' => $data->{"description"},
									'raw_data' => $raw_data,
									'image_url' => $data->{"image-url"},
									'buy_url' => $data->{"buy-url"},
									'price' => $data->{"price"},
									'sale_price' => is_object($data->{"sale-price"}) ? 0 : $data->{"sale-price"},
									'retail_price' => is_object($data->{"retail-price"}) ? 0 : $data->{"retail-price"},
									'category_id' => $category_id,
									'currency' => trim($data->{"currency"}),
									'brand_id' => $brand_id,
									'click_count' => 0,
									'buy_count' => 0,
									'vendor' => 'CJ',
									'status' => Product_model::STATUS_ACTIVE,
									'created_user_id' => $this->session->userdata('user_id'),
								];

								$this->product_model->update($product_id, $data_update);
							}else{
								$data_insert = [
									'sku' => $product_sku,
									'name' => $data->{"name"},
									'short_description' => '',
									'long_description' => $data->{"description"},
									'raw_data' => $raw_data,
									'image_url' => $data->{"image-url"},
									'buy_url' => $data->{"buy-url"},
									'price' => $data->{"price"},
									'sale_price' => is_object($data->{"sale-price"}) ? 0 : $data->{"sale-price"},
									'retail_price' => is_object($data->{"retail-price"}) ? 0 : $data->{"retail-price"},
									'category_id' => $category_id,
									'currency' => trim($data->{"currency"}),
									'brand_id' => $brand_id,
									'vendor' => 'CJ',
									'modified_user_id' => $this->session->userdata('user_id'),
									'modified_date' => date('Y-m-d H:m:s')
								];
								$product_id = $this->product_model->insert($data_insert);
							}
						}

						// $params = $this->input->get();
						// unset($params['action']);
						// redirect(admin_url("import/product/$vendor?".http_build_query($params)));
						echo $product_id;
						return;
					}else{
						return;
					}
				}

				$advertiser_ids = $this->input->get('advertiser_ids');
				$keywords = $this->input->get('keywords');
				$isbn = $this->input->get('isbn');
				$upc = $this->input->get('upc');
				$advertiser_sku = $this->input->get('advertiser_sku');
				$low_price = $this->input->get('low_price');
				$high_price = $this->input->get('high_price');
				$low_sale_price = $this->input->get('low_sale_price');
				$high_sale_price = $this->input->get('high_sale_price');
				$currency = $this->input->get('currency');
				$sort_by = $this->input->get('sort_by');
				$sort_order = $this->input->get('sort_order');
				$page_number = $this->input->get('page_number');
				$records_per_page = $this->input->get('records_per_page');


				$products = $this->_cj->productSearch($advertiser_ids, $keywords, $isbn, $upc, $advertiser_sku, 
														$low_price, $high_price, $low_sale_price, $high_sale_price, 
														$currency, $sort_by, $sort_order, $page_number, $records_per_page);

				$total_matched 		= $products->{"@attributes"}->{"total-matched"};
				$records_returned 	= $products->{"@attributes"}->{"records-returned"};

				$js_inline .= '
				jQuery(document).ready(function($) {
					$("select[name=advertiser_ids]").change(function(){
						if( $(this).val() == "CIDs" ){
							$("input[name=advertiser_ids]").removeAttr("disabled").show();
						}else{
							$("input[name=advertiser_ids]").attr("disabled", "true").hide();
						}
					});
					$("select[name=advertiser_ids]").trigger("change");
				});
				';
				break;
			case 'rakuten':
				$this->load->library('RakutenConnector', NULL, '_rakuten');
				
				$keyword = $this->input->get('keyword');
				$mid = $this->input->get('mid');
				$category = $this->input->get('category');
				$max = $this->input->get('max');
				$pagenumber = $this->input->get('pagenumber');
				$sort = $this->input->get('sort');
				$sorttype = $this->input->get('sorttype');

				$keyword = is_null($keyword) ? '' : $keyword;
				$mid = is_null($mid) ? '' : $mid;
				$category = is_null($category) ? '' : $category;
				$max = is_null($max) ? '20' : $max;
				$pagenumber = is_null($pagenumber) ? '1' : $pagenumber;
				$sort = is_null($sort) ? 'productname' : $sort;
				$sorttype = is_null($sorttype) ? 'asc' : $sorttype;

				$data = $this->_rakuten->searchProduct($keyword, $mid, $category, $max, $pagenumber, $sort, $sorttype);
				$products = $data->item;
				$total_matched = $data->TotalMatches;
				$records_returned = count($products);
				break;
			default:
		}


		$js_files = [
			'//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js'
		];

		$css_files = [
			'//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css'
		];

		$js_inline .= '
		$.fn.serializeObject = function(){
		    var o = {};
		    var a = this.serializeArray();
		    $.each(a, function() {
		        if (o[this.name] !== undefined) {
		            if (!o[this.name].push) {
		                o[this.name] = [o[this.name]];
		            }
		            o[this.name].push(this.value || "");
		        } else {
		            o[this.name] = this.value || "";
		        }
		    });
		    return o;
		};
		$(document).ready(function(){
		    $oTable = $(".data-table").DataTable({
				"fnDrawCallback": function( oSettings ) {
					$(".data-table .ajax_post").on("click", function(e){
				    	e.preventDefault();
						var form = $(this).closest("form");
						var self = $(this);
						self.attr("disabled", "disabled");
						self.text("Processing..");
						$.ajax({
						    type: "POST",
						    url: form.attr("action"),
						    data: form.serializeObject(),
						    success: function(data){
						    	if(data){
									self.attr("disabled", "disabled");
									self.text("Success");
						    	}
						    },
						    failure: function(errMsg) {
						    	self.removeAttr("disabled");
						        alert("Error.");
						    }
						});
				    });
				}
		    });
		});
		';

		$data_render = array(
			'vendor' => $vendor,
			'products' => $products,
			'total_matched' => $total_matched,
			'records_returned' => $records_returned,
			'js_files' => $js_files,
			'css_files' => $css_files,
			'js_inline' => $js_inline,
    		'page_title' => __('Product import'),
    		'page_desc' => __('Product importer'),
    		'sidebar_active' => 'import_product',
    		'module' => 'import/product',
    		'message' => $this->session->flashdata('message'),
    	);
    	$this->load->view('index', $data_render);
	}

	public function batch_product_category($page_number = 1, $records_per_page = 1000){
			/*
		$this->load->library('CJConnector', NULL, '_cj');

		$page_number = $this->input->get('page_number');
		$records_per_page = $this->input->get('records_per_page');
		$advertiser_ids = $this->input->get('status');

		$page_number = $page_number ? $page_number : '1';
		$records_per_page = $records_per_page ? $records_per_page : '1000';
		$advertiser_ids = $advertiser_ids ? 'joined' : $advertiser_ids;

		$keywords = '';
		$isbn = '';
		$upc = '';
		$advertiser_sku = '';
		$low_price = '';
		$high_price = '';
		$low_sale_price = '';
		$high_sale_price = '';
		$currency = '';
		$sort_by = '';
		$sort_order = '';


		$products = $this->_cj->productSearch($advertiser_ids, $keywords, $isbn, $upc, $advertiser_sku, 
												$low_price, $high_price, $low_sale_price, $high_sale_price, 
												$currency, $sort_by, $sort_order, $page_number, $records_per_page);
		*/
	
		$data = $this->input->post('xml');
		if($data){
			
			$data = new SimpleXMLElement($data);
			$products = $data->products;

			$total = $products->{"@attributes"}["total-matched"];
			//debug($products);
			$total = 1002752;
			//$products->{"@attributes"}->{"records-returned"}
	        //$products->{"@attributes"}->{"page-number"}
	        $catalogs = array();
	        foreach ($products->product as $item) {
	        	$catalogs['cj:'.$item->{"catalog-id"}] = $item->{"catalog-id"}."\t".$item->{"advertiser-name"}."\t".$item->{"advertiser-category"};
	        }

			//debug($catalogs, TRUE);
		
			//Save string to log, use FILE_APPEND to append.
			file_put_contents(APPPATH.'/../logs/catalog_'.date("j.n.Y").'.txt', implode(PHP_EOL, $catalogs).PHP_EOL, FILE_APPEND);

			redirect(admin_url('import/batch_product_category'));
		}else{

			//$this->load->view('batch/batch_product_category', ['result' => count($catalogs), 'page_number' => $page_number, 'records_per_page' => $records_per_page, 'total' => $total]);
			$this->load->view('batch/batch_product_category');
		}
	}
}