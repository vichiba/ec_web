<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Permission_denied extends MX_Controller{
	public function index(){
		$this->output->set_status_header('403'); 
		$this->load->view('errors/permission_denied');
		exit;
	}
}