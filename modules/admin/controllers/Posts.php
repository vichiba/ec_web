<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Posts extends MY_Controller{

	public function __construct(){
		parent::__construct();
	}

	public function index(){
		$crud = new grocery_CRUD();
 		$crud->unset_jquery();

	    $crud->set_table('posts')
	        ->set_subject('Post');
	    $crud->where('post_id not in (1,2,3,4)');
	    $crud->order_by('post_modified_date', 'desc');

	    $crud->columns('post_photo', 'post_title', 'category', 'post_view', 'post_status', 'post_modified_date');
	    $crud->set_column_width('100px', '0', '150px', '80px', '80px', '120px', '80px');

	    $crud->set_relation_n_n('category', 'post_category', 'categories', 'post_id', 'cat_id', 'cat_title', 'priority');

	    $crud->fields('post_title', 'post_photo', 'post_content', 'category', 'post_status', 'post_seo_title', 'post_seo_description', 'post_seo_keyword', 'post_type', 'post_user_id', 'post_modified_date');

	    $crud->wrap_fields(
	    	array(
	    		array('label' => 'General', 'icon' => 'icon-info'),
	    		array('label' => 'SEO', 'icon' => 'icon-search3'),
	    	),
	    	array(
		    	array('post_title', 'post_url', 'post_photo', 'post_content', 'category', 'post_status'),
		    	array('post_seo_title', 'post_seo_description', 'post_seo_keyword'),
	    	)
	    );

		$crud->display_as('post_photo', __('Photo'))
			->display_as('post_title', __('Title'))
			->display_as('post_content', __('Content'))
			->display_as('post_type', __('Type'))
			->display_as('post_view', __('View count'))
			->display_as('post_status', __('Status'))
			->display_as('post_modified_date', __('Updated date'))
			->display_as('post_seo_title', __('Title'))
			->display_as('post_seo_description', __('Description'))
			->display_as('post_seo_keyword', __('Keyword'));

	 	$crud->field_type('post_status','dropdown', array(SELF::STATUS_ACTIVE => 'Active', SELF::STATUS_INACTIVE => 'InActive'));
	 	$crud->field_type('post_title', 'string');
	 	$crud->field_type('post_url', 'string');
	 	$crud->field_type('post_photo', 'image');
	 	$crud->field_type('post_type', 'hidden', '1');
    	$crud->field_type('post_user_id', 'hidden', $this->session->userdata('user_id'));

    	//$crud->callback_field('post_url',array($this,'callback_post_url_field'));

	 	$state = $crud->getState();
    	$state_info = $crud->getStateInfo();
    	$crud->field_type('post_modified_date', 'hidden', get_current_time());
	 	
	 	$crud->unset_fields('post_created_date');
	    $crud->unset_read();

	    //$crud->set_js_config('assets/js/admin.friendly_url.js');
	 
	    $output = $crud->render();
	    
		$data_render = array(
    		'page_title' => __('Content Management'),
    		'page_desc' => __('Content manage'),
    		'sidebar_active' => ($state == 'add' ? 'content_post_add': 'content_post'),
    		'module' => 'grocery',
    		'message' => $this->session->flashdata('message'),
    	);
    	$this->load->view('index', $this->getDataOuput($data_render, $output));
	}

	// function callback_post_url_field($value = '', $primary_key = null){
	// 	return '<input id="field-post_url" class="form-control friendly-url" ref="#field-post_title" name="post_url" type="text" value="'.$value.'" />';
	// }
	private function single_post($the_post_id, $sidebar_tag, $page_title, $page_desc){

		
	}
	public function privacy(){
		$the_post_id = 1;
		$sidebar_tag = 'content_post_privacy';
		$page_title = __('Privacy & Policy Page Content');
		$page_desc = '';

		$crud = new grocery_CRUD();
		$crud->set_table('posts');
		$crud->where('post_id', $the_post_id);

		$crud->unset_list(); 
        $crud->unset_back_to_list();
        
        $crud->unset_delete();
        $crud->unset_add();
        $crud->unset_read();
        $crud->unset_export();
        $crud->unset_print();

		$crud->display_as('post_photo', __('Hình ảnh'))
		 	 ->display_as('post_title', __('Tiêu đề'))
		 	 ->display_as('post_content', __('Nội dung'));

		$crud->fields('post_title', 'post_photo','post_content');
		$crud->field_type('post_title', 'string');
		$crud->field_type('post_photo', 'image');

		try {
			$output = $crud->render();
			$data_render = array(
				'module'=> 'm_crud',
				'page_title' => $page_title,
				'page_desc' => $page_desc,
				'data' => $output
			);
			$data_render = array(
	    		'page_title' => $page_title,
	    		'page_desc' => $page_desc,
	    		'sidebar_active' => $sidebar_tag,
	    		'module' => 'grocery',
	    		'message' => $this->session->flashdata('message'),
	    	);
	    	$this->load->view('index', $this->getDataOuput($data_render, $output));
		} catch (Exception $e) {

            if ($e->getCode() == 14) {  //The 14 is the code of the error on grocery CRUD (don't have permission).
                //redirect using your user id
                redirect(admin_url(strtolower(__CLASS__) . '/' . strtolower(__FUNCTION__) . '/edit/' . $the_post_id));
                
            } else {
                show_error($e->getMessage());
                return false;
            }
        }
	}

	public function terms(){
		$the_post_id = 2;
		$sidebar_tag = 'content_post_terms';
		$page_title = __('Term And Service Page Content');
		$page_desc = '';

		$crud = new grocery_CRUD();
		$crud->set_table('posts');
		$crud->where('post_id', $the_post_id);

		$crud->unset_list(); 
        $crud->unset_back_to_list();
        
        $crud->unset_delete();
        $crud->unset_add();
        $crud->unset_read();
        $crud->unset_export();
        $crud->unset_print();

		$crud->display_as('post_photo', __('Hình ảnh'))
		 	 ->display_as('post_title', __('Tiêu đề'))
		 	 ->display_as('post_content', __('Nội dung'));

		$crud->fields('post_title', 'post_photo','post_content');
		$crud->field_type('post_title', 'string');
		$crud->field_type('post_photo', 'image');

		try {
			$output = $crud->render();
			$data_render = array(
				'module'=> 'm_crud',
				'page_title' => $page_title,
				'page_desc' => $page_desc,
				'data' => $output
			);
			$data_render = array(
	    		'page_title' => $page_title,
	    		'page_desc' => $page_desc,
	    		'sidebar_active' => $sidebar_tag,
	    		'module' => 'grocery',
	    		'message' => $this->session->flashdata('message'),
	    	);
	    	$this->load->view('index', $this->getDataOuput($data_render, $output));
		} catch (Exception $e) {

            if ($e->getCode() == 14) {  //The 14 is the code of the error on grocery CRUD (don't have permission).
                //redirect using your user id
                redirect(admin_url(strtolower(__CLASS__) . '/' . strtolower(__FUNCTION__) . '/edit/' . $the_post_id));
                
            } else {
                show_error($e->getMessage());
                return false;
            }
        }
	}

	public function aboutus(){
		$the_post_id = 3;
		$sidebar_tag = 'content_post_aboutus';
		$page_title = __('About Us Page Content');
		$page_desc = '';

		$crud = new grocery_CRUD();
		$crud->set_table('posts');
		$crud->where('post_id', $the_post_id);

		$crud->unset_list(); 
        $crud->unset_back_to_list();
        
        $crud->unset_delete();
        $crud->unset_add();
        $crud->unset_read();
        $crud->unset_export();
        $crud->unset_print();

		$crud->display_as('post_photo', __('Hình ảnh'))
		 	 ->display_as('post_title', __('Tiêu đề'))
		 	 ->display_as('post_content', __('Nội dung'));

		$crud->fields('post_title', 'post_photo','post_content');
		$crud->field_type('post_title', 'string');
		$crud->field_type('post_photo', 'image');

		try {
			$output = $crud->render();
			$data_render = array(
				'module'=> 'm_crud',
				'page_title' => $page_title,
				'page_desc' => $page_desc,
				'data' => $output
			);
			$data_render = array(
	    		'page_title' => $page_title,
	    		'page_desc' => $page_desc,
	    		'sidebar_active' => $sidebar_tag,
	    		'module' => 'grocery',
	    		'message' => $this->session->flashdata('message'),
	    	);
	    	$this->load->view('index', $this->getDataOuput($data_render, $output));
		} catch (Exception $e) {

            if ($e->getCode() == 14) {  //The 14 is the code of the error on grocery CRUD (don't have permission).
                //redirect using your user id
                redirect(admin_url(strtolower(__CLASS__) . '/' . strtolower(__FUNCTION__) . '/edit/' . $the_post_id));
                
            } else {
                show_error($e->getMessage());
                return false;
            }
        }
	}

	public function faq(){
		$the_post_id = 4;
		$sidebar_tag = 'content_post_faq';
		$page_title = __('FAQ Page Content');
		$page_desc = '';

		$crud = new grocery_CRUD();
		$crud->set_table('posts');
		$crud->where('post_id', $the_post_id);

		$crud->unset_list(); 
        $crud->unset_back_to_list();
        
        $crud->unset_delete();
        $crud->unset_add();
        $crud->unset_read();
        $crud->unset_export();
        $crud->unset_print();

		$crud->display_as('post_photo', __('Hình ảnh'))
		 	 ->display_as('post_title', __('Tiêu đề'))
		 	 ->display_as('post_content', __('Nội dung'));

		$crud->fields('post_title', 'post_photo','post_content');
		$crud->field_type('post_title', 'string');
		$crud->field_type('post_photo', 'image');

		try {
			$output = $crud->render();
			$data_render = array(
				'module'=> 'm_crud',
				'page_title' => $page_title,
				'page_desc' => $page_desc,
				'data' => $output
			);
			$data_render = array(
	    		'page_title' => $page_title,
	    		'page_desc' => $page_desc,
	    		'sidebar_active' => $sidebar_tag,
	    		'module' => 'grocery',
	    		'message' => $this->session->flashdata('message'),
	    	);
	    	$this->load->view('index', $this->getDataOuput($data_render, $output));
		} catch (Exception $e) {

            if ($e->getCode() == 14) {  //The 14 is the code of the error on grocery CRUD (don't have permission).
                //redirect using your user id
                redirect(admin_url(strtolower(__CLASS__) . '/' . strtolower(__FUNCTION__) . '/edit/' . $the_post_id));
                
            } else {
                show_error($e->getMessage());
                return false;
            }
        }
	}
}