<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Brands extends MY_Controller{
	public function __construct(){
		parent::__construct();
		$this->lang->load('brand', 'ja');
	}

	public function index(){
		$crud = new grocery_CRUD();
 		$crud->unset_jquery();

	    $crud->set_table('brands')
	        ->set_subject(__('Brand'));
	    $crud->order_by('modified_date', 'desc');

	    $crud->columns('image_url', 'name', 'vendor', 'search_keyword', 'domain', 'status', 'modified_date');
	    $crud->set_column_width('80px', '60px', '0', '60px', '120px', '80px');

	    $crud->fields('name', 'uri_path', 'vendor', 'ref_id',
	    				'image_url', 'domain', 'search_keyword', 'long_description', 'short_description'
	    				, 'status', 'created_user_id', 'modified_user_id', 'modified_date');

		$crud
			->display_as('name', __('Title'))
			->display_as('vendor', __('Vendor'))
			->display_as('ref_id', __('Brand ID'))
			->display_as('image_url', __('Image'))
			->display_as('uri_path', __('Friendly URL'))
			->display_as('parent_id', __('Parent'))
			->display_as('search_keyword', __('Search Keyword'))
			->display_as('short_description', __('Return Policy'))
			->display_as('long_description', __('Description'))
			->display_as('position', __('Position'))
			->display_as('status', __('Status'));

		$crud->field_type('vendor','dropdown', array('CJ' => 'CJ', 'Rakuten' => 'RAKUTEN', 'Skimlinks' => 'SKIMLINKS'));	
	 	$crud->field_type('status','dropdown', array(SELF::STATUS_ACTIVE => 'Active', SELF::STATUS_INACTIVE => 'Inactive'));	
    	$crud->field_type('image_url', 'image');

    	$crud->callback_field('uri_path',array($this,'callback_uri_path_field'));
    	$crud->callback_before_insert(array($this,'change_uri_path'));
    	$crud->callback_before_update(array($this,'change_uri_path'));

	 	$state = $crud->getState();
    	$state_info = $crud->getStateInfo();

    	if($state !== 'add'){
    		$crud->field_type('modified_date', 'hidden', get_current_time());
    		$crud->field_type('created_user_id', 'hidden');
    		$crud->field_type('modified_user_id', 'hidden', $this->session->userdata('user_id'));
    	}else{
    		$crud->field_type('modified_user_id', 'hidden');
    		$crud->field_type('modified_date', 'hidden');
    		$crud->field_type('created_user_id', 'hidden', $this->session->userdata('user_id'));
    	}


	    $crud->unset_read();
	    $crud->set_js_config('assets/js/admin.friendly_url.js');
	 
	    $output = $crud->render();
	    
		$data_render = array(
    		'page_title' => __('Product\'s Catalog Management'),
    		'page_desc' => __('Manage catalog of product'),
    		'sidebar_active' => ($state == 'add' ? 'content_brand_add': 'content_brand'),
    		'module' => 'grocery',
    		'message' => $this->session->flashdata('message'),
    	);
    	$this->load->view('index', $this->getDataOuput($data_render, $output));
	}

	function callback_uri_path_field($value = '', $primary_key = null){
		return '<input id="field-uri_path" class="form-control friendly-url" ref="#field-name" name="uri_path" type="text" value="'.$value.'" />';
	}

	function change_uri_path($post_array, $primary_key = null) {
		$post_array['uri_path'] = strtolower($post_array['uri_path']);
		return $post_array;
	}  
}