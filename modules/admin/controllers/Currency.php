<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once dirname(__FILE__) . '/../libraries/php-crontab-manager/src/CronEntry.php';
require_once dirname(__FILE__) . '/../libraries/php-crontab-manager/src/CliTool.php';
require_once dirname(__FILE__) . '/../libraries/php-crontab-manager/src/CrontabManager.php';

use php\manager\crontab\CrontabManager;

class Currency extends MY_Controller{
	
	const CMD = 'php '.FCPATH.'index.php cronjobs currency run 2>&1';

	public function __construct(){
		parent::__construct();

	}

	public function index(){
		$tab = $this->input->get("tab");
		if($tab == "auto"){
			$this->auto();
		}else{
			$this->management();
		}
	}

	private function auto(){
		$cronjob = $this->input->post('cronjob');

		if( $cronjob ){
			$this->load->model('config_model');
			$old_cronjob = get_config_value('CRONJOB_CURRENCY');
			
			if( $this->config_model->updateByKey('CRONJOB_CURRENCY', $cronjob) !== FALSE ){

				//Save CronJob to file
				$this->saveCronJob($old_cronjob, $cronjob);

				$this->session->set_flashdata('message', __('Crontab configurations has been save.'));
			}else{
				$this->session->set_flashdata('message', __('Error while saving Crontab configurations. Please try again latter.'));
			}
			redirect(admin_url('currency?tab=auto'));
			return;
		}

		//get new job config
		$cronjob = get_config_value('CRONJOB_CURRENCY');

		$js_files = [
			'/assets/cron/jquery-cron-min.js'
		];
		$css_files = [
			'/assets/cron/jquery-cron.css'
		];

		$js_inline = '
		$(document).ready(function(){
			$("#cronjob").cron({
			    initial: "'.$cronjob.'",
			    customValues: {
			        "'.__('15 Minutes').'" : "*/15 * * * *",
			        "'.__('30 Minutes').'" : "*/30 * * * *",
			        "'.__('2 Hours').'" : "* */2 * * *",
			        "'.__('4 Hours').'" : "* */2 * * *",
			        "'.__('6 Hours').'" : "* */6 * * *",
			        "'.__('10 Hours').'" : "* */10 * * *",
			    },
			    onChange: function() {
			    	var $val = $(this).cron("value");
			    	console.log($val);
			        $("input[name=cronjob]").val($val);
			    }
			});
		});
		';

		$crontab = $this->getCrontabManager();

		$data_render = array(
    		'page_title' => __('Currency converter Management'),
    		'page_desc' => __('Currency converter Management'),
    		'sidebar_active' => 'content_currency',
    		'module' => 'currency_crontab_config',
    		'tab' => "auto",
    		'js_files' => $js_files,
    		'css_files' => $css_files,
    		'js_inline' => $js_inline,
			'cronjob' => $cronjob,
			'joblists' => $crontab->listJobs(),
    		'message' => $this->session->flashdata('message'),
    	);
    	
    	$this->load->view('index', $data_render);
	}

	private function getCrontabManager(){
		$crontab = new CrontabManager();
		return $crontab;
	}

	private function saveCronJob($old_data, $data){
		$crontab = $this->getCrontabManager();
		
		//Delete old job
		$old_job = $crontab->newJob();
		$old_job->on($old_data)->doJob(self::CMD);
		$crontab->deleteJob($old_job);

		//add new job
		$job = $crontab->newJob();
		$job->on($data)->doJob(self::CMD);
		$crontab->add($job);
		
		//save
		$crontab->save(false);

	}

	private function management(){
		$crud = new grocery_CRUD();
 		$crud->unset_jquery();

	    $crud->set_table('currency')
	        ->set_subject('Currency Convert');
	    $crud->order_by('created_date', 'desc');

	    $crud->columns('name', 'currency', 'value', 'created_date');

	    $crud->fields('name', 'currency', 'value', 'created_date');

	    $crud->field_type('created_date', 'hidden', date('Y-m-d'));

		$crud
			->display_as('name', __('Currency Name'))
			->display_as('currency', __('Code'))
			->display_as('value', __('Convert to JP'))
			->display_as('created_date', __('Date'));
	    $crud->unset_read();

	    $crud->field_type('value', 'integer');
	 
	    $output = $crud->render();
	    
		$data_render = array(
    		'page_title' => __('Currency converter Management'),
    		'page_desc' => __('Currency converter Management'),
    		'sidebar_active' => 'content_currency',
    		'module' => 'currency',
    		'tab' => "management",
    		'message' => $this->session->flashdata('message'),
    	);
    	$this->load->view('index', $this->getDataOuput($data_render, $output));
	}
}