<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Messages extends MY_Controller{
	public function __construct(){
		parent::__construct();
		$this->load->model('message/message_model');
		$this->load->model('message/message_user_model');
	}

	public function index(){
		$date = $this->input->get('date');
		if( empty($date) ) $date = date('Y-m-d');

		$users = $this->message_user_model->find_all_user($date);
		$date_data_arr = $this->message_model->get_all_date();
		$date_data = array();
		foreach ($date_data_arr as $date_data_item) {
			$date_data[] = array('href' => admin_url('messages?date='.$date_data_item->date), 'label' => $date_data_item->date);
		}

		$js = array(site_url('assets/js/admin.messages.js'), site_url('/assets/js/js.cookie.js'), site_url('/assets/js/client.messages.js'));
		$css = array(site_url('/assets/css/client.messages.css'));
		$data_render = array(
			'js_files' => $js,
			'css_files' => $css,
    		'page_title' => 'Messages',
    		'page_desc' => 'Messages with customers',
    		'sidebar_active' => 'messages',
    		'module' => 'messages/list',
    		'users' => $users,
    		'breadcrumb_buttons_selected_label' => $date,
    		'breadcrumb_buttons' => $date_data,
    	);
    	$this->load->view('index', $data_render);
	}

	public function get_message($user_id){
		$this->load->module('message/rest');
		$from_user_id = 0;

		$this->rest->init(0, $user_id);
	}

	public function get_message_part(){
		$user_id = $this->input->get('user_id');

		$this->load->module('message/rest');
		$from_user_id = 0;

		$this->rest->get_message_multiple(explode(',', $user_id), 0);
	}

	public function push(){
		$from = $this->input->post('f');
		$to = $this->input->post('t');
		$message = $this->input->post('m');

		$this->load->module('message/rest');
		echo $this->rest->push_message($from, $to, $message);
	}
}