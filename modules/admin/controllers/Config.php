<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Config extends MY_Controller{
	
	const TYPE_SYSTEM 	= 1;
	const TYPE_WEBSITE 	= 2;
	const TYPE_3PARTY 	= 3;

	private $js_inline = '';

	public function __construct(){
		parent::__construct();

	}

	public function index(){
		
		$website_config = $this->getConfigUI(SELF::TYPE_WEBSITE);
	    
		$data_render = array(
    		'page_title' => __('Website Config'),
    		'page_desc' => __('Website config'),
    		'sidebar_active' => 'manage_website_config',
    		'module' => 'grocery',
    		'js_inline' => $this->js_inline,
    		'message' => $this->session->flashdata('message'),
    	);
    	$this->load->view('index', $this->getDataOuput($data_render, $website_config));
	}

	public function system(){
		
		$website_config = $this->getConfigUI(SELF::TYPE_SYSTEM);
	    
		$data_render = array(
    		'page_title' => __('System Config'),
    		'page_desc' => __('System config'),
    		'sidebar_active' => 'manage_system_config',
    		'module' => 'grocery',
    		'js_inline' => $this->js_inline,
    		'message' => $this->session->flashdata('message'),
    	);
    	$this->load->view('index', $this->getDataOuput($data_render, $website_config));
	}

	/**
	* Get config UI by type
	* @param $config_type:
	* 		1: System Config
	* 		2: Website Config
	* 		3: Thirtparty Config
	*/
	public function getConfigUI($config_type = SELF::TYPE_WEBSITE){
		$crud = new grocery_CRUD();
 		$crud->unset_jquery();

	    $crud->set_table('config')
	        ->set_subject('Config');
	    $crud->where('conf_type', $config_type);
	    $crud->order_by('conf_key', 'ASC');

	    $crud->columns('conf_key', 'conf_value', 'conf_desc', 'conf_status');
	    $crud->fields('conf_desc', 'conf_value', 'conf_status');

	    $crud->display_as('conf_key', __('Key'))
	    	->display_as('conf_value', __('Value'))
	    	->display_as('conf_status', __('Status'))
	    	->display_as('conf_desc', __('Description'));

	   	$crud->field_type('conf_key', 'readonly');
	   	$crud->field_type('conf_desc', 'readonly');
	   	$crud->field_type('conf_status','dropdown',
            array('2' => 'Active', '1' => 'Inactive'));

	   	$state = $crud->getState();
    	$state_info = $crud->getStateInfo();
    	if($state == 'edit' || $state == 'update'){
    		$this->load->model('config_model');
	        $primary_key = $state_info->primary_key;
    		$conf_obj = $this->config_model->find($primary_key);
	        if($conf_obj->conf_richeditor !== '1'){
	        	$crud->unset_texteditor('conf_value');
	        }
	        if( (int)$primary_key === 24 ){
	        	$crud->callback_field('conf_value', array($this, '_callback_address_field'));
	        	$crud->callback_before_update(array($this, '_callback_before_save_address'));

	        	$this->js_inline = '
	        	$(document).ready(function(){
	        		var save = $("#form-button-save");
	        		var save_goback = $("#save-and-go-back-button");
	        		var lat_input = $(".lat_input");
	        		var lng_input = $(".lng_input");
					$("#field-conf_value").blur(function(){
						var val = $(this).val();

						disable_save();
						$.getJSON("https://maps.googleapis.com/maps/api/geocode/json", {address: val}, function(response){
							try{
								var location = response.results[0].geometry.location;
								console.log(location);
								
								//save latitude
								lat_input.find("span").text(location.lat);
								lat_input.find("input").val(location.lat);
								
								//save lontitude
								lng_input.find("span").text(location.lng);
								lng_input.find("input").val(location.lng);
							}catch(e){
								//save latitude
								lat_input.find("span").text("'.__('Failed to get latitude').'");
								lat_input.find("input").val("0");
								
								//save lontitude
								lng_input.find("span").text("'.__('Failed to get lontitude').'");
								lng_input.find("input").val("0");
							}
						}).always(function(){
							enable_save();
						});
					});

					function enable_save(){
						save.removeAttr("disabled");
						save_goback.removeAttr("disabled");
					}

					function disable_save(){
						save.attr("disabled", "disabled");
						save_goback.attr("disabled", "disabled");
					}
	        	});
	        	';
	        }
	    }


	    $crud->unset_fields('updated_date');

	    $crud->unset_read();
	    $crud->unset_add();
	    $crud->unset_delete();
	 
	    return $crud->render();
	}

	function _callback_address_field($value = '', $primary_key = NULL){
		$lat = get_config_value('COMPANY_ADDRESS_LAT');
		$lng = get_config_value('COMPANY_ADDRESS_LNG');

		$html = '<textarea id="field-conf_value" name="conf_value">'.$value.'</textarea>';
		$html.= '<p class="lat_input row"><label class="col-xs-2">Latitude</label>: <span class="label label-warning">'.$lat.'</span><input type="hidden" name="latitude" value="'.$lat.'"/></p>';
		$html.= '<p class="lng_input row"><label class="col-xs-2">Longitude</label>: <span class="label label-warning">'.$lng.'</span><input type="hidden" name="longitude" value="'.$lng.'"/></p>';
		return $html;
	}

	function _callback_before_save_address($post_array = array(), $primary_key = NULL){
		$lat = $post_array['latitude'];
		$lng = $post_array['longitude'];
		$this->config_model->update(29, ['conf_value' => $lat]);
		$this->config_model->update(30, ['conf_value' => $lng]);

		unset($post_array['latitude']);
		unset($post_array['longitude']);

		return $post_array;
	}

	public function action(){
		$this->load->library('elfinder/connector');
	}

	public function logo(){
		$sidebar_tag = 'manage_logo';
		$page_title = __('Logo Configuration');
		$page_desc = '';
		$the_id = 47;

		$crud = new grocery_CRUD();
		$crud->set_table('config');
		$crud->set_subject('Logo');
		$crud->where('conf_id', $the_id);

		$crud->unset_list(); 
        $crud->unset_back_to_list();
        
        $crud->unset_delete();
        $crud->unset_add();
        $crud->unset_read();
        $crud->unset_export();
        $crud->unset_print();

		$crud->display_as('conf_value', 'Logo')->display_as('conf_desc', 'Type');

		$crud->fields('conf_desc', 'conf_value');
		$crud->field_type('conf_desc', 'readonly');
		$crud->field_type('conf_value', 'image');

		try {
			$output = $crud->render();
			$data_render = array(
	    		'page_title' => $page_title,
	    		'page_desc' => $page_desc,
	    		'sidebar_active' => $sidebar_tag,
	    		'module' => 'grocery',
	    		'message' => $this->session->flashdata('message'),
	    	);
	    	$this->load->view('index', $this->getDataOuput($data_render, $output));
		} catch (Exception $e) {

            if ($e->getCode() == 14) {  //The 14 is the code of the error on grocery CRUD (don't have permission).
                //redirect using your user id
                redirect(admin_url(strtolower(__CLASS__) . '/' . strtolower(__FUNCTION__) . '/edit/' . $the_id));
                
            } else {
                show_error($e->getMessage());
                return false;
            }
        }
	}
}