<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Email extends MY_Controller{

	public function __construct(){
		parent::__construct();

	}

	public function index(){

		$smtp_host = $this->input->post('smtp_host');
		$smtp_port = $this->input->post('smtp_port');
		$smtp_user = $this->input->post('smtp_user');
		$smtp_pass = $this->input->post('smtp_pass');

		if($smtp_host && $smtp_port && $smtp_user && $smtp_pass){
			$this->load->model('config_model');
			
			if( $this->config_model->updateByKey('SMTP_HOST', $smtp_host) !== FALSE &&
			$this->config_model->updateByKey('SMTP_PORT', $smtp_port) !== FALSE &&
			$this->config_model->updateByKey('SMTP_USER', $smtp_user) !== FALSE &&
			$this->config_model->updateByKey('SMTP_PASS', $smtp_pass) !== FALSE ){
				$this->session->set_flashdata('message',  __('Email configurations has been save.'));
			}else{
				$this->session->set_flashdata('message',  __('Error while saving Email configurations. Please try again latter.'));
			}
			redirect(admin_url('email'));
			return;
		}

		$data_render = array(
    		'page_title' =>  __('Email Configuration'),
    		'page_desc' =>  __('Email configuration'),
    		'sidebar_active' => 'manage_email',
    		'module' => 'email_config',
    		'smtp_host' => get_config_value('SMTP_HOST'),
		    'smtp_port' => get_config_value('SMTP_PORT'),
			'smtp_user' => get_config_value('SMTP_USER'),
			'smtp_pass' => get_config_value('SMTP_PASS'),
    		'message' => $this->session->flashdata('message'),
    	);
    	
    	$this->load->view('index', $data_render);
	}
}