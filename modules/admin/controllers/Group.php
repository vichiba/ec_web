<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Group extends MY_Controller 
{
	public function __construct(){
		parent::__construct();

	}
	function index(){
		$crud = new grocery_CRUD();
 		$crud->unset_jquery();

	    $crud->set_table('groups')
	        ->set_subject('Group');
	    $crud->order_by('created_date', 'desc');
	    //set_relation_n_n($field_name, $relation_table, $selection_table, $primary_key_alias_to_this_table, 
	    //$primary_key_alias_to_selection_table , $title_field_selection_table , $priority_field_relation_table = null, $where_clause = null)

	    $crud->set_relation_n_n('role_label', 'group_role', 'roles', 'group_id', 'role_id', 'role_label');

	    $crud->columns('group_label', 'role_label', 'group_status');
	    $crud->set_column_width('150px', '0', '80px');

	    $crud->display_as('group_label',  __('Group'))
	    	->display_as('role_label',  __('Privileges'))
	    	->display_as('group_status',  __('Status'));

	 	$crud->field_type('group_status','dropdown',
            array(SELF::STATUS_ACTIVE => 'Active', SELF::STATUS_INACTIVE => 'Inactive'));
	 	
	 	$crud->unset_fields('created_date');
	 	
	    //$crud->unset_read();
	 
	    $output = $crud->render();
	    
		$data_render = array(
    		'page_title' =>  __('Group Management'),
    		'page_desc' =>  __('Group manage'),
    		'sidebar_active' => 'manage_groups',
    		'module' => 'grocery',
    		'message' => $this->session->flashdata('message'),
    	);
    	$this->load->view('index', $this->getDataOuput($data_render, $output));
	}
}