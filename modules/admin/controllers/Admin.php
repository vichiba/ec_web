<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MY_Controller 
{
    public function index(){
        $this->auth();
    }

    public function auth(){
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $remember_me = $this->input->post('remember_me');
        $data_render = array();

        if( !empty($username) && !empty($password) ){
            $this->load->library('form_validation');

            $this->form_validation->set_rules('username', 'Username', 'trim|required');
            $this->form_validation->set_rules('password', 'Password', 'trim|required');
    
            if ($this->form_validation->run() === FALSE){
                    
                $message = get_error_block(__('Tên đăng nhập hoặc mật khẩu không hợp lệ.'));
                $data_render['message'] = $message;
            }else{
                $this->load->model('users_model');
                if( ($user_id = $this->users_model->login($username, $password)) ){
                    if($remember_me){
                        $remember_token = generate_token(40);
                        $this->remember_user($user_id, $remember_token);
                    }else{
                        $this->clean_remember_user();
                    }
                    $user = $this->users_model->find($user_id);

                    $this->session->set_userdata('loggedin', TRUE);
                    $this->session->set_userdata('user_id', $user->user_id);
                    $this->session->set_userdata('user_roles', $user->role_filter);
                    $this->session->set_userdata('user_fullname', $user->user_fullname);
                    $this->session->set_userdata('user_photo', $user->user_photo);
                    
                    $this->input->set_cookie('username',  $username);
                    redirect(admin_url('dashboard'));
                }else{
                    $message = get_error_block(__('Tên đăng nhập hoặc mật khẩu không đúng.'));
                    $data_render['message'] = $message;
                }
            }
        }
        $view_path = 'login';
        $remember_user_id = get_cookie($this->remember_id);
        if( !empty($remember_user_id) && is_numeric($remember_user_id) ){
            $remember_user_object = $this->users_model->find($remember_user_id);
            $view_path = 'login_advance';
            $data_render['user'] = $remember_user_object;
        }
        $this->load->view($view_path, $data_render);
    }

    private function remember_user($user_id, $remember_token){

        set_cookie(array(
            'name' => $this->remember_id,
            'value' => $user_id,
            'expire' => $this->remember_expired_time
        ));

        set_cookie(array(
            'name' => $this->remember_token,
            'value' => $remember_token,
            'expire' => $this->remember_expired_time
        ));

        $expired_time = date(get_time_format(), time() + (int)$this->remember_expired_time);
        $result = $this->users_model->set_remember_token($user_id, $remember_token, $expired_time);
    }

    private function clean_remember_user(){
        $user_id = get_cookie($this->remember_id);
        if( !empty($user_id) && is_numeric($user_id) ){
            set_cookie(array(
                'name' => $this->remember_id,
                'value' => NULL,
                'expire' => '0',
            ));

            set_cookie(array(
                'name' => $this->remember_token,
                'value' => NULL,
                'expire' => '0',
            ));
            $result = $this->users_model->set_remember_token($user_id, NULL, 0);
        }
    }

    public function signin_with_different_account(){
        $remember_user_id = get_cookie($this->remember_id);
        if( !empty($remember_user_id) && is_numeric($remember_user_id) ){
            $this->clean_remember_user($remember_user_id);
        }
        redirect(admin_url());
    }

    public function logout(){
        $this->session->unset_userdata('loggedin');
        $this->session->unset_userdata('user_id');
        $this->session->unset_userdata('user_roles');
        $this->session->unset_userdata('user_fullname');
        $this->session->unset_userdata('user_photo');
        $this->session->unset_userdata('remember_user_object');

        set_cookie(array(
            'name' => $this->remember_token,
            'value' => NULL,
            'expire' => '0',
        ));

        redirect(admin_url());
    }

    public function permission_denied(){
        echo 'permision denied';
    }

    public function dashboard(){
    	$data_render = array(
    		'page_title' => __('Dashboard'),
    		'page_desc' => __('Dashboard'),
    		'sidebar_active' => 'dashboard',
    		'module' => 'dashboard',
    	);
    	$this->load->view('index', $data_render);
    }

    public function admin_panel(){
    	$data_render = array(
    		'page_title' => __('Admin Panel'),
    		'page_desc' => __('Admin panel'),
    		'sidebar_active' => 'admin_panel',
    		'module' => 'admin_panel/general_panel',
    	);
    	$this->load->view('index', $data_render);
    }

    public function profile(){
        $user_id = $this->session->userdata('user_id');

        $crud = new grocery_CRUD();
        $crud->set_table('users');
        $crud->where('user_id', $user_id);

        $crud->unset_list();
        $crud->unset_back_to_list();
        
        $crud->unset_delete();
        $crud->unset_add();
        $crud->unset_read();
        $crud->unset_export();
        $crud->unset_print();

        $crud->columns('user_login', 'user_fullname', 'user_photo', 'user_status', 'user_timestamp');
        $crud->set_column_width('80px', '0', '100px', '80px', '80px', '80px');

        $crud->unset_columns('remember_token', 'remember_token_expired');
        $crud->display_as('user_login', __('Username'))
            ->display_as('user_password', __('Password'))
            ->display_as('user_fullname', __('Fullname'))
            ->display_as('user_photo', __('Avatar'))
            ->display_as('user_status', __('Status'))
            ->display_as('user_timestamp', __('Created date'));
        $crud->field_type('user_status','dropdown',
            array(SELFT::STATUS_ACTIVE => 'Active', SELFT::STATUS_INACTIVE => 'Inactive'));
        
        $crud->field_type('user_login', 'readonly');
        $crud->field_type('user_photo', 'image');
        $crud->field_type('user_password', 'password');
        $crud->callback_before_insert(array($this,'encrypt_password_callback'));
        $crud->callback_before_update(array($this,'encrypt_password_callback'));
        
        $crud->unset_fields('user_timestamp', 'remember_token', 'remember_token_expired');

        $state = $crud->getState();
        $state_info = $crud->getStateInfo();
        if($state == 'edit'){
            $primary_key = $state_info->primary_key;
            if($primary_key !== $user_id){
                show_403();
            }
        }

        try {
            $output = $crud->render();
            $data_render = array(
                'page_title' => __('Profile'),
                'page_desc' => __('Profile'),
                'sidebar_active' => 'dashboard',
                'module' => 'grocery'
            );
            $this->load->view('index', $this->getDataOuput($data_render, $output) );
        } catch (Exception $e) {

            if ($e->getCode() == 14) {  //The 14 is the code of the error on grocery CRUD (don't have permission).
                //redirect using your user id
                redirect(admin_url(strtolower(__CLASS__) . '/' . strtolower(__FUNCTION__) . '/edit/' . $user_id));
            } else {
                show_error($e->getMessage());
                return false;
            }
        }
    }

    function encrypt_password_callback($post_array, $primary_key = null){
        $post_array['user_password'] = md5($post_array['user_password']);
        return $post_array;
    }
}