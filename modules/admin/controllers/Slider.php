<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Slider extends MY_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->model('image/image_model');
		$this->load->model('category/category_model');
		$this->load->model('product/product_model');
		$this->load->model('brand/brand_model');

		$this->catalogs = $this->category_model->listActiveWithURI(0, '', 1, 1000, TRUE);
		$this->clear_catalog_list = $this->getClearCategoryWithLevel($this->catalogs);

		usort($this->clear_catalog_list, function ($a, $b) {
		    return strcmp($a->name_with_level, $b->name_with_level);
		});

		$this->brand_list = $this->brand_model->listActive(1, 100000);
	}

	/**
	 * Home page slider
	 * @return [type] [description]
	 */
	public function index(){
		
		$crud = new grocery_CRUD();
 		$crud->unset_jquery();

	    $crud->set_table('images')
	        ->set_subject('Config');
	    $crud->where('ref_type', Image_model::TYPE_HOMEPAGE);

	    $crud->columns('image_url', 'label', 'description', 'position', 'status');
	    $crud->fields('ref_type', 'label', 'description', 'image_url', 'click_type', 'click_value', 'position', 'status');

	    $crud->display_as('conf_key', __('Key'))
	    	->display_as('conf_value', __('Value'))
	    	->display_as('conf_status', __('Status'))
	    	->display_as('conf_desc', __('Description'));

	    $crud->field_type('ref_type', 'hidden', Image_model::TYPE_HOMEPAGE);
	    $crud->field_type('ref_id', 'hidden');

	   	$crud->field_type('image_url', 'image');
	   	$crud->field_type('click_type','dropdown',
            array(
            	Image_model::CLICK_TYPE_LINK => __('Link'),
            	Image_model::CLICK_TYPE_CATEGORY => __('Category'),
            	Image_model::CLICK_TYPE_BRAND => __('Brand'),
            	Image_model::CLICK_TYPE_PRODUCT => __('Product'), 
            )
        );
	   	$crud->field_type('status','dropdown',
            array(SELF::STATUS_ACTIVE => 'Active', SELF::STATUS_INACTIVE => 'Inactive'));

	    $crud->unset_read();

		$data_render = array(
    		'page_title' => __('Home page slider Config'),
    		'page_desc' => __('Home page slider Config'),
    		'sidebar_active' => 'slider_homepage',
    		'module' => 'grocery',
    		'message' => $this->session->flashdata('message'),
    	);
    	$this->load->view('index', $this->getDataOuput($data_render, $crud->render()));
	}

	/**
	 * Mobile slider for categories
	 * @return [type] [description]
	 */
	public function mobile(){

		$crud = new grocery_CRUD();
 		$crud->unset_jquery();

	    $crud->set_table('images')
	        ->set_subject('Config');
	    $crud->where('ref_type', Image_model::TYPE_CATEGORY);

	    $crud->columns('image_url', 'ref_id', 'click_type', 'click_value' , 'position', 'status');
	    $crud->fields('ref_type', 'ref_id', 'click_type', 'click_value', 'image_url', 'position', 'status');

	    $crud->display_as('ref_id', __('Diplay Page'));
	    $crud->display_as('click_type', __('Action'));
	    $crud->display_as('click_value', __('Value'));
	    $crud->display_as('image_url', __('Image'));

	    $crud->field_type('ref_type', 'hidden', Image_model::TYPE_CATEGORY);

	    $crud->callback_column('click_value',array($this,'_callback_click_value_col'));

	    $crud->callback_field('click_value',array($this,'_callback_click_value_field'));

	   	$crud->field_type('image_url', 'image');

	   	$root_catalog_list = [];
	   	foreach ($this->catalogs as $item) {
	   		$root_catalog_list[$item->category_id] = $item->name;
	   	}
	   	$crud->field_type('ref_id','dropdown', $root_catalog_list);
	   	$crud->field_type('click_type','dropdown',
            array(
            	Image_model::CLICK_TYPE_LINK => __('Jump to Link'),
            	Image_model::CLICK_TYPE_CATEGORY => __('Jump to Category'),
            	Image_model::CLICK_TYPE_BRAND => __('Jump to Brand'),
            	Image_model::CLICK_TYPE_PRODUCT => __('Jump to Product')
            )
        );
       /* $crud->field_type('ref_type','dropdown',
            array(
        		//Image_model::TYPE_PRODUCT => 'Product Page',
				Image_model::TYPE_CATEGORY => 'Category Page',
				//Image_model::TYPE_BRAND => 'Brand Page',
				//Image_model::TYPE_HOMEPAGE => 'Home Page'
	 		)
        );*/
	   	$crud->field_type('status','dropdown',
            array(SELF::STATUS_ACTIVE => __('Active'), SELF::STATUS_INACTIVE => __('Inactive')));

	    $crud->unset_read();

	    $js_inline = '
		$(document).ready(function(){
			disable_all();
			$("#field-click_type").change(function(){
				switch($(this).val()){
					case "'.Image_model::CLICK_TYPE_LINK.'":
						enable("link");
					break;
					case "'.Image_model::CLICK_TYPE_CATEGORY.'":
						enable("cat");
					break;
					case "'.Image_model::CLICK_TYPE_BRAND.'":
						enable("brand");
					break;
					case "'.Image_model::CLICK_TYPE_PRODUCT.'":
						enable("prod");
					break;
				}
			});
			$("#field-click_type").trigger("change");

			function disable_all(){
				$(".click_value").hide();
				$(".click_value").attr("disabled", "disabled");
			}

			function enable(field){
				disable_all();

				$("#field-click_value_"+field).show();
				$("#field-click_value_"+field).removeAttr("disabled");
			}
		});
	    ';

		$data_render = array(
    		'page_title' => __('Mobile slider Config'),
    		'page_desc' => __('Display slider for each category on Mobile APP'),
    		'sidebar_active' => 'slider_mobile',
    		'module' => 'grocery',
    		'js_inline' => $js_inline,
    		'message' => $this->session->flashdata('message'),
    	);
    	$this->load->view('index', $this->getDataOuput($data_render, $crud->render()));
	}

	function _callback_click_value_field($value = '', $primary_key = NULL){

		$html = '<input id="field-click_value_link" class="form-control click_value" placeholder="http://www.example.com" name="click_value" type="text" value="'.$value.'" maxlength="255" style="display: none">';
		$html.= '<select id="field-click_value_cat" class="click_value" placeholder="Select category" name="click_value" style="display: none">';
		foreach ($this->clear_catalog_list as $item) {
			$html.= '<option value="'.$item->category_id.'" '.($value == $item->category_id ? 'selected' : '').'>'.$item->name_with_level.'</option>';
		}
		$html.= '</select>';

		$html.= '<select id="field-click_value_brand" class="click_value" placeholder="Select brand" name="click_value" style="display: none">';
		foreach ($this->brand_list as $item) {
			$html.= '<option value="'.$item->brand_id.'" '.($value == $item->brand_id ? 'selected' : '').'>'.$item->vendor.' : '.$item->name.'</option>';
		}
		$html.= '</select>';

		$html.= '<input id="field-click_value_prod" class="form-control click_value" placeholder="Enter Product ID" name="click_value" type="text" value="'.$value.'" maxlength="255" style="display: none">';

		return $html;
	}

	function _callback_click_value_col($value, $row){
		switch ( $row->click_type) {
			case __('Jump to Link'):
				return '<a href="'.$value.'" target="_blank">'.$value.'</a>';
				break;
        	case __('Jump to Category'):
        		foreach ($this->clear_catalog_list as $item) {
					if( (int)$value === (int)$item->category_id) return $item->name_with_level;
				}
        		$category = $this->category_model->findActive($value);
        		return $category->name;
        		break;
        	case __('Jump to Brand'):
        		$brand = $this->brand_model->findActive($value);
        		return $brand->name;
        		break;
        	case __('Jump to Product'):
        		$product = $this->product_model->findActive($value);
        		return $product->name;
        		break;
		}
	}

	private function getClearCategoryWithLevel($list, $parent = ''){
		foreach ($list as $k => $item) {
			$list[$k]->name_with_level = ($parent ? $parent.' &raquo; ':'').$item->name;
			if($item->has_child){
				$list = array_merge($list, $this->getClearCategoryWithLevel($list[$k]->childs, $list[$k]->name_with_level));
				unset($list[$k]->childs);
			}
		}
		return $list;
	}
}