<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Privileges extends MY_Controller 
{
	public function __construct(){
		parent::__construct();

	}
	function index(){

		$crud = new grocery_CRUD();
 		$crud->unset_jquery();

	    $crud->set_table('roles')
	        ->set_subject('Role');
	    $crud->order_by('role_timestamp', 'desc');

	    $crud->columns('role_label', 'role_code', 'role_status', 'role_timestamp');
	    $crud->set_column_width('0', '80px', '60px', '80px', '60px');


	    $crud->display_as('role_label', __('Privilege'))
	    	->display_as('role_code', __('Code'))
	    	->display_as('role_filter', __('Filters'))
	    	->display_as('role_status', __('Status'))
	    	->display_as('role_timestamp', __('Created date'));

	 	$crud->field_type('role_status','dropdown',
            array(SELF::STATUS_ACTIVE => 'Active', SELF::STATUS_INACTIVE => 'Inactive'));
	 	
	 	$crud->unset_fields('role_timestamp');
	 	
	    $crud->unset_read();
	    $crud->field_type('role_filter', 'text')
	    	->unset_texteditor('role_filter');
	 
	    $output = $crud->render();
	    
		$data_render = array(
    		'page_title' => __('Privileges Management'),
    		'page_desc' => __('Privilege manage'),
    		'sidebar_active' => 'manage_privileges',
    		'module' => 'grocery',
    		'message' => $this->session->flashdata('message'),
    	);
    	$this->load->view('index', $this->getDataOuput($data_render, $output));
	}
}