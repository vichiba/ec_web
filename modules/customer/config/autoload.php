<?php defined('BASEPATH') OR exit('No direct script access allowed');

$autoload['libraries'] = array('form_validation', 'pagination');
$autoload['helper'] = array('language');
$autoload['language'] = array();
$autoload['model'] = array('admin/config_model', 'category/category_model', 'currency/currency_model');