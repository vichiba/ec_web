<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$lang['Go to Shop'] = 'ホーム';
$lang['Browse'] = '参照';
$lang['Fullname'] = '名前';
$lang['Avatar'] = 'イメージ';
$lang['Email'] = 'メール';
$lang['Current Password'] = '現在パスワード';
$lang['New Password'] = '新しいパスワード';
$lang['Confirm Password'] = '確認用パスワード';
$lang['Update profile'] = '更新する';
$lang['Your password has been updated successfully.'] = 'あなたのパスワードが変更させました。';
$lang['Your profile has been updated successfully.'] = 'あなたのプロフィールが変更させました。';
$lang['Current password not match'] = '現在パスワードは正しくありません。';
$lang['Confirm password not match'] = '確認用パスワードは正しくありません。';
$lang['Login to EC Shop'] = 'ログイン';
$lang['Enter your username and password to log on:'] = 'Eメールとパスワードを入力してください。';

$lang['Success'] = 'アクティブされました。';
$lang['Unsuccess'] = 'アクティブされない。';

$lang['Gender'] = '性別';

$lang['My Brand'] = 'マイブランド';
$lang['Email'] = 'メール';
$lang['Password'] = 'パスワード';
$lang['Your email'] = 'メールアドレス';

$lang['Sign in!'] = 'ログイン';
$lang['Create a account'] = '新規登録';

$lang['Register to EC Shop'] = '新規登録';
$lang['Enter your information to register new account'] = '必要な情報を入力してください。';
$lang['Fullname'] = 'お名前';
$lang['Email'] = 'Eメールアドレス';
$lang['Password'] = 'パスワード';
$lang['Confirm Password'] = '確認用パスワード';
$lang['Register!'] = '新規登録';
$lang['Return to login'] = 'ログイン画面に戻る';
$lang['Forgot ?'] = 'パスワードを忘れた場合';

$lang['Request new password'] = 'パスワードを再発行する';
$lang['Enter your email to request new password'] = 'メールアドレスを入力してください。';

$lang['Account have not actived yet, please check your email to active account.'] = 'まだアクティブしないため、メールをご確認ください。';
$lang['Username or password was not correct.'] = 'メールアドレスかまたはパスワードが正しくありません。';
$lang['Reset password has been sent to your email.'] = '新しいパスワードをメールに送りました。';
$lang['Register new account successfully. Please check your email to active your account.'] = '登録しました。メールを確認し、アカウントをアクティブしてください。';
$lang['Your account has been actived.'] = 'アカウントがアクティブされました。';
$lang['Cannot active your account at the moment, please try again latter.'] = 'アクティブできませんでした。暫く待ってから再度実行してください。';
$lang['Your token is invalid.'] = 'トークンが無効になりました。';
$lang['Your password has been changed, please relogin your account with new password.'] = 'パスワードが変更されました。再ログインしてください。';
$lang['Cannot change password at the moment, please try again latter.'] = 'パスワード変更できませんでした。暫く待ってから再度実行してください。';
$lang['Your token is invalid.'] = 'トークンが無効になりました。';
$lang['Message has been sent. We will answers as soon as posible.'] = 'メールを送信しました。確認次第、返事いたします。';

$lang['Complete login with twitter'] = 'Twitterでログインします。';
$lang['Enter your email to complete login with twitter'] = 'Eメールアドレスを入力してログインンします。';
$lang['By joining you agree to your <a href="%s" target="_blank">Terms</a> & <a href="%s" target="_blank">Privacy Policy</a>.'] = '<a href="%s" target="_blank">利用規約</a>及び<a href="%s" target="_blank">プライバシーポリシー</a>に同意してください。';
$lang['Password not secure'] = 'パスワードは8桁以上で、小文字、大文字及び数字が利用可能です。';

$lang['Reset your password successfully'] = 'リセットパスワードを完了しました';
$lang['Reset your password'] = 'リセットパスワード';
$lang['Enter your new password to change'] = '新しいパスワード入力';
$lang['Your new password'] = 'パスワード入力';
$lang['Confirm your new password'] = 'パスワード入力を確認';
$lang['Change password'] = 'パスワードをリセットする';

$lang['Gender None'] = 'なし';
$lang['Gender Women'] = '女性';
$lang['Gender Man'] = '男性';
$lang['Gender Unisex'] = 'ユニセックス';