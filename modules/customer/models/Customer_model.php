<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Customer_model extends MY_Model{
 	 	
 	public $table = 'customers';
	public $primary_key = 'customer_id';

	//0:Delete, 1: Deactive, 2: OK, 3: Block
	const STATUS_DELETED = 0;
	const STATUS_DEACTIVE = 1;
	const STATUS_OK = 2;
	const STATUS_BLOCK = 3;

	const GENDER_UNISEX = 0;
	const GENDER_WOMEN = 1;
	const GENDER_MAN = 2;
	const GENDER_NONE = 3;

	public $second_table = 'customer_product';
	public $thirth_table = 'customer_brand';

	public function login($email, $password, $refresh_token = ''){
		$conds = array(
			'email' => $email,
			'password' => md5($password)
		);
		$this->db->where('status', SELF::STATUS_OK);
		$customer_id = $this->exists($conds);
		if( $customer_id && is_numeric($customer_id) ){
			$data = array(
				'last_logon_time' => get_current_time(),
			);
			if( $refresh_token ){
				$data['refresh_token'] = $refresh_token;
			}

			$this->update($customer_id, $data);
			return $customer_id;
		}
		return FALSE;
	}

	public function regsiter($email, $password, $fullname, $gender, $google_id, $facebook_id, $twitter_id, $active_token, $status = SELF::STATUS_DEACTIVE, $avatar = ''){
		if( !$this->exists(['email' => $email]) ){
			$data = array(
				'email' => $email,
				'password' => md5($password),
				'fullname' => $fullname, 
				'google_id' => $google_id, 
				'facebook_id' => $facebook_id, 
				'twitter_id' => $twitter_id, 
				'status' => $status,
				'gender' => $gender,
				'active_token' => $active_token,
				'avatar' => $avatar,
			);
			$customer_id = $this->insert($data);
			return $customer_id;
		}

		return FALSE;	
	}

	public function active($customer_id, $active_token){
		if( $customer_id && is_numeric($customer_id) ){
			$data = array(
				'active_token' => NULL,
				'active_time' => get_current_time(),
				'status' => SELF::STATUS_OK
			);
			return $this->update($customer_id, $data);
		}
		return FALSE;
	}

	public function isActive($customer_id){
		$customer = $this->find($customer_id);
		return ($customer && (int)$customer->status === SELF::STATUS_OK);
	}

	public function setResetPWToken($email, $resetPwToken){
		$conds = array(
			'email' => $email
		);
		$customer_id = $this->exists($conds);
		if( $customer_id && is_numeric($customer_id) ){
			$data = array(
				'resetpw_token' => $resetPwToken
			);
			$this->update($customer_id, $data);
			return $customer_id;
		}
	}

	public function resetPassword($email, $password){
		$conds = array(
			'email' => $email
		);
		$this->db->where_in('status', [SELF::STATUS_DEACTIVE, SELF::STATUS_OK]);
		$customer_id = $this->exists($conds);
		if( $customer_id && is_numeric($customer_id) ){
			$data = array(
				'resetpw_token' => NULL,
				'password' => md5($password)
			);
			$this->update($customer_id, $data);
			return $customer_id;
		}
		return FALSE;
	}

	public function checkRefreshToken($refresh_token){
		$conds = array(
			'refresh_token' => $refresh_token
		);
		$customer_id = $this->exists($conds);
		if( $customer_id && is_numeric($customer_id) ){
			return $customer_id;
		}
		return FALSE;
	}

	public function addProductFavourite($customer_id, $product_id){
		$this->db->where('customer_id', $customer_id);
		$this->db->where('product_id', $product_id);
		$this->db->from($this->second_table);
		if( $this->db->count_all_results() <= 0 ){
			$this->db->query('UPDATE products set favourite_count = favourite_count + 1 WHERE product_id = ?', array($product_id));
			$this->db->query('INSERT INTO '.$this->second_table.'(customer_id, product_id) VALUES( ?, ?);', array($customer_id, $product_id) );

			return TRUE;
		}

		return FALSE;
	}

	public function removeProductFavourite($customer_id, $product_id){
		$this->db->where('customer_id', $customer_id);
		$this->db->where('product_id', $product_id);
		$this->db->from($this->second_table);
		if( $this->db->count_all_results() > 0 ){
			$this->db->where('customer_id', $customer_id);
			$this->db->where('product_id', $product_id);
			$this->db->delete($this->second_table);
			$this->db->query('UPDATE products set favourite_count = favourite_count - 1 WHERE product_id = ?', array($product_id));

			return TRUE;
		}

		return FALSE;
	}

	public function listProductFavourite($customer_id, $page = 1, $page_size = 20){
		$res = new stdClass();

		$this->db->from($this->second_table);
		$this->db->where('customer_id', $customer_id);
		$this->db->where('customer_id', $customer_id);
		$res->total = $this->db->count_all_results();
		$res->page = $page;
		$res->page_size = $page_size;

		$this->db->limit($page_size, ( (int)$page - 1 ) * (int)$page_size );
		$this->db->from($this->second_table);
		$this->db->where('customer_id', $customer_id);
		$this->db->order_by('created_date', 'DESC');
		$res->items = $this->db->get()->result();

		return $res;
	}

	public function isFollowBrand($customer_id, $brand_id){
		$this->db->from($this->thirth_table);
		$this->db->where('customer_id', $customer_id);
		$this->db->where('brand_id', $brand_id);
		return $this->db->count_all_results();
	}

	public function isLikeProduct($customer_id, $product_id){
		$this->db->from($this->second_table);
		$this->db->where('customer_id', $customer_id);
		$this->db->where('product_id', $product_id);
		return $this->db->count_all_results() > 0;
	}

	public function countFollowBrand($customer_id){
		$this->db->from($this->thirth_table);
		$this->db->where('customer_id', $customer_id);
		return $this->db->count_all_results();
	}

	public function countLikeProduct($customer_id){
		$this->db->from($this->second_table);
		$this->db->where('customer_id', $customer_id);
		return $this->db->count_all_results();
	}
}
