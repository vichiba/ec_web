<?php $this->load->view('header'); ?>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '<?php echo get_config_value('FACEBOOK_APP_ID') ?>',
      xfbml      : true,
      version    : 'v2.6'
    });

  // FB.getLoginStatus(function(response) {
  //   statusChangeCallback(response);
  // });

  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>
<script type="text/javascript">
  function statusChangeCallback(response) {
    console.log(response.status);
    if (response.status === 'connected') {
        var access_token =   FB.getAuthResponse()['accessToken'];
        $.ajax({
          url: '<?php echo site_url('rest/customer/social_login') ?>',
          type: 'POST',
          data: {'access_token': access_token, 'vendor': 'facebook'},
        })
        .always(function(response) {
          window.location.href = '<?php echo base_url() ?>';
        });
    }
  }

  $(document).ready(function(){
    $("#btn-facebook-login").click(function(e){
        e.preventDefault();
        if( $(this)[0].hasAttribute("disabled") ){
          e.preventDefault();
          return false;
        }
        FB.login(function(response){
            statusChangeCallback(response);
        }, {scope: 'public_profile,email'});
    });

    $("#btn-twitter-login").click(function(e){
        e.preventDefault();
      
    });

    $('.btn-link-1').click(function(e){
      if( $(this)[0].hasAttribute("disabled") ){
        e.preventDefault();
        return false;
      }
    });
    $(".login-form input[name='agree']").change(function(){
      var self = $(this);
      if( !self.is(":checked") ){
        $('button.btn-login').attr("disabled", "disabled");
        $('.btn-link-1').attr("disabled", "disabled");
      }else{
        $('button.btn-login').removeAttr("disabled");
        $('.btn-link-1').removeAttr("disabled");
      }
    });
    $(".login-form input[name='agree']").trigger('change');

  });
</script>

<div class="back-home">
  <a href="<?php echo site_url('customer') ?>"><?php echo __('Login to EC Shop') ?></a> / <a href="<?php echo base_url() ?>"><i class="fa fa-home" aria-hidden="true"></i> <?php echo __('Go to Shop') ?></a>
</div>
<!-- Top content -->
<div class="top-content">
    <div class="inner-bg">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 form-box">
                    <div class="form-top">
                        <div class="form-top-left">
                            <h2><?php echo __('Login to EC Shop') ?></h2>
                            <small><?php echo __('Enter your username and password to log on:') ?></small>
                        </div>
                        <div class="form-top-right">
                            <i class="fa fa-key"></i>
                        </div>
                    </div>
                    <div class="form-bottom">
                        <form role="form" action="<?php echo site_url('customer/do_login')?>" method="post" class="login-form" autocomplete="Off">
                            <div class="form-group text-danger">
                                <small><?php echo $flashdata;?></small>
                            </div>
                            <div class="form-group text-success">
                                <small><?php echo $this->session->flashdata('LOGIN_DONE');?></small>
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="form-username"><?php echo __('Email') ?></label>
                                <input type="text" name="email" placeholder="<?php echo __('Your email') ?>" style="display: none" />
                                <input type="text" name="email" placeholder="<?php echo __('Your email') ?>" class="form-username form-control" id="form-username" />
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="form-password"><?php echo __('Password') ?></label>
                                <input type="password" name="password" placeholder="<?php echo __('Password') ?>..." style="display: none" />
                                <input type="password" name="password" placeholder="<?php echo __('Password') ?>..." class="form-password form-control" id="form-password" />
                            </div>
                            <button type="submit" class="btn btn-login"><?php echo __('Sign in!') ?></button>
                            <div class="checkbox">
                              <label>
                                <input type="checkbox" name="agree" value="1" checked="" /> <?php echo sprintf(__('By joining you agree to your <a href="%s" target="_blank">Terms</a> & <a href="%s" target="_blank">Privacy Policy</a>.'), site_url('terms-of-service'), site_url('privacy')) ?>
                              </label>
                            </div>
                        </form>
                        <div class="create-text">
                            <a href="<?php echo site_url('customer/register')?>" style="float:left"><?php echo __('Create a account') ?></a>
                            <a href="<?php echo site_url('customer/forgot_password')?>" style="float:right"><?php echo __('Forgot ?') ?></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="social-login">
                    <div class="social-login-buttons">
                        <a class="btn btn-link-1 btn-link-1-facebook" href="#" id="btn-facebook-login">
                            <i class="fa fa-facebook"></i> Facebook
                        </a>
                        <a class="btn btn-link-1 btn-link-1-twitter" href="<?php echo $twitter_auth_url; ?>">
                            <i class="fa fa-twitter"></i> Twitter
                        </a>
                        <a class="btn btn-link-1 btn-link-1-google-plus" href="<?php echo $google_auth_url ?>">
                            <i class="fa fa-google-plus"></i> Google Plus
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('footer'); ?>