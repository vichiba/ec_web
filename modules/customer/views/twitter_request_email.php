<?php $this->load->view('header'); ?>
<div class="back-home">
  <a href="<?php echo site_url('customer') ?>"><?php echo __('Login to EC Shop') ?></a> / <a href="<?php echo base_url() ?>"><i class="fa fa-home" aria-hidden="true"></i> <?php echo __('Go to Shop') ?></a>
</div>
<!-- Top content -->
<div class="top-content">
    <div class="inner-bg">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 form-box">
                    <div class="form-top">
                        <h2><?php echo __('Complete login with twitter') ?></h2>
                        <small><?php echo __('Enter your email to complete login with twitter') ?>:</small>
                    </div>
                    <div class="form-bottom">
                        <form role="form" action="<?php echo site_url('customer/twitter_auth/do_login_verify')?>" method="get" class="login-form" autocomplete="Off">
                            <input type="hidden" name="access_token" value="<?php echo $access_token ?>" />
                            <input type="hidden" name="access_token_secret" value="<?php echo $access_token_secret ?>" />
                            <div class="form-group text-danger">
                                <small><?php echo $flashdata;?></small>
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="form-username"><?php echo __('Email') ?></label>
                                <input type="text" name="email" placeholder="<?php echo __('Email') ?>" style="display: none" />
                                <input type="text" name="email" placeholder="<?php echo __('Email') ?>" class="form-username form-control" id="form-username" />
                            </div>
                            <button type="submit" class="btn"><?php echo __('Sign in!') ?></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('footer'); ?>