<?php $this->load->view('header'); ?>
<div class="back-home">
  <a href="<?php echo site_url('customer') ?>"><?php echo __('Login to EC Shop') ?></a> / <a href="<?php echo base_url() ?>"><i class="fa fa-home" aria-hidden="true"></i> <?php echo __('Go to Shop') ?></a>
</div>
<!-- Top content -->
<div class="top-content">
    <div class="inner-bg">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 form-box">
                    <div class="form-top">
                        <div class="form-top-left">
                            <h2>Success</h2>
                        </div>
                    </div>
                    <div class="form-bottom">
                        <form role="form" action="<?php echo site_url('customer/do_request_newpassword')?>" method="post" class="login-form" autocomplete="Off">
                            <div class="form-group text-success">
                                <?php echo __('An email have just sent to your email with link to reset your password. Please check email.') ?>
                            </div>
                        </form>
                        <div class="create-text">
                            <a href="<?php echo site_url('customer/')?>" style="float:left">Return to login page</a>
                            <a href="<?php echo site_url('customer/register')?>" style="float:right">Create a account</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('footer'); ?>