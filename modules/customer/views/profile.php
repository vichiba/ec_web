<?php $this->load->view('theme/header'); ?>
<?php $this->load->view('theme/page_top'); ?>
<?php $this->load->view('theme/navigation'); ?>
<?php $this->load->view('theme/breadcrumbs'); ?>
<style type="text/css">
.btn-file {
    position: relative;
    overflow: hidden;
}
.btn-file input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    font-size: 100px;
    text-align: right;
    filter: alpha(opacity=0);
    opacity: 0;
    outline: none;
    background: white;
    cursor: inherit;
    display: block;
}
</style>
<script type="text/javascript">
$(document).ready(function(){
    $('input[type=file]').change(function() {
        var input = this;
        var self = $(this).parent();
        var text = self.text();
        self.attr('disabled', 'disabled').find('span').text('Loading...');
        if (input.files && input.files[0]) { 
            var reader = new FileReader();
            reader.onload = function (e) {
                self.removeAttr('disabled').find('span').text(text);
                $('#avatar').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    });
});
</script>
<!--products list-->
<div class="products-list-page">
    <div class="container">
        <div class="row">


            <!--menu list-products-->
            <div class="col-xs-12 col-sm-3 col-md-3">
                <?php $this->load->view('sidebar') ?>

            </div><!--e menu list-products-->

            <div class="tittle-user-box">
                <h4><?php echo __('Profile') ?></h4><hr>
            </div>
            <p>&nbsp;</p>
            <!--content list-products-->
            <div class="col-xs-12 col-sm-9 col-md-9">
                <form class="form-horizontal" role="form" action="<?php echo site_url('customer/profile/do_update')?>" method="post"  autocomplete="Off" enctype="multipart/form-data">
                    <div class="form-group text-danger text-center">
                        <small><?php echo $flashdata;?></small>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label"><?php echo __('Fullname') ?></label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="fullname" placeholder="Your Name" value="<?php echo $this->session->userdata('customer_name') ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label"><?php echo __('Avatar') ?></label>
                        <div class="col-sm-9">
                            <p>
                                <img src="<?php echo $this->session->userdata('customer_avatar') ?>" id="avatar" width="120" />
                            </p>
                            <span class="btn btn-warning btn-file">
                                <span><?php echo __('Browse') ?></span> <input type="file" name="avatar" accept="image/*" />
                            </span>
                        </div>
                    </div>
                    <!-- <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label"><?php echo __('Gender') ?></label>
                        <div class="col-sm-9">
                            <select name="gender" class="form-control">
                                <option value="<?php echo Customer_model::GENDER_NONE ?>" 
                                    <?php echo $this->session->userdata('customer_gender') == Customer_model::GENDER_NONE ? 'selected': ''; ?>>
                                    <?php echo __('Gender None') ?>
                                </option>
                                <option value="<?php echo Customer_model::GENDER_WOMEN ?>" 
                                    <?php echo $this->session->userdata('customer_gender') == Customer_model::GENDER_WOMEN ? 'selected': ''; ?>>
                                    <?php echo __('Gender Women') ?>
                                </option>
                                <option value="<?php echo Customer_model::GENDER_MAN ?>" 
                                    <?php echo $this->session->userdata('customer_gender') == Customer_model::GENDER_MAN ? 'selected': ''; ?>>
                                    <?php echo __('Gender Man') ?>
                                </option>
                                <option value="<?php echo Customer_model::GENDER_UNISEX ?>" 
                                    <?php echo $this->session->userdata('customer_gender') == Customer_model::GENDER_UNISEX ? 'selected': ''; ?>>
                                    <?php echo __('Gender Unisex') ?>
                                </option>
                            </select>
                        </div>
                    </div> -->
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label"><?php echo __('Email') ?></label>
                        <div class="col-sm-9">
                            <input type="email" class="form-control" name="email" placeholder="Email" value="<?php echo $this->session->userdata('customer_email') ?>" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-9">
                            <button type="submit" class="btn btn-primary"><?php echo __('Update profile') ?></button>
                        </div>
                    </div>
                </form>
            </div><!--e content list-products-->
        </div>
    </div>
</div>
<!--products list-->

<?php $this->load->view('theme/footer'); ?>