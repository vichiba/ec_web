<?php $this->load->view('theme/header'); ?>
<?php $this->load->view('theme/page_top'); ?>
<?php $this->load->view('theme/navigation'); ?>
<?php $this->load->view('theme/breadcrumbs'); ?>
<!--products list-->
<div class="products-list-page">
    <div class="container">
        <div class="row">


            <!--menu list-products-->
            <div class="col-xs-12 col-sm-3 col-md-3">
                <?php $this->load->view('sidebar') ?>

            </div><!--e menu list-products-->

            <div class="tittle-user-box">
                <h4><?php echo __('Update Password') ?></h4><hr>
            </div>
            <p>&nbsp;</p>
            <!--content list-products-->
            <div class="col-xs-12 col-sm-9 col-md-9">
                <form class="form-horizontal" role="form" action="<?php echo site_url('customer/profile/do_update')?>" method="post"  autocomplete="Off">
                    <input type="hidden" name="redirect" value="<?php echo site_url(uri_string()); ?>"/>
                    <input type="hidden" name="fullname" value="<?php echo $this->session->userdata('customer_name') ?>"/>
                    <div class="form-group text-danger text-center">
                        <small><?php echo $flashdata;?></small>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo __('Current Password') ?></label>
                        <div class="col-sm-9">
                            <input type="password" class="form-control" name="password_cr" placeholder="<?php echo __('Current Password') ?>入力">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo __('New Password') ?></label>
                        <div class="col-sm-9">
                            <input type="password" class="form-control" name="password" placeholder="<?php echo __('New Password') ?>入力">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo __('Confirm Password') ?></label>
                        <div class="col-sm-9">
                            <input type="password" class="form-control" name="password_cf" placeholder="<?php echo __('Confirm Password') ?>入力">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-9">
                            <button type="submit" class="btn btn-primary"><?php echo __('Update Password') ?></button>
                        </div>
                    </div>
                </form>
            </div><!--e content list-products-->
        </div>
    </div>
</div>
<!--products list-->

<?php $this->load->view('theme/footer'); ?>