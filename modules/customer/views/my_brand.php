<?php $this->load->view('theme/header'); ?>
<?php $this->load->view('theme/page_top'); ?>
<?php $this->load->view('theme/navigation'); ?>
<?php $this->load->view('theme/breadcrumbs'); ?>

<!--products list-->
<div class="products-list-page">
    <div class="container">
        <div class="row">


            <!--menu list-products-->
            <div class="col-xs-12 col-sm-3 col-md-3">
                 <?php $this->load->view('sidebar') ?>
                      
            </div><!--e menu list-products-->
  
            <!--content list-products-->
            <div class="col-xs-12 col-sm-9 col-md-9">
               
                 <div class="tittle-user-box">
                    <h4><?php echo __('My Brands') ?> (<?php echo $brands->total?>)</h4><hr>
                 </div>
                 <!--content products-->
                  <div class="content-products-list">
                    <!--items-->
                      <?php foreach ($brands->items as $item): ?>
                      <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="item">
                                    <li class="img-products">
                                      <div class="img-wrapper"><img src="<?php echo $item->image_url ?>" alt="Lion" class="img-responsive"></div>
                                      <div style="padding:10px 10px 0px 10px;">
                                        <a href="<?php echo site_url('brand/'.$item->uri_path.'.html') ?>"><?php echo $item->name ?></a><hr>
                                      </div>
  
                                    </li>
                              </div>
                      </div><!--e items-->
                    <?php endforeach ?>
                              
                  </div> <!--e content products-->
                  <div class="pagilation-box">
                    <nav>
                        <?php 
                        $config['base_url'] = site_url(uri_string());
                        $config['total_rows'] = $brands->total;
                        $config['per_page'] = $brands->page_size;
                        $config['use_page_numbers'] = TRUE;
                        $config['page_query_string'] = TRUE;
                        $config['enable_query_strings'] = TRUE;
                        $config['reuse_query_string'] = TRUE;
                        $config['query_string_segment'] = 'page';
                        $config['full_tag_open'] = '<ul class="pagination">';
                        $config['full_tag_close'] = '</ul>';
                        $config['first_tag_open'] = '<li class="hide">';
                        $config['first_tag_close'] = '</li>';
                        $config['last_tag_open'] = '<li class="hide">';
                        $config['last_tag_close'] = '</li>';
                        $config['next_link'] = '&raquo;';
                        $config['next_tag_open'] = '<li><span aria-hidden="true">';
                        $config['next_tag_close'] = '</span></li>';
                        $config['prev_link'] = '&laquo;';
                        $config['prev_tag_open'] = '<li><span aria-hidden="true">';
                        $config['prev_tag_close'] = '</span></li>';
                        $config['num_tag_open'] = '<li>';
                        $config['num_tag_close'] = '</li>';
                        $config['cur_tag_open'] = '<li class="active"><a href="#">';
                        $config['cur_tag_close'] = '</a></li>';

                        $this->pagination->initialize($config);

                        echo $this->pagination->create_links();
                        ?>
                    </nav>
                </div>

            </div><!--e content list-products-->
  


      </div>
    </div>
</div>
<!--products list-->

<?php $this->load->view('theme/footer'); ?>