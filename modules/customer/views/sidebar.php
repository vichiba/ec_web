<div class="nav-side-menu">
    <div class="brand"><b><?php echo __('My Account') ?></b></div>
    <i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"></i>
  
        <div class="menu-list">
  
            <ul id="menu-content" class="menu-content collapse out">
                <li <?php echo $tab == 'profile' ? 'class="active"':'' ?>><a href="<?php echo site_url('customer/profile') ?>"><?php echo __('Profile') ?></a></li>
                <li <?php echo $tab == 'my_favourite' ? 'class="active"':'' ?>><a href="<?php echo site_url('customer/favourite') ?>"><?php echo __('My Favourites') ?></a></li>
                <li <?php echo $tab == 'my_brand' ? 'class="active"':'' ?>><a href="<?php echo site_url('customer/brand') ?>"><?php echo __('My Brands') ?></a></li>
                <li <?php echo $tab == 'profile_password' ? 'class="active"':'' ?>><a href="<?php echo site_url('customer/profile/password') ?>"><?php echo __('Update Password') ?></a></li>
                <li><a href="<?php echo site_url('customer/logout') ?>"><?php echo __('Logout') ?></a></li>
            </ul>
     </div>
</div>