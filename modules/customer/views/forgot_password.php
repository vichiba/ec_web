<?php $this->load->view('header'); ?>
<div class="back-home">
  <a href="<?php echo site_url('customer') ?>"><?php echo __('Login to EC Shop') ?></a> / <a href="<?php echo base_url() ?>"><i class="fa fa-home" aria-hidden="true"></i> <?php echo __('Go to Shop') ?></a>
</div>
<!-- Top content -->
<div class="top-content">
    <div class="inner-bg">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 form-box">
                    <div class="form-top">
                        <div class="form-top-left">
                            <h2><?php echo __('Request new password') ?></h2>
                            <small><?php echo __('Enter your email to request new password') ?></small>
                        </div>
                        <div class="form-top-right">
                            <i class="fa fa-key"></i>
                        </div>
                    </div>
                    <div class="form-bottom">
                        <form role="form" action="<?php echo site_url('customer/do_request_newpassword')?>" method="post" class="login-form" autocomplete="Off">
                            <div class="form-group text-danger">
                                <small><?php echo $flashdata;?></small>
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="form-username"><?php echo __('Email') ?></label>
                                <input type="text" name="email" placeholder="<?php echo __('Your email') ?>" style="display: none" />
                                <input type="text" name="email" placeholder="<?php echo __('Your email') ?>" class="form-username form-control" id="form-username" />
                            </div>
                            <button type="submit" class="btn"><?php echo __('Request new password') ?>!</button>
                        </form>
                        <div class="create-text">
                            <a href="<?php echo site_url('customer/')?>" style="float:left"><?php echo __('Return to login') ?></a>
                            <a href="<?php echo site_url('customer/register')?>" style="float:right"><?php echo __('Create a account') ?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('footer'); ?>