<?php $this->load->view('header'); ?>
<div class="back-home">
  <a href="<?php echo site_url('customer') ?>"><?php echo __('Login to EC Shop') ?></a> / <a href="<?php echo base_url() ?>"><i class="fa fa-home" aria-hidden="true"></i> <?php echo __('Go to Shop') ?></a>
</div>
<!-- Top content -->
<div class="top-content">
    <div class="inner-bg">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 form-box">
                    <?php if ($is_success): ?>
                        <div class="form-top">
                            <div class="form-top-left">
                                <h2><?php echo __('Success') ?></h2>
                            </div>
                        </div>
                        <div class="form-bottom">
                            <div class="form-group text-success">
                                <?php echo __($flash_done) ?>
                            </div>
                            <div class="create-text">
                                <a href="<?php echo site_url('customer/')?>" style="float:left"><?php echo __('Return to login') ?></a>
                                <a href="<?php echo site_url('customer/register')?>" style="float:right"><?php echo __('Create a account') ?></a>
                            </div>
                        </div>
                    <?php else: ?>
                        <div class="form-top">
                            <div class="form-top-left">
                                <h2><?php echo __('Unsuccess') ?></h2>
                            </div>
                        </div>
                        <div class="form-bottom">
                            <div class="form-group text-danger">
                                <?php echo __($flash) ?>
                            </div>
                            <div class="create-text">
                                <a href="<?php echo site_url('customer/')?>" style="float:left"><?php echo __('Return to login') ?></a>
                                <a href="<?php echo site_url('customer/register')?>" style="float:right"><?php echo __('Create a account') ?></a>
                            </div>
                        </div>
                    <?php endif ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('footer'); ?>