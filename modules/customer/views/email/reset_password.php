<!DOCTYPE html>
<html>
<head>
    <title></title>
</head>
<body>
<div style="background:#f6f6f6;font-family:Verdana,Arial,Helvetica,sans-serif;font-size:12px;margin:0;padding:0">
    <table cellspacing="0" cellpadding="0" border="0" height="100%" width="100%">
        <tbody>
            <tr>
                <td align="center" valign="top" style="padding:20px 0 20px 0">
                    <h1><?php echo $fullname ?>,</h1>
                    <p>There was recently a request to change the password for your <span class="il">account</span>.</p>
                    <p>If you requested this password change, please reset your password here:</p>
                    <table bgcolor="FFFFFF" cellspacing="0" cellpadding="10" border="0" width="650" style="border:1px solid #e0e0e0">
                        <tbody><tr>
                            <td bgcolor="#EAEAEA" align="center" style="background:#eaeaea;text-align:center">
                                <a href="<?php echo $reset_link ?>"><span>Reset Password</span></a>
                            </td>
                        </tr>
                    </tbody></table>
                    <p>If you did not make this request, you can ignore this message and your password will remain the same.</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
</body>
</html>