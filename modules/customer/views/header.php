<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo isset($page_title) ? $page_title : get_config_value('SEO_TITLE') ?></title>
        
        <meta name="keywords" content="<?php echo isset($page_keywords) ? $page_keywords : get_config_value('SEO_KEYWORDS') ?>" />
        <meta name="description" content="<?php echo isset($page_description) ? $page_description : get_config_value('SEO_DESCRIPTION') ?>" />
        <meta name="resource-type" content="document" />
        <meta name="robots" content="noindex, nofollow"/>
        <meta name="googlebot" content="noindex, nofollow" />

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <link rel="stylesheet" href="/public/css/bootstrap.min.css">
        <link rel="stylesheet" href="/public/css/font-awesome.min.css">
		<link rel="stylesheet" href="/public/css/customer/form-elements.css">
        <link rel="stylesheet" href="/public/css/customer/style.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Favicon and touch icons -->
        
        <script type="text/javascript" src="<?php echo site_url('public/js/require.js')?>"></script>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script type="text/javascript" src="<?php echo site_url('public/js/jquery-1.9.1.min.js')?>"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?php echo site_url('public/js/bootstrap.min.js')?>"></script>
        <script type="text/javascript" src="<?php echo site_url('public/js/modernizr.custom.js')?>"></script>
        <script src="<?php echo site_url('public/js/jquery.backstretch.min.js')?>"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                $.backstretch("/public/images/backgrounds/bg.jpg");
            });
        </script>
    </head>

    <body>