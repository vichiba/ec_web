<?php $this->load->view('header'); ?>
<div class="back-home">
  <a href="<?php echo site_url('customer') ?>"><?php echo __('Login to EC Shop') ?></a> / <a href="<?php echo base_url() ?>"><i class="fa fa-home" aria-hidden="true"></i> <?php echo __('Go to Shop') ?></a>
</div>
<!-- Top content -->
<div class="top-content">
    <div class="inner-bg">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 form-box">
                <?php if ($flashdata_done): ?>
                    <div class="form-top">
                        <div class="form-top-left">
                            <h2><?php echo __('Reset your password successfully') ?></h2>
                        </div>
                        <div class="form-top-right">
                            <i class="fa fa-key"></i>
                        </div>
                    </div>
                   <div class="bg-danger text-success">
                        <div class="form-group" style="padding: 10px;">
                            <small><?php echo $flashdata_done;?></small>
                        </div>
                    </div> 
                    <div class="form-bottom">
                        <div class="create-text">
                            <a href="<?php echo site_url('customer/')?>" style="float:left"><?php echo __('Register to EC Shop') ?></a>
                            <a href="<?php echo site_url('customer/register')?>" style="float:right"><?php echo __('Create a account') ?></a>
                        </div>
                    </div>
                <?php else: ?>
                    <?php if ($is_valid): ?>
                    <div class="form-top">
                        <div class="form-top-left">
                            <h2><?php echo __('Reset your password') ?></h2>
                            <small><?php echo __('Enter your new password to change') ?>:</small>
                        </div>
                        <div class="form-top-right">
                            <i class="fa fa-key"></i>
                        </div>
                    </div>
                    <div class="form-bottom">
                        <form role="form" action="<?php echo site_url('customer/reset_password?token='.$token)?>" method="post" class="login-form" autocomplete="Off">
                            <div class="form-group text-danger">
                                <small><?php echo $flashdata;?></small>
                            </div>
                            <div class="form-group">
                                <label class="sr-only"><?php echo __('New password') ?></label>
                                <input type="text" name="password" placeholder="<?php echo __('Your new password') ?>" style="display: none" />
                                <input type="text" name="password" placeholder="<?php echo __('Your new password') ?>" class="form-control" />
                            </div>
                            <div class="form-group">
                                <label class="sr-only"><?php echo __('Confirm password') ?></label>
                                <input type="text" name="password_cf" placeholder="<?php echo __('Confirm your new password') ?>" style="display: none" />
                                <input type="text" name="password_cf" placeholder="<?php echo __('Confirm your new password') ?>" class="form-control" />
                            </div>
                            <button type="submit" class="btn"><?php echo __('Change password') ?></button>
                        </form>
                        <div class="create-text">
                            <a href="<?php echo site_url('customer/')?>" style="float:left"><?php echo __('Register to EC Shop') ?></a>
                            <a href="<?php echo site_url('customer/register')?>" style="float:right"><?php echo __('Create a account') ?></a>
                        </div>
                    </div>
                    <?php else: ?>
                        <div class="form-top">
                            <div class="form-top-left">
                                <h2><?php echo __('Reset your password') ?></h2>
                            </div>
                            <div class="form-top-right">
                                <i class="fa fa-key"></i>
                            </div>
                        </div>
                       <div class="bg-danger text-danger">
                            <div class="form-group" style="padding: 10px;">
                                <small><?php echo $flashdata;?></small>
                            </div>
                        </div> 
                        <div class="form-bottom">
                            <div class="create-text">
                                <a href="<?php echo site_url('customer/')?>" style="float:left"><?php echo __('Register to EC Shop') ?></a>
                            <a href="<?php echo site_url('customer/register')?>" style="float:right"><?php echo __('Create a account') ?></a>
                            </div>
                        </div>
                    <?php endif ?>
                <?php endif ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('footer'); ?>