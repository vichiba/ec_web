<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once(dirname(__FILE__).'/../libraries/Twitter/autoload.php');
use Abraham\TwitterOAuth\TwitterOAuth;

class Twitter_auth extends MX_Controller {

	private $CONSUMER_KEY;
	private $CONSUMER_SECRET;

	private $userinfo = NULL;
	
	public function __construct(){
		parent::__construct();
		$this->CONSUMER_KEY = get_config_value('TWITTER_APP_CONSUMER_KEY');
		$this->CONSUMER_SECRET = get_config_value('TWITTER_APP_CONSUMER_SECRET');
	}

	public function index(){
		$connection = new TwitterOAuth($this->CONSUMER_KEY, $this->CONSUMER_SECRET);
		//$content = $connection->get("account/verify_credentials");
		$request_token = $connection->oauth('oauth/request_token', array('oauth_callback' => site_url('customer/twitter_auth/do_login')));
		debug($request_token);
	}

	public function has_email($access_token, $access_token_secret){
		$connection = new TwitterOAuth($this->CONSUMER_KEY, $this->CONSUMER_SECRET, $access_token, $access_token_secret);
		$this->userinfo = $connection->get("account/verify_credentials", ["include_email" => TRUE]);
		if( $this->userinfo && is_object($this->userinfo) ){
			$twitter_id = $this->userinfo->id;
			$this->load->model('customer/customer_model');
            $customer_id = $this->customer_model->exists(['twitter_id' => $twitter_id]);

            if( $customer_id && is_numeric($customer_id) ){
            	$customer = $this->customer_model->find($customer_id);
            	return $customer->email;
            }

			return isset($this->userinfo->email_address) ? $this->userinfo->email_address : FALSE;
		}
		return FALSE;
	}
	public function do_login_verify(){
		$email = $this->input->get('email');
		$access_token = $this->input->get('access_token');
		$access_token_secret = $this->input->get('access_token_secret');
		$this->session->set_flashdata('REDIRECT_OAUTH2', base_url());
		$this->do_login($email, $access_token, $access_token_secret);
	}

	public function do_login($email = '', $access_token = '', $access_token_secret = ''){
		if( empty($access_token) || empty($access_token_secret) ){
			//get access token
			$oauth_token 		= $this->input->get('oauth_token');
			$oauth_verifier 	= $this->input->get('oauth_verifier');
			$oauth_token_secret = $this->session->userdata('twitter_oauth_token_secret');

			$connection = new TwitterOAuth($this->CONSUMER_KEY, $this->CONSUMER_SECRET, $oauth_token, $oauth_token_secret);
			$response = $connection->oauth("oauth/access_token", ["oauth_verifier" => $oauth_verifier]);
			$access_token = $response['oauth_token'];
			$access_token_secret = $response['oauth_token_secret'];
			if( ($email = $this->has_email($access_token, $access_token_secret)) === FALSE ){
				$this->load->view('twitter_request_email', ['access_token' => $access_token, 'access_token_secret' => $access_token_secret, 'flashdata' => $this->session->flashdata('TWITTER_REQUEST_EMAIL')]);
				return;
			}
			if( empty($email) ){
				$email = $this->input->get('email');
			}
		}

        $fullname = NULL;
        $google_id = NULL;
        $facebook_id = NULL;
        $twitter_id = NULL;
        $customer_id = NULL;

        if( !($this->userinfo && is_object($this->userinfo)) ){
			//get user info
			$connection = new TwitterOAuth($this->CONSUMER_KEY, $this->CONSUMER_SECRET, $access_token, $access_token_secret);
			$this->userinfo = $connection->get("account/verify_credentials", ["include_email" => TRUE]);
		}

		if( $this->userinfo && is_object($this->userinfo) ){
            $twitter_id = $this->userinfo->id;
            $fullname = $this->userinfo->name;
            $avatar = $this->userinfo->profile_image_url;
            $password = $twitter_id;
            $gender = Customer_model::GENDER_NONE;
            $this->load->model('customer/customer_model');
            $customer_id = $this->customer_model->exists(['email' => $email, 'twitter_id' => $twitter_id]);

            if( $customer_id && is_numeric($customer_id) ){}else{
                $customer_id = $this->customer_model->exists(['email' => $email]);
                if( $customer_id && is_numeric($customer_id) ){
                    $data = [
                        'twitter_id' => $twitter_id
                    ];

                    $this->customer_model->update($customer_id, $data);
                }else{
                    $customer_id = $this->customer_model->regsiter($email, $password, $fullname, $gender, $google_id, $facebook_id, $twitter_id, '', Customer_model::STATUS_OK, $avatar);
                }
            }

            if( $customer_id && is_numeric($customer_id) ){
                $customer = $this->customer_model->find($customer_id);
                $this->session->set_userdata('customer_id', $customer_id);
                $this->session->set_userdata('customer_name', $customer->fullname);
                $this->session->set_userdata('customer_avatar', $customer->avatar);
                $this->session->set_userdata('customer_email', $customer->email);
                $this->session->set_userdata('customer_gender', $customer->gender);

                $this->session->set_userdata('customer_fid', $customer->facebook_id);
                $this->session->set_userdata('customer_gi', $customer->google_id);
                $this->session->set_userdata('customer_tid', $customer->twitter_id);
            }
            $redirect = $this->session->flashdata('REDIRECT_OAUTH2');
            if( $redirect ){
            	redirect($redirect);
            }else{
            	return $customer_id;
            }
        }
		return NULL;
		
	}

	public function getAuthUrl(){
		$connection = new TwitterOAuth($this->CONSUMER_KEY, $this->CONSUMER_SECRET);
		$request_token = $connection->oauth('oauth/request_token');
		
		$this->session->set_userdata('twitter_oauth_token', $request_token['oauth_token']);
		$this->session->set_userdata('twitter_oauth_token_secret', $request_token['oauth_token_secret']);

		$url = $connection->url('oauth/authorize', array('oauth_token' => $request_token['oauth_token']));
		return $url;
	}
}