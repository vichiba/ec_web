<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends MX_Controller 
{
    private $customer_id;
    private $is_logon = FALSE;

    const RESETPW_TOKEN_LENGTH = 30;
    const ACTIVE_TOKEN_LENGTH = 30;

    function __construct()
    {
        $this->customer_id  = $this->session->userdata('customer_id');
        if( $this->customer_id && is_numeric($this->customer_id) ){
            $this->is_logon = TRUE;
        }

        $this->load->model('customer_model', '_model');
        parent::__construct();
    }

    public function  is_logon(){
        return $this->is_logon;
    }

    public function index(){
    	if( $this->is_logon() ){
            $this->load->module('customer/brand');
            $this->brand->index();
        }else{
            $this->login();
        }
    }

    public function logout(){
        $this->session->unset_userdata('customer_id');
        $this->session->unset_userdata('customer_name');
        $this->session->unset_userdata('customer_avatar');
        $this->session->unset_userdata('customer_email');
        $this->session->unset_userdata('customer_gender');
        $this->session->unset_userdata('customer_fid');
        $this->session->unset_userdata('customer_gi');
        $this->session->unset_userdata('customer_tid');

        redirect(base_url());
    }
 
    public function login(){
        $js_files = [
            'public/js/jquery.backstretch.min.js',
        ];
        $js_inline = '
        require(["jquery"], function(){
            jQuery(document).ready(function($) {
                
                /*
                    Fullscreen background
                */
                $.backstretch("assets/img/backgrounds/1.jpg");
                
                /*
                    Form validation
                */
                $(".login-form input[type="text"], .login-form input[type="password"], .login-form textarea").on("focus", function() {
                    $(this).removeClass("input-error");
                });
                
                $(".login-form").on("submit", function(e) {
                    $(this).find("input[type="text"], input[type="password"], textarea").each(function(){
                        if( $(this).val() == "" ) {
                            e.preventDefault();
                            $(this).addClass("input-error");
                        }
                        else {
                            $(this).removeClass("input-error");
                        }
                    });
                    
                });
            });

        });
        ';
        $this->load->module('customer/oauth2');
        $google_auth_url = $this->oauth2->getAuthUrl();
        $this->session->set_flashdata('REDIRECT_OAUTH2', site_url('customer'));

        $this->load->module('customer/twitter_auth');
        $twitter_auth_url = $this->twitter_auth->getAuthUrl();
        $data_render = [
            'js_files' => $js_files,
            'js_inline' => $js_inline,
            'google_auth_url' => $google_auth_url,
            'twitter_auth_url' => $twitter_auth_url,
            'page_title' => __('Login to EC Shop'),
            'flashdata' => $this->session->flashdata('LOGIN')
        ];
        $this->load->view('login', $data_render);
    }

    public function is_password_strong($str){
        if( preg_match('#[0-9]#', $str) && preg_match('#[a-z]#', $str)  && preg_match('#[A-Z]#', $str) ){
            return TRUE;
        }
        $this->form_validation->set_message('is_password_strong', __('Password not secure'));
        return FALSE;
    }

    public function do_login(){
        $email = $this->input->post('email');
        $password = $this->input->post('password');

        $validation_rules = array(
            array(
                    'field' => 'email',
                    'label' => __('Email'),
                    'rules' => 'trim|required|valid_email'
            ),
            array(
                    'field' => 'password',
                    'label' => __('Password'),
                    'rules' => 'trim|required|min_length[8]|callback_is_password_strong'
            )
        );

        $this->form_validation->set_rules($validation_rules);


        if ($this->form_validation->run($this) == FALSE){
            $error = implode('<br/>', explode(PHP_EOL, trim(strip_tags($this->form_validation->error_string()), PHP_EOL)));
            $this->session->set_flashdata('LOGIN', $error);
        }else{
            $customer_id    = $this->_model->login($email, $password);

            if( $customer_id && is_numeric($customer_id) ){
                $customer = $this->_model->find($customer_id);
                
                $this->session->set_userdata('customer_id', $customer_id);
                $this->session->set_userdata('customer_name', $customer->fullname);
                $this->session->set_userdata('customer_avatar', $customer->avatar);
                $this->session->set_userdata('customer_email', $customer->email);
                $this->session->set_userdata('customer_gender', $customer->gender);

                $this->session->set_userdata('customer_fid', $customer->facebook_id);
                $this->session->set_userdata('customer_gi', $customer->google_id);
                $this->session->set_userdata('customer_tid', $customer->twitter_id);
                redirect(base_url());
            }else{
                $customer_id = $this->_model->exists(['email' => $email]);
                if( $customer_id && is_numeric($customer_id) && !$this->_model->isActive($customer_id) ){
                    $this->session->set_flashdata('LOGIN', __('Account have not actived yet, please check your email to active account.'));  
                }else{
                    $this->session->set_flashdata('LOGIN', __('Username or password was not correct.'));
                }
            }
        }
        redirect(site_url('customer'));
    }

    public function forgot_password(){
        $js_files = [
            'public/js/jquery.backstretch.min.js',
        ];
        $js_inline = '
        require(["jquery"], function(){
            jQuery(document).ready(function($) {
                
                /*
                    Fullscreen background
                */
                $.backstretch("assets/img/backgrounds/1.jpg");
                
                /*
                    Form validation
                */
                $(".login-form input[type="text"], .login-form input[type="password"], .login-form textarea").on("focus", function() {
                    $(this).removeClass("input-error");
                });
                
                $(".login-form").on("submit", function(e) {
                    $(this).find("input[type="text"], input[type="password"], textarea").each(function(){
                        if( $(this).val() == "" ) {
                            e.preventDefault();
                            $(this).addClass("input-error");
                        }
                        else {
                            $(this).removeClass("input-error");
                        }
                    });
                    
                });
            });

        });
        ';
        $data_render = [
            'js_files' => $js_files,
            'js_inline' => $js_inline,
            'page_title' => __('Forgot ?'),
            'flashdata' => $this->session->flashdata('LOSTPASSWORD')
        ];
        
        if($this->session->flashdata('LOSTPASSWORD_DONE') == 'DONE'){
            $this->load->view('forgot_password_done', $data_render);
        }else{
            $this->load->view('forgot_password', $data_render);
        }
    }

    public function do_request_newpassword(){
        $email = $this->input->post('email');
        $validation_rules = array(
            array(
                    'field' => 'email',
                    'label' => __('Email'),
                    'rules' => 'trim|required|valid_email'
            )
        );

        $this->form_validation->set_rules($validation_rules);


        if ($this->form_validation->run() == FALSE){
            $error = implode('<br/>', explode(PHP_EOL, trim(strip_tags($this->form_validation->error_string()), PHP_EOL)));
            $this->session->set_flashdata('LOSTPASSWORD', $error);
        }else{
            $customer_id = $this->_model->exists(['email' => $email]);
            if( $customer_id ){
                $customer = $this->_model->find($customer_id);
                $resetpw_token = generate_token(SELF::RESETPW_TOKEN_LENGTH);
                $this->_model->setResetPWToken($email, $resetpw_token);
                $this->session->set_flashdata('LOSTPASSWORD_DONE', __('Reset password has been sent to your email.'));
                $this->sendRequestPasswordEmail($customer->fullname, $email, $resetpw_token);
            }else{
                $this->session->set_flashdata('LOSTPASSWORD', __('Email address is not exists.'));
            }
        }
        redirect(site_url('customer/forgot_password'));
    }

    public function register(){
        $js_files = [
            'public/js/jquery.backstretch.min.js',
        ];
        $js_inline = '
        require(["jquery"], function(){
            jQuery(document).ready(function($) {
                
                /*
                    Fullscreen background
                */
                $.backstretch("assets/img/backgrounds/1.jpg");
                
                /*
                    Form validation
                */
                $(".login-form input[type="text"], .login-form input[type="password"], .login-form textarea").on("focus", function() {
                    $(this).removeClass("input-error");
                });
                
                $(".login-form").on("submit", function(e) {
                    $(this).find("input[type="text"], input[type="password"], textarea").each(function(){
                        if( $(this).val() == "" ) {
                            e.preventDefault();
                            $(this).addClass("input-error");
                        }
                        else {
                            $(this).removeClass("input-error");
                        }
                    });
                    
                });
            });

        });
        ';

        $this->load->module('customer/oauth2');
        $google_auth_url = $this->oauth2->getAuthUrl();
        $this->session->set_flashdata('REDIRECT_OAUTH2', site_url('customer'));

        $this->load->module('customer/twitter_auth');
        $twitter_auth_url = $this->twitter_auth->getAuthUrl();
        
        $data_render = [
            'js_files' => $js_files,
            'js_inline' => $js_inline,
            'google_auth_url' => $google_auth_url,
            'twitter_auth_url' => $twitter_auth_url,
            'page_title' => __('Register to EC Shop'),
            'flashdata' => $this->session->flashdata('REGISTER')
        ];
        
        $this->load->view('register', $data_render);
    }

    public function is_unique($str, $field)
    {
        if( is_null($str) ) return TRUE; //Xu ly cho TH Goole+, Facebook, Twitter
        sscanf($field, '%[^.].%[^.]', $table, $field);
        return $this->db->limit(1)->get_where($table, array($field => $str))->num_rows() === 0;

    }

    public function do_register(){
        $email          = $this->input->post('email');
        $password       = $this->input->post('password');
        $fullname       = $this->input->post('fullname');
        $google_id      = $this->input->post('google_id');
        $facebook_id    = $this->input->post('facebook_id');
        $twitter_id     = $this->input->post('twitter_id');
        $gender         = Customer_model::GENDER_NONE;

        $this->session->set_userdata('register_email', $email);
        $this->session->set_userdata('register_fullname', $fullname);
        

        $validation_rules = array(
            array(
                    'field' => 'fullname',
                    'label' => __('Fullname'),
                    'rules' => 'trim|required|min_length[3]|max_length[20]'
            ),
            array(
                    'field' => 'email',
                    'label' => __('Email'),
                    'rules' => 'trim|required|valid_email|callback_is_unique[customers.email]'
            ),
            array(
                    'field' => 'password',
                    'label' => __('Password'),
                    'rules' => 'trim|required|min_length[8]|callback_is_password_strong'
            ),
            array(
                    'field' => 'password_cf',
                    'label' => __('Confirm Password'),
                    'rules' => 'trim|required|min_length[5]|matches[password]'
            )
        );

        $this->form_validation->set_rules($validation_rules);


        if ($this->form_validation->run($this) == FALSE){
            $error = implode('<br/>', explode(PHP_EOL, trim(strip_tags($this->form_validation->error_string()), PHP_EOL)));
            $this->session->set_flashdata('REGISTER', $error);
        }else{
            $active_token = generate_token(SELF::ACTIVE_TOKEN_LENGTH);
            if( $this->_model->regsiter($email, $password, $fullname, $gender, $google_id, $facebook_id, $twitter_id, $active_token) ){
                $this->sendRegisterConfirmationEmail($fullname, $email, $active_token);
                $this->session->set_flashdata('LOGIN_DONE', __('Register new account successfully. Please check your email to active your account.'));
                redirect(site_url('customer'));
                return;
            }
        }
        redirect(site_url('customer/register'));
    }

    public function do_active(){
        $active_token = $this->input->get('token');
        $customer_id = $this->_model->exists(['active_token' => trim($active_token)]);
        $is_success = FALSE;
        if( $active_token && $customer_id && is_numeric($customer_id) ){
            if( $this->_model->active($customer_id, $active_token) !== FALSE){
                $is_success = TRUE;
                $this->session->set_flashdata('ACTIVE_DONE', __('Your account has been actived.'));
            }else{
                $this->session->set_flashdata('ACTIVE', __('Cannot active your account at the moment, please try again latter.'));
            }
        }else{
            $this->session->set_flashdata('ACTIVE', __('Your token is invalid.'));
        }
        $this->load->view('customer/active', ['is_success' => $is_success, 
                'flash_done' => $this->session->flashdata('ACTIVE_DONE'),
                'flash' => $this->session->flashdata('ACTIVE')
            ]);
    }

    public function reset_password(){
        $js_files = [
            'public/js/jquery.backstretch.min.js',
        ];
        $js_inline = '
        require(["jquery"], function(){
            jQuery(document).ready(function($) {
                
                /*
                    Fullscreen background
                */
                $.backstretch("assets/img/backgrounds/1.jpg");
                
                /*
                    Form validation
                */
                $(".login-form input[type="text"], .login-form input[type="password"], .login-form textarea").on("focus", function() {
                    $(this).removeClass("input-error");
                });
                
                $(".login-form").on("submit", function(e) {
                    $(this).find("input[type="text"], input[type="password"], textarea").each(function(){
                        if( $(this).val() == "" ) {
                            e.preventDefault();
                            $(this).addClass("input-error");
                        }
                        else {
                            $(this).removeClass("input-error");
                        }
                    });
                    
                });
            });

        });
        ';

        $token = $this->input->get('token');
        $customer_id = $this->_model->exists(['resetpw_token' => $token]);
        $is_valid = FALSE;
        if( $token && $customer_id ){
            $is_valid = TRUE;
            $password = $this->input->post('password');
            if($password){
                $validation_rules = array(
                    array(
                            'field' => 'password',
                            'label' => __('Password'),
                            'rules' => 'trim|required|min_length[8]|callback_is_password_strong'
                    ),
                    array(
                            'field' => 'password_cf',
                            'label' => __('Confirm Password'),
                            'rules' => 'trim|required|min_length[5]|matches[password]'
                    )
                );

                $this->form_validation->set_rules($validation_rules);

                if ($this->form_validation->run($this) !== FALSE){
                    $customer = $this->_model->find($customer_id);
                    if($this->_model->resetPassword($customer->email, $password)){
                        $this->session->set_flashdata('RESET_PASSWORD_DONE', __('Your password has been changed, please relogin your account with new password.'));
                    }else{
                        $this->session->set_flashdata('RESET_PASSWORD', __('Cannot change password at the moment, please try again latter.'));
                    }
                }
            }
        }else{
            $this->session->set_flashdata('RESET_PASSWORD', __('Your token is invalid.'));
        }

        $data_render = [
            'token' => $token,
            'is_valid' => $is_valid,
            'js_files' => $js_files,
            'js_inline' => $js_inline,
            'page_title' => __('Request new password'),
            'flashdata' => $this->session->flashdata('RESET_PASSWORD'),
            'flashdata_done' => $this->session->flashdata('RESET_PASSWORD_DONE'),
        ];
        
        $this->load->view('reset_password', $data_render);
    }

    function test(){
        $this->load->library('email');
    }

    public function sendRegisterConfirmationEmail($fullname, $email, $active_token){
        $data = [
            'fullname' => $fullname,
            'active_link' => site_url('customer/do_active/?token='.$active_token)
        ];

        $this->load->library('email');

        $this->email->initialize(['mailtype' => 'html']);

        $this->email->from('ecsystem.ytasia@gmail.com', 'ecsystem.com');
        
        $this->email->to($email);

        $this->email->subject('Register new account Confirmation for '.$fullname);
        
        $this->email->message($this->load->view('email/register', $data, TRUE));

        $this->email->send();
    }

    public function sendRequestPasswordEmail($fullname, $email, $resetpw_token){

        $data = [
            'fullname' => $fullname,
            'reset_link' => site_url('customer/reset_password/?token='.$resetpw_token)
        ];

        $this->load->library('email');

        $this->email->initialize(['mailtype' => 'html']);

        $this->email->from('ecsystem.ytasia@gmail.com', 'ecsystem.com');
        
        $this->email->to($email);

        $this->email->subject('Password Reset Confirmation for '.$fullname);
        
        $this->email->message($this->load->view('email/reset_password', $data, TRUE));

        $this->email->send();
    }

    public function save(){
        if( $_SERVER['REQUEST_METHOD'] === 'POST' ){
            $name = $this->input->post('name');
            $mail = $this->input->post('mail');
            $subject = $this->input->post('subject');
            $message = $this->input->post('message');

            $data = [
                'name' => $name,
                'email' => $email,
                'subject' => $subject,
                'message' => $message,
                'is_read' => 1
            ];
            $this->load->model('contact_model');
            $contact_id = $this->contact_model->insert($data);
            if( $contact_id && is_numeric($contact_id) ){
                $this->session->set_flashdata('CONTACT', __('Message has been sent. We will answers as soon as posible.'));
            }
        }
    }

    public function product_like(){
        $product_id = $this->input->get('product_id');

        if( $this->is_logon() && $product_id && is_numeric($product_id) )
        {
            $is_like = $this->_model->isLikeProduct($this->customer_id, $product_id);
            if( $is_like !== TRUE )
            {
                echo $this->_model->addProductFavourite($this->customer_id, $product_id);
            }
            else
            {
                echo $this->_model->removeProductFavourite($this->customer_id, $product_id);
            }
        }
    }
}