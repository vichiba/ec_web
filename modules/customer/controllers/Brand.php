<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Brand extends MX_Controller 
{
    private $customer_id;
    private $is_logon = FALSE;
    function __construct()
    {
        $this->customer_id  = $this->session->userdata('customer_id');
        if( $this->customer_id && is_numeric($this->customer_id) ){
            $this->is_logon = TRUE;
        }else{
            redirect(site_url('customer'));
        }

        $this->load->model('customer_model', '_model');
        $this->load->model('brand/brand_model');
        parent::__construct();
    }

    public function  is_logon(){
        return $this->is_logon;
    }

    public function index(){
    	$breadcrumbs = [
            'page_title' => $this->session->userdata('customer_name'),
            'items' => [
                ['label' => __('Home'), 'link' => base_url()],
                ['label' => __('Account'), 'link' => '#'],
                ['label' => __('My Brand'), 'link' => site_url(uri_string())],
            ]
        ];

        $page = $this->input->get('page');
        $page_size = $this->input->get('page_size');

        if( !$page || !is_numeric($page) || (int)$page < 1 ) $page = "1";
        if( !$page_size || !is_numeric($page_size) || (int)$page_size < 1 ) $page_size = "20";

        $brands = $this->brand_model->listBeFollowed($this->customer_id, $page, $page_size);
        foreach ($brands->items as $k => $item) {
            $brands->items[$k] = $this->brand_model->findActive($item->brand_id);
        }
        
        $data_render = [
            'breadcrumbs' => $breadcrumbs,
            'flashdata' => $this->session->flashdata('MY_BRAND'),
            'tab' => 'my_brand',
            'brands' => $brands,
            'page_title' => __('My Brand'),
        ];
        $this->load->view('my_brand', $data_render);
    }
}