<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends MX_Controller 
{
    private $customer_id;
    private $is_logon = FALSE;
    function __construct()
    {
        $this->customer_id  = $this->session->userdata('customer_id');
        if( $this->customer_id && is_numeric($this->customer_id) ){
            $this->is_logon = TRUE;
        }else{
            redirect(site_url('customer'));
        }

        $this->load->model('customer_model', '_model');
        parent::__construct();
    }

    public function  is_logon(){
        return $this->is_logon;
    }

    public function index(){
    	$breadcrumbs = [
            'page_title' => $this->session->userdata('customer_name'),
            'items' => [
                ['label' => __('Home'), 'link' => base_url()],
                ['label' => __('Account'), 'link' => '#'],
                ['label' => __('Profile'), 'link' => site_url(uri_string())],
            ]
        ];

        $data_render = [
            'breadcrumbs' => $breadcrumbs,
            'flashdata' => $this->session->flashdata('UPDATE_PROFILE'),
            'tab' => 'profile',
        ];
        $this->load->view('profile', $data_render);
    }

    public function password(){
        $breadcrumbs = [
            'page_title' => $this->session->userdata('customer_name'),
            'items' => [
                ['label' => __('Home'), 'link' => base_url()],
                ['label' => __('Account'), 'link' => '#'],
                ['label' => __('Profile'), 'link' => site_url(uri_string())],
            ]
        ];

        $data_render = [
            'breadcrumbs' => $breadcrumbs,
            'flashdata' => $this->session->flashdata('UPDATE_PROFILE'),
            'tab' => 'profile_password',
        ];
        $this->load->view('profile_password', $data_render);
    }

    public function is_password_strong($str){
        if( preg_match('#[0-9]#', $str) && preg_match('#[a-z]#', $str)  && preg_match('#[A-Z]#', $str) ){
            return TRUE;
        }
        $this->form_validation->set_message('is_password_strong', __('Password not secure'));
        return FALSE;
    }

    public function do_update(){
        $fullname       = $this->input->post('fullname');
        $crpassword     = $this->input->post('password_cr');
        $password       = $this->input->post('password');
        $password_cf    = $this->input->post('password_cf');
        $redirect       = $this->input->post('redirect');
        $gender         = $this->input->post('gender');
        //debug($_POST, TRUE);
        $validation_rules = array(
            array(
                    'field' => 'fullname',
                    'label' => __('Fullname'),
                    'rules' => 'trim|required|min_length[3]|max_length[20]'
            ),
            array(
                    'field' => 'password',
                    'label' => __('Password'),
                    'rules' => 'trim|min_length[8]'
            )
        );

        $this->form_validation->set_rules($validation_rules);

        if($crpassword && $password && $password_cf != $password){
            $this->session->set_flashdata('UPDATE_PROFILE', __('Confirm password not match'));
        }else if ($this->form_validation->run() == FALSE){
            $error = implode('<br/>', explode(PHP_EOL, trim(strip_tags($this->form_validation->error_string()), PHP_EOL)));

            $this->session->set_flashdata('UPDATE_PROFILE', $error);
        }else{
            if( $this->_model->exists(['customer_id' => $this->customer_id]) ){
                $data = ['fullname' => $fullname];
                //$data['gender'] = $gender;  //Khong xu ly gender tren WebAPP
                if( !empty($password) ){
                    //Check current password
                    if( !$this->_model->exists(['customer_id' => $this->customer_id, 'password' => md5($crpassword)]) ){
                        $this->session->set_flashdata('UPDATE_PROFILE', __('Current password not match'));
                        redirect(site_url('customer/profile'));
                        return;
                    }

                    if( $this->is_password_strong($password) ) $data['password'] = md5($password);
                    else redirect(site_url('customer/profile'));
                }

                $_file = isset($_FILES['avatar'])?$_FILES['avatar']:NULL;
                if( isset($_file) && $_file && isset($_file['name']) && $_file['name'] ){
                    //upload image
                    $upload_path = '/uploads/profiles/';
                    $config                         = [];
                    $config['upload_path']          = '.'.$upload_path;
                    $config['allowed_types']        = 'gif|jpg|png|bmp';
                    $config['max_size']             = 2048;
                    $config['max_width']            = 600;
                    $config['max_height']           = 600;
                    $config['file_ext_tolower']     = TRUE;

                    $this->load->library('upload', $config);

                    if ( $this->upload->do_upload('avatar')){
                        $result = $this->upload->data();
                        //debug($result, TRUE);
                        if( $result && isset($result['file_name']) ){
                            $data['avatar'] = $upload_path.$result['file_name'];
                            unlink('.'.$this->session->userdata('customer_avatar'));
                            $this->session->set_userdata('customer_avatar', $data['avatar']);
                        }
                    }else{
                        $this->session->set_flashdata('UPDATE_PROFILE', $this->upload->display_errors());
                        redirect(site_url('customer/profile'));
                        return;
                    }
                }

                $this->_model->update($this->customer_id, $data);
                if($redirect) 
                    $this->session->set_flashdata('UPDATE_PROFILE', __('Your password has been updated successfully.'));
                else 
                    $this->session->set_flashdata('UPDATE_PROFILE', __('Your profile has been updated successfully.'));
                
                $this->session->set_userdata('customer_name', $fullname);
                $this->session->set_userdata('customer_gender', $gender);
            }else{
                $this->session->set_flashdata('UPDATE_PROFILE', __('Account does not exists.'));
            }
        }
        if($redirect) redirect($redirect);
        else redirect(site_url('customer/profile'));
    }
}