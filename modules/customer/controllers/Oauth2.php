<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Oauth2 extends MX_Controller {

	private $client;

	public function __construct(){
		parent::__construct();
		try{
			$this->require_google_service();

	  		$OAUTH2_CLIENT_ID = get_config_value('GOOGLE_OAUTH2_CLIENT_ID');
			$OAUTH2_CLIENT_SECRET = get_config_value('GOOGLE_OAUTH2_CLIENT_SECRET');
			$DEVELOPER_KEY = get_config_value('GOOGLE_DEVELOPER_KEY');
			$OAUTH2_REDIRECT_URI = site_url('customer/oauth2/do_login');
			
			//log_message('debug', "OAUTH2_CLIENT_ID = $OAUTH2_CLIENT_ID");
			//log_message('debug', "OAUTH2_CLIENT_SECRET = $OAUTH2_CLIENT_SECRET");
			//log_message('debug', "OAUTH2_REDIRECT_URI = $OAUTH2_REDIRECT_URI");

			$this->client = new Google_Client();
			$this->client->setClientId($OAUTH2_CLIENT_ID);
			$this->client->setClientSecret($OAUTH2_CLIENT_SECRET);
			$this->client->setDeveloperKey($DEVELOPER_KEY);
			$this->client->setAccessType("offline");
			$this->client->setApprovalPrompt("force");
			$this->client->setScopes(array(
				'https://www.googleapis.com/auth/userinfo.email',
				'https://www.googleapis.com/auth/userinfo.profile'));
			$this->client->setRedirectUri($OAUTH2_REDIRECT_URI);
			$access_token = $this->session->userdata('access_token');
			$refresh_token = $this->session->userdata('refresh_token');
			if($access_token){
				$this->client->setAccessToken($access_token);
			}
			if($refresh_token){
				if($this->client->isAccessTokenExpired()) {
					$this->client->refreshToken($refresh_token);
					$access_token = $this->client->getAccessToken();
					$this->session->set_userdata('access_token', $access_token);
				}
			}
		}catch(Exception $e){
			log_message('error', $e->getMessage());
		}
	}
	public function require_google_service(){
		require_once(dirname(__FILE__).'/../libraries/Google/autoload.php');
		require_once(dirname(__FILE__).'/../libraries/Google/Client.php');
		require_once(dirname(__FILE__).'/../libraries/Google/Service/Oauth2.php');
	}

	public function getAuthUrl(){
		$state = mt_rand();
  		$this->client->setState($state);
		return $this->client->createAuthUrl();
	}

	public function do_login($auth_code = ''){
		if( empty($auth_code) ){
			$auth_code = $this->input->get('code');
		}
		
		$email = NULL;
        $fullname = NULL;
        $google_id = NULL;
        $facebook_id = NULL;
        $twitter_id = NULL;
        $customer_id = NULL;

		$this->client->authenticate($auth_code);
		$plus = $this->getGooglePlusService();
		$userinfo = $plus->userinfo->get();
        if( $userinfo && is_object($userinfo) ){
            $google_id = $userinfo->id;
            $email = $userinfo->email;
            $fullname = $userinfo->name;
            $avatar = $userinfo->picture;
            $password = $google_id;
            $gender = Customer_model::GENDER_NONE;
            $this->load->model('customer/customer_model');
            $customer_id = $this->customer_model->exists(['email' => $email, 'google_id' => $google_id]);

            if( $customer_id && is_numeric($customer_id) ){

            }else{
                $customer_id = $this->customer_model->exists(['email' => $email]);
                if( $customer_id && is_numeric($customer_id) ){
                    $data = [
                        'google_id' => $google_id
                    ];

                    $this->customer_model->update($customer_id, $data);
                }else{
                    $customer_id = $this->customer_model->regsiter($email, $password, $fullname, $gender, $google_id, $facebook_id, $twitter_id, '', Customer_model::STATUS_OK, $avatar);
                }
            }

            if( $customer_id && is_numeric($customer_id) ){
                $customer = $this->customer_model->find($customer_id);
                $this->session->set_userdata('customer_id', $customer_id);
                $this->session->set_userdata('customer_name', $customer->fullname);
                $this->session->set_userdata('customer_avatar', $customer->avatar);
                $this->session->set_userdata('customer_email', $customer->email);
                $this->session->set_userdata('customer_gender', $customer->gender);

                $this->session->set_userdata('customer_fid', $customer->facebook_id);
                $this->session->set_userdata('customer_gi', $customer->google_id);
                $this->session->set_userdata('customer_tid', $customer->twitter_id);
            }
            $redirect = $this->session->flashdata('REDIRECT_OAUTH2');
            if( $redirect ){
            	redirect($redirect);
            }else{
            	return $customer_id;
            }
        }
		return NULL;
	}

	public function getGooglePlusService($client=NULL){
		if($client == NULL) $client = $this->client;
		$plus = new Google_Service_Oauth2($client);
		return $plus;
	}

}