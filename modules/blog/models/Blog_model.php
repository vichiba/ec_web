<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Blog_model extends MY_Model{
 	 	
 	public $table = 'posts';
	public $primary_key = 'post_id';

	public $second_table = 'categories';
	public $second_key = 'cat_id';
	public $second_key2 = 'cat_url';

	public $reference_table = 'post_category';


	//0:Delete, 1: inactive, 2: OK
	const STATUS_INACTIVE = 1;
	const STATUS_ACTIVE = 2;

	public function findActive($post_id){
		$post = $this->find($post_id);
		if($post && (int)$post->post_status === SELF::STATUS_ACTIVE){
			$this->db->from($this->reference_table);
			$this->db->where($this->reference_table.'.post_id', $post->post_id);
			$post->category = [];
			foreach($this->db->get()->result() as $ref){
				$post->category[] = $ref->cat_id;
			}
			return $post;
		}
		return NULL;
	}

	public function listActive($category_id, $page = 1, $page_size = 20, $extra_conds = array()){
		$posts = new stdClass();
		$this->listActiveConds($category_id, $extra_conds);
		$posts->total = $this->db->count_all_results();
		$this->db->limit($page_size, ( (int)$page - 1 ) * (int)$page_size );
		$this->listActiveConds($category_id, $extra_conds);
		$posts->items = $this->db->get()->result();
		$posts->page  = $page;
		$posts->page_size  = $page_size;

		return $posts;
	}

	private function listActiveConds($category_id, $extra_conds = array()){
		$this->db->select($this->table.'.*, '.$this->second_table.'.cat_id');
		$this->db->from($this->table);
		$this->db->join($this->reference_table, $this->table.'.post_id = '.$this->reference_table.'.post_id');
		$this->db->join($this->second_table, $this->second_table.'.cat_id = '.$this->reference_table.'.cat_id');
		if (is_array($category_id)) {
			$this->db->where_in($this->second_table.'.'.$this->second_key, $category_id);
		}elseif( $category_id && is_numeric($category_id) ){
			$this->db->where($this->second_table.'.'.$this->second_key, $category_id);
		}	
		$this->db->where($this->table.'.post_status', SELF::STATUS_ACTIVE);
		$this->db->where($this->second_table.'.cat_status', SELF::STATUS_ACTIVE);
		$this->db->order_by($this->reference_table.'.priority ASC');
		if( is_array($extra_conds) && count($extra_conds) ){
			foreach ($extra_conds as $cond) {
				$this->db->where($cond, NULL, FALSE);
			}
		}
	}

	public function findCategoryActive($category_id){
		$this->db->from($this->second_table);
		$this->db->where($this->second_key, $category_id);
		$this->db->where($this->second_table.'.cat_status', SELF::STATUS_ACTIVE);
		return $this->db->get()->row();
	}

	public function findCategoryActiveByURI($category_uri){
		$this->db->from($this->second_table);
		$this->db->where($this->second_key2, $category_uri);
		$this->db->where($this->second_table.'.cat_status', SELF::STATUS_ACTIVE);
		return $this->db->get()->row();
	}


	public function listCategoryActive($page = 1, $page_size = 20){

		$this->db->from($this->second_table);
		$this->db->where($this->second_table.'.cat_status', SELF::STATUS_ACTIVE);
		$this->db->limit( $page_size, ((int)$page - 1 ) * (int)$page_size );

		return $this->db->get()->result();
	}
}
