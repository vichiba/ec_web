<?php $this->load->view('theme/header'); ?>
<?php $this->load->view('theme/page_top'); ?>
<?php $this->load->view('theme/navigation'); ?>
<?php $this->load->view('theme/breadcrumbs'); ?>
<!-- news-->
        <div class="news">
          <div class="container">
              <div class="header-list">
                  <h3><?php echo $blog_header ?></h3>
                  <p><?php echo $blog_desc ?></p>
              </div>
            <div class="row">
              <?php foreach ($posts->items as $item): ?>
                <!-- news items-->
                  <div class="col-xs-12 col-sm-12 col-md-6 mr-bottom">
                    <div class="col-xs-12 col-sm-6 col-md-6">
                          <div class="grid">
                                <figure class="effect-layla">
                                  <a href="<?php echo site_url('blog/'.$item->post_id.'.html') ?>">
                                  <img src="<?php echo site_url($item->post_photo) ?>" class="img-responsive"></a>    
                                </figure>
                          </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6">
                      <div class="time-line">
                        <i class="fa fa-calendar" aria-hidden="true"></i><span><?php echo date('Y年m月d日', strtotime($item->post_modified_date)) ?></span>
                      </div>
                      <a href="<?php echo site_url('blog/'.$item->post_id.'.html') ?>"><h4><?php echo $item->post_title ?></h4></a>
                      <p class="news-title"><?php echo character_limiter(strip_tags($item->post_content), 30); ?></p>

                       <button type="button" class="btn btn-default"><a href="<?php echo site_url('blog/'.$item->post_id.'.html') ?>"><i class="fa fa-angle-double-right" aria-hidden="true"></i> <?php echo __('Read More') ?></a></button>

                    </div>
                  </div>
                <!-- end news items-->
                <?php endforeach ?>

                      <div class="pagilation-box">
                          <nav>
                              <?php 
                        $config['total_rows'] = $posts->total;
                        $config['per_page'] = $posts->page_size;
                        $config['use_page_numbers'] = TRUE;
                        $config['page_query_string'] = TRUE;
                        $config['enable_query_strings'] = TRUE;
                        $config['reuse_query_string'] = TRUE;
                        $config['query_string_segment'] = 'page';
                        $config['full_tag_open'] = '<ul class="pagination">';
                        $config['full_tag_close'] = '</ul>';
                        $config['first_tag_open'] = '<li class="hide">';
                        $config['first_tag_close'] = '</li>';
                        $config['last_tag_open'] = '<li class="hide">';
                        $config['last_tag_close'] = '</li>';
                        $config['next_link'] = '&raquo;';
                        $config['next_tag_open'] = '<li><span aria-hidden="true">';
                        $config['next_tag_close'] = '</span></li>';
                        $config['prev_link'] = '&laquo;';
                        $config['prev_tag_open'] = '<li><span aria-hidden="true">';
                        $config['prev_tag_close'] = '</span></li>';
                        $config['num_tag_open'] = '<li>';
                        $config['num_tag_close'] = '</li>';
                        $config['cur_tag_open'] = '<li class="active"><a href="#">';
                        $config['cur_tag_close'] = '</a></li>';

                        $this->pagination->initialize($config);

                        echo $this->pagination->create_links();
                        ?>
                            </nav>
                        </div>


                        
            </div>
          </div>
        </div> <!--end news-->
<?php $this->load->view('theme/footer'); ?>