<?php $this->load->view('theme/header'); ?>
<?php $this->load->view('theme/page_top'); ?>
<?php $this->load->view('theme/navigation'); ?>
<?php $this->load->view('theme/breadcrumbs'); ?>
<!-- news-->
<div class="news">
  <div class="container">
    <div class="row">
        <!-- news items left-->
          <div class="col-xs-12 <?php echo count($related_post->items) ? 'col-sm-8 col-md-9' : ''?>">
              <?php if (isset($single_post) && $single_post == TRUE): ?>
              <?php else: ?>
              <h4><?php echo $blog->post_title ?></h4>
               <div class="time-line">
                <i class="fa fa-calendar" aria-hidden="true"></i>
                <span><?php echo date('Y年m月d日', strtotime($blog->post_modified_date)) ?></span>
                <!-- <i class="fa fa-user" aria-hidden="true"></i><span>Admin</span> -->
              </div><hr>
              <?php endif ?>
              <div class="news-view-content">
                  <?php if ($blog->post_photo): ?>
                    <img src="<?php echo site_url($blog->post_photo) ?>" <?php echo (isset($single_post) && $single_post == TRUE) ? ' class="img-responsive"' : 'align="left"' ?>> 
                  <?php endif ?>
                  <?php echo $blog->post_content ?>
                  <hr>
                  <!-- <div class="tag-list">Tag: <a href=""> Post Format</a><a href=""> News</a><a href=""> Post News</a></div> -->
              </div>


          </div><!-- e news items left-->
          <?php if ( count($related_post->items) ): ?>
          <!--right-->
          <div class="col-xs-12 col-sm-4 col-md-3">
              <h4><?php echo __('Top NEWS') ?></h4><hr>
              <?php foreach ($related_post->items as $item): ?>  
              <div class="items-news-more">
                  <a href="<?php echo site_url('blog/'.$item->post_id.'.html') ?>" title="<?php echo $item->post_title ?>">
                  <img src="<?php echo site_url($item->post_photo) ?>" class="img-responsive"  alt="<?php echo $item->post_title ?>"> </a>    
                  <div class="time-line">
                    <i class="fa fa-calendar" aria-hidden="true"></i><span><?php echo date('Y年m月d日', strtotime($item->post_modified_date)) ?></span>
                  </div>
                  <a href="<?php echo site_url('blog/'.$item->post_id.'.html') ?>" title="<?php echo $item->post_title ?>"><h4><?php echo $item->post_title ?></h4></a><hr>
              </div>
              <?php endforeach ?>
          </div>
          <?php endif ?>
        <!-- e news items-->      
    </div>
  </div>
</div> <!--end news-->

<?php $this->load->view('theme/footer'); ?>