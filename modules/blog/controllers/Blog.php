<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends MX_Controller 
{

    public function index($blog_id){
    	$blog = $this->blog_model->findActive($blog_id);
    	if( !$blog ){
    		show_404(); 
    		return;
    	}
    	$breadcrumbs = [
        	'page_title' => __('Blog'),
        	'items' => [
        		['label' => __('Home'), 'link' => base_url()],
                ['label' => __('Blog'), 'link' => base_url('blog')],
                ['label' => $blog->post_title, 'link' => '#']
        	]
        ];

        //debug($blog, TRUE);

        $related_post = $this->blog_model->listActive($blog->category, 1, 6, ['posts.post_id != '.$blog->post_id, 'posts.post_id not in (1,2,3,4)']);

    	$data_render = [
    		'related_post' => $related_post, 
    		'blog' => $blog,
            'page_title' => $blog->post_title,
            'page_description' => strip_tags($blog->post_content),
    		'breadcrumbs' => $breadcrumbs
    	];
        $this->load->view('blog_view', $data_render);
    }

    public function list($uri = ''){
        $this->load->library('pagination');
        $category_id = NULL;
        $breadcrumbs = [
            'page_title' => __('Blog'),
            'items' => [
                ['label' => __('Home'), 'link' => base_url()],
                ['label' => __('Blog'), 'link' => base_url('blog')],
            ]
        ];

        $category = $this->blog_model->findCategoryActiveByURI($uri);
        if( $category ){
            $category_id = $category->cat_id;
            $breadcrumbs['page_title'] = $category->cat_title;
             $breadcrumbs['items'][] = ['label' => $category->cat_title, 'link' => base_url('blog/'.$category->cat_url.'.html')];
        }

        $page               = $this->input->get('page');
        $page_size          = $this->input->get('page_size');
        
        if( !$page || !is_numeric($page) || (int)$page < 1 ) $page = 1;
        if( !$page_size || !is_numeric($page_size) || (int)$page_size < 1 ) $page_size = 20;

        $posts = $this->blog_model->listActive($category_id, $page, $page_size, ['posts.post_id not in (1,2,3,4)']);

        

        $data_render = [
            'blog_header' => get_config_value('HOMEPAGE_BLOG'),
            'blog_desc' => get_config_value('HOMEPAGE_BLOG_DESC'),
            'category' => $category,
            'posts' => $posts,
            'breadcrumbs' => $breadcrumbs
        ];
    	$this->load->view('blog_list', $data_render);	
    }

    public function privacy(){
        $blog_id = 1;
        $blog = $this->blog_model->findActive($blog_id);
        if( !$blog ){
            show_404(); 
            return;
        }
        $breadcrumbs = [
            'page_title' => __('Privacy Policy'),
            'items' => [
                ['label' => __('Home'), 'link' => base_url()],
                ['label' => $blog->post_title, 'link' => '#']
            ]
        ];

        $data_render = [
            'blog' => $blog,
            'page_title' => $blog->post_title,
            'single_post' => TRUE,
            'breadcrumbs' => $breadcrumbs
        ];
        $this->load->view('blog_view', $data_render);
    }

    public function terms_of_service(){
        $blog_id = 2;
        $blog = $this->blog_model->findActive($blog_id);
        if( !$blog ){
            show_404(); 
            return;
        }
        $breadcrumbs = [
            'page_title' => __('Terms of Service'),
            'items' => [
                ['label' => __('Home'), 'link' => base_url()],
                ['label' => $blog->post_title, 'link' => '#']
            ]
        ];

        $data_render = [
            'blog' => $blog, 
            'single_post' => TRUE,
            'page_title' => $blog->post_title,
            'breadcrumbs' => $breadcrumbs
        ];
        $this->load->view('blog_view', $data_render);
    }

    public function aboutus(){
        $blog_id = 3;
        $blog = $this->blog_model->findActive($blog_id);
        if( !$blog ){
            show_404(); 
            return;
        }
        $breadcrumbs = [
            'page_title' => __('About Us'),
            'items' => [
                ['label' => __('Home'), 'link' => base_url()],
                ['label' => $blog->post_title, 'link' => '#']
            ]
        ];

        $data_render = [
            'blog' => $blog, 
            'single_post' => TRUE,
            'page_title' => $blog->post_title,
            'breadcrumbs' => $breadcrumbs
        ];
        $this->load->view('blog_view', $data_render);
    }

    public function faq(){
        $blog_id = 4;
        $blog = $this->blog_model->findActive($blog_id);
        if( !$blog ){
            show_404(); 
            return;
        }
        $breadcrumbs = [
            'page_title' => __('FAQ'),
            'items' => [
                ['label' => __('Home'), 'link' => base_url()],
                ['label' => $blog->post_title, 'link' => '#']
            ]
        ];

        $data_render = [
            'blog' => $blog, 
            'single_post' => TRUE,
            'page_title' => $blog->post_title,
            'breadcrumbs' => $breadcrumbs
        ];
        $this->load->view('blog_view', $data_render);
    }
}