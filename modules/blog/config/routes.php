<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['blog'] = 'blog/list';
$route['blog/(:num).html'] = 'blog/index/$1';
$route['blog/(:any).html'] = 'blog/list/$1';