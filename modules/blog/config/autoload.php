<?php defined('BASEPATH') OR exit('No direct script access allowed');

$autoload['helper'] = array('language', 'text', 'string');
$autoload['language'] = array();
$autoload['model'] = array('admin/config_model', 'category/category_model', 'blog_model');