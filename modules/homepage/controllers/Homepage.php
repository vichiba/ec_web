<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Homepage extends MX_Controller 
{
	function __construct()
	{
		$this->load->model('category/category_model');
		$this->load->model('brand/brand_model');
		$this->load->model('product/product_model');
        $this->load->model('blog/blog_model');

		parent::__construct();
	}
    public function index(){
        $catalogs = $this->category_model->listActiveWithURI(0, '', 1, 6);
    	$brands = $this->brand_model->listActive(1, 8);
        $blogs = $this->blog_model->listActive(NULL, 1, 4, array('posts.post_id not in (1,2,3,4)'));
    	$home_slider = $this->image_model->listActive(0, Image_model::TYPE_HOMEPAGE);

    	$data_render = [
    		'home_catalogs' => $catalogs,
    		'catalog_header' => get_config_value('HOMEPAGE_CATALOG'),
            'catalog_desc' => get_config_value('HOMEPAGE_CATALOG_DESC'),
    		'product_header' => get_config_value('HOMEPAGE_PRODUCT'),
    		'home_brands' => $brands,
    		'brand_header' => get_config_value('HOMEPAGE_BRAND'),
            'brand_desc' => get_config_value('HOMEPAGE_BRAND_DESC'),
            'app_header' =>  get_config_value('HOMEPAGE_APP'),
            'app_desc' =>  get_config_value('HOMEPAGE_APP_DESC'),
            'app_android_link' => get_config_value('APP_ANDROID_LINK'),
            'app_ios_link' => get_config_value('APP_IOS_LINK'),
            'blog_header' => get_config_value('HOMEPAGE_BLOG'),
            'blog_desc' => get_config_value('HOMEPAGE_BLOG_DESC'),
            'home_slider' => $home_slider,
            'home_blogs' => $blogs
    	];

        $this->load->view('homepage', $data_render);
    }
}