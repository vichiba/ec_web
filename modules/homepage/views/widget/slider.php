<?php 
if( $home_slider && count($home_slider) ):
?>
<!--slider-->   
<div class="carousel slide" data-ride="carousel" id="homeCarousel" style="width: 100%; ">  
    <!-- Indicators --> 
    <ol class="carousel-indicators">  
        <?php 
        $indicator_index = 0;
        foreach ($home_slider as $item): ?>
        <li class=" <?php echo $indicator_index === 0 ? 'active' : ''; ?>" data-slide-to="<?php echo $indicator_index++; ?>" data-target="#homeCarousel"></li>  
        <?php endforeach ?>
    </ol>  
    <!-- Wrapper for slides -->  

    <div class="carousel-inner" role="listbox" style="">  
        <?php
        $is_slider_active = FALSE;
        foreach ($home_slider as $item): ?>
        <div class="item <?php echo $is_slider_active == FALSE ? 'active' : ''; $is_slider_active = TRUE; ?>">  
            <img alt="<?php echo $item->label ?>" src="<?php echo $item->image_url?>">  
            <?php if ($item->label || $item->description): ?>
            <div class="carousel-caption hidden-xs">  
                <?php if ($item->label): ?>
                    <h2><?php echo $item->label ?></h2>
                <?php endif ?>
                <?php if ($item->description): ?>
                    <p><?php echo $item->description ?></p>
                <?php endif ?>
                <a href=""><?php echo __('Go to shop') ?> <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
            </div>
            <?php endif ?>
        </div>
        <?php endforeach ?>
    </div> 

    <!-- Left and right controls -->  
    <a class="left carousel-control" data-slide="prev" href="#homeCarousel" role="button">  
        <span aria-hidden="true" class="glyphicon glyphicon-chevron-left"></span>  
        <span class="sr-only">Previous</span>  
    </a>  
    <a class="right carousel-control" data-slide="next" href="#homeCarousel" role="button">  
        <span aria-hidden="true" class="glyphicon glyphicon-chevron-right"></span>  
        <span class="sr-only">Next</span>  
    </a>

</div>  
<!--end slider-->   
<?php
endif;
?>