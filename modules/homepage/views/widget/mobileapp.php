<!--apps-->
<div class="container-fluid apps">
    <div class="row">
        <div class="container">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <h3 class="h1"><?php echo $app_header ?></h3>
                <p><?php echo $app_desc ?></p>
                <button type="button" class="btn btn-default"><a href="<?php echo $app_android_link?>" target="_blank"><img src="<?php echo site_url('public/images/google-play.png')?>"></a></button>
                <button type="button" class="btn btn-default"><a href="<?php echo $app_ios_link?>" target="_blank"><img src="<?php echo site_url('public/images/app-store.png')?>"></a></button>
            </div>
        </div>
    </div>
</div>
<!--e app-->