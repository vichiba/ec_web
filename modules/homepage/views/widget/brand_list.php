<!--products list img-size = all -->
<?php function homepage_brand_item($item){ if($item): ?>
    <div class="grid">
        <figure class="effect-oscar">
            <a href="<?php echo site_url('brand/'.$item->uri_path.'.html')?>">
                <div class="img-wrapper">
                    <img src="<?php echo $item->image_url?>" class="img-responsive" />
                </div>
            </a>
            <!--<a href="<?php echo site_url('brand/'.$item->uri_path.'.html')?>">
                <figcaption>
                    <h3><?php echo $item->name?></h3>
                </figcaption>
            </a>-->
        </figure>
    </div>
<?php endif;}
?>
<div class="products-list products-list-1">
    <div class="container">
        <div class="header-list">
            <h3><?php echo $brand_header ?></h3>
            <p><?php echo $brand_desc?></p>
        </div>

        <div class="row">
            <?php foreach ($home_brands as $brand): ?>
            <div class="col-xs-6 col-sm-4 col-md-3">
                <?php homepage_brand_item($brand); ?>
            </div>
            <?php endforeach ?>
        </div>
    </div>
</div>
<!--end products list-->