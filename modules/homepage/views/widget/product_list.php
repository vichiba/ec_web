<!--tab products list-->
<div class="products-list">
    <?php if ($product_header): ?>        
    <div class="header-list">
        <h3><?php echo $product_header ?></h3>
    </div>
    <?php endif ?>

    <div class="container">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <?php $has_active = FALSE; 
            foreach ($home_catalogs as $catalog): ?>
                <li role="presentation" <?php echo !$has_active ? 'class="active"' : ''; $has_active = TRUE ?>>
                    <a href="#home-cat-tab<?php echo $catalog->category_id ?>" 
                        aria-controls="<?php echo $catalog->name ?>" 
                        title="<?php echo $catalog->name ?>" 
                        role="tab" data-toggle="tab">
                        <?php echo $catalog->name ?>
                    </a>
                </li>
            <?php endforeach ?>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <?php $has_active = FALSE; 
            foreach ($home_catalogs as $catalog): ?>
            <div role="tabpanel" class="tab-pane <?php echo !$has_active ? 'active' : ''; $has_active = TRUE ?>" id="home-cat-tab<?php echo $catalog->category_id ?>">
                <div class="owl-carousel">
                    <!--  item-->
                    <?php 
                    $products = $this->product_model->searchActive(NULL, $catalog->category_id, NULL, NULL, NULL, 1, 16);
                    foreach($products->items as $item){?>
                        <div class="item">
                            <div class="img-products">
                                <div class="img-wrapper">
                                    <img src="<?php echo $item->image_url?>" alt="<?php echo $item->name?>" class="img-responsive">
                                    <div class="caption-img">
                                        <a href="<?php echo trackurl('Home page', 'PRODUCT', $item->product_id, uri_string().'?'.$this->input->server('QUERY_STRING'), $item->buy_url) ?>" target="_blank" title="Buy now"><i class="fa fa-cart-plus" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                </div>
                                <div style="padding:10px 10px 0px 10px;">
                                    <a href="<?php echo site_url($item->product_id.'.html') ?>" class="product_title">
                                        <?php echo $item->name ?>
                                    </a>
                                    <hr>
                                    <strong style=" color:#5bc0de;"><?php echo display_price($item)?></strong>

                                    <a href="<?php echo site_url('customer/product_like?product_id='.$item->product_id)?>" 
                                        class="text-muted pull-right ajax_get <?php echo isset($item->is_like) && (int)$item->is_like > 0 ? 'active' : ''?>">
                                        <i class="fa fa-heart<?php echo (int)$item->is_like > 0 ? '' : '-o'?>" aria-hidden="true"></i>
                                        <small class="text-muted"><?php echo (int)$item->favourite_count > 0 ? number_format($item->favourite_count) : ''?></small>
                                    </a>

                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <?php endforeach ?>
        </div><!-- e Tab panes -->
    </div>
</div>
<!--end tab products list-->