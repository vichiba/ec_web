<!-- news-->
<div class="news">
    <div class="container">
        <div class="header-list">
            <h3><?php echo $blog_header ?></h3>
            <p><?php echo $blog_desc ?></p>
        </div>
        <div class="row">
            <?php foreach ($home_blogs->items as $item): ?>
            <!-- news items-->
            <div class="col-xs-12 col-sm-12 col-md-6 mr-bottom">
                <div class="col-xs-12 col-sm-6 col-md-6">
                    <div class="grid">
                        <figure class="effect-layla">
                            <a href="<?php echo site_url('blog/'.$item->post_id.'.html') ?>"><img src="<?php echo site_url($item->post_photo)?>" class="img-responsive" alt="<?php echo $item->post_title ?>"></a>    
                        </figure>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6">
                    <div class="time-line">
                        <i class="fa fa-calendar" aria-hidden="true"></i>
                        <span><?php echo date('Y年m月d日', strtotime($item->post_modified_date)) ?></span>
                    </div>
                    <a href="<?php echo site_url('blog/'.$item->post_id.'.html') ?>" title="<?php echo $item->post_title ?>">
                        <h4><?php echo $item->post_title ?></h4>
                        </a>
                    <p class="news-title"><?php echo character_limiter(strip_tags($item->post_content), 30); ?></p>
                    <button type="button" class="btn btn-default">
                        <a href="<?php echo site_url('blog/'.$item->post_id.'.html') ?>" title="<?php echo $item->post_title ?>">
                            <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                            <?php echo __('Read More') ?>
                        </a>
                    </button>
                </div>
            </div>
            <!-- end news items-->
            <?php endforeach ?>
        </div>
    </div>
</div> <!--end news-->