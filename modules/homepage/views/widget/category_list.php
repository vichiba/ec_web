<?php function home_category_item($item){ if($item):?>
    <div class="grid">
        <figure class="effect-oscar">
            <div class="img-wrapper">
                <img src="<?php echo site_url($item->image_url) ?>" class="img-responsive" />
            </div>
            <a href="<?php echo site_url($item->uri_path.'.html') ?>" title="<?php echo $item->name;?>">
                <figcaption>
                    <h3><?php echo $item->name;?></h3>
                    <p class="hidden-xs"><?php echo $item->short_description;?></p>
                </figcaption>
            </a>  
        </figure>
    </div>
<?php endif; }?>
<!--products list img-size = all -->
<div class="products-list home-catalog">
    <div class="container-fluid">
        <div class="header-list">
            <h3><?php echo $catalog_header ?></h3>
            <p><?php echo $catalog_desc ?></p>
        </div>
        <div class="row" style="padding-left:5px; padding-right:5px;">
            <div class="col-xs-12 col-sm-6 col-md-3">
                <?php home_category_item(array_shift($home_catalogs)); ?>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="row img-haft">
                    <div class="col-md-12">
                        <?php home_category_item(array_shift($home_catalogs)); ?>
                    </div>
                </div>
                <div class="row img-haft">
                    <div class="col-md-12">
                        <?php home_category_item(array_shift($home_catalogs)); ?>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3">
                <?php home_category_item(array_shift($home_catalogs)); ?>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="row img-haft">
                    <div class="col-md-12">
                        <?php home_category_item(array_shift($home_catalogs)); ?>
                    </div>
                </div>
                <div class="row img-haft">
                    <div class="col-md-12">
                        <?php home_category_item(array_shift($home_catalogs)); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--end products list-->