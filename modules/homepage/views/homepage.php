<?php $this->load->view('theme/header'); ?>
<?php $this->load->view('theme/page_top'); ?>
<?php $this->load->view('theme/navigation'); ?>

<?php $this->load->view('widget/slider'); ?>
<?php $this->load->view('widget/category_list'); ?>
<?php $this->load->view('widget/brand_list'); ?>
<?php $this->load->view('widget/product_list'); ?>
<?php $this->load->view('widget/mobileapp'); ?>
<?php $this->load->view('widget/post_list'); ?>

<?php $this->load->view('theme/footer'); ?>