<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Image_model extends MY_Model{
 	 	
 	public $table = 'images';
	public $primary_key = 'image_id';

	const TYPE_PRODUCT 		= 1;
	const TYPE_CATEGORY 	= 2;
	const TYPE_BRAND 		= 3;
	const TYPE_HOMEPAGE 	= 4;
	const TYPE_NOTIFY 		= 5;

	const CLICK_TYPE_BLANK	 	= 0;
	const CLICK_TYPE_PRODUCT 	= 1;
	const CLICK_TYPE_CATEGORY 	= 2;
	const CLICK_TYPE_BRAND 		= 3;
	const CLICK_TYPE_LINK 		= 4;

	private function normImage($image){
		unset($image->image_id);
		unset($image->ref_id);
		unset($image->ref_type);
		unset($image->created_date);
		unset($image->position);
		unset($image->status);

		// Remove all illegal characters from a url
		$url = filter_var($image->image_url, FILTER_SANITIZE_URL);
		// Validate url
		if (!filter_var($url, FILTER_VALIDATE_URL) === false) {
		    //echo("$url is a valid URL");
		} else {
		    //echo("$url is not a valid URL");
		    $image->image_url = site_url($image->image_url);
		}

		return $image;
	}

	public function listActive($ref_id, $ref_type, $page = 1, $page_size = 0){

		$conds = ['ref_type' => $ref_type, 'ref_id' => $ref_id, 'status' => SELF::STATUS_ACTIVE];
		$sort = 'position ASC, image_id DESC';

		if($page_size > 0) $this->db->limit($page_size, ( (int)$page - 1 ) * (int)$page_size );		
		$items = $this->find_all($conds, $sort);
		foreach ($items as $k => $item) {
			$items[$k] = $this->normImage($item);
		}

		if($page_size > 0){
			$result = new stdClass();
			$result->total = $this->count_all($conds);
			$result->page = $page;
			$result->page_size = $page_size;
			$result->items = $items;

			return $result;
		}
		return $items;
	}
}
